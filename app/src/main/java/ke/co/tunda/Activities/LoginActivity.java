/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/15/19 10:42 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Slide;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.CheckUserExistsParams;
import ke.co.tunda.ApiConnector.Models.CheckUserExistsResponse;
import ke.co.tunda.ApiConnector.Models.RegisterUserParams;
import ke.co.tunda.ApiConnector.Models.RegisterUserResponse;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Models.GoogleSignParams;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_with_phoneNumber)
    Button login_with_phone_number;
    @BindView(R.id.login_with_facebook)
    LoginButton loginButton;
    MaterialDialog dialog;
    @BindView(R.id.welcome_txt)
    TextView mWelcomeTxt;
    @BindView(R.id.sign_in_google)
    Button mGoogleSignIn;
    @BindView(R.id.edit_email_adress)
    EditText mEmailAdress;
    @BindView(R.id.submit_email)
    Button mSubmitEmail;


    @BindView(R.id.terms_policy)
    HtmlTextView mTermsPolicy;
    MaterialDialog mDialog;

    private static final String EMAIL = "email";
    private static final String PROFILE_PHOTO = "email";
    private static final String TAG = "LG";
    private int user_id;
    private String mEmail;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    String firstName, lastName, profileURL, social_id, email;

    SharedPreferences sp;
    SharedPreferences.Editor editor;

    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 001;
    private String countryName;
    private boolean isLoggedIn = false;
    AccessToken accessToken;
    ApiCaller apiCaller;


    @Override
    protected void onStart() {

        super.onStart();


        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            mGoogleSignIn.setText(getString(R.string.continue_as, account.getDisplayName()));
        }

        accessToken = AccessToken.getCurrentAccessToken();
        isLoggedIn = accessToken != null && !accessToken.isExpired();
        if (isLoggedIn) {
            updateLoginInformation(accessToken);
        }

        accessTokenTracker = new AccessTokenTracker() {
            // This method is invoked everytime access token changes
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                useLoginInformation(currentAccessToken);

            }
        };


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "ke.co.tunda",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d(TAG, "hashes = " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }


        // Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setExitTransition(new Slide());
        }


        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
        // Defining the AccessTokenTracker


        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        apiCaller = Builder.getClient().create(ApiCaller.class);

        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult != null && loginResult.getAccessToken() != null) {
                    useLoginInformation(loginResult.getAccessToken());

                } else {
                    Log.d(TAG, "onSuccess: null/logged out");
                }
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel: cancelled");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d(TAG, "onError: "+exception.getMessage());
                // App code
            }
        });


        mEmailAdress.setFocusableInTouchMode(true);

        mEmailAdress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mSubmitEmail.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isValidEmail(s.toString())) {
                    mSubmitEmail.setEnabled(true);
                    mEmailAdress.setError(null);
                } else {
                    mSubmitEmail.setEnabled(false);
                    mEmailAdress.setError("enter a valid email adress");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isValidEmail(s.toString())) {
                    mSubmitEmail.setEnabled(true);
                    mEmailAdress.setError(null);
                } else {
                    mSubmitEmail.setEnabled(false);
                    mEmailAdress.setError("enter a valid email adress");
                }

            }
        });


        sp = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        editor = sp.edit();
        countryName = sp.getString("country", "");
        user_id = sp.getInt("user_id", 0);


        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }


        mTermsPolicy.setHtml(this.getResources().getString(R.string.agree_to_terms));

//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        if (loginResult != null && loginResult.getAccessToken() != null) {
//                            useLoginInformation(loginResult.getAccessToken());
//
//                        } else {
//                            Log.d(TAG, "onSuccess: null/logged out");
//                            loginButton.setText("Continue with facebook");
//                        }
//                        // App code
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        Log.d(TAG, "onCancel: ");
//                        // App code
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        Log.d(TAG, "onError: " + exception.getLocalizedMessage());
//                        // App code
//                    }
//                });


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Extras.GOOGLE_CLIENT_ID)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        // making notification bar transparent
        changeStatusBarColor();
        mGoogleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.get(LoginActivity.this).setKeyIsEnteringNo(false);
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        login_with_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPhoneNumber();
            }
        });

//        loginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("HTTP", "onClick: ");
//                fbLogin();
//            }
//        });



        mSubmitEmail.setOnClickListener(v -> {
            MaterialDialog mDialog;
            Preferences.get(LoginActivity.this).setKeyIsEnteringNo(false);
            MaterialDialog.Builder builder = new MaterialDialog.Builder(LoginActivity.this);
            builder.content("logging you in ....")
                    .widgetColor(getResources().getColor(R.color.colorAccent))
                    .progress(true, 0);
            dialog = builder.build();
            dialog.setCancelable(false);
            dialog.show();
            mEmail = mEmailAdress.getText().toString();
            if (!mEmail.matches("")) {

                if (isValidEmail(mEmail)) {
                    checkIfUserExists(mEmail);


                } else {
                    dialog.dismiss();
                    mEmailAdress.setError("Email invalid");
                }

            } else {
                dialog.dismiss();
                mEmailAdress.setError("You did not enter email");
            }

        });


    }

    private void fbLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(EMAIL,"public_profile"));

    }

    private boolean isValidEmail(String mEmail) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches();
    }


    private void saveDetails(RegisterUserResponse.Body body, String name, String email) {

        try {

            editor.putString("first_name", name);
            editor.putString("email", email);
            editor.putInt("user_id", body.getUserId());
            editor.putInt("package_id", body.getPackageId());

            if (body.getGender() != null) {
                editor.putString("gender", body.getGender());
            }
            if (body.getDob() != null) {
                editor.putString("dob", body.getDob());
            }

            if (body.getGenderOfInterest() != null) {
                editor.putString("gender_of_interest", body.getGenderOfInterest());
            }

            if (body.getSource() != null) {
                editor.putString("source", body.getSource());
            }

            if (body.getAboutMe() != null) {
                editor.putString("about_me", body.getAboutMe());
            }


            editor.putString("job_title", body.getJobTitle());
            editor.putString("company", body.getCompany());
            editor.putString("school", body.getSchool());
            editor.putString("show_age", body.getShowAge());
            editor.putString("show_location", body.getShowLocation());
            editor.putString("last_location", body.getLastLocation());
            editor.putString("user_status", body.getUserStatus());
            editor.putInt("age_from", body.getAgeFrom());
            editor.putInt("age_to", body.getAgeTo());
            editor.putInt("distance", body.getDistance());
            editor.putString("package_expiry_date", body.getPackageExpiryDate());
            editor.putInt("photo_count", body.getPhotoCount());
            editor.putString("show_age", body.getShowAge());
            editor.putString("show_location", body.getShowLocation());
            editor.apply();

            dialog.dismiss();

            loginWithFacebook();


        } catch (Exception e) {
            Log.d("GHT", "saveDetails: ");
        }


    }


    private void loginWithFacebook() {
        Log.d("TESTING", "loginWithFacebook: ");
        Preferences.get(getApplicationContext()).setKeyIsFirstTimeLaunch(false);
        Preferences.get(this).setKeyIsLoggedIn(true);
        Preferences.get(this).setKeyIsFirstLogin(false);
        Intent intent = new Intent(LoginActivity.this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        finish();
    }

    private void launchHomeScreen() {
        Preferences.get(this).setKeyIsFirstTimeLaunch(false);

        Preferences.get(this).setKeyIsLoggedIn(true);

    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }


    }

    private void loginPhoneNumber() {

        Intent intent = new Intent(LoginActivity.this, Authentication.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                updateUI(account);
            }

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getMessage());
        }
    }


    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            Log.d(TAG, "updateUI: " + account.getGivenName());
            loginwithGoogle(account);

        } else {

            Log.d(TAG, "updateUI: ");
            Toasty.error(LoginActivity.this, "Problem signing In").show();

        }
    }

    private void loginwithGoogle(GoogleSignInAccount account) {
        String regToken = account.getIdToken();
        MaterialDialog.Builder builder = new MaterialDialog.Builder(LoginActivity.this);
        builder.content("logging you in ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        GoogleSignParams params = new GoogleSignParams();
        params.setRegToken(regToken);
        params.setRequest_type("VERIFY_TOKEN_FOR_GUSER");
        Call<RegisterUserResponse> call = apiCaller.googleSignIn(params);
        call.enqueue(new Callback<RegisterUserResponse>() {
            @Override
            public void onResponse(Call<RegisterUserResponse> call, Response<RegisterUserResponse> response) {

                if (response.isSuccessful()) {
                    RegisterUserResponse registerUserResponse = response.body();
                    if (registerUserResponse != null && registerUserResponse.getBody() != null) {
                        if (registerUserResponse.getBody().getStatusMessage().equals("SUCCESS")) {


                            Toasty.success(LoginActivity.this, "Success").show();
                            dialog.dismiss();


                            if (registerUserResponse.getBody().getGender() == null || registerUserResponse.getBody().getGenderOfInterest() == null
                                    || registerUserResponse.getBody().getDob() == null) {
                                dialog.dismiss();
                                Intent intent = new Intent(LoginActivity.this, PesonalInfo.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("id", registerUserResponse.getBody().getUserId());
                                intent.putExtra("name", account.getGivenName());
                                if (account.getPhotoUrl() != null) {
                                    intent.putExtra("image", account.getPhotoUrl().toString());
                                }

                                if (account.getGivenName() != null) {
                                    intent.putExtra("name", account.getGivenName());
                                }
                                Log.d(TAG, "onResponse: " + account.getPhotoUrl());
                                intent.putExtra("package_id", registerUserResponse.getBody().getPackageId());

                                startActivity(intent);
                                return;
                            }

                            saveDetails(registerUserResponse.getBody(), account.getGivenName(), account.getEmail());


                        } else {
                            dialog.dismiss();
                            Toasty.warning(LoginActivity.this, "Something Went wrong,try again").show();
                            signOut();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<RegisterUserResponse> call, Throwable t) {
                dialog.dismiss();
                Toasty.warning(LoginActivity.this, "Something Went wrong,try again").show();
                signOut();


            }
        });

    }


    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mGoogleSignIn.setText(getString(R.string.login_google));

                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mDialog!=null && mDialog.isShowing()){
            mDialog.dismiss();
        }
        Preferences.get(getApplicationContext()).setKeyIsEnteringNo(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mDialog!=null && mDialog.isShowing()){
            mDialog.dismiss();
        }


    }

    private void checkIfUserExists(String primaryId) {
        CheckUserExistsParams checkUserExistsParams = new CheckUserExistsParams();
        checkUserExistsParams.setEmail(primaryId);
        checkUserExistsParams.setRequestType("VERIFY_USER_EMAIL");
        Call<CheckUserExistsResponse> call = apiCaller.checkIfUserExists(checkUserExistsParams);
        call.enqueue(new Callback<CheckUserExistsResponse>() {
            @Override
            public void onResponse(Call<CheckUserExistsResponse> call, Response<CheckUserExistsResponse> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "checkIfUserExists: response");
                    CheckUserExistsResponse checkUserExistsResponse = response.body();
                    CheckUserExistsResponse.Body body = checkUserExistsResponse.getBody();
                    Log.d(TAG, "checkIfUserExists: result" + body.getUserExist());
                    if (body.getUserExist()) {
                        saveUserDetails(body.getUserData());
                        loginWithEmail();

                    } else {
                        ActionCodeSettings actionCodeSettings =
                                ActionCodeSettings.newBuilder()
                                        // URL you want to redirect back to. The domain (www.example.com) for this
                                        // URL must be whitelisted in the Firebase Console.
                                        .setUrl("https://play.google.com/store/apps/details?id=ke.co.tunda")
                                        // This must be true
                                        .setHandleCodeInApp(true)
                                        .setAndroidPackageName(
                                                "ke.co.tunda",
                                                true, /* installIfNotAvailable */
                                                "16"    /* minimumVersion */)
                                        .build();
                        FirebaseAuth auth = FirebaseAuth.getInstance();
                        auth.sendSignInLinkToEmail(mEmail, actionCodeSettings)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            dialog.dismiss();
                                            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(LoginActivity.this);
                                            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                                            builder.setTitle("Verification link sent to email");
                                            builder.setCancelable(true);
                                            builder.setTextGravity(Gravity.START);
                                            builder.setMessage("Steps to follow:\n" +
                                                    "1.Ensure to open your email on this phone.\n" +
                                                    "2.Click on the verification link.\n" +
                                                    "3.Choose Tunda when prompted to select the app to open with.");
                                            builder.setCancelable(false);
                                            builder.addButton("Got it", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();

                                                        }
                                                    });

                                            builder.show();
                                            editor.putString("email", primaryId);
                                            editor.apply();

                                            Log.d(TAG, "Email sent.");
                                        } else {
                                            dialog.dismiss();
                                            Toasty.error(getApplicationContext(), "Something went wrong, try Again").show();
                                        }
                                    }
                                });

                    }

                }
            }

            @Override
            public void onFailure(Call<CheckUserExistsResponse> call, Throwable t) {
                dialog.dismiss();
                Preferences.get(getApplicationContext()).setKeyIsEnteringNo(false);
                Toasty.warning(getApplicationContext(), "Something went wrong, Ensure internet & try again", Toasty.LENGTH_LONG).show();

            }
        });


    }

    private void checkIfUserExistsFacebook(String primaryId, String name, String image) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(LoginActivity.this);
        builder.content("verifying ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        mDialog = builder.build();
        mDialog.setCancelable(false);
        if (((Activity) LoginActivity.this).hasWindowFocus()) {
            mDialog.show();
        }

        CheckUserExistsParams checkUserExistsParams = new CheckUserExistsParams();
        checkUserExistsParams.setEmail(primaryId);
        checkUserExistsParams.setRequestType("VERIFY_USER_EMAIL");
        Call<CheckUserExistsResponse> call = apiCaller.checkIfUserExists(checkUserExistsParams);
        call.enqueue(new Callback<CheckUserExistsResponse>() {
            @Override
            public void onResponse(Call<CheckUserExistsResponse> call, Response<CheckUserExistsResponse> response) {
                if (response.isSuccessful()) {
                    mDialog.dismiss();
                    Log.d(TAG, "checkIfUserExists: response");
                    CheckUserExistsResponse checkUserExistsResponse = response.body();
                    CheckUserExistsResponse.Body body = null;
                    if (checkUserExistsResponse != null) {
                        body = checkUserExistsResponse.getBody();
                    }
                    if (body != null) {
                        if (!body.getUserExist()) {
                            registerFacebookUser(name, image, primaryId);

                        } else {
                            saveUserFbDetails(body.getUserData());
                            loginWithFacebook();


                        }
                    }

                } else {
                    mDialog.dismiss();
                    Log.d(TAG, "onResponse: not successful");
                }
            }

            @Override
            public void onFailure(Call<CheckUserExistsResponse> call, Throwable t) {
                mDialog.dismiss();
                Toasty.warning(getApplicationContext(), "Something went wrong", Toasty.LENGTH_LONG).show();

            }
        });


    }

    private void registerFacebookUser(String name, String image,String primary_id) {


        FullRegistration.open(LoginActivity.this,name,image,primary_id);
        finish();

//        Intent intent = new Intent(LoginActivity.this, FullRegistration.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("name", name);
//        intent.putExtra("email", primary_id);
//        intent.putExtra("origin","FACEBOOK");
//        if (!image.isEmpty()) {
//            intent.putExtra("image", image);
//
//        }
//
//        startActivity(intent);

    }


    private void loginWithEmail() {
        dialog.dismiss();
        Preferences.get(this).setKeyIsFirstTimeLaunch(false);
        Preferences.get(this).setKeyIsLoggedIn(true);
        Intent intent = new Intent(LoginActivity.this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        finish();
    }

    private void saveUserDetails(CheckUserExistsResponse.UserData userData) {

        try {
            Log.d(TAG, "saveUserDetails: " + userData.getPhotoPath());
            editor = sp.edit();
            editor.putInt("user_id", userData.getUserId());
            editor.putString("profile_photo_id", userData.getPhotoPath());
            editor.putInt("package_id", userData.getPackageId());
            editor.putString("first_name", userData.getFirstName());
            editor.putString("gender", userData.getGender());
            editor.putString("gender_of_interest", userData.getGenderOfInterest());
            editor.putString("dob", userData.getDob());
            editor.putString("interest", userData.getInterest());
            editor.putString("source", userData.getSource());
            editor.putString("about_me", userData.getAboutMe());
            editor.putString("job_title", userData.getJobTitle());
            editor.putString("company", userData.getCompany());
            editor.putString("school", userData.getSchool());
            editor.putString("show_age", userData.getShowAge());
            editor.putString("show_location", userData.getShowLocation());
            editor.putString("last_location", userData.getLastLocation());
            editor.putString("user_status", userData.getUserStatus());
            editor.putString("image", userData.getPhotoPath());
            editor.putInt("age_from", userData.getAgeFrom());
            editor.putInt("age_to", userData.getAgeTo());
            editor.putInt("distance", userData.getDistance());
            editor.putString("package_expiry_date", userData.getPackageExpiryDate());
            editor.putInt("photo_count", userData.getPhoto_count());
            editor.putString("show_age", userData.getShowAge());
            editor.putString("show_location", userData.getShowLocation());
            editor.apply();

        } catch (Exception e) {
            Log.d(TAG, "saveUserDetails: ");
        }


    }

    private void saveUserFbDetails(CheckUserExistsResponse.UserData userData) {

        try {
            Log.d(TAG, "saveUserDetails: " + userData.getPhotoPath());
            editor = sp.edit();
            editor.putInt("user_id", userData.getUserId());
            editor.putString("profile_photo_id", userData.getPhotoPath());
            editor.putInt("package_id", userData.getPackageId());
            editor.putString("first_name", userData.getFirstName());
            editor.putString("gender", userData.getGender());
            editor.putString("gender_of_interest", userData.getGenderOfInterest());
            editor.putString("dob", userData.getDob());
            editor.putString("interest", userData.getInterest());
            editor.putString("source", userData.getSource());
            editor.putString("about_me", userData.getAboutMe());
            editor.putString("job_title", userData.getJobTitle());
            editor.putString("company", userData.getCompany());
            editor.putString("school", userData.getSchool());
            editor.putString("show_age", userData.getShowAge());
            editor.putString("show_location", userData.getShowLocation());
            editor.putString("last_location", userData.getLastLocation());
            editor.putString("user_status", userData.getUserStatus());
            editor.putString("image", userData.getPhotoPath());
            editor.putInt("age_from", userData.getAgeFrom());
            editor.putInt("age_to", userData.getAgeTo());
            editor.putInt("distance", userData.getDistance());
            editor.putString("package_expiry_date", userData.getPackageExpiryDate());
            editor.putInt("photo_count", userData.getPhoto_count());
            editor.putString("show_age", userData.getShowAge());
            editor.putString("show_location", userData.getShowLocation());
            editor.apply();

        } catch (Exception e) {
            Log.d(TAG, "saveUserDetails: ");
        }


    }

    private void useLoginInformation(AccessToken accessToken) {


        if (accessToken != null && !accessToken.isExpired()) {
            GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                //OnCompleted is invoked once the GraphRequest is successful
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    Log.d(TAG, "onCompleted: "+response.toString());
                    try {
                        if(object!=null){
                            String name = object.getString("name");
                            String public_id = object.getString("id");
                            if (isLoggedIn) {
//                                loginButton.setText("Continue as " + name);
                            }

                            String image = "";

                            if (object.getJSONObject("picture").getJSONObject("data").getString("url") != null) {
                                image = object.getJSONObject("picture").getJSONObject("data").getString("url");
                            }


                            checkIfUserExistsFacebook(public_id, name, image);
                        }




                    } catch (JSONException e) {
                        Log.d(TAG, "onCompleted: "+e.getMessage());
                    }
                }
            });
            // We set parameters to the GraphRequest using a Bundle.
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,picture.width(200)");
            request.setParameters(parameters);
            // Initiate the GraphRequest
            request.executeAsync();
        } else {
            Log.d(TAG, "useLoginInformation: logged out");
            loginButton.setText("Continue with facebook");
        }

    }

    private void updateLoginInformation(AccessToken accessToken) {

        if(accessToken!=null){
            GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    try {
                        if(object!=null){
                            String name = object.getString("name");
                            String public_id = object.getString("id");
                            if (isLoggedIn) {
//                                loginButton.setText("Continue as " + name);
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            // We set parameters to the GraphRequest using a Bundle.
            Bundle parameters = new Bundle();
            parameters.putString("fields", "name");
            request.setParameters(parameters);
            // Initiate the GraphRequest
            request.executeAsync();
        }


    }

    private void sendFbDetailsToserver(String public_id) {
        RegisterUserParams registerUserParams = new RegisterUserParams();
        registerUserParams.setOrigin("FACEBOOK");
        registerUserParams.setEmail(public_id);
        Call<RegisterUserResponse> call = apiCaller.registerUser(registerUserParams);
        call.enqueue(new Callback<RegisterUserResponse>() {
            @Override
            public void onResponse(Call<RegisterUserResponse> call, Response<RegisterUserResponse> response) {
                if (response.isSuccessful()) {
                    RegisterUserResponse userResponse = response.body();

                }
            }

            @Override
            public void onFailure(Call<RegisterUserResponse> call, Throwable t) {

            }
        });

    }


}
