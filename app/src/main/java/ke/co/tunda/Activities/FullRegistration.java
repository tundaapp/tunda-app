/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/22/19 6:06 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.DialogOnDeniedPermissionListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rm.rmswitch.RMSwitch;
import com.shuhart.stepview.StepView;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Builder2;
import ke.co.tunda.ApiConnector.Models.RegisterUserParams;
import ke.co.tunda.ApiConnector.Models.RegisterUserResponse;
import ke.co.tunda.Helpers.ImageCrop.ImagePickerActivity;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.utils.AppUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FullRegistration extends AppCompatActivity implements TagView.OnTagClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {

    private static final int GALLERY_REQUEST_CODE = 1234;
    private static final int CAMERA_REQUEST_CODE = 4321;
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 1234;
    private int currentStep = 0;
    private String cameraFilePath;
    public static final int REQUEST_IMAGE = 100;
    private String filePath = "";

    DatePickerDialog datePickerDialog;
    private MaterialDialog dialog;
    private int user_id;
    SharedPreferences.Editor editor;
    SharedPreferences sp;


    @BindView(R.id.step_view)
    StepView stepView;
    @BindView(R.id.layout0)
    RelativeLayout layout0;
    @BindView(R.id.layout1)
    RelativeLayout layout1;
    @BindView(R.id.layout2)
    RelativeLayout layout2;
    @BindView(R.id.layout3)
    RelativeLayout layout3;
    @BindView(R.id.switch_men)
    RMSwitch rmSwitchMen;
    @BindView(R.id.switch_women)
    RMSwitch rmSwitchWomen;
    @BindView(R.id.btn_submit_selected_gender)
    Button mSubmitSelectedGender;
    @BindView(R.id.selected_txt)
    TextView mtxtSelected;
    @BindView(R.id.upload_profile_photo)
    RelativeLayout mUploadProfilePhoto;

    @BindView(R.id.layout4)
    RelativeLayout layout4;
    @BindView(R.id.layout5)
    RelativeLayout layout5;
    @BindView(R.id.layout6)
    RelativeLayout layout6;

    @BindView(R.id.btn_next_1)
    Button mNext1;
    @BindView(R.id.btn_next_3)
    Button mNext3;

    @BindView(R.id.btn_next_4)
    Button mNext4;

    @BindView(R.id.img_profile)
    CircleImageView mImgProfile;
    @BindView(R.id.img_plus)
    CircleImageView mImgPlus;
    @BindView(R.id.male_container)
    RelativeLayout mChooseMale;
    @BindView(R.id.female_container)
    RelativeLayout mChooseFemale;

    @BindView(R.id.txt_name)
    TextView mTxtxName;
    @BindView(R.id.txt_enter_name)
    MaterialEditText mEditName;
    @BindView(R.id.txt_birthday)
    TextView mTxtBirthday;
    @BindView(R.id.dob_edit)
    MaterialEditText mBirthdayEdit;
    @BindView(R.id.btn_submit_date)
    Button mSubMitDate;
    @BindView(R.id.bio_edit)
    MaterialEditText mBioEdit;
    @BindView(R.id.btn_submit_bio)
    Button mSUbmitBio;

    @BindView(R.id.text_tag)
    MaterialEditText mEditTag;
    @BindView(R.id.btn_add_tag)
    Button mAddTag;

    @BindView(R.id.tagcontainerLayout)
    TagContainerLayout mTagContainerLayout;


    private ApiCaller apiCaller;
    private Bitmap bitmap;
    String jsonInterests;
    private String mGenderIntereset = "FEMALE";
    String phone_number;

    private static String name, email, image,mOrigin;


    //Global

    private static String first_name = "", last_name, selectedGender = "FEMALE", date_of_birth, interested_in, bio;
    private boolean interest_checked = false, interested_two_checked = false;
    private boolean men_checked = false, women_checked = false;
    private String TAG = "FLXDR";

    public static void open(Context context, String fb_name, String fb_image, String fb_email) {
        name = fb_name;
        email = fb_email;
        image = fb_image;
        mOrigin="FACEBOOK";


        context.startActivity(new Intent(context, FullRegistration.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: "+email);


        sp = PreferenceManager.getDefaultSharedPreferences(FullRegistration.this);
        editor = sp.edit();
        if (email == null||email.isEmpty()) {
            email = sp.getString("email", "");
            Log.d(TAG, "onCreate: email share_prefs>>" + email);
        }




//        if (Build.VERSION.SDK_INT >= 21) {
//            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            getWindow().setExitTransition(new Explode());
//        }

        FirebaseAuth auth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        if (intent.getData() != null) {
            String emailLink = intent.getData().toString();

            if (auth.isSignInWithEmailLink(emailLink)) {

                if (email != null && !email.isEmpty()) {
                    auth.signInWithEmailLink(email, emailLink)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "Successfully signed in with email link!" + email);
                                        mOrigin = "FACEBOOK";
                                        Toasty.success(getApplicationContext(), "Successfully signed in with email link!").show();
                                        AuthResult result = task.getResult();
                                        // You can access the new user via result.getUser()
                                        // Additional user info profile *not* available via:
                                        // result.getAdditionalUserInfo().getProfile() == null
                                        // You can check if the user is new or existing:
                                        // result.getAdditionalUserInfo().isNewUser()
                                    } else {
                                        Toasty.error(getApplicationContext(), "Error signing in with email link").show();
                                        Intent mErroBack = new Intent(FullRegistration.this, LoginActivity.class);
                                        startActivity(mErroBack);
                                    }
                                }
                            });
                } else {
                    Toasty.error(getApplicationContext(), "Error signing in with email link").show();
                    Intent mErroBack = new Intent(FullRegistration.this, LoginActivity.class);
                    startActivity(mErroBack);
                }
            }
        }


        setContentView(R.layout.activity_registration);
//        new CommentKeyboardFix(this);
        ButterKnife.bind(this);

        if(name!=null&&!name.isEmpty()){
            mTxtxName.setText(getResources().getString(R.string.hey_don_your_are,name));
        }else {
            mTxtxName.setText(getResources().getString(R.string.hey_don_your_are,""));
        }


        mTagContainerLayout.setOnTagClickListener(this);

        apiCaller = Builder.getClient().create(ApiCaller.class);

        user_id = sp.getInt("user_id", 0);
        phone_number = sp.getString("phone_number", "");

        // making notification bar transparent
//        changeStatusBarColor();

        stepView = findViewById(R.id.step_view);
        stepView.setStepsNumber(7);

        if (name != null && !name.isEmpty()) {
            mTxtxName.setText(getResources().getString(R.string.hey_don_your_are, name));
        }

//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            image = bundle.getString("image");
//            name = bundle.getString("name");
//            first_name = name;
//            mTxtxName.setText(getResources().getString(R.string.hey_don_your_are, name));
//            mEditName.setText(name);
//            first_name = name;
////            email = bundle.getString("email");
//
//            mOrigin = bundle.getString("origin");
//
//
//
//        }else {
//            mTxtxName.setText(getResources().getString(R.string.hey_don_your_are, ""));
//
//        }

        if (Preferences.get(getApplicationContext()).isImageRemaining()) {
            stepView.animate().alpha(1.0f);
            stepView.setVisibility(View.VISIBLE);
            stepView.go(6, true);

            layout6.setVisibility(View.VISIBLE);
        } else {
            stepView.go(0, true);
            layout0.setVisibility(View.VISIBLE);
        }


        mtxtSelected.setText("Women");



        mBirthdayEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mSubMitDate.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSubMitDate.setEnabled(true);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() <= 0) {
                    mSubMitDate.setEnabled(false);
                }

            }
        });


        mEditName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mNext1.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mNext1.setEnabled(true);

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() <= 0) {
                    mNext1.setEnabled(false);
                }

            }
        });

        rmSwitchMen.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked) {
                    if (rmSwitchWomen.isChecked()) {
                        mtxtSelected.setText("Both");
                        mGenderIntereset = "BOTH";
                    } else {
                        mtxtSelected.setText("MEN");
                        mGenderIntereset = "MALE";
                    }
                } else {
                    if (rmSwitchWomen.isChecked()) {
                        mtxtSelected.setText("WOMEN");
                        mGenderIntereset = "FEMALE";
                    } else {
                        rmSwitchWomen.setChecked(true);
                        mtxtSelected.setText("WOMEN");
                        mGenderIntereset = "FEMALE";
                    }
                }
            }
        });

        rmSwitchWomen.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked) {
                    if (rmSwitchMen.isChecked()) {
                        mtxtSelected.setText("Both");
                        mGenderIntereset = "Both";
                    } else {
                        mtxtSelected.setText("WOMEN");
                        mGenderIntereset = "FEMALE";
                    }
                } else {
                    if (rmSwitchWomen.isChecked()) {
                        mtxtSelected.setText("MEN");
                        mGenderIntereset = "MALE";
                    } else {
                        rmSwitchMen.setChecked(true);
                        mtxtSelected.setText("MEN");
                        mGenderIntereset = "MALE";
                    }
                }
            }
        });

        mChooseMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedGender = "MALE";
                if (name != null && !name.isEmpty()) {
                    first_name = name;
                    mTxtBirthday.setText(getResources().getString(R.string.txt_birthday_user, first_name));
                    moveNext(3, layout0, layout2);

                    return;
                }

                moveNext(1, layout0, layout1);


            }
        });


        mChooseFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedGender = "FEMALE";
                if (name != null && !name.isEmpty()) {
                    first_name = name;
                    mTxtBirthday.setText(getResources().getString(R.string.txt_birthday_user, first_name));
                    moveNext(3, layout0, layout2);
                    return;
                }
                moveNext(1, layout0, layout1);

            }
        });

        mEditTag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mAddTag.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAddTag.setEnabled(true);
                mNext3.setEnabled(false);


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() <= 0) {
                    mAddTag.setEnabled(false);
                    mNext3.setEnabled(true);
                }

            }
        });


        mAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditTag.getText() != null && !mEditTag.toString().isEmpty()) {
                    if (mEditTag.getText().toString().length() > 2) {

                        if (mEditTag.getText().toString().length() <= 18) {
                            mTagContainerLayout.addTag(mEditTag.getText().toString());
                            mEditTag.setText("");
                        } else {
                            mEditTag.setError("Be brief, remember you can add as many as you want");
                        }


                    } else {
                        mEditTag.setError("Interest must be more that 2 words");
                    }

                } else {
                    mEditTag.setError("Add Interest");
                }
            }
        });


        mNext1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditName.getText() != null) {
                    first_name = mEditName.getText().toString();
                    last_name = "";
                    if (!first_name.isEmpty()) {
                        if (first_name.length() > 2 && first_name.length() < 20) {
                            moveNext(2, layout1, layout2);
                            mEditName.clearFocus();
                            mTxtBirthday.setText(getResources().getString(R.string.txt_birthday_user, first_name));


                        } else {
                            mEditName.setError("Enter a valid Name");
                        }
                    } else {
                        mEditName.setError("Enter a valid Name");
                    }
                } else {
                    mEditName.setError("Please add Name");
                }


            }
        });


        mBirthdayEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCalender();
            }
        });

        mSubMitDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (date_of_birth != null && !date_of_birth.isEmpty()) {
                    moveNext(3, layout2, layout3);
                    mBirthdayEdit.clearFocus();
                } else {
                    mBirthdayEdit.setError("Enter date of birth");
                }


            }
        });


        mNext3.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {

                try {

                    List<String> interests = mTagContainerLayout.getTags();
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    jsonInterests = gson.toJson(interests);
                    Log.d("REG", "onClick: " + jsonInterests);
                    System.out.println(jsonInterests);

                    moveNext(4, layout3, layout4);
                    mEditTag.clearFocus();


                } catch (Exception e) {
                    Log.d(TAG, "onClick: " + e.getMessage());

                }


            }
        });

        mSubmitSelectedGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveNext(5, layout4, layout5);
            }
        });


        mSUbmitBio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: email= " + email);

                if (mBioEdit.getText() != null) {
                    bio = mBioEdit.getText().toString();
                } else {
                    bio = "";
                }
                MaterialDialog dialog;
                MaterialDialog.Builder builder = new MaterialDialog.Builder(FullRegistration.this);
                builder.content("Uploading ....")
                        .widgetColor(getResources().getColor(R.color.colorAccent))
                        .progress(true, 0);
                dialog = builder.build();
                dialog.setCancelable(false);
                dialog.show();

                RegisterUserParams params = new RegisterUserParams();
                params.setRequestType("REGISTER_USER");
                params.setPhoneNumber(phone_number);
                params.setFirstName(first_name);
                params.setJsonInterest(jsonInterests);
                params.setLastName(last_name);
                params.setGender(selectedGender);
                params.setDob(date_of_birth);
                params.setProfilePicture(image);
                params.setEmail(email);
                params.setGenderOfInterest(mGenderIntereset);
                params.setAboutMe(bio);
                params.setOrigin(mOrigin);
                Call<RegisterUserResponse> call = apiCaller.registerUser(params);
                call.enqueue(new Callback<RegisterUserResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<RegisterUserResponse> call, @NonNull Response<RegisterUserResponse> response) {
                        if (response.isSuccessful()) {
                            RegisterUserResponse rur = response.body();
                            if (rur != null) {
                                if (rur.getStatusCode().equals("00")) {
                                    if (rur.getBody().getStatusMessage().equals("SUCCESS")) {


                                        dialog.dismiss();
                                        user_id = rur.getBody().getUserId();
                                        saveUserDetails(rur.getBody().getPackageId());
                                        Toasty.success(getApplicationContext(), "Done, One more Step").show();

                                        moveNext(6, layout5, layout6);

                                        Preferences.get(getApplicationContext()).setIsImageRemaining(true);

                                    } else {
                                        dialog.dismiss();
                                        Toasty.warning(getApplicationContext(), "Something went wrong, try again").show();
                                    }
                                } else {
                                    dialog.dismiss();
                                    Toasty.warning(getApplicationContext(), "Something went wrong, try again").show();
                                }
                            }


                        } else {
                            dialog.dismiss();
                            if (response.body() != null) {
                                dialog.dismiss();
                                Toasty.warning(getApplicationContext(), "Something went wrong, try again").show();
                            }
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<RegisterUserResponse> call, @NonNull Throwable t) {
                        dialog.dismiss();
                        if (t.getMessage().contains("Failed to connect")) {
                            Toasty.warning(getApplicationContext(), "Something Went wrong,Check Internet and try again").show();
                        } else {
                            Toasty.warning(getApplicationContext(), "Something Went wrong,try again").show();
                        }


                    }
                });


            }
        });


        mNext4.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick(View v) {
                if (filePath != null && filePath.isEmpty()) {
                    AppUtils.showToast(getApplicationContext(), "No photo uploaded", true);
                } else {

//                    try {
                    Log.d("TESTING", "uploadToServer: " + filePath);
                    MaterialDialog dialog;
                    MaterialDialog.Builder builder = new MaterialDialog.Builder(FullRegistration.this);
                    builder.content("Uploading ....")
                            .widgetColor(getResources().getColor(R.color.colorAccent))
                            .progress(true, 0);
                    dialog = builder.build();
                    dialog.setCancelable(false);
                    dialog.show();
                    ApiCaller apiCaller = Builder2.getClient().create(ApiCaller.class);
                    //Create a file object using file path
                    File file = new File(filePath);
                    // Create a request body with file and image media type
                    RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
                    // Create MultipartBody.Part using file request-body,file name and part name
                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("profile_picture", file.getName(), fileReqBody);
                    //Create request body with text description and text media type
                    RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");
                    RequestBody mUserId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(user_id));
                    Log.d(TAG, "onClick: " + user_id);
                    RequestBody mIsProfile = RequestBody.create(MediaType.parse("text/plain"), "1");

                    //


                    Call<RegisterUserResponse> call = apiCaller.uploadImage1(fileToUpload, description, mUserId, mIsProfile);
                    call.enqueue(new Callback<RegisterUserResponse>() {

                        @Override
                        public void onResponse(@NonNull Call<RegisterUserResponse> call, @NonNull Response<RegisterUserResponse> response) {
                            if (response.isSuccessful()) {
                                RegisterUserResponse upr = response.body();
                                if (upr.getBody().getStatusMessage().equals("SUCCESS")) {
                                    dialog.dismiss();
                                    savePhotoPath(upr.getBody().getPhoto_path());
                                    Preferences.get(getApplicationContext()).setIsImageRemaining(false);
//                                    Glide.with(getApplicationContext()).load(upr.getBody().getPhoto_path()).into(mImgProfile);


                                    Toasty.success(getApplicationContext(), "Done").show();
                                } else {
                                    dialog.dismiss();
                                    AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);
                                }

                            } else {
                                dialog.dismiss();
                                AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);
                            }

                        }

                        @Override
                        public void onFailure(@NonNull Call<RegisterUserResponse> call, @NonNull Throwable throwable) {
                            Log.d("TESTING", "onFailure: " + throwable.getMessage());
                            dialog.dismiss();
                            AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);

                        }
                    });
//                    }catch (Exception e){
//                        Log.d("TESTING", e.getMessage());
//
//                    }

                }


            }
        });


        mImgProfile.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });


        mImgPlus.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                loadImage();
            }
        });

        mUploadProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImage();
            }
        });


    }

    private void openCalender() {

        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = null;
        dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                FullRegistration.this, 2000, 01, 01);
        dpd.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
        dpd.setOnDateSetListener(this);
        dpd.setTitle("Pick Date of Birth");
        dpd.setScrollOrientation(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.ScrollOrientation.HORIZONTAL);

        dpd.showYearPickerFirst(true);
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");
    }

    private void moveNext(int i, RelativeLayout mL1, RelativeLayout mL2) {
        if (stepView.getVisibility() == View.GONE) {
            stepView.setVisibility(View.VISIBLE);
        }

        mL1.animate().alpha(0.1f);
        mL1.setVisibility(View.GONE);
        mL2.animate().alpha(1.0f);
        mL2.setVisibility(View.VISIBLE);


        stepView.go(i, true);
    }

    private void loadImage() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    ).withListener(new MultiplePermissionsListener() {
                @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if(report.areAllPermissionsGranted()){
                        showImagePickerOptions();
                    }else {
                        Toasty.error(getApplicationContext(),"permissions required, allow to continue",Toasty.LENGTH_SHORT).show();
                    }
                }
                @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
            }).check();

        } else {
            showImagePickerOptions();
        }


    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void showSettingsDialog() {

        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(FullRegistration.this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle(R.string.dialog_permission_title);
        builder.setCancelable(true);
        builder.setTextGravity(Gravity.CENTER);
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setCancelable(false);
        builder.addButton(getString(R.string.go_to_settings), -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openSettings();
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(FullRegistration.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        //Appending ImageView


        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(FullRegistration.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void savePhotoPath(String photo_path) {
        editor.putString("image", photo_path);
        editor.apply();
        loginUserPhoneNumber();

    }

    private void loginUserPhoneNumber() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(FullRegistration.this);
            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
            builder.setTitle("Welcome on board");
            builder.setCancelable(true);
            builder.setTextGravity(Gravity.CENTER);
            builder.setMessage("To get you started, Allow Tunda to access your location and find people near you.");
            builder.setCancelable(false);
            builder.addButton("Allow", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Dexter.withActivity(FullRegistration.this)
                                    .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                                    .withListener(new MultiplePermissionsListener() {
                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                                            if (report.areAllPermissionsGranted()) {
                                                Preferences.get(FullRegistration.this).setKeyIsFirstTimeLaunch(false);
                                                Preferences.get(FullRegistration.this).setKeyIsLoggedIn(true);
                                                Intent intent = new Intent(FullRegistration.this, SplashActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                                dialog.dismiss();
                                            }

                                            if (report.isAnyPermissionPermanentlyDenied()) {
                                                showSettingsDialog();
                                            }
                                        }


                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                            token.continuePermissionRequest();
                                        }
                                    }).check();


                        }
                    });

            builder.show();

        } else {
            Preferences.get(FullRegistration.this).setKeyIsFirstTimeLaunch(false);
            Preferences.get(FullRegistration.this).setKeyIsLoggedIn(true);
            Intent intent = new Intent(FullRegistration.this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }


    }

    PermissionListener dialogPermissionListener =
            DialogOnDeniedPermissionListener.Builder
                    .withContext(FullRegistration.this)
                    .withTitle("Location permission")
                    .withMessage("Location permission is needed to find people near you")
                    .withButtonText("Ok")
                    .build();

    MultiplePermissionsListener dialogMultiplePermissionsListener =
            DialogOnAnyDeniedMultiplePermissionsListener.Builder
                    .withContext(FullRegistration.this)
                    .withTitle("Camera & external storage permission")
                    .withMessage("Both camera and external storage permission are needed to upload your photo")
                    .withButtonText("Ok")
                    .build();


    private void saveUserDetails(int package_id) {


        editor = sp.edit();
        editor.putInt("user_id", user_id);
        if (last_name != null) {
            editor.putString("first_name", first_name + " " + last_name);
        } else {
            editor.putString("first_name", first_name);
        }

        editor.putString("gender", selectedGender);
        editor.putString("dob", date_of_birth);
        editor.putString("gender_of_interest", mGenderIntereset);
        editor.putInt("package_id", package_id);
        editor.putString("about_me", bio);
        editor.putString("user_status", "ACTIVE");
        if (jsonInterests != null && !jsonInterests.isEmpty()) {
            editor.putString("interest", jsonInterests);
        }


        editor.apply();
    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }


    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }


    }


    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = null;
                if (data != null) {
                    uri = data.getParcelableExtra("path");
                }
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    // loading profile image from local cache
                    if (uri != null) {
                        filePath = getRealPathFromURIPath(uri, FullRegistration.this);
                        loadProfile(uri.toString(), filePath);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    private void loadProfile(String s, String filePath) {
        Glide.with(FullRegistration.this).load(s).into(mImgProfile);
        mNext4.setEnabled(true);
    }


    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            cursor.close();
            return cursor.getString(idx);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
            }
        }

    }

    private int calculateAge(String mDob) {
        Calendar calendar = Calendar.getInstance();
        Log.d("TESTING", mDob);
        String DAY;

        String YEAR = mDob.substring(0, 4);
        Log.d("TESTING", "year=" + YEAR);
        String MONTH = mDob.substring(5, 7);
        if (MONTH.contains("-")) {
            MONTH = MONTH.replace("-", "");
            Log.d("TESTING", "month=" + MONTH);
            DAY = mDob.substring(7, mDob.length());
        } else {
            DAY = mDob.substring(8, mDob.length());
        }


        Calendar x = new GregorianCalendar(Integer.valueOf(YEAR), Integer.valueOf(MONTH), Integer.valueOf(DAY));

        Log.d("TESTING", YEAR + "/" + MONTH + "/" + DAY);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));

        Date date = x.getTime();

//        date.setYear();
//        date.setMonth();
//        date.setDate();

        Log.d("TESTING", String.valueOf(date) + ">>" + date.getYear());

        Date currentDate = new Date();


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(date));
        int d2 = Integer.parseInt(formatter.format(currentDate));
        int age = (d2 - d1) / 10000;
        return age;


    }


    @Override
    public void onTagClick(int position, String text) {
        mTagContainerLayout.removeTag(position);

    }

    @Override
    public void onTagLongClick(int position, String text) {
        mTagContainerLayout.removeTag(position);

    }

    @Override
    public void onSelectedTagDrag(int position, String text) {

    }

    @Override
    public void onTagCrossClick(int position) {
        mTagContainerLayout.removeTag(position);

    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        Date chosenDate = cal.getTime();

        DateFormat df_medium_us = DateFormat.getDateInstance(DateFormat.SHORT);
        String _date_of_birth = df_medium_us.format(chosenDate);
        String _year = String.valueOf(year);
        int month_selected = monthOfYear + 1;
        String _month = String.valueOf(month_selected);
        if (_month.length() == 1) {
            _month = "0" + _month;
        }
        String _day = String.valueOf(dayOfMonth);
        if (_day.length() == 1) {
            _day = "0" + _day;
        }
        date_of_birth = _year + "-" + _month + "-" + _day;
        Log.d("TESTING", "onDateSet: " + date_of_birth);
        int ageOfUser = calculateAge(date_of_birth);

        if (ageOfUser < 18) {
            mBirthdayEdit.setText("");
            mBirthdayEdit.setError("You have to be over 18");
            mSubMitDate.setEnabled(false);
            Toasty.error(FullRegistration.this, "Check Date! You have to be over 18", Toasty.LENGTH_LONG).show();
        } else {
            mBirthdayEdit.setText(_date_of_birth);
            mBirthdayEdit.setError(null);
            mSubMitDate.setEnabled(true);
        }


    }

    @Override
    public void onBackPressed() {
        if (stepView.getCurrentStep() == 0) {
            finish();
            return;
        }

        stepView.go(stepView.getCurrentStep() - 1, true);
        rewind();
    }

    private void rewind() {

        if (layout6.getVisibility() == View.VISIBLE) {
            layout6.animate().alpha(0.1f);
            layout6.setVisibility(View.GONE);
            layout5.animate().alpha(1.0f);
            layout5.setVisibility(View.VISIBLE);
            return;
        }


        if (layout5.getVisibility() == View.VISIBLE) {
            layout5.animate().alpha(0.1f);
            layout5.setVisibility(View.GONE);
            layout4.animate().alpha(1.0f);
            layout4.setVisibility(View.VISIBLE);
            return;
        }

        if (layout4.getVisibility() == View.VISIBLE) {
            layout4.animate().alpha(0.1f);
            layout4.setVisibility(View.GONE);
            layout3.animate().alpha(1.0f);
            layout3.setVisibility(View.VISIBLE);
            return;
        }

        if (layout3.getVisibility() == View.VISIBLE) {
            layout3.animate().alpha(0.1f);
            layout3.setVisibility(View.GONE);
            layout2.animate().alpha(1.0f);
            layout2.setVisibility(View.VISIBLE);
            return;
        }

        if (layout2.getVisibility() == View.VISIBLE) {
            layout2.animate().alpha(0.1f);
            layout2.setVisibility(View.GONE);
            layout1.animate().alpha(1.0f);
            layout1.setVisibility(View.VISIBLE);
            return;
        }

        if (layout1.getVisibility() == View.VISIBLE) {
            layout1.animate().alpha(0.1f);
            layout1.setVisibility(View.GONE);
            layout0.animate().alpha(1.0f);
            layout0.setVisibility(View.VISIBLE);
            return;
        }

        if (layout0.getVisibility() == View.VISIBLE) {
            layout0.animate().alpha(0.1f);
            layout0.setVisibility(View.GONE);
            finish();
        }

    }
}
