

package ke.co.tunda.Activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.willowtreeapps.spruce.Spruce;
import com.willowtreeapps.spruce.animation.DefaultAnimations;
import com.willowtreeapps.spruce.sort.DefaultSort;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.Adapters.SendLikeSwipeDataInterface;
import ke.co.tunda.Adapters.UserLikesAdapter;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.GetLikesparams;
import ke.co.tunda.Callbacks.LoadUserLike;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Models.UserLikesArray;
import ke.co.tunda.Models.UserLikesModel;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Swipe;
import ke.co.tunda.SwipeRecorder.SwipeRecordsParams;
import ke.co.tunda.SwipeRecorder.SwipeResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserLikes extends AppCompatActivity implements LoadUserLike, SendLikeSwipeDataInterface {

    static int user_identity;
    static String number_of_likes;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    static ApiCaller apiCaller;
    private Animator spruceAnimator;
    private static UserLikesAdapter userLikesAdapter;
    private static Context mContext;

    @BindView(R.id.recycler)
    RecyclerView mRecycler;
    @BindView(R.id.tool_bar)
    Toolbar mToolbar;
    @BindView(R.id.simpleProgressBar)
    ProgressBar mBarProgress;

    private static int TYPE_SKIP = 1;
    private static int TYPE_LIKE = 2;
    private static int TYPE_SUPERLIKE = 3;
    private static String TAG = "US";
    private AsyncTask<Void, Void, Void> mSendTopPicks;


    public static void open(Context context, int user_id, String number) {

        user_identity = user_id;
        number_of_likes = number;
        mContext = context;
        context.startActivity(new Intent(context, UserLikes.class));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_likes);

        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mToolbar.setTitle(getString(R.string.you_have_likes, number_of_likes));
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        apiCaller = Builder.getClient().create(ApiCaller.class);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
//                initSpruce();
            }
        };

        // Mock data objects
        List<UserLikesArray> placeHolderList = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            placeHolderList.add(new UserLikesArray());
        }

        mBarProgress.animate().alpha(1.0f);
        mBarProgress.setVisibility(View.VISIBLE);


        new getUserLikes().execute();


        userLikesAdapter = new UserLikesAdapter(getApplicationContext(), this, this);


        mRecycler.setAdapter(userLikesAdapter);
//        userLikesAdapter.addLikes(placeHolderList);

        mRecycler.setLayoutManager(gridLayoutManager);

    }

    @Override
    public void loadUserLike(Dialog dialog, Boolean hasMultipleImages, Boolean show_controls, int position) {
        FullProfile.open(mContext, dialog, hasMultipleImages, show_controls, Extras.ORIGIN_USERLIKES, position, null);

//        try {
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                    ActivityOptions options = ActivityOptions
//                            .makeSceneTransitionAnimation( mContext, image, "match_photo");
//                    FullProfile.open(mContext, dialog, hasMultipleImages, false, 1, 1234, options);
//
//
//                } else {
////
//
//                }
//            } catch (Exception e) {
//                Log.d("TEST", e.getLocalizedMessage());
//            }


    }

    private void initSpruce() {
        spruceAnimator = new Spruce.SpruceBuilder(mRecycler)
                .sortWith(new DefaultSort(100))
                .animateWith(DefaultAnimations.shrinkAnimator(mRecycler, 800),
                        ObjectAnimator.ofFloat(mRecycler, "translationX", -mRecycler.getWidth(), 0f).setDuration(800))
                .start();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (spruceAnimator != null) {
            spruceAnimator.start();
        }


        FullProfile.updateUserLikes(new FullProfile.UpdateUserLikes() {
            @Override
            public void executeSwipeAction(int type, int position) {
                if (type == TYPE_SKIP) {
                    Log.d(TAG, "executeSwipeAction: ");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            skipUser(position);

                        }
                    }, 500);


                    return;
                }

                if (type == TYPE_LIKE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            likeUser(position);

                        }
                    }, 500);

                    return;
                }

                if (type == TYPE_SUPERLIKE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            superLikeUser(position);

                        }
                    }, 500);

                }

            }


        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void skipUser(int position) {
        userLikesAdapter.skipUser(position);
        Log.d("posin", "skipUser: " + position);


    }

    private void likeUser(int position) {
        userLikesAdapter.likeUser(position);

    }

    private void superLikeUser(int position) {
        userLikesAdapter.superLikeUser(position);
    }

    @Override
    public void sendLikeSwipes(int user_id, ArrayList<Swipe> mArrayList) {
        SwipeRecordsParams swipeRecordsParams = new SwipeRecordsParams();
        swipeRecordsParams.setRequestType("USER_SWIPE");
        swipeRecordsParams.setUserId(user_id);
        swipeRecordsParams.setArrayList(mArrayList);
        mSendTopPicks = new sendTopPicks(swipeRecordsParams);
        mSendTopPicks.execute();

    }

    private class getUserLikes extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {


            GetLikesparams getLikesparams = new GetLikesparams();
            getLikesparams.setRequestType("GET_MY_LIKES");
            getLikesparams.setUserId(user_identity);
            Call<UserLikesModel> userLikesModelCall = apiCaller.getUserLikes(getLikesparams);
            userLikesModelCall.enqueue(new Callback<UserLikesModel>() {
                @Override
                public void onResponse(Call<UserLikesModel> call, Response<UserLikesModel> response) {
                    if (response.isSuccessful()) {
                        UserLikesModel userLikesModel = response.body();
                        if (userLikesModel != null && userLikesModel.getBody() != null) {
                            mBarProgress.animate().alpha(0.1f);
                            mBarProgress.setVisibility(View.GONE);

                            userLikesAdapter.addLikes(userLikesModel.getBody());

                        } else {

                            mBarProgress.animate().alpha(0.1f);
                            mBarProgress.setVisibility(View.GONE);
                            if (userLikesModel != null) {
                                Toasty.error(mContext, userLikesModel.getStatusCode(), Toasty.LENGTH_SHORT).show();
                            }
                        }
                    }

                }

                @Override
                public void onFailure(Call<UserLikesModel> call, Throwable t) {
                    mBarProgress.animate().alpha(0.1f);
                    mBarProgress.setVisibility(View.GONE);
                    Toasty.error(mContext, t.getMessage(), Toasty.LENGTH_SHORT).show();

                }
            });

            return null;
        }
    }

    private class sendTopPicks extends AsyncTask<Void, Void, Void> {
        SwipeRecordsParams mSwipeRecordsParams;
        ArrayList<Swipe> mSwipe;

        sendTopPicks(SwipeRecordsParams swipeRecordsParams) {
            this.mSwipeRecordsParams = swipeRecordsParams;
            this.mSwipe = mSwipeRecordsParams != null ? mSwipeRecordsParams.getArrayList() : null;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<SwipeResponse> call = apiCaller.sendSwipeRecords(mSwipeRecordsParams);

            call.enqueue(new Callback<SwipeResponse>() {
                @Override
                public void onResponse(Call<SwipeResponse> call, Response<SwipeResponse> response) {
                    if (response.isSuccessful()) {

                        SwipeResponse swipeResponse = response.body();
                        if (swipeResponse != null && swipeResponse.getStatusCode().equals("00")) {
                            Preferences.get(UserLikes.this).setIsTimeToRefreshConnections(true);
                            Log.d(TAG, "onResponse: 00");
                            if (mSwipe != null) {
                                if (mSwipe.get(0).getSwipe_action().equals("LIKE") || mSwipe.get(0).getSwipe_action().equals("SUPERLIKE")) {
                                    Toasty.success(UserLikes.this, getString(R.string.you_got_a_match), Toasty.LENGTH_SHORT).show();
                                }
                            }


                        } else if (swipeResponse != null && swipeResponse.getStatusCode().equals("99")) {

                            if (mSwipe != null) {
                                if (mSwipe.get(0).getSwipe_action().equals("DISLIKE")) {
                                    Toasty.warning(UserLikes.this, getString(R.string.oops_you_missed), Toasty.LENGTH_SHORT).show();
                                }
                            }


                        }


                    } else {
                        Log.d(TAG, "onResponse: 78");

                    }
                }

                @Override
                public void onFailure(Call<SwipeResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: 001" + t.getMessage());


                }
            });
            return null;
        }
    }


}
