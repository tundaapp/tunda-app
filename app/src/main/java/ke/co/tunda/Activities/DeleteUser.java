

package ke.co.tunda.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.Models.DeleteAccountParams;
import ke.co.tunda.Models.DeleteAccountResponse;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.SqLite.DatabaseHandler;
import ke.co.tunda.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteUser extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {


    @BindView(R.id.edit_other)
    EditText mEditOther;
    @BindView(R.id.deleteUser)
    Button mDeleteUser;
    @BindView(R.id.cancel_delete)
    Button mCancelDelete;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.radio_found_person)
    RadioButton mRadioFoundPerson;
    @BindView(R.id.radio_found_elsewhere)
    RadioButton mRadioFoundElseWhere;
    @BindView(R.id.app_not_good)
    RadioButton mAppNotGood;
    @BindView(R.id.radio_group)
    RadioGroup mRadio;
    MaterialDialog md;
    int user_id;


    private String mReason = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_delete_user);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(DeleteUser.this);
        user_id = sp.getInt("user_id", 0);
        ButterKnife.bind(this);
        mRadio.setOnCheckedChangeListener(this);

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mEditOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRadio.clearCheck();
                mReason = "";
                if (mReason.isEmpty()) {
                    showKeyboard(mEditOther);
                } else {
                    AppUtils.showToast(getApplicationContext(), "You already chose one up there", false);
                    hideSoftKeyboard(mEditOther);
                }

            }
        });


        mDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hideSoftKeyboard(mEditOther);
                if (!mReason.isEmpty()) {
                    deleteUser();

                } else {
                    mReason = mEditOther.getText().toString();
                    if (!mReason.isEmpty()) {
                        deleteUser();


                    } else {
                        AppUtils.showToast(getApplicationContext(), "Please give us a reason", false);
                    }

                }
            }
        });


        mCancelDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void deleteUser() {


        try{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(DeleteUser.this, R.style.myDialog));
            alertDialogBuilder.setTitle("Confirm Account Deletion!");
            alertDialogBuilder.setMessage("You will no longer be available on Tunda\nAll your matches and conversations will be deleted.\n" +
                    "Are you sure?");
            alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MaterialDialog.Builder builder = new MaterialDialog.Builder(DeleteUser.this);
                            builder.content("Deleting ....")
                                    .widgetColor(getResources().getColor(R.color.colorAccent))
                                    .progress(true, 0);
                            md = builder.build();
                            md.setCancelable(false);
                            md.show();
                        }
                    });





                    ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
                    DeleteAccountParams prm = new DeleteAccountParams();
                    prm.setRequestType("DELETE_ACCOUNT");
                    prm.setUserId(user_id);
                    prm.setReason(mReason);
                    Call<DeleteAccountResponse> call = apiCaller.deleteAccount(prm);
                    call.enqueue(new Callback<DeleteAccountResponse>() {
                        @Override
                        public void onResponse(Call<DeleteAccountResponse> call, Response<DeleteAccountResponse> response) {
                            if (response.isSuccessful()) {
                                DeleteAccountResponse deleteAccountResponse = response.body();
                                if (deleteAccountResponse.getBody().equals("SUCCESS")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            md.dismiss();
                                            Preferences.get(DeleteUser.this).setKeyIsFirstTimeLaunch(true);
                                            Preferences.get(DeleteUser.this).setKeyIsLoggedIn(false);
                                            Preferences.get(DeleteUser.this).setKeyIsFirstLogin(true);
                                            DatabaseHandler handler = new DatabaseHandler(DeleteUser.this);
                                            handler.deleteAllDialogs();
                                            handler.deleteAllMactches();

                                            AppUtils.showToast(getApplicationContext(), "Sorry to see you go, Start a fresh", true);
                                            WelcomeActivity.open(DeleteUser.this);
                                            finish();
                                        }
                                    });


                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            md.dismiss();
                                            AppUtils.showToast(getApplicationContext(), "Something went wrong", true);
                                        }
                                    });

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<DeleteAccountResponse> call, Throwable t) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    md.dismiss();
                                    AppUtils.showToast(getApplicationContext(), "Something went wrong", true);
                                }
                            });


                        }
                    });


                }
            });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();


                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }catch (Exception e){
            Log.d("TAG", "deleteUser: ");
        }

    }

    public static void open(Context context) {
        context.startActivity(new Intent(context, DeleteUser.class));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radio_group:
                if (mRadioFoundPerson.isChecked()) {
                    AppUtils.showToast(getApplicationContext(), "clicked", true);
                    mReason = "Found my match on Tunda";
                    break;

                } else if (mRadioFoundElseWhere.isChecked()) {
                    mReason = "Found my match elsewhere";
                    break;
                } else if (mAppNotGood.isChecked()) {
                    mReason = "App is not good";
                    break;
                }
                break;
        }

    }


    public void showKeyboard(final EditText ettext) {

        ettext.setClickable(false);
        ettext.setFocusable(true);
        ettext.setFocusableInTouchMode(true);
        ettext.requestFocus();
        ettext.setSelection(ettext.getText().length());

        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 200);
    }


    private void hideSoftKeyboard(EditText ettext) {
        ettext.setClickable(false);
        ettext.setFocusable(false);
        ettext.setFocusableInTouchMode(false);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.radio_group:
                if (mRadioFoundPerson.isChecked()) {
                    mReason = "Found my match on Tunda";
                    break;

                } else if (mRadioFoundElseWhere.isChecked()) {
                    mReason = "Found my match elsewhere";
                    break;
                } else if (mAppNotGood.isChecked()) {
                    mReason = "App is not good";
                    break;
                }
                break;
        }
    }
}
