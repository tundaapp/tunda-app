/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/21/19 12:11 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guardanis.applock.AppLock;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.androidtagview.TagContainerLayout;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.Adapters.MultiplePhotoAdapter;
import ke.co.tunda.Adapters.MultiplePhotoAdapterIg;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Builder4;
import ke.co.tunda.ApiConnector.Models.MultiPlePhotosResponse;
import ke.co.tunda.ApiConnector.Models.MultiplePhotoRequest;
import ke.co.tunda.ApiConnector.Models.ReportUserParams;
import ke.co.tunda.ApiConnector.Models.RequestCallParams;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Helpers.MyBounceInterpolator;
import ke.co.tunda.Models.BaseResponse;
import ke.co.tunda.Models.SendViewParams;
import ke.co.tunda.POJO.AccessTokenResponse;
import ke.co.tunda.POJO.Datum;
import ke.co.tunda.POJO.GetIgPhotoResponse;
import ke.co.tunda.POJO.GetIgPhotosRequest;
import ke.co.tunda.POJO.MediaRepsonse;
import ke.co.tunda.POJO.PhotoResponse;
import ke.co.tunda.POJO.SectionsList;
import ke.co.tunda.POJO.SendIgPhotosRequest;
import ke.co.tunda.R;
import ke.co.tunda.fragments.Dialogs.AuthenticationDialog;
import ke.co.tunda.listener.AuthenticationListener;
import ke.co.tunda.rest.RestClient;
import ke.co.tunda.rest.RetrofitService;
import ke.co.tunda.utils.AppUtils;
import ke.co.tunda.utils.Constants;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FullProfile extends AppCompatActivity implements View.OnClickListener, AuthenticationListener {

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    ApiCaller apicaller;
    private MultiplePhotoAdapter multiplePhotoAdapter;
    private TextView[] mDots;
    int page = 0;
    private int mSize;
    ArrayList<String> mPhotos;
    ArrayList<String> mPhotosIg;
    ArrayList<String> mPhotosIgUser;


    @BindView(R.id.txt_name_age)
    TextView mNameAge;
    @BindView(R.id.txt_bio)
    TextView mTxtBio;
    @BindView(R.id.txt_location)
    TextView mTxtLocation;
    @BindView(R.id.txt_job)
    TextView mTxtxJob;
    @BindView(R.id.txt_education)
    TextView mTxtEducation;
    @BindView(R.id.container_bio)
    RelativeLayout mBioContainer;
    @BindView(R.id.container_location)
    RelativeLayout mLocationContainer;
    @BindView(R.id.container_job)
    RelativeLayout mJobContainer;
    @BindView(R.id.container_education)
    RelativeLayout mEducationContainer;
    @BindView(R.id.photos_pager)
    ViewPager mViewPager;
    @BindView(R.id.linearLayout)
    LinearLayout mDotLayout;
    @BindView(R.id.fab)
    FloatingActionButton mFab;
    @BindView(R.id.divider_1)
    View mDivider1;
    @BindView(R.id.divider_2)
    View mDivider2;
    @BindView(R.id.other_layout)
    RelativeLayout mOther;
    @BindView(R.id.report_user)
    TextView mActionReport;
    @BindView(R.id.cause)
    EditText mEdit;
    @BindView(R.id.tagcontainerLayout)
    TagContainerLayout mTagContainer;
    @BindView(R.id.interests_holder)
    RelativeLayout mIntHolder;
    //    @BindView(R.id.button_container)
//    FrameLayout mButtonContainer;
    @BindView(R.id.skip_button)
    ImageView mSkipButton;
    @BindView(R.id.like_button)
    ImageView mLikeButton;
    @BindView(R.id.super_like_button)
    ImageView mSuperLikeButton;
    @BindView(R.id.title_tags)
    TextView mTitleTags;
    @BindView(R.id.user_verified)
    ImageView mUerVerified;
    @BindView(R.id.divider_3)
    View mDivider3;

    @BindView(R.id.verification_status)
    LinearLayout mVerificationStatus;
    @BindView(R.id.verify_button)
    Button mVerifyButton;
    @BindView(R.id.divider_6)
    View mDivider6;
    @BindView(R.id.likes_holder)
    LinearLayout mLikesHolder;
    @BindView(R.id.number_of_likes)
    TextView mNumberOfLikes;
    @BindView(R.id.divider_4)
    View mDivider4;

    @BindView(R.id.divider_call)
    View mDividerCall;
    @BindView(R.id.container_call)
    RelativeLayout mContainerCall;
    @BindView(R.id.txt_call_name)
    TextView mTxtCall;
    private String mPhoneNumber;


    @BindView(R.id.ig_photos_holder)
    RelativeLayout mIgPhotosHolder;
    @BindView(R.id.dot_layout)
    LinearLayout mDotLayoutIg;
    @BindView(R.id.ig_photo_count)
    TextView mIgPhotoCount;
    @BindView(R.id.ig_photos_pager)
    ViewPager mIgPager;
    @BindView(R.id.connect_to_ig)
    Button mConnect;
    @BindView(R.id.connect_holder)
    LinearLayout mConnectHolder;

    static FrameLayout mButtonContainer;


    private String photo, first_name, dob, distance, bio, job, school;
    String age = "";
    private static Dialog mDialog;
    private static boolean mHasMultiple;
    private String report_type = "Inappropriate Messages";
    private List<String> mInterests;
    private static UpdateFragment mCallback;
    private static UpdateTopPicks mCallbackTopPicks;
    private static UpdateLikes mCallbackLikes;
    private static UpdateUserLikes mCallbackUserLikesLikes;

    private static boolean mShowSwipeActions = false;
    private static int mOrigin;
    private static int mPosition;
    private static String TAG = "FP";
    private Integer isShowLikes;
    private int user_id;
    static int origin_page;


    private static boolean has_ig = false;
    private static boolean user_has_ig = false;

    private MultiplePhotoAdapterIg multiplePhotoAdapterIg;
    private TextView[] mDotsIg;
    int pageIg = 0;
    private int mSizeIg;
    private int mSizeIgUser;
    private ArrayList<SectionsList> mList;
    private int limit = 3;
    private String jsonStringPhootos = "";

    private AuthenticationDialog auth_dialog;

    ProgressDialog pd;
    RetrofitService retrofitService;
    RetrofitService retrofitServiceGraph;
    Datum[] data;


    public static void open(Context context, Dialog dialog, boolean hasMultiple,
                            boolean showSwipeActions, int origin, int position, ActivityOptions options) {
        origin_page = origin;
        Intent intent = new Intent(context, FullProfile.class);
        if (options != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.startActivity(intent);
            } else {
                context.startActivity(intent);
            }
        } else {
            context.startActivity(intent);
        }

        mDialog = dialog;
        mHasMultiple = hasMultiple;
        mShowSwipeActions = showSwipeActions;
        mOrigin = origin;
        mPosition = position;
        has_ig = mDialog.isHas_insta_photo();

        Log.d("posin", "open: " + mPosition);

    }

    public static void setBackground(Drawable gd) {
        mButtonContainer.setBackground(gd);
    }

    public static boolean isBackgroundnull() {
        return mButtonContainer.getBackground() == null;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.layout_full_profile);
        ButterKnife.bind(this);
        mButtonContainer = findViewById(R.id.button_container);
        if (mShowSwipeActions) {
            mButtonContainer.animate().alpha(1.0f);
            mButtonContainer.setVisibility(View.VISIBLE);
        } else {
            mButtonContainer.animate().alpha(0.1f);
            mButtonContainer.setVisibility(View.GONE);
        }
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height * 2 / 3);
        mViewPager.setLayoutParams(params);

        mInterests = new ArrayList<>();

        retrofitService = RestClient.getClient().create(RetrofitService.class);
        retrofitServiceGraph = RestClient.getClientGraph().create(RetrofitService.class);
        mPhotosIg = new ArrayList<>();
        mPhotosIgUser = new ArrayList<>();
        multiplePhotoAdapterIg = new MultiplePhotoAdapterIg(FullProfile.this);
        mIgPager.setAdapter(multiplePhotoAdapterIg);
        mIgPager.addOnPageChangeListener(viewListenerIg);
        mList = new ArrayList<>();

        mSuperLikeButton.setOnClickListener(this);
        mSkipButton.setOnClickListener(this);
        mLikeButton.setOnClickListener(this);
        mContainerCall.setOnClickListener(this);
        mConnect.setOnClickListener(this);


        try {
            apicaller = Builder.getClient().create(ApiCaller.class);
            sp = PreferenceManager.getDefaultSharedPreferences(this);
            editor = sp.edit();

            user_id = sp.getInt(Extras.USER_ID, 0);
            mPhoneNumber = sp.getString("phone_number", "");
            user_has_ig = sp.getBoolean("has_ig_photos", false);

            if (has_ig) {
                new getIgPhotos().execute();

            }



            if (origin_page == Extras.ORIGIN_TOP_PICKS) {
                if (user_id > 0) {
                    SendViewParams sendViewParams = new SendViewParams();
                    sendViewParams.setRequestType("UPDATE_BOOST_VIEW");
                    sendViewParams.setUserId(Integer.valueOf(mDialog.getRecipient_id()));
                    sendViewParams.setViewerId(user_id);


                    new sendView(sendViewParams).execute();
                }


            }


            String jsonString = mDialog.getInterest();

            if (jsonString != null && jsonString.length() >= 1 && !jsonString.equals("[]")) {
                mIntHolder.setVisibility(View.VISIBLE);
                mDivider4.setVisibility(View.VISIBLE);
                mTitleTags.setVisibility(View.VISIBLE);
                mDivider6.setVisibility(View.GONE);

                JSONArray jr = new JSONArray(jsonString);
                for (int i = 0; i < jr.length(); i++) {

                    String interest = jr.getString(i);
                    Log.d("FULL", "onCreate: interest number " + i + ">>" + interest);
                    mInterests.add(interest);

                    // loop and add it to array or arraylistlide
                }
                mTagContainer.setTags(mInterests);

            } else {
                mIntHolder.animate().alpha(0.5f);
                mIntHolder.setVisibility(View.GONE);
                mDivider4.setVisibility(View.GONE);
                mTitleTags.setVisibility(View.GONE);
                mDivider6.setVisibility(View.VISIBLE);
            }


        } catch (Exception e) {
            Log.d("FULL", e.getMessage());
        }

        mPhotos = new ArrayList<>();

        multiplePhotoAdapter = new MultiplePhotoAdapter(FullProfile.this);

        try {
            if (mHasMultiple) {
                mPhotos.add(mDialog.getDialogPhoto());
                multiplePhotoAdapter.addPhotos(mPhotos);
                mViewPager.setAdapter(multiplePhotoAdapter);
                mSize = mDialog.getPhoto_count();

                new loadImages().execute();
                mViewPager.addOnPageChangeListener(viewListener);

            } else {
                mPhotos.add(mDialog.getDialogPhoto());
                multiplePhotoAdapter.addPhotos(mPhotos);
                mViewPager.setAdapter(multiplePhotoAdapter);
                mSize = 0;
//                addDotsIndicator(0);
            }


        } catch (Exception e) {
            Log.d("TESTING", e.getMessage());

        }


        sp = PreferenceManager.getDefaultSharedPreferences(this);
        isShowLikes = sp.getInt("show_my_likes", 1);

        apicaller = Builder.getClient().create(ApiCaller.class);
        boolean verified = sp.getBoolean("verified", false);


        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mActionReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    reportUser();
                } catch (Exception e) {
                    Log.d("TESTING", e.getMessage());
                }


            }
        });

        mVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhoneNumberVerify.open(FullProfile.this);
            }
        });


        try {

            Log.d("TESTING", "Date of birth" + dob);

            if (mDialog.getDob() != null) {
                try {
                    calculateAge(mDialog.getDob());
                } catch (Exception e) {
                    Log.d("TEST", e.getMessage());

                }

            } else {
                age = "";
            }

            if (mDialog.getVerfication_status() != null && mDialog.getVerfication_status() == 1) {
                mUerVerified.setVisibility(View.VISIBLE);
                mDivider3.setVisibility(View.VISIBLE);
                mDivider6.setVisibility(View.VISIBLE);
                mVerificationStatus.setVisibility(View.VISIBLE);
                if (!verified) {
                    mVerifyButton.setVisibility(View.VISIBLE);
                } else {
                    mVerifyButton.setVisibility(View.GONE);
                }
            } else {
                mUerVerified.setVisibility(View.GONE);
//                mDivider3.setVisibility(View.GONE);
                mDivider6.setVisibility(View.GONE);
                mVerificationStatus.setVisibility(View.GONE);
                mVerifyButton.setVisibility(View.GONE);
            }

            if (origin_page == Extras.ORIGIN_USERLIKES || origin_page == Extras.ORIGIN_MESSAGES) {
                mDividerCall.setVisibility(View.VISIBLE);
                mContainerCall.setVisibility(View.VISIBLE);
                mTxtCall.setText(getResources().getString(R.string.call_so_so, mDialog.getDialogName()));
            } else {
                mDividerCall.setVisibility(View.GONE);
                mDividerCall.setVisibility(View.GONE);
            }


            if (mDialog.getDialogName() != null) {

                if (age != null && !age.isEmpty()) {
                    mNameAge.setText(getResources().getString(R.string.about_details, mDialog.getDialogName(), ",", age));
                    mActionReport.setText(getResources().getString(R.string.report_txt, mDialog.getDialogName()));


                } else {
                    mNameAge.setText(getResources().getString(R.string.about_details, mDialog.getDialogName(), "", ""));
                    mActionReport.setText(getResources().getString(R.string.report_txt, mDialog.getDialogName()));
                }
            }


            if (mDialog.getBio() != null && !mDialog.getBio().isEmpty()) {
                mDivider1.setVisibility(View.VISIBLE);
                mBioContainer.setVisibility(View.VISIBLE);
                mTxtBio.setText(mDialog.getBio());
            } else {
                mDivider1.setVisibility(View.GONE);
                mBioContainer.setVisibility(View.GONE);
            }

            if (mDialog.getDistance() != null && Integer.valueOf(mDialog.getDistance()) > -1) {
                mDivider3.setVisibility(View.GONE);
                mLocationContainer.setVisibility(View.VISIBLE);
                if (mDialog.getDistance().contains(".")) {
                    Double distance = Double.parseDouble(mDialog.getDistance());
                    int y = (int) Math.round(distance);
                    if (y < 1) {
                        mTxtLocation.setText((getResources().getString(R.string.less_than_km_away)));
                    } else if (y > 100) {
                        mTxtLocation.setText((getResources().getString(R.string.more_than_km_away)));
                    } else {
                        mTxtLocation.setText(getResources().getString(R.string.km_away, String.valueOf(y)));
                    }

                } else {
                    int y = Integer.valueOf(mDialog.getDistance());
                    if (y < 1) {
                        mTxtLocation.setText(getResources().getString(R.string.less_than_km_away));
                    } else {
                        mTxtLocation.setText(getResources().getString(R.string.km_away, String.valueOf(y)));
                    }


                }

            } else {
                mDivider3.setVisibility(View.VISIBLE);
                mLocationContainer.setVisibility(View.GONE);
            }

            if (mDialog.getJob() != null) {
                mJobContainer.setVisibility(View.VISIBLE);
                mTxtxJob.setText(mDialog.getJob());
            } else {
                mJobContainer.setVisibility(View.GONE);
            }

            if (mDialog.getEducation() != null) {
                mEducationContainer.setVisibility(View.VISIBLE);
                mTxtEducation.setText(mDialog.getEducation());
            } else {
                mEducationContainer.setVisibility(View.GONE);
            }


            if (mDialog.getLikes_count() != null && mDialog.getLikes_count() > 0) {
                Log.d(TAG, "onCreate: count x" + mDialog.getLikes_count());
                mDivider2.setVisibility(View.VISIBLE);
                mLikesHolder.setVisibility(View.VISIBLE);
                mNumberOfLikes.setText(String.valueOf(mDialog.getLikes_count()));
            } else {
                Log.d(TAG, "onCreate: null");
                mDivider2.setVisibility(View.GONE);
                mLikesHolder.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            Log.d("TESTING", "exception: " + e.getLocalizedMessage());

        }


    }

    private void reportUser() {


        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle("What seems to be the issue");
        builder.setSingleChoiceItems(new String[]{"Inappropriate Images", "Inappropriate Messages", "Other"}, 1,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index) {
                        if (index == 2) {
                            dialogInterface.dismiss();
                            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(FullProfile.this);
                            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                            builder.setTitle("Other");
                            builder.setTextGravity(Gravity.CENTER_HORIZONTAL);
                            mEdit.setVisibility(View.VISIBLE);
                            builder.setFooterView(mEdit);
                            builder.addButton("Report", -1, getResources().getColor(R.color.colorAccent),
                                    CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            String text = mEdit.getText().toString();
                                            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

                                            ReportUserParams params = new ReportUserParams();
                                            params.setSuspectId(mDialog.getRecipient_id());
                                            params.setUserId(mDialog.getId());
                                            params.setRequestType("REPORT_USER");
                                            params.setReportType(text);

                                            Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.reportUser(params);
                                            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                                                @Override
                                                public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                                                    if (response.isSuccessful()) {
                                                        ke.co.tunda.ApiConnector.Models.Response resp = response.body();
                                                        if (resp.getBody().equals("SUCCESS")) {
                                                            AppUtils.showToast(getApplicationContext(), "Reported", false);
                                                        } else {
                                                            AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                                        }
                                                    } else {
                                                        AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                                                    AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);

                                                }
                                            });

                                        }
                                    });
                            if (mEdit.getParent() != null) {
                                ((ViewGroup) mEdit.getParent()).removeView(mEdit);
                            }
                            builder.show();

                        } else if (index == 0) {
                            report_type = "Inappropriate Images";
                        } else if (index == 1) {
                            report_type = "Inappropriate Messges";
                        }
                    }
                });
        builder.addButton("Report", -1, getResources().getColor(R.color.colorAccent),
                CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

                        ReportUserParams params = new ReportUserParams();
                        params.setSuspectId(mDialog.getRecipient_id());
                        params.setUserId(mDialog.getId());
                        params.setRequestType("REPORT_USER");
                        params.setReportType(report_type);

                        Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.reportUser(params);
                        call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                            @Override
                            public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                                if (response.isSuccessful()) {
                                    ke.co.tunda.ApiConnector.Models.Response resp = response.body();
                                    if (resp.getBody().equals("SUCCESS")) {
                                        AppUtils.showToast(getApplicationContext(), "Reported", false);
                                    } else {
                                        AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                    }
                                } else {
                                    AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                }
                            }

                            @Override
                            public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                                AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);

                            }
                        });
                    }
                });

        builder.show();


    }

    @Override
    public void onCodeReceived(String auth_code) {
        if (auth_code == null) {
            auth_dialog.dismiss();
            Toast.makeText(getApplicationContext(), "problem connecting", Toast.LENGTH_SHORT).show();
        } else {
            getAccessToken(auth_code);
        }
    }

    private class loadImages extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {
            MultiplePhotoRequest multiplePhotoRequest = new MultiplePhotoRequest();
            multiplePhotoRequest.setRequestType("GET_USER_PHOTOS");
            multiplePhotoRequest.setUserId(mDialog.getRecipient_id());
            Call<MultiPlePhotosResponse> call = apicaller.getUserPhotos(multiplePhotoRequest);
            call.enqueue(new Callback<MultiPlePhotosResponse>() {
                @Override
                public void onResponse(Call<MultiPlePhotosResponse> call, Response<MultiPlePhotosResponse> response) {
                    if (response.isSuccessful()) {
                        MultiPlePhotosResponse res = response.body();
                        if (res != null) {
                            if (res.getStatusCode().equals("00")) {
                                mPhotos.clear();
                                for (int i = 0; i < res.getBody().size(); i++) {

                                    if (!res.getBody().get(i).getIsProfile().equals("1")) {


                                        mPhotos.add(res.getBody().get(i).getPhotoPath());


                                    }

                                }
                                multiplePhotoAdapter.addPhotos(mPhotos);
                                addDotsIndicator(0);


                            } else if (res.getStatusCode().equals("99")) {
                                Toasty.warning(getApplicationContext(), "photos not loaded, shame on us", Toasty.LENGTH_SHORT).show();
                            }
                        } else {
                            Toasty.warning(getApplicationContext(), "photos not loaded, check internet connection", Toasty.LENGTH_SHORT).show();

                        }
                    } else {
                        Toasty.warning(getApplicationContext(), "photos not loaded, check internet connection", Toasty.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<MultiPlePhotosResponse> call, Throwable t) {
                    if (t.getMessage().contains("Failed to connect")) {
                        Toasty.warning(getApplicationContext(), "photos not loaded, check internet connection", Toasty.LENGTH_SHORT).show();

                    }


                }
            });
            return null;
        }
    }

    private void calculateAge(String mDob) {
        Calendar calendar = Calendar.getInstance();
        Log.d("TESTING", mDob);
        String DAY;

        String YEAR = mDob.substring(0, 4);
        Log.d("TESTING", "year=" + YEAR);
        String MONTH = mDob.substring(5, 7);
        if (MONTH.contains("-")) {
            MONTH = MONTH.replace("-", "");
            Log.d("TESTING", "month=" + MONTH);
            DAY = mDob.substring(7, mDob.length());
        } else {
            DAY = mDob.substring(8, mDob.length());
        }


        Calendar x = new GregorianCalendar(Integer.valueOf(YEAR), Integer.valueOf(MONTH), Integer.valueOf(DAY));

        Log.d("TESTING", YEAR + "/" + MONTH + "/" + DAY);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));

        Date date = x.getTime();

//        date.setYear();
//        date.setMonth();
//        date.setDate();

        Log.d("TESTING", String.valueOf(date) + ">>" + date.getYear());

        Date currentDate = new Date();


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(date));
        int d2 = Integer.parseInt(formatter.format(currentDate));
        int agex = (d2 - d1) / 10000;
        age = String.valueOf(agex);
        Log.d(TAG, "calculateAge: age= " + age);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        FullProfile.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    }


    public void addDotsIndicator(int position) {
        Log.d("TESTING", "addDotsIndicator: mSize=" + mSize + "position=" + position);

        mDots = new TextView[mSize];
        mDotLayout.removeAllViews();


        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(45);
            mDots[i].setTextColor(getResources().getColor(R.color.grey_500));
            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.colorPrimary));
        }


    }


    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
//            mButtonContainer.setBackground(multiplePhotoAdapter.getBackground(i));


        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
            Log.d("TEST", "onPageSelected: " + i);

            mButtonContainer.setBackground(multiplePhotoAdapter.getBackground(i));
//            setImages(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skip_button:
                didTapButton(mSkipButton);
                OnUserSwiped(1);
                break;
            case R.id.like_button:
                didTapButton(mLikeButton);
                OnUserSwiped(2);
                break;
            case R.id.super_like_button:
                didTapButton(mSuperLikeButton);
                OnUserSwiped(3);
                break;
            case R.id.container_call:
                if (mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
                    if (ActivityCompat.checkSelfPermission(FullProfile.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(FullProfile.this);
                        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                        builder.setTitle("Make call");
                        builder.setCancelable(true);
                        builder.setTextGravity(Gravity.CENTER);
                        builder.setMessage("Allow Tunda to make a call");
                        builder.addButton("Allow", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Dexter.withActivity(FullProfile.this)
                                                .withPermissions(Manifest.permission.CALL_PHONE)
                                                .withListener(new MultiplePermissionsListener() {
                                                    @Override
                                                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                                                        if (report.areAllPermissionsGranted()) {
                                                            dialog.dismiss();
                                                            openCall();
                                                        }

                                                        if (report.isAnyPermissionPermanentlyDenied()) {
                                                            dialog.dismiss();
                                                            AppUtils.showToast(FullProfile.this, "Tunda needs your permission to call", false);

                                                        }
                                                    }


                                                    @Override
                                                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                                        dialog.dismiss();
                                                        token.continuePermissionRequest();
                                                    }
                                                }).check();


                                    }
                                });

                        builder.show();
                    } else {
                        openCall();
                    }
                } else {
                    Toasty.warning(getApplicationContext(), "please Add and verify your phone number", Toasty.LENGTH_LONG).show();
                }
                break;
            case R.id.connect_to_ig:
                connectInstagram();
                break;

        }

    }

    private void OnUserSwiped(int i) {
        Log.d(TAG, "OnUserSwiped: " + i);
        if (mOrigin == Extras.ORIGIN_TOP_PICKS) {
            mCallbackTopPicks.executeSwipeAction(i, mPosition);
            onBackPressed();
            return;
        }

        if (mOrigin == Extras.ORIGIN_LIKES) {
            mCallbackLikes.executeSwipeAction(i, mPosition);
            onBackPressed();
            return;
        }

        if (mOrigin == Extras.ORIGIN_USERLIKES) {
            mCallbackUserLikesLikes.executeSwipeAction(i, mPosition);
            onBackPressed();
            return;
        }


        mCallback.executeSwipeAction(i, mPosition);
        onBackPressed();


    }

    public static void updateApi(UpdateFragment listener) {
        mCallback = listener;
    }

    public interface UpdateFragment {
        void executeSwipeAction(int position, int adapterPosition);
    }

    public interface UpdateTopPicks {
        void executeSwipeAction(int position, int adapterPosition);
    }

    public interface UpdateLikes {
        void executeSwipeAction(int position, int adapterPosition);
    }

    public interface UpdateUserLikes {
        void executeSwipeAction(int position, int adapterPosition);
    }

    public static void updateTopPicks(UpdateTopPicks listener) {
        mCallbackTopPicks = listener;
    }

    public static void updateLikes(UpdateLikes listener) {
        mCallbackLikes = listener;
    }

    public static void updateUserLikes(UpdateUserLikes listener) {
        mCallbackUserLikesLikes = listener;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppLock.onActivityResumed(this);
    }


    private class sendView extends AsyncTask<Void, Void, Void> {
        SendViewParams mSendViewParams;

        public sendView(SendViewParams sendViewParams) {
            this.mSendViewParams = sendViewParams;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<BaseResponse> call = apicaller.sendViews(mSendViewParams);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) {
                        BaseResponse base = response.body();
                        if (base != null && base.getStatusCode().equals("00")) {
                            Log.d(TAG, "onResponse: 00");

                        } else {
                            Log.d(TAG, "onResponse: 99");
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Log.d(TAG, "onResponse: " + t.getMessage());


                }
            });

            return null;
        }
    }

    public void didTapButton(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(FullProfile.this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        view.startAnimation(myAnim);
    }

    private void openCall() {


        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(FullProfile.this);
        builder.content("Connecting ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        RequestCallParams params = new RequestCallParams();
        params.setRequestType("REQUEST_CALL");
        params.setUserId(user_id);
        params.setmPhoneNumber(mPhoneNumber);
        params.setRecipient_id(Integer.valueOf(mDialog.getRecipient_id()));
        ApiCaller mx = Builder4.getClient().create(ApiCaller.class);

        Call<ke.co.tunda.ApiConnector.Models.Response> call = mx.sendCall(params);
        call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
            @Override
            public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                if (response.isSuccessful()) {
                    ke.co.tunda.ApiConnector.Models.Response response1 = response.body();
                    if (response1 != null && response1.getStatusCode() != null && response1.getStatusCode().equals("0")) {
                        dialog.dismiss();
                        Toasty.success(getApplicationContext(), response1.getBody(), Toasty.LENGTH_SHORT).show();


                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + "0900 620 470"));
                        FullProfile.this.startActivity(intent);


                    } else {
                        dialog.dismiss();
                        if (response1 != null) {
                            Toasty.normal(getApplicationContext(), response1.getBody(), Toasty.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    dialog.dismiss();
                    Toasty.error(getApplicationContext(), "Something went wrong, its our fault", Toasty.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                dialog.dismiss();
                Toasty.normal(getApplicationContext(), "Problem connecting. Ensure internet and try again", Toasty.LENGTH_SHORT).show();

            }
        });
    }


    /**
     * Instagram
     */

    private class getIgPhotos extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            GetIgPhotosRequest getIgPhotosRequest = new GetIgPhotosRequest();
            getIgPhotosRequest.setRequestType("GET_INSTAGRAM_PHOTOS");
            getIgPhotosRequest.setUserId(Integer.valueOf(mDialog.getRecipient_id()));
            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
            Call<GetIgPhotoResponse> getIgPhotoResponseCall = apiCaller.getIgPhotos(getIgPhotosRequest);
            getIgPhotoResponseCall.enqueue(new Callback<GetIgPhotoResponse>() {
                @Override
                public void onResponse(Call<GetIgPhotoResponse> call, Response<GetIgPhotoResponse> response) {
                    if (response.isSuccessful()) {
                        GetIgPhotoResponse getIgPhotoResponse = response.body();
                        if (getIgPhotoResponse != null && getIgPhotoResponse.getStatusCode().equals("00")) {
                            int mSizeServer = getIgPhotoResponse.getBody().size();
                            String mJsonUrls = getIgPhotoResponse.getBody().get(0).getPhotoPath();

                            try {
                                JSONArray jr = null;
                                String url = null;
                                jr = new JSONArray(mJsonUrls);

                                for (int i = 0; i < jr.length(); i++) {
                                    url = jr.getString(i);
                                    mPhotosIg.add(url);
                                }

                                if (mPhotosIg.size() > 0) {
                                    mSizeIg = mPhotosIg.size();
                                    mIgPhotosHolder.setVisibility(View.VISIBLE);

                                    if(!user_has_ig){
                                        mConnectHolder.setVisibility(View.VISIBLE);
                                    }else {
                                        mConnectHolder.setVisibility(View.GONE);
                                    }


                                    mIgPhotoCount.setText(getResources().getString(R.string.ig_photo_count, String.valueOf(mPhotosIg.size())));

                                    for (int n = 0; n < mPhotosIg.size(); n += limit) {
                                        int chunkSize = Math.min(mPhotosIg.size(), n + limit);
                                        List<String> chunk = mPhotosIg.subList(n, chunkSize);
                                        SectionsList sectionsList = new SectionsList();
                                        sectionsList.setPage(page++);
                                        sectionsList.setuRls(chunk);
                                        mList.add(sectionsList);

                                        multiplePhotoAdapterIg.addPhoto(mList,mDialog.getDialogName(),mDialog.getDialogPhoto());
                                        addDotsIndicatorIg(0);
                                        mList.clear();
                                    }

                                } else {
                                    mIgPhotosHolder.setVisibility(View.GONE);
                                    mConnectHolder.setVisibility(View.GONE);
                                }


                            } catch (Exception e) {
                                Log.d(TAG, "onResponse: " + e.getMessage());

                            }


                        } else {
                            if (getIgPhotoResponse != null) {
                                Log.d(TAG, "onResponse: " + getIgPhotoResponse.getStatusCode());
                                mIgPhotosHolder.setVisibility(View.GONE);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetIgPhotoResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    mIgPhotosHolder.setVisibility(View.GONE);

                }
            });
            return null;
        }
    }

    public void addDotsIndicatorIg(int position) {

        int dotSize = (int) Math.ceil(mSizeIg / limit);

        mDotsIg = new TextView[dotSize];
        mDotLayoutIg.removeAllViews();


        for (int i = 0; i < mDotsIg.length; i++) {
            mDotsIg[i] = new TextView(this);
            mDotsIg[i].setText(Html.fromHtml("&#8226"));
            mDotsIg[i].setTextSize(45);
            mDotsIg[i].setTextColor(Color.parseColor("#9E9E9E"));
            mDotLayoutIg.addView(mDotsIg[i]);
        }

        if (mDotsIg.length > 0) {
            mDotsIg[position].setTextColor(getResources().getColor(R.color.colorPrimary));
        }


    }

    private void connectInstagram() {
        auth_dialog = new AuthenticationDialog(FullProfile.this, this);
        auth_dialog.setCancelable(true);
        auth_dialog.show();

    }

    private void getAccessToken(String access_token) {
        pd = new ProgressDialog(FullProfile.this);
        pd.setMessage("please, wait ... ");
        pd.show();
        RequestBody app_id_body = RequestBody.create(MediaType.parse("text/plain"), Constants.CLIENT_ID);
        RequestBody app_secret_body = RequestBody.create(MediaType.parse("text/plain"), Constants.APP_SECRET);
        RequestBody grant_type_body = RequestBody.create(MediaType.parse("text/plain"), Constants.GRANT_TYPE);
        RequestBody redirect_uri_body = RequestBody.create(MediaType.parse("text/plain"), Constants.REDIRECT_URI);
        RequestBody access_token_body = RequestBody.create(MediaType.parse("text/plain"), access_token);
        Call<AccessTokenResponse> call = retrofitService.
                getAccessToken(app_id_body,
                        app_secret_body,
                        grant_type_body,
                        redirect_uri_body,
                        access_token_body);
        call.enqueue(new Callback<AccessTokenResponse>() {
            @Override
            public void onResponse(Call<AccessTokenResponse> call, Response<AccessTokenResponse> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    AccessTokenResponse accessTokenResponse = response.body();
                    if (accessTokenResponse != null) {
                        String user_id = accessTokenResponse.getUserId();
                        String access_token = accessTokenResponse.getAccessToken();
                        Log.d(TAG, "onResponse: user_id = " + user_id);
                        Log.d(TAG, "onResponse: access_token = " + access_token);
                        getUserMedia(user_id, access_token);

                    } else {
                        Log.d(TAG, "onResponse:accesstoken null  ");
                    }


                }
            }

            @Override
            public void onFailure(Call<AccessTokenResponse> call, Throwable t) {
                pd.dismiss();
                Log.d(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    private void getUserMedia(String user_id, final String access_token) {
        List<String> field = new ArrayList<>();
        field.add("id");
        field.add("caption");
        Call<MediaRepsonse> mediaRepsonseCall = retrofitServiceGraph.getUserMedia(android.text.TextUtils.join(",", field), access_token);
        mediaRepsonseCall.enqueue(new Callback<MediaRepsonse>() {
            @Override
            public void onResponse(Call<MediaRepsonse> call, Response<MediaRepsonse> response) {
                if (response.isSuccessful()) {
                    MediaRepsonse mediaRepsonse = response.body();
                    if (mediaRepsonse != null) {
                        data = mediaRepsonse.getData();
                        loadUserFeed(data, access_token);


                    }


                }

            }

            @Override
            public void onFailure(Call<MediaRepsonse> call, Throwable t) {

            }
        });


    }

    private void loadUserFeed(Datum[] data, String access_token) {
        mSizeIgUser = data.length;
        for (Datum aData : data) {
            new getMediaInfo(aData, access_token).execute();
        }
    }

    private class getMediaInfo extends AsyncTask<Void, Void, Void> {
        private Datum datum;
        private String access;
        List<String> fields;


        public getMediaInfo(Datum datum, String access_token) {
            this.datum = datum;
            this.access = access_token;
            fields = new ArrayList<>();
            ;
            fields.add("id");
            fields.add("media_type");
            fields.add("media_url");
            fields.add("username");
            fields.add("timestamp");

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<PhotoResponse> call = retrofitServiceGraph.getMediaInfo(datum.getId(), android.text.TextUtils.join(",", fields),
                    access);
            call.enqueue(new Callback<PhotoResponse>() {
                @Override
                public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {
                    if (response.isSuccessful()) {
                        PhotoResponse photoResponse = response.body();
                        if (photoResponse != null) {


                            mPhotosIgUser.add(photoResponse.getMediaUrl() + "~~" + photoResponse.getTimestamp());
                            if (mPhotosIgUser.size() == mSizeIgUser) {

                                new sendIgPhotos(mPhotosIgUser).execute();

                            }

//

                        }


                    }
                }

                @Override
                public void onFailure(Call<PhotoResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });
            return null;
        }
    }

    private class sendIgPhotos extends AsyncTask<Void, Void, Void> {
        private ArrayList<String> photoList;

        public sendIgPhotos(ArrayList<String> mPhotos) {
            this.photoList = mPhotos;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            SendIgPhotosRequest sendIgPhotosRequest = new SendIgPhotosRequest();
            sendIgPhotosRequest.setUserId(user_id);
            sendIgPhotosRequest.setRequestType("ADD_INSTAGRAM_PHOTOS");
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            jsonStringPhootos = gson.toJson(photoList);
            sendIgPhotosRequest.setPhotoPath(jsonStringPhootos);
            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

            Call<BaseResponse> call = apiCaller.sendIgPhotos(sendIgPhotosRequest);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) {
                        BaseResponse baseResponse = response.body();
                        if (baseResponse != null && baseResponse.getStatusMessage() != null && baseResponse.getStatusMessage().equals("SUCCESS")) {
                            Log.d(TAG, "onResponse: photos successfully added");
                            Toasty.success(FullProfile.this,"linked to instagram",Toasty.LENGTH_SHORT).show();
                            editor.putBoolean("has_ig_photos", true);
                            editor.commit();

                            mConnectHolder.setVisibility(View.GONE);

                        } else {
                            if (baseResponse != null) {
                                Toasty.error(FullProfile.this,"failed linking to instagram",Toasty.LENGTH_SHORT).show();

                                Log.d(TAG, "onResponse: " + baseResponse.getStatusMessage());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });

            return null;
        }
    }

    ViewPager.OnPageChangeListener viewListenerIg = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {


        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicatorIg(i);

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}
