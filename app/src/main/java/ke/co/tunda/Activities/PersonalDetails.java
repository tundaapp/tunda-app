

package ke.co.tunda.Activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalDetails extends AppCompatActivity implements View.OnClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.personal_details_card)
    CardView personal_details_card;

    @BindView(R.id.edittxt_name)
    EditText mEditTextName;

    @BindView(R.id.edittxt_age)
    EditText mEditTextAge;

    @BindView(R.id.radio_male)
    RadioButton mMale;
    @BindView(R.id.radio_female)
    RadioButton mFemale;
    @BindView(R.id.radio_group)
    RadioGroup mRdGroup;
    @BindView(R.id.btn_save)
    Button mSave;

    private ApiCaller apicaller;
    private String mNames;
    private DatePickerDialog datePickerDialog;
    UpdateUserProfileRequest updateUserProfileRequest;

    private static String mFirst_name, mDob, mUserId, mGender;

    private boolean isEdited = false;
    private boolean isNameEdited = false;
    private boolean iSAgeEdited = false;
    private boolean isGenderEdited = false;


    public static void open(Context context, String first_name, String dob, String user_id, String gender) {
        context.startActivity(new Intent(context, PersonalDetails.class));
        mFirst_name = first_name;
        mDob = dob;
        mUserId = user_id;
        mGender = gender;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);
        ButterKnife.bind(this);
        mBack.setOnClickListener(this);
        mEditTextName.setText(mFirst_name);

        updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setUserId(mUserId);
        updateUserProfileRequest.setRequestType("UPDATE_USER_PROFILE");

        if (!mDob.isEmpty()) {
            try {
                calculateAge(mDob);
            } catch (Exception e) {

            }

        } else {
            mEditTextAge.setText("Add your Age");

        }


        if (!mGender.isEmpty()) {
            if (mGender.equals("FEMALE")) {
                mFemale.setChecked(true);
            } else {
                mMale.setChecked(true);
            }

        }


        apicaller = Builder.getClient().create(ApiCaller.class);


        mEditTextName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEdited = true;
                isNameEdited = true;
                mSave.setEnabled(true);

            }
        });

        mEditTextName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                isEdited = true;
                isNameEdited = true;
                mSave.setEnabled(true);
            }
        });


        mEditTextAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSave.setEnabled(true);
                isEdited = true;
                iSAgeEdited = true;
                openCalender();
            }
        });

        mMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isEdited = true;
                mSave.setEnabled(true);
                isGenderEdited = true;
                if (isChecked) {
                    mGender = "MALE";
                } else {
                    mGender = "FEMALE";
                }

                updateUserProfileRequest.setGender(mGender);

            }

        });

        mFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                isEdited = true;
                isGenderEdited = true;
                mSave.setEnabled(true);
                if (isChecked) {
                    mGender = "FEMALE";
                } else {
                    mGender = "MALE";
                }

                updateUserProfileRequest.setGender(mGender);

            }
        });


        if (isEdited) {
            mSave.setEnabled(true);
        } else {
            mSave.setEnabled(false);
        }

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateChanges();


            }
        });


    }


    private int calculateAge(String mDob) {
        Calendar calendar = Calendar.getInstance();
        Log.d("TESTING", mDob);
        String DAY;

        String YEAR = mDob.substring(0, 4);
        Log.d("TESTING", "year=" + YEAR);
        String MONTH = mDob.substring(5, 7);
        if (MONTH.contains("-")) {
            MONTH = MONTH.replace("-", "");
            Log.d("TESTING", "month=" + MONTH);
            DAY = mDob.substring(7, mDob.length());
        } else {
            DAY = mDob.substring(8, mDob.length());
        }


        Calendar x = new GregorianCalendar(Integer.valueOf(YEAR), Integer.valueOf(MONTH), Integer.valueOf(DAY));

        Log.d("TESTING", YEAR + "/" + MONTH + "/" + DAY);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));

        Date date = x.getTime();

//        date.setYear();
//        date.setMonth();
//        date.setDate();

        Log.d("TESTING", String.valueOf(date) + ">>" + date.getYear());

        Date currentDate = new Date();


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(date));
        int d2 = Integer.parseInt(formatter.format(currentDate));
        int age = (d2 - d1) / 10000;
        mEditTextAge.setText(String.valueOf(age));
        mEditTextAge.setError(null);
        return age;


    }

    @Override
    public void onBackPressed() {
        if (isEdited) {
            updateChanges();
        } else {
            super.onBackPressed();
        }


    }

    private void updateChanges() {
        if (isNameEdited) {
            if (!mEditTextName.getText().toString().isEmpty() && mEditTextName.getText().length() >= 3) {
//                    hideSoftKeyboard(mEditTextName);
                updateUserProfileRequest.setFirstName(mEditTextName.getText().toString());
                updateUserProfileRequest.setLastName("");


            } else {
                Toasty.error(getApplicationContext(), "Invalid Name").show();

            }
        }

        if (iSAgeEdited) {
            if (mEditTextAge.getText() != null) {
                updateUserProfileRequest.setDob(mDob);

            }
        }

        if (isGenderEdited) {
            updateUserProfileRequest.setGender(mGender);

        }


        uploadChanges(updateUserProfileRequest);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;


        }

    }

    private void saveToSp(String key, String s) {
        SharedPreferences.Editor editor;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        editor.putString(key, s);
        editor.apply();
    }

    private void uploadChanges(UpdateUserProfileRequest string) {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(PersonalDetails.this);
        builder.content("Updating ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        mNames = mEditTextName.getText().toString();

        Call<UpdateUserProfileResponse> call = apicaller.updateUser(string);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse response1 = response.body();
                    if (response1 != null) {
                        if (response1.getBody().equals("SUCCESS")) {
                            dialog.dismiss();

                            if (iSAgeEdited) {
                                saveToSp("dob", mDob);
                            }

                            if (isGenderEdited) {
                                saveToSp("gender", mGender);
                            }

                            if (isNameEdited) {
                                saveToSp("first_name", mEditTextName.getText().toString());
                            }


                            Toasty.success(getApplicationContext(), "Updated").show();
                            finish();


                        } else {
                            dialog.dismiss();
                            Toasty.success(getApplicationContext(), "Updated").show();
                            finish();

                        }
                    }

                } else {
                    Toasty.warning(getApplicationContext(), "Not updated. Something went Wrong, Try Again").show();
                    dialog.dismiss();
                    finish();


                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable throwable) {
                dialog.dismiss();
                Toasty.warning(getApplicationContext(), "Not updated. Something went Wrong, Try Again").show();
                finish();


            }
        });


    }

    public void showKeyboard(EditText ettext) {
        Log.d("TESTING", "showKeyboard: ");

        ettext.setClickable(true);
        ettext.setFocusable(true);
        ettext.setFocusableInTouchMode(true);
        ettext.requestFocus();
        ettext.setSelection(ettext.getText().length());


        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 100);
    }


    private void hideSoftKeyboard(EditText ettext) {
        ettext.clearFocus();
        ettext.setFocusable(false);
        ettext.setFocusableInTouchMode(false);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }


    private void openCalender() {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = null;
        dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                PersonalDetails.this, 2000, 01, 01);
        dpd.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
        dpd.setOnDateSetListener(this);
        dpd.setTitle("Pick Date of Birth");
        dpd.setScrollOrientation(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.ScrollOrientation.HORIZONTAL);

        dpd.showYearPickerFirst(true);
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");


    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        Date chosenDate = cal.getTime();

        DateFormat df_medium_us = DateFormat.getDateInstance(DateFormat.SHORT);
        String _date_of_birth = df_medium_us.format(chosenDate);
        String _year = String.valueOf(year);
        int month_selected = monthOfYear + 1;
        String _month = String.valueOf(month_selected);
        if (_month.length() == 1) {
            _month = "0" + _month;
        }
        String _day = String.valueOf(dayOfMonth);
        if (_day.length() == 1) {
            _day = "0" + _day;
        }
        mDob = _year + "-" + _month + "-" + _day;
        Log.d("TESTING", "onDateSet: " + mDob);
        int ageOfUser = calculateAge(mDob);

        if (ageOfUser < 18) {
            mEditTextAge.setError("You have to be over 18");
            Toasty.error(PersonalDetails.this, "Check Date! You have to be over 18", Toasty.LENGTH_LONG).show();
        }

    }
}
