

package ke.co.tunda.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.guardanis.applock.AppLock;
import com.guardanis.applock.dialogs.LockCreationDialogBuilder;
import com.guardanis.applock.dialogs.UnlockDialogBuilder;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;
import com.suke.widget.SwitchButton;
import com.wang.avi.AVLoadingIndicatorView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Models.ActivateBoostParams;
import ke.co.tunda.Models.BoostStatusParams;
import ke.co.tunda.Models.BoostStatusResp;
import ke.co.tunda.Models.DefaultResp;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.fragments.Dialogs.TundaBoostsBuy;
import ke.co.tunda.fragments.Dialogs.TundaGoldBuy;
import ke.co.tunda.fragments.Dialogs.TundaPlusBuy;
import ke.co.tunda.fragments.Dialogs.TundaSuperLikesBuy;
import ke.co.tunda.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity implements
        View.OnClickListener {

    private static final String TAG = "3422";
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    View view;
    private boolean isFromOther = false;
    private int boostCap = 0;
    private boolean toActivate = false;
    private boolean toBuy = false;
    private boolean isOnline = false;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.phone_no)
    TextView mPhone_no;
    @BindView(R.id.account_card)
    CardView mAccountCard;
    @BindView(R.id.mVerified)
    TextView mVerified;
    @BindView(R.id.discovery_card)
    CardView mDiscoveryCard;
    @BindView(R.id.my_location)
    TextView mLocation;
    @BindView(R.id.switch_men)
    SwitchButton mSwitchMen;
    @BindView(R.id.switch_women)
    SwitchButton mSwitchWomen;
    @BindView(R.id.interest_not_set)
    TextView mNoInterest;
    @BindView(R.id.show_interest_results)
    TextView mShowInterstResults;
    @BindView(R.id.seekbar_age)
    RangeSeekBar mSeekAge;
    @BindView(R.id.txtAge_from)
    TextView mAgeFrom;
    @BindView(R.id.txtAge_to)
    TextView mAgeTo;
    @BindView(R.id.seekbar_distance)
    RangeSeekBar mSeekDistance;
    @BindView(R.id.txtAge_plus)
    TextView mAgePlus;
    @BindView(R.id.distance_range_results)
    TextView mDistanceRangeResults;
    @BindView(R.id.switch_visibility)
    Switch mSwitchVisibility;
    @BindView(R.id.tunda_boosts_card)
    CardView mTundaBoosts;
    @BindView(R.id.tunda_superLikes_card)
    CardView mTundaSuperlikes;
    @BindView(R.id.signout_card)
    CardView mSignOut;
    @BindView(R.id.deleteAccount_card)
    CardView mDeleteAccount;
    @BindView(R.id.privacy_card)
    CardView mPrivacy;
    @BindView(R.id.current_location)
    TextView mCurrentLocation;
    int selectedMode;
    @BindView(R.id.tunda_plus_card)
    CardView mTundaPlusCard;
    @BindView(R.id.tunda_gold_card)
    CardView mTundaGoldCard;
    @BindView(R.id.boost_status_txt)
    HtmlTextView mHtmlTextView_boost_status;
    @BindView(R.id.boost_status_action)
    HtmlTextView mBoostStatusAction;
    @BindView(R.id.boost_console_card)
    CardView mBoostConsole;
    @BindView(R.id.boost_status_txt1)
    HtmlTextView mHtmlTextView_boost_status1;
    @BindView(R.id.boost_status_action1)
    HtmlTextView mBoostStatusAction1;
    @BindView(R.id.boost_console_card1)
    CardView mBoostConsole1;
    @BindView(R.id.container_boost_console1)
    RelativeLayout mContainerBoostConsole1;

    @BindView(R.id.container_boost_console)
    RelativeLayout mContainerBoostConsole;
    @BindView(R.id.passcode_card)
    CardView mPasscodeCard;
    @BindView(R.id.passcode_status)
    TextView mPasscodeStatus;
    @BindView(R.id.share_to_earn)
    Button mShareToEarn;
    @BindView(R.id.div11)
    View mDiv11;
//    @BindView(R.id.share_desc)
//    TextView mShareAppDesc;
    @BindView(R.id.switch_show_likes)
    Switch mShowLikes;
    @BindView(R.id.views_holder)
    LinearLayout mViewholder;
    @BindView(R.id.number_of_views)
    TextView mNumberOfViews;
    @BindView(R.id.views_holder1)
    LinearLayout mViewholder1;
    @BindView(R.id.number_of_views1)
    TextView mNumberOfViews1;


    private String mPhoneNumber;
    private boolean verified;
    private String address, locality, user_id;
    int user_id_int;
    int package_id;
    private String mInterest;
    private String mGender;
    private boolean edited = false;
    private boolean editedInterest = false;
    private boolean editedAge = false;
    private boolean editedDistance = false;
    private boolean editedVisibility = false;
    private String isVisible;
    private int isShowLikes;
    private String selectedInterest;
    private ApiCaller apicaller;
    private UpdateUserProfileRequest updateUserProfileRequest;
    private int minAge, maxAge, mDistance;
    int amt = 0;
    private int wallet_balance;
    private BoostStatusResp mStatusResp;
    private ActivateBoostParams activateBoostParams;
    private BoostStatusParams params;
//    private String share_url;
    private boolean editedShowLikes = false;


    public static void open(Context context) {
        context.startActivity(new Intent(context, SettingsActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
        if (AppLock.isEnrolled(getApplicationContext())) {
            mPasscodeStatus.setText(getString(R.string.passcode_enabled));
        }
        mBack.setOnClickListener(this);
        mAccountCard.setOnClickListener(this);
        mDiscoveryCard.setOnClickListener(this);
        mTundaGoldCard.setOnClickListener(this);
        mTundaPlusCard.setOnClickListener(this);
        mTundaSuperlikes.setOnClickListener(this);
        mTundaBoosts.setOnClickListener(this);
        mSignOut.setOnClickListener(this);
        mDeleteAccount.setOnClickListener(this);
        mPrivacy.setOnClickListener(this);
        mBoostConsole.setOnClickListener(this);
        mBoostConsole1.setOnClickListener(this);
        mPasscodeCard.setOnClickListener(this);
        mShareToEarn.setOnClickListener(this);


        mContainerBoostConsole1.setOnClickListener(this);
        mBoostStatusAction1.setOnClickListener(this);
        mHtmlTextView_boost_status1.setOnClickListener(this);

        mContainerBoostConsole.setOnClickListener(this);
        mBoostStatusAction.setOnClickListener(this);
        mHtmlTextView_boost_status.setOnClickListener(this);


        sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        apicaller = Builder.getClient().create(ApiCaller.class);
        updateUserProfileRequest = new UpdateUserProfileRequest();

        mPhoneNumber = sp.getString("phone_number", "");
        verified = sp.getBoolean("verified", false);
        package_id = sp.getInt("package_id", 1);
//        share_url = sp.getString("share_url", "");
        address = sp.getString("address", "");
        locality = sp.getString("locality", "");
        user_id_int = sp.getInt("user_id", 0);
        user_id = String.valueOf(user_id_int);
        Log.d("TESTING", "user_id>>>" + user_id_int);
        mInterest = sp.getString("gender_of_interest", "");
        mGender = sp.getString("gender", "");
        minAge = sp.getInt("age_from", 0);
        maxAge = sp.getInt("age_to", 0);
        mDistance = sp.getInt("distance", 0);
        isVisible = sp.getString("user_status", "ACTIVE");
        isShowLikes = sp.getInt("show_my_likes", 1);
        String image = sp.getString("image", "");
        Log.d(TAG, "onCreate: " + isShowLikes);


        try {


            params = new BoostStatusParams();
            params.setRequetType(Extras.BOOST_STATUS_REG_TYPE);
            params.setUserId(user_id_int);

            try {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        getFromSp();

                    }
                });
            } catch (Exception e) {
                Log.d(TAG, "onCreate: " + e.getMessage());
            }


            if (package_id == 3) {
                mTundaGoldCard.setVisibility(View.GONE);
                mTundaPlusCard.setVisibility(View.GONE);
            }

            if (package_id == 2) {
                mTundaPlusCard.setVisibility(View.GONE);

            }

            if (package_id == 1) {
                Preferences.get(SettingsActivity.this).setLocationAsSet(false);
                mBoostConsole.setVisibility(View.GONE);
                mTundaPlusCard.setVisibility(View.VISIBLE);
                mTundaGoldCard.setVisibility(View.VISIBLE);
            }

            new checkBoostStatus(params, apicaller).execute();

            if (isVisible.equals("INVISIBLE")) {
                mSwitchVisibility.setChecked(false);
            } else {
                mSwitchVisibility.setChecked(true);
            }


            if (isShowLikes == 1) {
                mShowLikes.setChecked(true);
            } else {
                mShowLikes.setChecked(false);
            }


            if (mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
                mPhone_no.setText(mPhoneNumber);

            } else {
                mPhone_no.setText("Add Phone Number");
            }

            if (verified) {
                mVerified.setText("Verified");
                mVerified.setTextColor(getResources().getColor(R.color.green));
            } else {
                mVerified.setText("UnVerified");
                mVerified.setTextColor(getResources().getColor(R.color.red));
            }


            if (Preferences.get(SettingsActivity.this).isLocationSet()) {
                if (!address.isEmpty()) {
                    mCurrentLocation.setText(address);
                } else {

                    mCurrentLocation.setText("Add location");

                }

            } else {
                if (!address.isEmpty()) {
                    mCurrentLocation.setText("My Current Location");
                } else {
                    mCurrentLocation.setText("Add adress");

                }

            }


            if (!mInterest.isEmpty()) {
                mNoInterest.setVisibility(View.GONE);
                if (mInterest.equals("MALE")) {
                    mShowInterstResults.setText("MEN");
                    mSwitchMen.setChecked(true);
                    mSwitchWomen.setChecked(false);
                }

                if (mInterest.equals("FEMALE")) {
                    mShowInterstResults.setText("WOMEN");
                    mSwitchWomen.setChecked(true);
                    mSwitchMen.setChecked(false);
                }

                if (mInterest.equals("BOTH")) {
                    mShowInterstResults.setText("BOTH");
                    mSwitchWomen.setChecked(true);
                    mSwitchMen.setChecked(true);
                }
            } else {
                mNoInterest.setVisibility(View.VISIBLE);
                mShowInterstResults.setText("BOTH");
                mSwitchWomen.setChecked(true);
                mSwitchMen.setChecked(true);


            }


            mSwitchMen.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                    edited = true;
                    editedInterest = true;
                    mNoInterest.setVisibility(View.GONE);
                    if (isChecked) {
                        if (mSwitchWomen.isChecked()) {
                            mShowInterstResults.setText("BOTH");
                            selectedInterest = "BOTH";
                        } else {
                            mShowInterstResults.setText("MEN");
                            edited = true;
                            selectedInterest = "MALE";
                        }

                    } else {
                        mSwitchMen.setChecked(false);
                        if (!mSwitchWomen.isChecked()) {
                            mSwitchWomen.setChecked(true);
                            selectedInterest = "FEMALE";
                        } else {
                            mShowInterstResults.setText("WOMEN");
                            selectedInterest = "FEMALE";
                        }

                    }

                }
            });


            mSwitchWomen.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                    edited = true;
                    editedInterest = true;
                    mNoInterest.setVisibility(View.GONE);
                    if (isChecked) {
                        if (mSwitchMen.isChecked()) {
                            mShowInterstResults.setText("BOTH");
                            selectedInterest = "BOTH";
                        } else {
                            mShowInterstResults.setText("WOMEN");
                            edited = true;
                            selectedInterest = "FEMALE";
                        }

                    } else {
                        mSwitchWomen.setChecked(false);
                        if (!mSwitchMen.isChecked()) {
                            mSwitchMen.setChecked(true);
                            selectedInterest = "MALE";
                        } else {
                            mShowInterstResults.setText("MEN");
                            selectedInterest = "MALE";
                        }
                    }

                }
            });


            if (minAge != 0 && maxAge != 0) {
                float min = minAge;
                float max = maxAge;
                mSeekAge.setProgress(min, max);
                mAgeFrom.setText(String.valueOf(minAge));
                mAgeTo.setText(String.valueOf(maxAge));
                if (maxAge >= 55) {
                    mAgePlus.setVisibility(View.VISIBLE);
                }

            } else {
                mSeekAge.setRange((float) 18, (float) 55);
                mAgeFrom.setText(String.valueOf(18));
                mAgeTo.setText(String.valueOf(55));
                mAgePlus.setVisibility(View.VISIBLE);


            }

            if (mDistance != 0) {
                float dist = mDistance;
                mSeekDistance.setProgress(dist);
                mDistanceRangeResults.setText(String.valueOf(mDistance) + " Km");

            } else {
                mSeekDistance.setProgress((float) 150);
                mDistanceRangeResults.setText(String.valueOf(150) + " Km");
            }


        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }


        mSwitchVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (package_id != 1) {
                    edited = true;
                    editedVisibility = true;
                    if (isChecked) {
                        isVisible = "ACTIVE";
                    } else {
                        isVisible = "INVISIBLE";
                    }
                } else {
                    TundaPlusBuy.open(SettingsActivity.this);
                    buttonView.setChecked(true);
                }
            }
        });


        mShowLikes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                edited = true;
                editedShowLikes = true;
                if (isChecked) {
                    isShowLikes = 1;
                } else {
                    isShowLikes = 0;
                }
            }
        });


        mSeekAge.setOnRangeChangedListener(new OnRangeChangedListener() {

            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                edited = true;
                editedAge = true;
                minAge = (int) leftValue;
                mAgeFrom.setText(String.valueOf(minAge));

                maxAge = (int) rightValue;
                if (maxAge >= 55) {
                    mAgePlus.setVisibility(View.VISIBLE);
                } else {
                    mAgePlus.setVisibility(View.GONE);
                }
                mAgeTo.setText(String.valueOf(maxAge));

            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });


        mSeekDistance.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                edited = true;
                editedDistance = true;
                mDistance = (int) leftValue;
                mDistanceRangeResults.setText(String.valueOf(mDistance) + " Km");
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });


    }

    private void getFromSp() {
        if (package_id >= 1) {

            if (package_id == 3) {

                mShareToEarn.setVisibility(View.VISIBLE);
                mDiv11.setVisibility(View.VISIBLE);
//                mShareAppDesc.setVisibility(View.VISIBLE);

            } else if (package_id == 2) {
                mShareToEarn.setVisibility(View.VISIBLE);
                mDiv11.setVisibility(View.VISIBLE);
//                mShareAppDesc.setVisibility(View.VISIBLE);

            } else if (package_id == 1) {
                mShareToEarn.setVisibility(View.VISIBLE);
                mDiv11.setVisibility(View.VISIBLE);
//                mShareAppDesc.setVisibility(View.VISIBLE);
            }


        }
    }


    @Override
    public void onBackPressed() {
        if (edited) {
            if (editedInterest) {
                updateUserProfileRequest.setGenderOfInterest(selectedInterest);
            }

            if (editedAge) {
                updateUserProfileRequest.setMinAge(String.valueOf(minAge));
                updateUserProfileRequest.setMaxAge(String.valueOf(maxAge));
            }

            if (editedDistance) {
                updateUserProfileRequest.setDistance(String.valueOf(mDistance));
            }

            if (editedVisibility) {
                updateUserProfileRequest.setUserStatus(isVisible);
            }

            if (editedShowLikes) {
                updateUserProfileRequest.setShow_my_likes(isShowLikes);
            }

            updateChanges();
        } else {
            super.onBackPressed();
        }


    }

    private void updateChanges() {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(SettingsActivity.this);
        builder.content("Updating Settings ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();

        updateUserProfileRequest.setRequestType("UPDATE_DISCOVERY_SETTINGS");
        updateUserProfileRequest.setUserId(user_id);

        Call<UpdateUserProfileResponse> call = apicaller.updateUser(updateUserProfileRequest);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse upr = response.body();
                    if (upr != null && upr.getBody().equals("SUCCESS")) {
                        Toasty.success(getApplicationContext(), "updated", Toasty.LENGTH_SHORT).show();
                        dialog.dismiss();
                        if (editedInterest) {
                            Preferences.get(SettingsActivity.this).setDiscoveryChanged(true);
                            saveInterest("gender_of_interest", selectedInterest);
                        }

                        if (editedAge) {
                            saveints("age_from", minAge);
                            saveints("age_to", maxAge);
                        }


                        if (editedDistance) {
                            saveints("distance", mDistance);
                        }

                        if (editedVisibility) {
                            saveInterest("user_status", isVisible);
                        }

                        if (editedShowLikes) {
                            saveints("show_my_likes", isShowLikes);
                        }

                        finish();


                    } else {
                        Preferences.get(SettingsActivity.this).setDiscoveryChanged(false);
                        dialog.dismiss();
                        AppUtils.showToast(getApplicationContext(), "Something went wrong, try again", false);
                        finish();
                    }
                } else {
                    Preferences.get(SettingsActivity.this).setDiscoveryChanged(false);
                    dialog.dismiss();
                    AppUtils.showToast(getApplicationContext(), "Something went wrong, try again", false);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable t) {
                Preferences.get(SettingsActivity.this).setDiscoveryChanged(false);
                dialog.dismiss();
                AppUtils.showToast(getApplicationContext(), "Something went wrong, try again", false);
                finish();
            }
        });


    }

    private void saveInterest(String interest, String selectedInterest) {
        SharedPreferences.Editor editor;
        editor = sp.edit();
        editor.putString(interest, selectedInterest);
        editor.apply();
//        finish();
    }

    private void saveints(String key, int value) {
        SharedPreferences.Editor editor;
        editor = sp.edit();
        editor.putInt(key, value);
        editor.apply();
//        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;
            case R.id.account_card:
                PhoneNumberVerify.open(SettingsActivity.this);
                break;
            case R.id.discovery_card:
                if (package_id == 1) {
                    TundaPlusBuy.open(SettingsActivity.this);
                    if (Preferences.get(SettingsActivity.this).isLocationSet()) {
                        Preferences.get(SettingsActivity.this).setLocationAsSet(false);
                    }
                } else {
                    changeLocation();
                }
                break;

            case R.id.tunda_gold_card:
                TundaGoldBuy.open(SettingsActivity.this);
                break;
            case R.id.tunda_plus_card:
                TundaPlusBuy.open(SettingsActivity.this);
                break;
            case R.id.tunda_boosts_card:
                TundaBoostsBuy.open(SettingsActivity.this);
                break;
            case R.id.tunda_superLikes_card:
                TundaSuperLikesBuy.open(SettingsActivity.this);
                break;
            case R.id.signout_card:
                signOutUser();
                break;
            case R.id.deleteAccount_card:
                deleteUser();
                break;
            case R.id.privacy_card:
                loadPrivacy();
                break;
            case R.id.boost_console_card1:
                activateBoosts();
                break;
            case R.id.boost_console_card:
                activateBoosts();
                break;
            case R.id.container_boost_console:
                activateBoosts();
                break;
            case R.id.boost_status_txt:
                activateBoosts();
                break;
            case R.id.container_boost_console1:
                activateBoosts();
                break;
            case R.id.boost_status_txt1:
                activateBoosts();
                break;
            case R.id.boost_status_action:
                activateBoosts();
                break;
            case R.id.boost_status_action1:
                activateBoosts();
                break;
            case R.id.passcode_card:
                if (AppLock.isEnrolled(getApplicationContext())) {
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
                    builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                    builder.setTitle("Remove Passcode Lock");
                    builder.setTextGravity(Gravity.CENTER);
                    builder.setMessage("No passcode will be needed to open your Tunda app. Continue?");
                    builder.setCancelable(false);
                    builder.addButton("Yes", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                            CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    new UnlockDialogBuilder(SettingsActivity.this)
                                            .onUnlocked(() -> {
                                                AppLock.getInstance(getApplicationContext()).invalidateEnrollments();
                                            })
                                            .onCanceled(() -> {
                                                Toasty.error(getApplicationContext(), "Enter your Pin").show();
                                            })
                                            .showIfRequiredOrSuccess(TimeUnit.SECONDS.toMillis(15));
                                    mPasscodeStatus.setText("Disabled");


                                }
                            }).addButton("No", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                            CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                }
                            });

                    builder.show();

                } else {
                    if (package_id >= 2) {
                        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
                        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                        builder.setTitle("Tunda Passcode Lock");
                        builder.setTextGravity(Gravity.CENTER);
                        builder.setCancelable(true);
                        builder.setMessage("If you enable Passcode Lock, you will be required to enter your passcode whenever opening" +
                                "Tunda. If you forget your passcode, you will have to re-install Tunda. Please be sure to choose a passcode that you will " +
                                "remember. The passcode is stored locally on your device and Tunda cannot reset it");
                        builder.setCancelable(false);
                        builder.addButton("Continue", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        new LockCreationDialogBuilder(SettingsActivity.this)
                                                .onCanceled(() -> {
                                                    showIndicatorMessage("You canceled...");
                                                })
                                                .onLockCreated(() -> {
                                                    showIndicatorMessage("Lock created!");
                                                    mPasscodeStatus.setText("Enabled");

                                                })
                                                .show();


                                    }
                                }).addButton("Cancel", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                });

                        builder.show();
                    } else {
                        TundaPlusBuy.open(SettingsActivity.this);
                    }


                }

                break;

            case R.id.share_to_earn:
                shareApp();
                break;


        }
    }

    private void shareApp() {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_body, getResources().getString(R.string.app_name)));
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.email_client)));
    }

    private void showIndicatorMessage(String s) {
        Toasty.success(getApplicationContext(), s).show();
    }


    private void activateBoosts() {
        if (!isOnline) {
            if (toBuy) {
                TundaBoostsBuy.open(SettingsActivity.this);
            } else if (toActivate) {
                if (boostCap >= 1) {
                    activateBoostParams = new ActivateBoostParams();
                    loadActivateConsole(activateBoostParams);
                } else {
                    Toasty.normal(SettingsActivity.this, "You have no boosts", Toasty.LENGTH_SHORT).show();

                }


            }
        } else {
            Toasty.normal(SettingsActivity.this, "You are currently on boost", Toasty.LENGTH_SHORT).show();
        }


    }

    private void loadActivateConsole(ActivateBoostParams activateBoostParams) {
        CFAlertDialog.Builder builderMpesa = new CFAlertDialog.Builder(SettingsActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.dialog_boost_profile, null);
        builderMpesa.setHeaderView(view);
        builderMpesa.setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET);
        RelativeLayout mLoader = view.findViewById(R.id.circular_progress);


        EditText mBoostsHrs = view.findViewById(R.id.boost_hrs);
        AVLoadingIndicatorView loader = view.findViewById(R.id.newton_cradle_loading);
        mBoostsHrs.setHint(getResources().getString(R.string.enter_boost_time_in_hours, String.valueOf(boostCap)));


        builderMpesa.addButton("Go", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (!TextUtils.isEmpty(mBoostsHrs.getText())) {

                            String boost_hrs = mBoostsHrs.getText().toString();
                            int int_boost_hrs = Integer.valueOf(boost_hrs);
                            if (int_boost_hrs >= 1) {
                                if (int_boost_hrs <= boostCap) {
                                    activateBoostParams.setHours(int_boost_hrs);
                                    activateBoostParams.setUserId(user_id_int);
                                    activateBoostParams.setRequestType("ACTIVATE_USER_BOOST");
                                    loader.animate().alpha(1.0f);
                                    loader.setVisibility(View.VISIBLE);
                                    Call<DefaultResp> call = apicaller.activateBoost(activateBoostParams);
                                    call.enqueue(new Callback<DefaultResp>() {
                                        @Override
                                        public void onResponse(Call<DefaultResp> call, Response<DefaultResp> response) {
                                            if (response.isSuccessful()) {
                                                DefaultResp resp = response.body();
                                                if (resp != null && resp.getStatusMessage().equals("SUCCESS")) {
                                                    loader.animate().alpha(0.1f);
                                                    loader.setVisibility(View.GONE);
                                                    Toasty.success(SettingsActivity.this, "success", Toasty.LENGTH_SHORT).show();
                                                    dialog.dismiss();
                                                    new checkBoostStatus(params, apicaller).execute();

                                                } else {
                                                    loader.animate().alpha(0.1f);
                                                    loader.setVisibility(View.GONE);
                                                    dialog.dismiss();
                                                    Toasty.warning(SettingsActivity.this, "Something " +
                                                            "went wrong, Try Again").show();

                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<DefaultResp> call, Throwable t) {
                                            loader.animate().alpha(0.1f);
                                            loader.setVisibility(View.GONE);
                                            dialog.dismiss();
                                            Toasty.warning(SettingsActivity.this, "Something " +
                                                    "went wrong, Try Again").show();

                                        }
                                    });


                                } else {
                                    Toasty.warning(SettingsActivity.this, "You have " + boostCap + " available", Toasty.LENGTH_SHORT).show();

                                }

                            } else {
                                Toasty.warning(SettingsActivity.this, "More than One please", Toasty.LENGTH_SHORT).show();
                            }


                        } else {
                            Toasty.warning(SettingsActivity.this, "How many hours?", Toasty.LENGTH_SHORT).show();


                        }


                    }

                });
        builderMpesa.show();

    }


    private void loadPrivacy() {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("https://www.privacypolicies.com/privacy/view/1dbfeaad9e658e84652988ba6da7d715"));
        startActivity(openURL);
    }

    private void deleteUser() {
        DeleteUser.open(SettingsActivity.this);

    }

    private void signOutUser() {

        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle("Sign Out");
        builder.setTextGravity(Gravity.CENTER);
        builder.setMessage("Are you sure you want to sign out?");
        builder.setCancelable(false);
        builder.addButton("Yes", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Preferences.get(SettingsActivity.this).setLocationAsSet(false);
                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestIdToken(Extras.GOOGLE_CLIENT_ID)
                                .requestEmail()
                                .requestProfile()
                                .build();
                        LoginManager.getInstance().logOut();

                        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(SettingsActivity.this, gso);
                        mGoogleSignInClient.signOut();
                        FirebaseAuth.getInstance().signOut();
                        Preferences.get(getApplicationContext()).setKeyIsLoggedIn(false);
                        finish();

                        startActivity(new Intent(SettingsActivity.this, WelcomeActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));


                    }
                }).addButton("Cancel", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

        builder.show();


    }

    private void changeLocation() {
        ChangeLocation.open(SettingsActivity.this, user_id, address);

    }


    private boolean isNumberValid(String phoneNumber) {
        String regex = "^[+]?[0-9]{10,12}$";
        if (phoneNumber.matches(regex)) {
            return true;
        }
        return false;
    }


    final class MpesaPayDetails {
        private final String phone;
        private final int amount;
        private final boolean isReady;

        public MpesaPayDetails(String phone, int amount, boolean isReady) {
            this.phone = phone;
            this.amount = amount;
            this.isReady = isReady;
        }

        public int getAmount() {
            return amount;
        }

        public String getPhone() {
            return phone;
        }

        public boolean isReady() {
            return isReady;
        }
    }


    private class checkBoostStatus extends AsyncTask<Void, Void, BoostStatusResp> {
        BoostStatusParams mParams;
        ApiCaller mApiCaller;


        checkBoostStatus(BoostStatusParams params, ApiCaller apiCaller) {
            this.mParams = params;
            this.mApiCaller = apiCaller;

        }

        @Override
        protected BoostStatusResp doInBackground(Void... voids) {
            Call<BoostStatusResp> call = mApiCaller.getBoostStatus(mParams);
            call.enqueue(new Callback<BoostStatusResp>() {
                @Override
                public void onResponse(Call<BoostStatusResp> call, Response<BoostStatusResp> response) {
                    if (response.isSuccessful()) {
                        BoostStatusResp resp = response.body();
                        if (resp != null && resp.getStatusMessage() != null && resp.getStatusCode().equals("00")
                                & resp.getBoostStatus() != null) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showBoostedPanel(resp);
                                }
                            });


                        } else {
                            Log.d(TAG, "onResponse: check boost 1" + response.message());
                        }


                    } else {
                        Log.d(TAG, "onResponse: check boost 3" + response.message());
                    }
                }

                @Override
                public void onFailure(Call<BoostStatusResp> call, Throwable t) {
                    Log.d(TAG, "onFailure: getbooststatus");

                }
            });
            return null;


        }


    }

    private void showBoostedPanel(BoostStatusResp resp) {




        if (resp.getBoostStatus().equals("ONLINE")) {

            if (resp.getViews() != null && resp.getViews() > 0) {
                Log.d(TAG, "showBoostedPanel: views = " + resp.getViews());
                mViewholder.setVisibility(View.VISIBLE);
                mViewholder1.setVisibility(View.VISIBLE);
                mNumberOfViews.setText(String.valueOf(resp.getViews()));
                mNumberOfViews1.setText(String.valueOf(resp.getViews()));
            } else {
                mViewholder.setVisibility(View.GONE);
                mViewholder1.setVisibility(View.GONE);
            }


            if (resp.getHoursLeft() < 0) {
                hoursFinished(resp);
                return;
            }

            isOnline = true;
            if (package_id == 1) {


                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole1.animate().alpha(1.0f);

                mBoostConsole1.setVisibility(View.VISIBLE);
                mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status_ongoing_expired, resp.getBoostStatus()));
                if (resp.getHoursLeft() == 0) {
                    mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_status_hrs_left_one));

                } else {
                    mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_status_hrs_left, String.valueOf(resp.getHoursLeft())));

                }
            } else if (package_id == 2) {

                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole1.animate().alpha(1.0f);

                mBoostConsole1.setVisibility(View.VISIBLE);
                mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status_ongoing_expired, resp.getBoostStatus()));
                if (resp.getHoursLeft() == 0) {
                    mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_status_hrs_left_one));

                } else {
                    mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_status_hrs_left, String.valueOf(resp.getHoursLeft())));

                }
            } else if (package_id == 3) {

                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole.animate().alpha(1.0f);
                mBoostConsole.setVisibility(View.VISIBLE);
                mBoostConsole1.setVisibility(View.GONE);
                mHtmlTextView_boost_status.setHtml(getResources().getString(R.string.boost_status_ongoing_expired, resp.getBoostStatus()));
                if (resp.getHoursLeft() == 0) {
                    mBoostStatusAction.setHtml(getResources().getString(R.string.boost_status_hrs_left_one));

                } else {
                    mBoostStatusAction.setHtml(getResources().getString(R.string.boost_status_hrs_left, String.valueOf(resp.getHoursLeft())));

                }

            }
            return;

        }

        if (resp.getBoostStatus().equals("EXPIRED")) {


            isOnline = false;
            if (package_id == 1) {
                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole1.animate().alpha(1.0f);
                mBoostConsole1.setVisibility(View.VISIBLE);
                mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
                mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_action, String.valueOf(resp.getActionState())));
            } else if (package_id == 2) {
                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole1.animate().alpha(1.0f);
                mBoostConsole1.setVisibility(View.VISIBLE);
                mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
                mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_action, String.valueOf(resp.getActionState())));

            } else if (package_id == 3) {
                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole.animate().alpha(1.0f);
                mBoostConsole.setVisibility(View.VISIBLE);
                mBoostConsole1.setVisibility(View.GONE);
                mHtmlTextView_boost_status.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
                mBoostStatusAction.setHtml(getResources().getString(R.string.boost_action, String.valueOf(resp.getActionState())));

            }
            return;

        }

        if (resp.getBoostStatus().equals("NEW")) {
            if (package_id == 1) {
                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole1.animate().alpha(1.0f);
                mBoostConsole1.setVisibility(View.VISIBLE);
                mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
                mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_action, String.valueOf(resp.getActionState())));
            } else if (package_id == 2) {
                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }

                mBoostConsole1.animate().alpha(1.0f);
                mBoostConsole1.setVisibility(View.VISIBLE);
                mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
                mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_action, String.valueOf(resp.getActionState())));

            } else if (package_id == 3) {
                if (resp.getBoostLeft() >= 1) {
                    boostCap = resp.getBoostLeft();
                    toActivate = true;
                    toBuy = false;
                } else {
                    toActivate = false;
                    toBuy = true;
                }
                mBoostConsole.animate().alpha(1.0f);
                mBoostConsole.setVisibility(View.VISIBLE);
                mBoostConsole1.setVisibility(View.GONE);
                mHtmlTextView_boost_status.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
                mBoostStatusAction.setHtml(getResources().getString(R.string.boost_action, String.valueOf(resp.getActionState())));

            }
        }


    }

    private void hoursFinished(BoostStatusResp resp) {
        isOnline = false;
        String boost_action = null;
        if (package_id == 1) {
            if (resp.getBoostLeft() >= 1) {
                boost_action = "ACTIVATE";
                boostCap = resp.getBoostLeft();
                toActivate = true;
                toBuy = false;
            } else {
                boost_action = "BUY";
                toActivate = false;
                toBuy = true;
            }
            mBoostConsole1.animate().alpha(1.0f);
            mBoostConsole1.setVisibility(View.VISIBLE);
            mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
            mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_action, boost_action));
        } else if (package_id == 2) {
            if (resp.getBoostLeft() >= 1) {
                boost_action = "ACTIVATE";
                boostCap = resp.getBoostLeft();
                toActivate = true;
                toBuy = false;
            } else {
                boost_action = "BUY";
                toActivate = false;
                toBuy = true;
            }
            mBoostConsole1.animate().alpha(1.0f);
            mBoostConsole1.setVisibility(View.VISIBLE);
            mHtmlTextView_boost_status1.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
            mBoostStatusAction1.setHtml(getResources().getString(R.string.boost_action, boost_action));

        } else if (package_id == 3) {

            if (resp.getBoostLeft() >= 1) {
                boost_action = "ACTIVATE";
                boostCap = resp.getBoostLeft();
                toActivate = true;
                toBuy = false;
            } else {
                boost_action = "BUY";
                toActivate = false;
                toBuy = true;
            }
            mBoostConsole.animate().alpha(1.0f);
            mBoostConsole.setVisibility(View.VISIBLE);
            mBoostConsole1.setVisibility(View.GONE);
            mHtmlTextView_boost_status.setHtml(getResources().getString(R.string.boost_status, String.valueOf(resp.getBoostLeft()), resp.getBoostType()));
            mBoostStatusAction.setHtml(getResources().getString(R.string.boost_action, boost_action));

        }
    }


}
