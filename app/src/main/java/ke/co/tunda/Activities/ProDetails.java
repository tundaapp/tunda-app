

package ke.co.tunda.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.R;
import ke.co.tunda.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProDetails extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.edit_job_title)
    ImageView mEditJobTitle;
    @BindView(R.id.edit_job_title_check)
    ImageView mEditJobTitleCheck;
    @BindView(R.id.edittxt_jobTitle)
    EditText mEditTextJobTitle;
    @BindView(R.id.edit_company_title)
    ImageView mEditCoTitle;
    @BindView(R.id.edit_co_title_check)
    ImageView mEditCoTitleCheck;
    @BindView(R.id.edittxt_coTitle)
    EditText mEditTextCompany;
    @BindView(R.id.edit_college_title)
    ImageView mEditCollegeTitle;
    @BindView(R.id.edit_college_title_check)
    ImageView mEditCollegeTitleCheck;
    @BindView(R.id.edittxt_collegeTitle)
    EditText mEditTextCollege;


    private static String mJob, mCompany, mCollege, mUser_Id;
    private UpdateUserProfileRequest updateUserProfileRequest;
    private ApiCaller apiCaller;


    public static void open(Context context, String job, String company, String college, String user_id) {
        context.startActivity(new Intent(context, ProDetails.class));
        mJob = job;
        mCollege = college;
        mCompany = company;
        mUser_Id = user_id;

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pro_details);
        ButterKnife.bind(this);

        apiCaller = Builder.getClient().create(ApiCaller.class);


        if (!mJob.isEmpty()) {
            mEditTextJobTitle.setText(mJob);
        } else {
            mEditTextJobTitle.setText("");
        }

        if (!mCompany.isEmpty()) {
            mEditTextCompany.setText(mCompany);
        }

        if (!mCollege.isEmpty()) {
            mEditTextCollege.setText(mCollege);
        }


        mBack.setOnClickListener(this);
        mEditJobTitle.setOnClickListener(this);
        mEditJobTitleCheck.setOnClickListener(this);
        mEditCoTitle.setOnClickListener(this);
        mEditCoTitleCheck.setOnClickListener(this);
        mEditCollegeTitle.setOnClickListener(this);
        mEditCollegeTitleCheck.setOnClickListener(this);

        updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setUserId(mUser_Id);
        updateUserProfileRequest.setRequestType("UPDATE_USER_PROFILE");


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;
            case R.id.edit_job_title:
                showKeyboard(mEditTextJobTitle);
                mEditJobTitleCheck.setVisibility(View.VISIBLE);
                mEditJobTitle.setVisibility(View.GONE);
                break;
            case R.id.edit_job_title_check:
                if (!mEditTextJobTitle.getText().toString().isEmpty()) {
                    hideSoftKeyboard(mEditTextJobTitle);
                    updateUserProfileRequest.setJobTitle(mEditTextJobTitle.getText().toString());
                    uploadChanges(updateUserProfileRequest, mEditJobTitleCheck, mEditJobTitle, "job", mEditTextJobTitle.getText().toString());
                } else {
                    AppUtils.showToast(getApplicationContext(), "Please Add Job Title", true);
                }
                break;


            case R.id.edit_company_title:
                showKeyboard(mEditTextCompany);
                mEditCoTitle.setVisibility(View.GONE);
                mEditCoTitleCheck.setVisibility(View.VISIBLE);
                break;
            case R.id.edit_co_title_check:
                if (!mEditTextCompany.getText().toString().isEmpty()) {
                    hideSoftKeyboard(mEditTextCompany);
                    updateUserProfileRequest.setCompany(mEditTextCompany.getText().toString());
                    uploadChanges(updateUserProfileRequest, mEditCoTitleCheck, mEditCoTitle, "company", mEditTextCompany.getText().toString());
                } else {
                    Toasty.warning(getApplicationContext(),"Please Add Company Title").show();

                }
                break;
            case R.id.edit_college_title:
                showKeyboard(mEditTextCollege);
                mEditCollegeTitle.setVisibility(View.GONE);
                mEditCollegeTitleCheck.setVisibility(View.VISIBLE);
                break;
            case R.id.edit_college_title_check:
                if (!mEditTextCollege.getText().toString().isEmpty()) {
                    hideSoftKeyboard(mEditTextCollege);
                    updateUserProfileRequest.setSchool(mEditTextCollege.getText().toString());
                    uploadChanges(updateUserProfileRequest, mEditCollegeTitleCheck, mEditCollegeTitle, "college", mEditTextCollege.getText().toString());
                } else {
                    Toasty.warning(getApplicationContext(),"Please Add College Title").show();

                }
                break;

        }
    }

    public void showKeyboard(final EditText ettext) {

        ettext.setClickable(true);
        ettext.setFocusable(true);
        ettext.setFocusableInTouchMode(true);
        ettext.requestFocus();
        ettext.hasFocus();
        ettext.setSelection(ettext.getText().length());

        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 200);
    }


    private void hideSoftKeyboard(EditText ettext) {
        ettext.setClickable(false);
        ettext.setFocusable(false);
        ettext.setFocusableInTouchMode(false);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    private void uploadChanges(UpdateUserProfileRequest string, ImageView m1, ImageView m2, String key, String value) {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ProDetails.this);
        builder.content("Updating ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();

        Call<UpdateUserProfileResponse> call = apiCaller.updateUser(string);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse response1 = response.body();
                    if (response1.getBody().equals("SUCCESS")) {
                        dialog.dismiss();
                        saveToSp(key, value);
                        Toasty.success(getApplicationContext(),"Saved").show();
                        m1.setVisibility(View.GONE);
                        m2.setVisibility(View.VISIBLE);
                    } else {
                        Toasty.warning(getApplicationContext(),"Something went wrong,Try Again").show();

                        dialog.dismiss();
                        m1.setVisibility(View.GONE);
                        m2.setVisibility(View.VISIBLE);
                    }

                } else {
                    Toasty.warning(getApplicationContext(),"Something went wrong,Try Again").show();

                    dialog.dismiss();
                    m1.setVisibility(View.GONE);
                    m2.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable throwable) {
                dialog.dismiss();
                Toasty.warning(getApplicationContext(),"Something went wrong,Try Again").show();

                m1.setVisibility(View.GONE);
                m2.setVisibility(View.VISIBLE);

            }
        });


    }

    private void saveToSp(String key, String s) {
        SharedPreferences.Editor editor;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        editor.putString(key, s);
        editor.apply();
    }
}
