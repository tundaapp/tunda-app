/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 6/26/19 5:56 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.R;
import ke.co.tunda.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InterestDetails extends AppCompatActivity implements View.OnClickListener, TagView.OnTagClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.text_tag)
    EditText mEditTag;
    @BindView(R.id.btn_add_tag)
    Button mAddTag;
    @BindView(R.id.btn_save)
    Button mSave;

    @BindView(R.id.tagcontainerLayout)
    TagContainerLayout mTagContainerLayout;

    SharedPreferences sp;
    String about_me = "";

    private static String mBio, mUser_Id;
    UpdateUserProfileRequest updateUserProfileRequest;
    private ApiCaller apiCaller;
    private static String mJonInterests;
    private List<String> mInterests;
    private String jsonInterests;


    public static void open(Context context, String user_id, String jsonInterests) {
        context.startActivity(new Intent(context, InterestDetails.class));
        mJonInterests = jsonInterests;
        mUser_Id = user_id;

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest_details);
        ButterKnife.bind(this);
        mInterests = new ArrayList<>();
        mTagContainerLayout.setOnTagClickListener(this);


        sp = PreferenceManager.getDefaultSharedPreferences(this);

        try {


            if (mJonInterests != null && mJonInterests.length() > 0) {
                mTagContainerLayout.setVisibility(View.VISIBLE);
                JSONArray jr = new JSONArray(mJonInterests);
                for (int i = 0; i < jr.length(); i++) {

                    String interest = jr.getString(i);
                    Log.d("FULL", "onCreate: interest number " + i + ">>" + interest);
                    mInterests.add(interest);

                    // loop and add it to array or arraylist
                }

            }


            mTagContainerLayout.setTags(mInterests);
        } catch (Exception e) {
            Log.d("FULL", e.getMessage());
        }


        mBack.setOnClickListener(this);
        updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setUserId(mUser_Id);
        updateUserProfileRequest.setRequestType("UPDATE_USER_PROFILE");
        apiCaller = Builder.getClient().create(ApiCaller.class);

        mEditTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(mEditTag);
            }
        });


        mAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mTagContainerLayout.setVisibility(View.VISIBLE);
                if (!mEditTag.getText().toString().isEmpty()) {
                    if (mEditTag.getText().toString().length() > 2) {

                        if (mTagContainerLayout.getTags().size() <= 30) {
                            mTagContainerLayout.addTag(mEditTag.getText().toString());
                            mEditTag.setText("");

                        } else {
                            Toasty.warning(getApplicationContext(), "Maximum is 30 please").show();
                        }


                    } else {
                        mEditTag.setError("Interest must be more that 2 words");
                    }

                } else {
                    mEditTag.setError("Add Interest");
                }
            }
        });


        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mTagContainerLayout.getTags().size() > 0) {
                    update();


                } else {
                    Toasty.warning(getApplicationContext(), "Add Tags to save").show();
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;


        }
    }

    private void saveToSp(String s) {
        SharedPreferences.Editor editor;

        editor = sp.edit();
        editor.putString("interest", s);
        editor.apply();

    }

    private void update() {

        try {
            List<String> interests = mTagContainerLayout.getTags();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            jsonInterests = gson.toJson(interests);

        } catch (Exception e) {

        }


        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(InterestDetails.this);
        builder.content("Updating ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        updateUserProfileRequest.setJsonInterest(jsonInterests);
        Call<UpdateUserProfileResponse> call = apiCaller.updateUser(updateUserProfileRequest);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse upr = response.body();
                    if (upr.getBody().equals("SUCCESS")) {
                        dialog.dismiss();
                        saveToSp(jsonInterests);
                        Toasty.success(getApplicationContext(), "Saved").show();

                    } else {
                        dialog.dismiss();
                        AppUtils.showToast(getApplicationContext(), "Something went wrong,try again", false);
                    }
                } else {
                    dialog.dismiss();
                    AppUtils.showToast(getApplicationContext(), "Something went wrong,try again", false);

                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable throwable) {
                dialog.dismiss();
                AppUtils.showToast(getApplicationContext(), "Something went wrong,try again", false);

            }
        });

    }

    public void showKeyboard(final EditText ettext) {

        ettext.setClickable(false);
        ettext.setFocusable(true);
        ettext.setFocusableInTouchMode(true);
        ettext.requestFocus();
        ettext.setSelection(ettext.getText().length());

        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 200);
    }


    private void hideSoftKeyboard(EditText ettext) {
        ettext.setClickable(false);
        ettext.setFocusable(false);
        ettext.setFocusableInTouchMode(false);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    @Override
    public void onTagClick(int position, String text) {

    }

    @Override
    public void onTagLongClick(int position, String text) {

    }

    @Override
    public void onSelectedTagDrag(int position, String text) {

    }

    @Override
    public void onTagCrossClick(int position) {
        mTagContainerLayout.removeTag(position);


    }
}
