package ke.co.tunda.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Adapters.StoriesInterface;
import ke.co.tunda.Adapters.StoriesPhotoAdapter;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.Progress;
import ke.co.tunda.Models.Mood;
import ke.co.tunda.Models.ViewMoodRequest;
import ke.co.tunda.Models.ViewMoodResponse;
import ke.co.tunda.R;
import ke.co.tunda.Room.MainMoodsTable;
import ke.co.tunda.Room.MyMoodsTable;
import ke.co.tunda.Views.StoryStatusView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatusStories extends AppCompatActivity implements StoryStatusView.UserInteractionListener, StoriesInterface, View.OnClickListener {

    @BindView(R.id.status_viewpager)
    ViewPager mStatusViewpager;
    StoryStatusView storyStatusView;
    @BindView(R.id.back)
    ImageView mBack;
    @BindView(R.id.user_photo)
    CircleImageView mUserPhoto;
    @BindView(R.id.user_name)
    TextView mUserName;
    @BindView(R.id.mood_time)
    TextView mMoodTime;
    @BindView(R.id.mood_caption)
    TextView mMoodCaption;
    @BindView(R.id.views_layout)
    LinearLayout mViewLayout;
    @BindView(R.id.num_of_views)
    TextView mNumViewsText;


    StoriesPhotoAdapter storiesPhotoAdapter;
    private int page = 0;
    private int count = 0;


    //    static GetMyMoodResponse getMyMoodResponse;
    private static List<MyMoodsTable> mPhotoPaths;
    private static List<Mood> mainPhotoPaths;
    private ArrayList<Progress> mProgressList;
    private static final String TAG = "SXAXUS";

    private SharedPreferences sp;
    private static String name, image;
    private int user_id;
    private ApiCaller apiCaller;
    private static boolean is_self;


    public static void open(Context context, List<MyMoodsTable> moods, List<Mood> mainmoods, String mName, String mImage, boolean self) {
        Intent intent = new Intent(context, StatusStories.class);
        context.startActivity(intent);
        mPhotoPaths = new ArrayList<>();
        mainPhotoPaths = new ArrayList<>();
        mPhotoPaths = moods;
        mainPhotoPaths = mainmoods;
        name = mName;
        image = mImage;
        is_self = self;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_stories);

        try {
            storyStatusView = findViewById(R.id.storiesStatus);

            sp = PreferenceManager.getDefaultSharedPreferences(this);
            user_id = sp.getInt("user_id", 0);


            ButterKnife.bind(this);
            mBack.setOnClickListener(this);
            changeStatusBarColor();
            LinearLayoutManager horizontalLayoutManagaerx = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);


            mProgressList = new ArrayList<>();
//        mPhotoPaths = getMyMoodResponse.getBody();
            if(is_self){
                count = mPhotoPaths.size();
            }else {
                count = mainPhotoPaths.size();
            }



            Glide.with(this).load(image).into(mUserPhoto);
            mUserName.setText(name);

            if(is_self){
                storyStatusView.setStoriesCount(mPhotoPaths.size());
            }else {
                storyStatusView.setStoriesCount(mainPhotoPaths.size());
            }

            storyStatusView.setStoryDuration(5000);
            // or
            // statusView.setStoriesCountWithDurations(statusResourcesDuration);
            storyStatusView.setUserInteractionListener(this);

//        storiesPhotoAdapter.addPhotos(mPhotoPaths);
            storiesPhotoAdapter = new StoriesPhotoAdapter(mPhotoPaths,mainPhotoPaths,is_self, this, this);
            mStatusViewpager.setOffscreenPageLimit(1);
            mStatusViewpager.setAdapter(storiesPhotoAdapter);

            mStatusViewpager.addOnPageChangeListener(viewListener);

//            storyStatusView.playStories();
//            storyStatusView.pause();

            viewListener.onPageSelected(0);

        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @Override
    public void onNext() {
        Log.d(TAG, "onNext: ");

        if (page >= count) {
            return;
        }


        storyStatusView.pause();
        ++page;
        viewListener.onPageSelected(page);
        if (!is_self) {
            if (page > 0) {

                new updateStatusViewed(mainPhotoPaths.get(page - 1).getMoodId()).execute();

            }
        }


    }

    @Override
    public void onPrev() {
        if (page - 1 < 0) {
            return;
        }
        storyStatusView.pause();
        --page;
        viewListener.onPageSelected(page);


    }

    @Override
    public void onComplete() {
        Log.d(TAG, "onComplete: ");


        finish();

    }


    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            Log.d(TAG, "onPageSelected: " + i);
                mStatusViewpager.setCurrentItem(i, true);
                storyStatusView.resume();


            if (!is_self) {

                if (page + 1 == count) {
                    Log.d(TAG, "onPageSelected: viewing..");
                    new updateStatusViewed(mainPhotoPaths.get(count - 1).getMoodId()).execute();

                }


            }

            if (is_self) {
                if (mPhotoPaths.get(i).view_count > 0) {
                    mViewLayout.setVisibility(View.VISIBLE);
                    mNumViewsText.setText(String.valueOf(mPhotoPaths.get(i).view_count));
                } else {
                    mViewLayout.setVisibility(View.GONE);
                    mNumViewsText.setText("");
                }
            }


            try {
                if(is_self){
                    String formatted_date = formatToYesterdayOrToday(mPhotoPaths.get(i).time_posted);
                    mMoodTime.setText(formatted_date);
                    Log.d(TAG, "setTime: " + formatted_date);

                }else {
                    String formatted_date = formatToYesterdayOrToday(mainPhotoPaths.get(i).getTime_posted());
                    mMoodTime.setText(formatted_date);
                    Log.d(TAG, "setTime: " + formatted_date);

                }

            } catch (ParseException e) {


                Log.d(TAG, "instantiateItem: " + e.getMessage());
            }

            if(is_self){
                mMoodCaption.setText(mPhotoPaths.get(i).mood_caption);

            }else {
                mMoodCaption.setText(mainPhotoPaths.get(i).getMoodCaption());

            }


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    public void resume() {
        storyStatusView.resume();
    }

    @Override
    public void reverse() {
        storyStatusView.pause();
        if (page - 1 < 0) {
            return;
        }
        storyStatusView.reverse();
        viewListener.onPageSelected(page);

    }

    @Override
    public void skip() {

        if (page > count) {
            return;
        }

        if (page == count) {
            storyStatusView.pause();
            viewListener.onPageSelected(page);
            storyStatusView.skip();
        } else {
            storyStatusView.pause();
            viewListener.onPageSelected(page);
            storyStatusView.skip();
        }

        Log.d(TAG, "skip: page = " + page + "count=" + count);


    }

    @Override
    public void pause() {
        storyStatusView.pause();

    }

    @Override
    public void back() {
        finish();
    }

    @Override
    public void setTime(int position) {


//        try {
//            String formatted_date = formatToYesterdayOrToday(getMyMoodResponse.getBody().get(page).getTime_posted());
//            mMoodTime.setText(formatted_date);
//            Log.d(TAG, "setTime: " + formatted_date);
//        } catch (ParseException e) {
//
//
//            Log.d(TAG, "instantiateItem: " + e.getMessage());
//        }


    }

    @Override
    public void setCaption() {
//        mMoodCaption.setText(getMyMoodResponse.getBody().get(page).getMoodCaption());
    }

    @Override
    public void play() {
        storyStatusView.playStories();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                storyStatusView.pause();
                finish();
        }

    }

    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss").parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday " + timeFormatter.format(dateTime);
        } else {
            return date;
        }
    }

    private class updateStatusViewed extends AsyncTask<Void, Void, Void> {
        Integer mood_id;

        public updateStatusViewed(Integer moodId) {
            this.mood_id = moodId;


        }

        @Override
        protected Void doInBackground(Void... voids) {
            ViewMoodRequest request = new ViewMoodRequest();
            request.setRequestType("VIEW_USER_MOOD");
            request.setMoodId(mood_id);
            request.setViewerId(user_id);
            apiCaller = Builder.getClient().create(ApiCaller.class);
            Call<ViewMoodResponse> call = apiCaller.updateMoodView(request);
            call.enqueue(new Callback<ViewMoodResponse>() {
                @Override
                public void onResponse(@NonNull Call<ViewMoodResponse> call, @NonNull Response<ViewMoodResponse> response) {
                    if (response.isSuccessful()) {
                        ViewMoodResponse viewMoodResponse = response.body();
                        if (viewMoodResponse != null && viewMoodResponse.getStatusCode().equals("00")) {
                            Log.d(TAG, "onResponse: " + viewMoodResponse.getStatusCode());
                        } else {
                            Log.d(TAG, "onResponse: some error occured");
                        }
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ViewMoodResponse> call, @NonNull Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });
            return null;
        }
    }
}



