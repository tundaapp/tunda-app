

package ke.co.tunda.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guardanis.applock.AppLock;
import com.guardanis.applock.dialogs.UnlockDialogBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.androidtagview.TagContainerLayout;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.Adapters.MultiplePhotoAdapterIg;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Builder2;
import ke.co.tunda.ApiConnector.Models.ImageUploadResponse;
import ke.co.tunda.ApiConnector.Models.MultiPlePhotosResponse;
import ke.co.tunda.ApiConnector.Models.MultiplePhotoRequest;
import ke.co.tunda.ApiConnector.Models.PackageIdParams;
import ke.co.tunda.ApiConnector.Models.PackageResponse;
import ke.co.tunda.ApiConnector.Models.RemovePhotoRequest;
import ke.co.tunda.ApiConnector.Models.RemovePhotoResponse;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import com.bumptech.glide.Glide;
import ke.co.tunda.Helpers.ImageCrop.ImagePickerActivity;
import ke.co.tunda.Models.BaseResponse;
import ke.co.tunda.POJO.AccessTokenResponse;
import ke.co.tunda.POJO.Datum;
import ke.co.tunda.POJO.GetIgPhotoResponse;
import ke.co.tunda.POJO.GetIgPhotosRequest;
import ke.co.tunda.POJO.MediaRepsonse;
import ke.co.tunda.POJO.PhotoResponse;
import ke.co.tunda.POJO.SectionsList;
import ke.co.tunda.POJO.SendIgPhotosRequest;
import ke.co.tunda.R;
import ke.co.tunda.fragments.Dialogs.AuthenticationDialog;
import ke.co.tunda.fragments.Dialogs.TundaPlusBuy;
import ke.co.tunda.listener.AuthenticationListener;
import ke.co.tunda.rest.RestClient;
import ke.co.tunda.rest.RetrofitService;
import ke.co.tunda.utils.AppUtils;
import ke.co.tunda.utils.Constants;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, AuthenticationListener {


    SharedPreferences sp;
    SharedPreferences.Editor editor;
    String first_name, user_id, bio;
    int user_id_int;
    String job, company, college;
    String image;
    String dob, year_of_birth;
    String gender;
    String is_profile;
    private AsyncTask mAsyncTask;
    private int photo_count;
    ArrayList<String> mPhotos;
    private boolean edited = false;
    private boolean editedShowAge = false;
    private boolean editedShowLocation = false;


    private static final String TAG = "EDITPPP";
    public static final int REQUEST_IMAGE = 100;


    private ImageView mImageToSet;
    private ImageView mAddImage;
    private RelativeLayout mLayout;
    private ImageView mRemove;
    private String sp_key;
    private int spImage;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.personal_details_card)
    CardView personal_details_card;
    @BindView(R.id.bio_card)
    CardView mBioCard;
    @BindView(R.id.professional_card)
    CardView mProCard;
    @BindView(R.id.img_1)
    ImageView mImage1;
    @BindView(R.id.add_img_1)
    ImageView mAddImg1;
    @BindView(R.id.image2)
    RelativeLayout mImage2Holder;
    @BindView(R.id.img_2)
    ImageView mImage2;
    @BindView(R.id.add_img_2)
    ImageView mAddImage2;
    @BindView(R.id.add_img_3)
    ImageView mAddImage3;
    @BindView(R.id.img_3)
    ImageView mImage3;
    @BindView(R.id.image1)
    RelativeLayout mImage1Holder;
    @BindView(R.id.image3)
    RelativeLayout mImage3Holder;
    @BindView(R.id.image4)
    RelativeLayout mImage4Holder;
    @BindView(R.id.img_4)
    ImageView mImage4;
    @BindView(R.id.add_img_4)
    ImageView mAddImage4;
    @BindView(R.id.image5)
    RelativeLayout mImage5Holder;
    @BindView(R.id.img_5)
    ImageView mImage5;
    @BindView(R.id.add_img_5)
    ImageView mAddImage5;
    @BindView(R.id.image6)
    RelativeLayout mImage6Holder;
    @BindView(R.id.img_6)
    ImageView mImage6;
    @BindView(R.id.add_img_6)
    ImageView mAddImage6;
    @BindView(R.id.txt_gender)
    TextView txt_gender;

    @BindView(R.id.txt_age)
    TextView mAge;
    @BindView(R.id.txt_name)
    TextView mName;
    @BindView(R.id.txt_bio)
    TextView mtxtBio;
    @BindView(R.id.txt_int_header)
    TextView mInteHeader;

    @BindView(R.id.interest_card)
    CardView mInterestCard;
    @BindView(R.id.tagcontainerLayout)
    TagContainerLayout mTagContainer;


    @BindView(R.id.jobTitleData)
    TextView mJob;
    @BindView(R.id.companyData)
    TextView mCompany;
    @BindView(R.id.collegeData)
    TextView mCollege;

    @BindView(R.id.switch_hide_myAge)
    Switch mHideAge;
    @BindView(R.id.switch_hide_myDistance)
    Switch mHideDistance;


    @BindView(R.id.no_ig_photos_holder)
    RelativeLayout mNoIgPhotosHolder;
    @BindView(R.id.ig_photos_holder)
    RelativeLayout mIgPhotosHolder;
    @BindView(R.id.dot_layout)
    LinearLayout mDotLayout;
    @BindView(R.id.ig_photo_count)
    TextView mIgPhotoCount;
    @BindView(R.id.ig_photos_pager)
    ViewPager mIgPager;
    @BindView(R.id.disconnect)
    Button mDisconnect;
//    @BindView(R.id.ig_card)
//    CardView mIgCard;


    int packageId;

    private String img2 = "", img3 = "", img4 = "", img5 = "", img6 = "";
    private String showAge = "";
    private String showDistance = "";
    private UpdateUserProfileRequest updateUserProfileRequest;

    private List<String> mInterests;
    private String jsonStringInterests = "";
    private String jsonStringPhootos = "";

    private AuthenticationDialog auth_dialog;

    ProgressDialog pd;
    RetrofitService retrofitService;
    RetrofitService retrofitServiceGraph;
    Datum[] data;
    private RecyclerView mRecycler;
    private GridLayoutManager mGrid;

    private MultiplePhotoAdapterIg multiplePhotoAdapter;
    private TextView[] mDots;
    int page = 0;
    private int mSize;
    private ArrayList<SectionsList> mList;
    private int limit = 3;


    //Remove

    @BindView(R.id.rmv_img_1)
    ImageView mRmv1;
    @BindView(R.id.rmv_img_2)
    ImageView mRmv2;
    @BindView(R.id.rmv_img_3)
    ImageView mRmv3;
    @BindView(R.id.rmv_img_4)
    ImageView mRmv4;
    @BindView(R.id.rmv_img_5)
    ImageView mRmv5;
    @BindView(R.id.rmv_img_6)
    ImageView mRmv6;


    public static void open(Context context) {
        context.startActivity(new Intent(context, EditProfileActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_layout);
        ButterKnife.bind(this);

        mInterests = new ArrayList<>();
        updateUserProfileRequest = new UpdateUserProfileRequest();

        mBack.setOnClickListener(this);
        personal_details_card.setOnClickListener(this);
        mBioCard.setOnClickListener(this);
        mProCard.setOnClickListener(this);
        mInterestCard.setOnClickListener(this);
        mImage1Holder.setOnClickListener(this);
        mImage2Holder.setOnClickListener(this);
        mImage3Holder.setOnClickListener(this);
        mImage4Holder.setOnClickListener(this);
        mImage5Holder.setOnClickListener(this);
        mImage6Holder.setOnClickListener(this);
        mNoIgPhotosHolder.setOnClickListener(this);
        mNoIgPhotosHolder.setOnClickListener(this);
        mDisconnect.setOnClickListener(this);

        mRmv1.setOnClickListener(this);
        mRmv2.setOnClickListener(this);
        mRmv3.setOnClickListener(this);
        mRmv4.setOnClickListener(this);
        mRmv5.setOnClickListener(this);
        mRmv6.setOnClickListener(this);


        sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        first_name = sp.getString("first_name", "");
        image = sp.getString("image", "");
        packageId = sp.getInt("package_id", 0);
        photo_count = sp.getInt("photo_count", 1);
        dob = sp.getString("dob", "");
        bio = sp.getString("about_me", "");
        job = sp.getString("job", "");
        jsonStringInterests = sp.getString("interest", "");

        showAge = sp.getString("show_age", "");
        showDistance = sp.getString("show_location", "");
        company = sp.getString("company", "");
        college = sp.getString("college", "");
        gender = sp.getString("gender", "");
        img2 = sp.getString("image2", "");
        img3 = sp.getString("image3", "");
        img4 = sp.getString("image4", "");
        img5 = sp.getString("image5", "");
        img6 = sp.getString("image6", "");

        user_id_int = sp.getInt("user_id", 0);
        user_id = String.valueOf(user_id_int);

        retrofitService = RestClient.getClient().create(RetrofitService.class);
        retrofitServiceGraph = RestClient.getClientGraph().create(RetrofitService.class);
        mPhotos = new ArrayList<>();
        multiplePhotoAdapter = new MultiplePhotoAdapterIg(EditProfileActivity.this);
        mIgPager.setAdapter(multiplePhotoAdapter);
        mIgPager.addOnPageChangeListener(viewListener);
        mList = new ArrayList<>();




        if (showAge != null && !showAge.isEmpty()) {
            if (showAge.equals("NO")) {
                mHideAge.setChecked(true);
            } else {
                mHideAge.setChecked(false);
            }
        }

        try {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    getPackageId();
                }
            });

            new getIgPhotos().execute();


            if (jsonStringInterests != null && jsonStringInterests.length() > 0) {
                mTagContainer.setVisibility(View.VISIBLE);
                JSONArray jr = new JSONArray(jsonStringInterests);
                for (int i = 0; i < jr.length(); i++) {

                    String interest = jr.getString(i);
                    Log.d("FULL", "onCreate: interest number " + i + ">>" + interest);
                    mInterests.add(interest);

                    // loop and add it to array or arraylist
                }

            }


            mTagContainer.setTags(mInterests);
        } catch (Exception e) {
            Log.d("FULL", e.getMessage());
        }

        if (showDistance != null && !showDistance.isEmpty()) {
            if (showDistance.equals("NO")) {
                mHideDistance.setChecked(true);
            } else {
                mHideDistance.setChecked(false);
            }
        }




        mHideAge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (packageId == 1) {
                    TundaPlusBuy.open(EditProfileActivity.this);
                    mHideAge.setChecked(false);


                } else {

                    edited = true;
                    editedShowAge = true;
                    if (isChecked) {
                        showAge = "NO";
                    } else {
                        showAge = "YES";
                    }

                }
            }
        });


        mHideDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (packageId == 1) {
                    TundaPlusBuy.open(EditProfileActivity.this);
                    mHideDistance.setChecked(false);

                } else {
                    edited = true;
                    editedShowLocation = true;
                    if (isChecked) {
                        showDistance = "NO";
                    } else {
                        showDistance = "YES";
                    }

                }

            }
        });

        loadImages();


        if (photo_count > 1 &&
                img2.isEmpty() &&
                img3.isEmpty() &&
                img4.isEmpty() &&
                img5.isEmpty() &&
                img6.isEmpty()) {

        }

        if (image != null && !image.isEmpty()) {
            Glide.with(this).load(image)
                    .into(mImage1);
            mAddImg1.setVisibility(View.GONE);
            mRmv1.setVisibility(View.VISIBLE);

            mLayout = mImage1Holder;
            mLayout.setClickable(false);
            mLayout.setFocusable(false);

        }

        if (img2 != null && !img2.isEmpty()) {
            Glide.with(this).load(img2).into(mImage2);
            mAddImage2.setVisibility(View.GONE);
            mRmv2.setVisibility(View.VISIBLE);

        }

        if (img3 != null && !img3.isEmpty()) {
            Glide.with(this).load(img3).into(mImage3);
            mAddImage3.setVisibility(View.GONE);
            mRmv3.setVisibility(View.VISIBLE);
        }

        if (img4 != null && !img4.isEmpty()) {
            Glide.with(this).load(img4)
                    .into(mImage4);
            mAddImage4.setVisibility(View.GONE);
            mRmv4.setVisibility(View.VISIBLE);
        }

        if (img5 != null && !img5.isEmpty()) {
            Glide.with(this).load(img5)
                    .into(mImage5);
            mAddImage5.setVisibility(View.GONE);
            mRmv5.setVisibility(View.VISIBLE);
        }

        if (img6 != null && !img6.isEmpty()) {
            Glide.with(this).load(img6)
                    .into(mImage6);
            mAddImage6.setVisibility(View.GONE);
            mRmv6.setVisibility(View.VISIBLE);
        }


        Log.d("TESTING", "Date of birth.xx" + dob);

        if (!dob.isEmpty()) {
            try {
                Log.d("TESTING", "Calculationg: ");
                calculateAge(dob);

            } catch (Exception e) {
                Log.d("TESTING", e.toString());
            }
        } else {
            mAge.setText("Age not Set");
        }

        if (!first_name.isEmpty()) {
            mName.setText(first_name);
        }

        if (!gender.isEmpty()) {
            txt_gender.setText(gender);
        } else {
            txt_gender.setText("Pick Gender");
        }

        if (!bio.isEmpty()) {
            mtxtBio.setText(bio);
        } else {
            mtxtBio.setText("Add Bio");
        }

        if (!job.isEmpty()) {
            mJob.setText(job);
        } else {
            mJob.setText("Add a job");
        }

        if (!company.isEmpty()) {
            mCompany.setText(company);
        } else {
            mCompany.setText("Add Company");
        }

        if (!college.isEmpty()) {
            mCollege.setText(college);
        } else {
            mCollege.setText("Add college");
        }

    }

    private void loadImages() {
        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
        MultiplePhotoRequest multiplePhotoRequest = new MultiplePhotoRequest();
        multiplePhotoRequest.setRequestType("GET_USER_PHOTOS");
        multiplePhotoRequest.setUserId(user_id);
        Call<MultiPlePhotosResponse> call = apiCaller.getUserPhotos(multiplePhotoRequest);
        call.enqueue(new Callback<MultiPlePhotosResponse>() {
            @Override
            public void onResponse(Call<MultiPlePhotosResponse> call, Response<MultiPlePhotosResponse> response) {
                if (response.isSuccessful()) {
                    MultiPlePhotosResponse res = response.body();
                    if (res != null) {
                        if (res.getStatusCode().equals("00")) {
                            editor.remove("image").commit();
                            editor.remove("image2").commit();
                            editor.remove("image3").commit();
                            editor.remove("image4").commit();
                            editor.remove("image5").commit();
                            editor.remove("image6").commit();

                            mImage1.setImageResource(0);
                            mImage2.setImageResource(0);
                            mImage3.setImageResource(0);
                            mImage4.setImageResource(0);
                            mImage5.setImageResource(0);
                            mImage6.setImageResource(0);

                            int photo_no = res.getBody().size();

                            for (int i = 0; i < res.getBody().size(); i++) {

                                if (!res.getBody().get(i).getIsProfile().equals("1")) {


                                    mPhotos.add(res.getBody().get(i).getPhotoPath());


                                } else if (res.getBody().get(i).getIsProfile().equals("1")) {

                                    editor.putString("image", res.getBody().get(i).getPhotoPath());
                                    image = res.getBody().get(i).getPhotoPath();
                                    Glide.with(EditProfileActivity.this).load(image).into(mImage1);
                                }





                            }
                            setLoadedPhotos(mPhotos);

                            editor.putInt("photo_count", photo_no);
                            editor.apply();


                        } else {
                            Log.d(TAG, "onResponse: Something went wrong");
                        }
                    }
                } else {
                    Log.d(TAG, "onResponse: Something went wrong");
                }
            }

            @Override
            public void onFailure(Call<MultiPlePhotosResponse> call, Throwable t) {
                Log.d(TAG, "onResponse: Something went wrong");

            }
        });
    }

    private void setLoadedPhotos(ArrayList<String> mPhotos) {
        try {

            if (mPhotos.size() == 1) {
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(0))

                        .into(mImage2);
                mRmv2.setVisibility(View.VISIBLE);
                mAddImage2.setVisibility(View.GONE);
                img2 = mPhotos.get(0);
                saveToSp("image2", mPhotos.get(0));
            } else if (mPhotos.size() == 2) {
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(0))

                        .into(mImage2);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(1))

                        .into(mImage3);
                mRmv2.setVisibility(View.VISIBLE);
                mRmv3.setVisibility(View.VISIBLE);
                mAddImage2.setVisibility(View.GONE);
                mAddImage3.setVisibility(View.GONE);
                img2 = mPhotos.get(0);
                img3 = mPhotos.get(1);
                saveToSp("image2", mPhotos.get(0));
                saveToSp("image3", mPhotos.get(1));
            } else if (mPhotos.size() == 3) {
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(0))

                        .into(mImage2);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(1))

                        .into(mImage3);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(2))

                        .into(mImage4);
                mRmv2.setVisibility(View.VISIBLE);
                mRmv3.setVisibility(View.VISIBLE);
                mRmv4.setVisibility(View.VISIBLE);
                mAddImage2.setVisibility(View.GONE);
                mAddImage3.setVisibility(View.GONE);
                mAddImage4.setVisibility(View.GONE);
                img2 = mPhotos.get(0);
                img3 = mPhotos.get(1);
                img4 = mPhotos.get(2);
                saveToSp("image2", mPhotos.get(0));
                saveToSp("image3", mPhotos.get(1));
                saveToSp("image4", mPhotos.get(2));
            } else if (mPhotos.size() == 4) {
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(0))

                        .into(mImage2);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(1))

                        .into(mImage3);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(2))

                        .into(mImage4);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(3))

                        .into(mImage5);
                mRmv2.setVisibility(View.VISIBLE);
                mRmv3.setVisibility(View.VISIBLE);
                mRmv4.setVisibility(View.VISIBLE);
                mRmv5.setVisibility(View.VISIBLE);
                mAddImage2.setVisibility(View.GONE);
                mAddImage3.setVisibility(View.GONE);
                mAddImage4.setVisibility(View.GONE);
                mAddImage5.setVisibility(View.GONE);
                img2 = mPhotos.get(0);
                img3 = mPhotos.get(1);
                img4 = mPhotos.get(2);
                img5 = mPhotos.get(3);
                saveToSp("image2", mPhotos.get(0));
                saveToSp("image3", mPhotos.get(1));
                saveToSp("image4", mPhotos.get(2));
                saveToSp("image5", mPhotos.get(3));
            } else if (mPhotos.size() == 5) {
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(0))

                        .into(mImage2);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(1))

                        .into(mImage3);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(2))

                        .into(mImage4);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(3))

                        .into(mImage5);
                Glide.with(EditProfileActivity.this)
                        .load(mPhotos.get(4))

                        .into(mImage6);
                mRmv2.setVisibility(View.VISIBLE);
                mRmv3.setVisibility(View.VISIBLE);
                mRmv4.setVisibility(View.VISIBLE);
                mRmv5.setVisibility(View.VISIBLE);
                mRmv6.setVisibility(View.VISIBLE);
                mAddImage2.setVisibility(View.GONE);
                mAddImage3.setVisibility(View.GONE);
                mAddImage4.setVisibility(View.GONE);
                mAddImage5.setVisibility(View.GONE);
                mAddImage6.setVisibility(View.GONE);
                img2 = mPhotos.get(0);
                img3 = mPhotos.get(1);
                img4 = mPhotos.get(2);
                img5 = mPhotos.get(3);
                img6 = mPhotos.get(4);
                saveToSp("image2", mPhotos.get(0));
                saveToSp("image3", mPhotos.get(1));
                saveToSp("image4", mPhotos.get(2));
                saveToSp("image5", mPhotos.get(3));
                saveToSp("image6", mPhotos.get(4));
            }

        } catch (Exception e) {
            Log.d("TH", "setLoadedPhotos: ");
        }

    }


    private void loadProfile(String url, String filePath) {

        if (mImageToSet != mImage1) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(EditProfileActivity.this, R.style.myDialog));
            alertDialogBuilder.setMessage("Would you like to move this photo to first?");
            alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    is_profile = "1";
                    dialog.dismiss();

                    uploadToServer(filePath, is_profile, mImageToSet, mImage1);


                }
            });
            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    is_profile = "0";

                    uploadToServer(filePath, is_profile, mImageToSet, null);

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else {
            is_profile = "1";
            uploadToServer(filePath, is_profile, mImageToSet, null);
        }


    }

    private void updateSp(String image, String sp_key) {
        editor = sp.edit();
        editor.putString(sp_key, image);
        editor.apply();

    }

    private void loadProfileDefault() {
        Glide.with(this).load(R.drawable.ic_add_image)
                .into(mImage2);
        mImage2.setColorFilter(ContextCompat.getColor(this, R.color.profile_default_tint));
    }

    private void loadImage() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();


    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(EditProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        //Appending ImageView


        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(EditProfileActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = null;
                if (data != null) {
                    uri = data.getParcelableExtra("path");
                }
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    // loading profile image from local cache
                    if (uri != null) {
                        String filePath = getRealPathFromURIPath(uri, EditProfileActivity.this);
                        loadProfile(uri.toString(), filePath);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onBackPressed() {

        if (edited) {
            if (editedShowAge) {
                updateUserProfileRequest.setShow_age(showAge);
            }

            if (editedShowLocation) {
                updateUserProfileRequest.setShow_location(showDistance);
            }

            updateDetails();
        } else {
            finish();
        }

    }

    private void updateDetails() {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(EditProfileActivity.this);
        builder.content("Saving ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();

        updateUserProfileRequest.setRequestType("UPDATE_DISCOVERY_SETTINGS");
        updateUserProfileRequest.setUserId(user_id);
        ApiCaller apicaller = Builder.getClient().create(ApiCaller.class);

        Call<UpdateUserProfileResponse> call = apicaller.updateUser(updateUserProfileRequest);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse upr = response.body();
                    if (upr != null && upr.getBody().equals("SUCCESS")) {
                        Toasty.success(getApplicationContext(), "Updated", Toasty.LENGTH_SHORT).show();
                        dialog.dismiss();
                        if (editedShowAge) {
                            saveToSp("show_age", showAge);
                        }

                        if (editedShowLocation) {
                            saveToSp("show_location", showDistance);
                        }
                        finish();


                    } else {
                        dialog.dismiss();
                        Toasty.warning(getApplicationContext(), "Something went wrong, try again", Toasty.LENGTH_SHORT).show();
                        finish();
                    }
                } else {
                    dialog.dismiss();
                    Toasty.warning(getApplicationContext(), "Something went wrong, try again", Toasty.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable t) {
                dialog.dismiss();
                Toasty.warning(getApplicationContext(), "Something went wrong, try again", Toasty.LENGTH_SHORT).show();
                finish();
            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;
            case R.id.personal_details_card:
                PersonalDetails.open(this, first_name, dob, user_id, gender);
                break;
            case R.id.bio_card:
                BioDetails.open(this, bio, user_id);
                break;
            case R.id.professional_card:
                ProDetails.open(this, job, company, college, user_id);
                break;
            case R.id.interest_card:
                InterestDetails.open(this, user_id, jsonStringInterests);
                break;
            case R.id.image1:
                spImage = 1;
                mAddImage = mAddImg1;
                mImageToSet = mImage1;
                mLayout = mImage1Holder;
                mRemove = mRmv1;
                sp_key = "image";
                loadImage();


                break;
            case R.id.image2:
                spImage = 2;


                mAddImage = mAddImage2;
                mRemove = mRmv2;
                mImageToSet = mImage2;
                mLayout = mImage2Holder;
                sp_key = "image2";
                loadImage();

                break;
            case R.id.image3:
                spImage = 3;


                mRemove = mRmv3;
                mAddImage = mAddImage3;
                mImageToSet = mImage3;
                mLayout = mImage3Holder;
                sp_key = "image3";
                mAsyncTask = new UploadImageAsync(EditProfileActivity.this).
                        activity().execute();
                loadImage();

                break;
            case R.id.image4:
                spImage = 4;

                mRemove = mRmv4;
                mAddImage = mAddImage4;
                mImageToSet = mImage4;
                mLayout = mImage4Holder;
                sp_key = "image4";
                loadImage();
                break;
            case R.id.image5:
                spImage = 5;
                mRemove = mRmv5;
                mAddImage = mAddImage5;
                mImageToSet = mImage5;
                mLayout = mImage5Holder;
                sp_key = "image5";
                loadImage();

                break;
            case R.id.image6:
                spImage = 6;
                mRemove = mRmv6;
                mAddImage = mAddImage6;
                mImageToSet = mImage6;
                mLayout = mImage6Holder;
                sp_key = "image6";
                loadImage();
                break;

            case R.id.switch_hide_myAge:
                TundaPlusBuy.open(this);
                break;
            case R.id.switch_hide_myDistance:
                TundaPlusBuy.open(this);
                break;
            case R.id.rmv_img_1:
                mLayout = mImage1Holder;

                if (img2 != null && !img2.isEmpty() || img3 != null && !img3.isEmpty() ||
                        img4 != null && !img4.isEmpty() || img5 != null && !img5.isEmpty() ||
                        img6 != null && !img6.isEmpty()) {
                    updateServer(image, mImage1, "image", mAddImg1, mRmv1, mLayout);
                } else {
                    spImage = 1;
                    mAddImage = mAddImg1;
                    mImageToSet = mImage1;
                    mLayout = mImage1Holder;
                    mRemove = mRmv1;
                    sp_key = "image";
                    loadImage();

                }


                break;

            case R.id.rmv_img_2:

                mLayout = mImage2Holder;
                updateServer(img2, mImage2, "image2", mAddImage2, mRmv2, mLayout);
                break;
            case R.id.rmv_img_3:
                mLayout = mImage3Holder;
                updateServer(img3, mImage3, "image3", mAddImage3, mRmv3, mLayout);
                break;
            case R.id.rmv_img_4:
                mLayout = mImage4Holder;
                updateServer(img4, mImage4, "image4", mAddImage4, mRmv4, mLayout);
                break;
            case R.id.rmv_img_5:
                mLayout = mImage5Holder;
                updateServer(img5, mImage5, "image5", mAddImage5, mRmv5, mLayout);
                break;
            case R.id.rmv_img_6:
                mLayout = mImage6Holder;
                updateServer(img6, mImage6, "image6", mAddImage6, mRmv6, mLayout);
                break;
            case R.id.no_ig_photos_holder:
                connectInstagram();
                break;
            case R.id.disconnect:
//                disconnectInstagram();
                break;


        }
    }

    private void disconnectInstagram() {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle("Disconnect instagram feed");
        builder.setTextGravity(Gravity.CENTER);
        builder.setMessage("People will not be able to see your instagram photos on Tunda. Continue?");
        builder.setCancelable(false);
        builder.addButton("Yes", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();



                    }
                }).addButton("Cancel", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

        builder.show();
    }

    private void connectInstagram() {
        auth_dialog = new AuthenticationDialog(EditProfileActivity.this, this);
        auth_dialog.setCancelable(true);
        auth_dialog.show();

    }

    private void updateServer(String image, ImageView mImage, String imageKey, ImageView mAdd, ImageView mRmv, RelativeLayout mLayout) {

        Log.d("TESTING", "updateServerx: " + image);
        String imageName = removeTillWord(image, "/user-photos/");
        Log.d("TESTING", "updateServer: " + imageName);

        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(EditProfileActivity.this);
        builder.content("Removing ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();

        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
        RemovePhotoRequest removePhotoRequest = new RemovePhotoRequest();
        removePhotoRequest.setRequestType("DELETE_USER_PHOTO");
        removePhotoRequest.setPhotoPath(imageName);
        removePhotoRequest.setUser_id(user_id);

        Call<RemovePhotoResponse> call = apiCaller.removePhoto(removePhotoRequest);
        call.enqueue(new Callback<RemovePhotoResponse>() {
            @Override
            public void onResponse(Call<RemovePhotoResponse> call, Response<RemovePhotoResponse> response) {
                if (response.isSuccessful()) {
                    RemovePhotoResponse rpr = response.body();
                    if (rpr.getBody().getStatusMessage().equals("SUCCESS")) {
                        dialog.dismiss();
                        mRmv.setVisibility(View.GONE);
                        mAdd.setVisibility(View.VISIBLE);
                        mLayout.setFocusable(true);
                        mLayout.setClickable(true);
                        mLayout.setFocusableInTouchMode(true);
                        updatePhotoCount();


                        AppUtils.showToast(getApplicationContext(), "Removed", false);

                        if (mImage == mImage1) {
                            emptyImageView(mImage);

                        } else {
                            emptyImageViewNormal(mImage);
                            editSp(imageKey);
                        }


                    } else {
                        dialog.dismiss();
                        AppUtils.showToast(getApplicationContext(), "Something Went wrong", false);
                    }

                } else {
                    dialog.dismiss();
                    AppUtils.showToast(getApplicationContext(), "Something Went wrong", false);
                }
            }

            @Override
            public void onFailure(Call<RemovePhotoResponse> call, Throwable throwable) {
                dialog.dismiss();
                AppUtils.showToast(getApplicationContext(), "Something Went wrong", false);

            }
        });


    }

    private void updatePhotoCount() {
        SharedPreferences.Editor editor;
        editor = sp.edit();
        editor.putInt("photo_count", photo_count--);
        editor.apply();


    }

    private void editSp(String image) {
        SharedPreferences.Editor editor;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        editor.remove(image);
        editor.apply();
    }

    private void emptyImageView(ImageView mImage) {


        if (mImage == mImage1) {
            if (mRmv2.getVisibility() == View.GONE) {
                if (mRmv3.getVisibility() == View.GONE) {
                    if (mRmv4.getVisibility() == View.GONE) {
                        if (mRmv5.getVisibility() == View.GONE) {
                            if (mRmv6.getVisibility() == View.GONE) {
                                Log.d("TEST", "emptyImageView: Done");


                            } else {
                                moveToFirst("image6", img6, mImage6, mRmv6, mAddImage6);
                                img6 = null;
                            }

                        } else {
                            moveToFirst("image5", img5, mImage5, mRmv5, mAddImage5);
                            img5 = null;
                        }

                    } else {
                        moveToFirst("image4", img4, mImage4, mRmv4, mAddImage4);
                        img4 = null;
                    }

                } else {
                    moveToFirst("image3", img3, mImage3, mRmv3, mAddImage3);
                    img3 = null;
                }

            } else {
                moveToFirst("image2", img2, mImage2, mRmv2, mAddImage2);
                img2 = null;

            }
        }
        mImage.setImageResource(0);
    }

    private void emptyImageViewNormal(ImageView mImage) {


        mImage.setImageResource(0);
    }


    private void moveToFirst(String key, String imgToMove, ImageView image, ImageView mRmv, ImageView mAdd) {
        Glide.with(getApplicationContext())
                .load(imgToMove)
                .into(mImage1);


        image.setImageResource(0);
        mAddImg1.setVisibility(View.GONE);
        mRmv1.setVisibility(View.VISIBLE);
        mRmv.setVisibility(View.GONE);
        mAdd.setVisibility(View.VISIBLE);
        Log.d("PHOTO_SAVE", "moveToFirst: imgToMove" + img2);
        updateSp(imgToMove, "image");
        editSp(key);

        imgToMove = "";


    }


    private void uploadToServer(String filePath, String is_profilex, ImageView toSet, ImageView first) {
        Log.d("PHOTOSV", "uploadToServer: sp_key=" + sp_key);
        Log.d("TESTING", "uploadToServer: " + filePath);
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(EditProfileActivity.this);
        builder.content("Uploading ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        ApiCaller apiCaller = Builder2.getClient().create(ApiCaller.class);
        //Create a file object using file path
        File file = new File(filePath);
        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("profile_picture", file.getName(), fileReqBody);
        //Create request body with text description and text media type
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");
        RequestBody mUserId = RequestBody.create(MediaType.parse("text/plain"), user_id);


        RequestBody mIsProfile = RequestBody.create(MediaType.parse("text/plain"), is_profilex);
        //

        Call<ImageUploadResponse> call = apiCaller.uploadImage(fileToUpload, description, mUserId, mIsProfile);
        call.enqueue(new Callback<ImageUploadResponse>() {

            @Override
            public void onResponse(@NonNull Call<ImageUploadResponse> call, @NonNull Response<ImageUploadResponse> response) {
                if (response.isSuccessful()) {
                    ImageUploadResponse upr = response.body();
                    if (upr.getBody().getStatusMessage().equals("SUCCESS")) {
                        dialog.dismiss();
                        if (first != null) {


                            if (!image.isEmpty()) {
                                Log.d("TESTING", "image not null: " + image);
                                Glide.with(EditProfileActivity.this).load(upr.getBody().getPhotoPath()).into(first);
                                Glide.with(EditProfileActivity.this).load(image).into(toSet);
                                new UploadImageAsync(EditProfileActivity.this).passString(image);
                                mAddImg1.setVisibility(View.GONE);
                                mRmv1.setVisibility(View.VISIBLE);
                                mImage1Holder.setFocusable(false);
                                mImage1Holder.setClickable(false);
                                mAddImage.setVisibility(View.GONE);
                                mRemove.setVisibility(View.VISIBLE);
                                mLayout.setClickable(false);
                                mLayout.setFocusable(false);
                                toSet.setColorFilter(ContextCompat.getColor(EditProfileActivity.this, android.R.color.transparent));
                                String imageProfile = upr.getBody().getPhotoPath();

                                updateSp(imageProfile, "image");
                                saveToSp(sp_key, image);
                                Log.d("PHOTOSV", "onResponse: sp_key=" + sp_key);


                            } else {
                                Log.d("TESTING", "image null: " + image);
                                spImage = 1;
                                Glide.with(EditProfileActivity.this).load(upr.getBody().getPhotoPath()).into(first);
                                new UploadImageAsync(EditProfileActivity.this).passString(upr.getBody().getPhotoPath());
                                mAddImg1.setVisibility(View.GONE);
                                mRmv1.setVisibility(View.VISIBLE);
                                mImage1Holder.setFocusable(false);
                                mImage1Holder.setClickable(false);
                                mAddImage.setVisibility(View.VISIBLE);
                                mRemove.setVisibility(View.GONE);
                                mLayout.setClickable(true);
                                mLayout.setFocusable(true);
                                image = upr.getBody().getPhotoPath();
                                updateSp(image, "image");


                            }


                        } else {
                            new UploadImageAsync(EditProfileActivity.this).passString(upr.getBody().getPhotoPath());
                            Log.d("TESTING", "spImage: " + spImage + ">>  img2>>" + img2);
                            Glide.with(EditProfileActivity.this).load(upr.getBody().getPhotoPath())
                                    .into(mImageToSet);
                            mAddImage.setVisibility(View.GONE);
                            mRemove.setVisibility(View.VISIBLE);
                            mLayout.setClickable(false);
                            mLayout.setFocusable(false);
                            toSet.setColorFilter(ContextCompat.getColor(EditProfileActivity.this, android.R.color.transparent));
                            saveToSp(sp_key, upr.getBody().getPhotoPath());

                        }


                        AppUtils.showToast(getApplicationContext(), "Saved", false);
                    } else {
                        dialog.dismiss();
                        AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);
                    }

                } else {
                    dialog.dismiss();
                    AppUtils.showToast(getApplicationContext(), "Something went wron, please try again", false);
                }

            }

            @Override
            public void onFailure(@NonNull Call<ImageUploadResponse> call, @NonNull Throwable throwable) {
                Log.d(TAG, "onFailure: " + throwable.getMessage());
                dialog.dismiss();
                AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);

            }
        });


    }

    private void saveToSp(String key, String value) {
        Log.d("PHOTOSV", "saveToSp: key=" + key + "val=" + value);
        editor.putString(key, value);
        editor.apply();

    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void calculateAge(String mDob) {
        Calendar calendar = Calendar.getInstance();
        Log.d("TESTING", mDob);
        String DAY;

        String YEAR = mDob.substring(0, 4);
        Log.d("TESTING", "year=" + YEAR);
        String MONTH = mDob.substring(5, 7);
        if (MONTH.contains("-")) {
            MONTH = MONTH.replace("-", "");
            Log.d("TESTING", "month=" + MONTH);
            DAY = mDob.substring(7, mDob.length());
        } else {
            DAY = mDob.substring(8, mDob.length());
        }


        Calendar x = new GregorianCalendar(Integer.valueOf(YEAR), Integer.valueOf(MONTH), Integer.valueOf(DAY));

        Log.d("TESTING", YEAR + "/" + MONTH + "/" + DAY);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));

        Date date = x.getTime();

//        date.setYear();
//        date.setMonth();
//        date.setDate();

        Log.d("TESTING", String.valueOf(date) + ">>" + date.getYear());

        Date currentDate = new Date();


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(date));
        int d2 = Integer.parseInt(formatter.format(currentDate));
        int age = (d2 - d1) / 10000;
        mAge.setText(String.valueOf(age));


    }

    public static String  removeTillWord(String input, String word) {
        String x = "https://portal.tunda.mobi/user-photos/";
        Log.d("TESTING", "removeTillWord: " + input.substring(x.length(), input.length()));

        return input.substring(x.length());

    }

    @Override
    public void onCodeReceived(String auth_code) {
        if (auth_code == null) {
            auth_dialog.dismiss();
            Toast.makeText(getApplicationContext(), "problem connecting", Toast.LENGTH_SHORT).show();
        } else {
            getAccessToken(auth_code);
        }

    }


    private void getAccessToken(String access_token) {
        pd = new ProgressDialog(EditProfileActivity.this);
        pd.setMessage("please, wait ... ");
        pd.show();
        RequestBody app_id_body = RequestBody.create(MediaType.parse("text/plain"), Constants.CLIENT_ID);
        RequestBody app_secret_body = RequestBody.create(MediaType.parse("text/plain"), Constants.APP_SECRET);
        RequestBody grant_type_body = RequestBody.create(MediaType.parse("text/plain"), Constants.GRANT_TYPE);
        RequestBody redirect_uri_body = RequestBody.create(MediaType.parse("text/plain"), Constants.REDIRECT_URI);
        RequestBody access_token_body = RequestBody.create(MediaType.parse("text/plain"), access_token);
        Call<AccessTokenResponse> call = retrofitService.
                getAccessToken(app_id_body,
                        app_secret_body,
                        grant_type_body,
                        redirect_uri_body,
                        access_token_body);
        call.enqueue(new Callback<AccessTokenResponse>() {
            @Override
            public void onResponse(Call<AccessTokenResponse> call, Response<AccessTokenResponse> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    AccessTokenResponse accessTokenResponse = response.body();
                    if (accessTokenResponse != null) {
                        String user_id = accessTokenResponse.getUserId();
                        String access_token = accessTokenResponse.getAccessToken();
                        Log.d(TAG, "onResponse: user_id = " + user_id);
                        Log.d(TAG, "onResponse: access_token = " + access_token);
                        getUserMedia(user_id, access_token);

                    } else {
                        Log.d(TAG, "onResponse:accesstoken null  ");
                    }


                }
            }

            @Override
            public void onFailure(Call<AccessTokenResponse> call, Throwable t) {
                pd.dismiss();
                Log.d(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    private void getUserMedia(String user_id, final String access_token) {
        List<String> field = new ArrayList<>();
        field.add("id");
        field.add("caption");
        Call<MediaRepsonse> mediaRepsonseCall = retrofitServiceGraph.getUserMedia(android.text.TextUtils.join(",", field), access_token);
        mediaRepsonseCall.enqueue(new Callback<MediaRepsonse>() {
            @Override
            public void onResponse(Call<MediaRepsonse> call, Response<MediaRepsonse> response) {
                if (response.isSuccessful()) {
                    MediaRepsonse mediaRepsonse = response.body();
                    if (mediaRepsonse != null) {
                        data = mediaRepsonse.getData();
                        loadUserFeed(data, access_token);


                    }


                }

            }

            @Override
            public void onFailure(Call<MediaRepsonse> call, Throwable t) {

            }
        });


    }

    private void loadUserFeed(Datum[] data, String access_token) {
        mSize = data.length;
        for (Datum aData : data) {
            new getMediaInfo(aData, access_token).execute();
        }
    }

    private class getMediaInfo extends AsyncTask<Void, Void, Void> {
        private Datum datum;
        private String access;
        List<String> fields;


        public getMediaInfo(Datum datum, String access_token) {
            this.datum = datum;
            this.access = access_token;
            fields = new ArrayList<>();
            ;
            fields.add("id");
            fields.add("media_type");
            fields.add("media_url");
            fields.add("username");
            fields.add("timestamp");

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<PhotoResponse> call = retrofitServiceGraph.getMediaInfo(datum.getId(), android.text.TextUtils.join(",", fields),
                    access);
            call.enqueue(new Callback<PhotoResponse>() {
                @Override
                public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {
                    if (response.isSuccessful()) {
                        PhotoResponse photoResponse = response.body();
                        if (photoResponse != null) {


                            Log.d("MMADAP", "onResponse: mphoto size = " + mPhotos.size());
                            Log.d("MMADAP", "onResponse: m size = " + mSize);
                            mPhotos.add(photoResponse.getMediaUrl()+"~~"+photoResponse.getTimestamp());
                            if (mPhotos.size() == mSize) {
                                mIgPhotoCount.setText(getResources().getString(R.string.ig_photo_count, String.valueOf(mPhotos.size())));
                                mNoIgPhotosHolder.setVisibility(View.GONE);
                                mIgPhotosHolder.setVisibility(View.VISIBLE);

                                new sendIgPhotos(mPhotos).execute();
                                for (int n = 0; n < mPhotos.size(); n += limit) {
                                    int chunkSize = Math.min(mPhotos.size(), n + limit);
                                    Log.d("MMADAP", "onResponse: n" + n);
                                    Log.d("MMADAP", "onResponse: chunksize" + chunkSize);
                                    List<String> chunk = mPhotos.subList(n, chunkSize);
                                    Log.d("MMADAP", "onResponse: chunk" + chunk.size());
                                    SectionsList sectionsList = new SectionsList();
                                    sectionsList.setPage(page++);
                                    sectionsList.setuRls(chunk);
                                    mList.add(sectionsList);

                                    multiplePhotoAdapter.addPhoto(mList,first_name,image);
                                    addDotsIndicator(0);
                                    mList.clear();
                                }
                            }

//

                        }


                    }
                }

                @Override
                public void onFailure(Call<PhotoResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });
            return null;
        }
    }

    private class sendIgPhotos extends AsyncTask<Void, Void, Void> {
        private ArrayList<String> photoList;

        public sendIgPhotos(ArrayList<String> mPhotos) {
            this.photoList = mPhotos;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            SendIgPhotosRequest sendIgPhotosRequest = new SendIgPhotosRequest();
            sendIgPhotosRequest.setUserId(user_id_int);
            sendIgPhotosRequest.setRequestType("ADD_INSTAGRAM_PHOTOS");
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            jsonStringPhootos = gson.toJson(photoList);
            sendIgPhotosRequest.setPhotoPath(jsonStringPhootos);
            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

            Call<BaseResponse> call = apiCaller.sendIgPhotos(sendIgPhotosRequest);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) {
                        BaseResponse baseResponse = response.body();
                        if (baseResponse != null && baseResponse.getStatusMessage() != null && baseResponse.getStatusMessage().equals("SUCCESS")) {
                            Log.d(TAG, "onResponse: photos successfully added");
                            editor.putBoolean("has_ig_photos",true);
                            editor.commit();


                        } else {
                            if (baseResponse != null) {
                                Log.d(TAG, "onResponse: " + baseResponse.getStatusMessage());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });

            return null;
        }
    }


    private class UploadImageAsync extends AsyncTask<String, Integer, Boolean> {

        private Context context;

        private UploadImageAsync(@NonNull Context context) {
            this.context = context;
        }

        private UploadImageAsync activity() {
            return this;
        }


        @Override
        protected Boolean doInBackground(String... strings) {
            loadImage();


            return null;
        }


        private void passString(String string) {
            if (spImage == 1) {
                image = string;
            } else if (spImage == 2) {
                img2 = string;
            } else if (spImage == 3) {
                img3 = string;
            } else if (spImage == 4) {
                img4 = string;
            } else if (spImage == 5) {
                img5 = string;
            } else if (spImage == 6) {
                img6 = string;
            }


        }


    }


    private void getPackageId() {
        ApiCaller apicaller = Builder.getClient().create(ApiCaller.class);
        PackageIdParams packageIdParams = new PackageIdParams();
        packageIdParams.setUserId(user_id_int);
        packageIdParams.setRequest_type("GET_USER_PACKAGE");

        Call<PackageResponse> call = apicaller.getPackageId(packageIdParams);
        call.enqueue(new Callback<PackageResponse>() {
            @Override
            public void onResponse(Call<PackageResponse> call, Response<PackageResponse> response) {
                if (response.isSuccessful()) {
                    PackageResponse packageResponse = response.body();
                    if (packageResponse != null) {
                        packageId = packageResponse.getBody().get(0).getPackageId();
                        editor.putInt("package_id", packageId);
                        editor.commit();

                    } else {
                        Log.d(TAG, "onResponse: ");
                    }
                }
            }

            @Override
            public void onFailure(Call<PackageResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppLock.onActivityResumed(this);
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {


        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    public void addDotsIndicator(int position) {

        int dotSize = (int) Math.ceil(mSize / limit);

        mDots = new TextView[dotSize];
        mDotLayout.removeAllViews();


        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(45);
            mDots[i].setTextColor(Color.parseColor("#9E9E9E"));
            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.colorPrimary));
        }


    }


    private class getIgPhotos extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            GetIgPhotosRequest getIgPhotosRequest = new GetIgPhotosRequest();
            getIgPhotosRequest.setRequestType("GET_INSTAGRAM_PHOTOS");
            getIgPhotosRequest.setUserId(user_id_int);
            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
            Call<GetIgPhotoResponse> getIgPhotoResponseCall = apiCaller.getIgPhotos(getIgPhotosRequest);
            getIgPhotoResponseCall.enqueue(new Callback<GetIgPhotoResponse>() {
                @Override
                public void onResponse(Call<GetIgPhotoResponse> call, Response<GetIgPhotoResponse> response) {
                    if (response.isSuccessful()) {
                        GetIgPhotoResponse getIgPhotoResponse = response.body();
                        if (getIgPhotoResponse != null && getIgPhotoResponse.getStatusCode().equals("00")) {
                            int mSizeServer = getIgPhotoResponse.getBody().size();
                            String mJsonUrls = getIgPhotoResponse.getBody().get(0).getPhotoPath();

                            try {
                                JSONArray jr = null;
                                String url = null;
                                jr = new JSONArray(mJsonUrls);

                                for (int i = 0; i < jr.length(); i++) {
                                    url = jr.getString(i);
                                    mPhotos.add(url);
                                }

                                if (mPhotos.size() > 0) {
                                    mSize = mPhotos.size();
                                    mNoIgPhotosHolder.setVisibility(View.GONE);
                                    mIgPhotosHolder.setVisibility(View.VISIBLE);
                                    mIgPhotoCount.setText(getResources().getString(R.string.ig_photo_count, String.valueOf(mPhotos.size())));

                                    Log.d("MMADAP", "onResponse: mphoto size = " + mPhotos.size());
                                    Log.d("MMADAP", "onResponse: m size = " + mSizeServer);
                                    for (int n = 0; n < mPhotos.size(); n += limit) {
                                        int chunkSize = Math.min(mPhotos.size(), n + limit);
                                        Log.d("MMADAP", "onResponse: n" + n);
                                        Log.d("MMADAP", "onResponse: chunksize" + chunkSize);
                                        List<String> chunk = mPhotos.subList(n, chunkSize);
                                        Log.d("MMADAP", "onResponse: chunk" + chunk.size());
                                        SectionsList sectionsList = new SectionsList();
                                        sectionsList.setPage(page++);
                                        sectionsList.setuRls(chunk);
                                        mList.add(sectionsList);

                                        multiplePhotoAdapter.addPhoto(mList,first_name,image);
                                        addDotsIndicator(0);
                                        mList.clear();
                                    }

                                }


                            } catch (Exception e) {
                                Log.d(TAG, "onResponse: " + e.getMessage());

                            }


                        } else {
                            if (getIgPhotoResponse != null) {
                                Log.d(TAG, "onResponse: " + getIgPhotoResponse.getStatusCode());
                                mNoIgPhotosHolder.setVisibility(View.VISIBLE);
                                mIgPhotosHolder.setVisibility(View.GONE);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetIgPhotoResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });
            return null;
        }
    }


}
