

package ke.co.tunda.Activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.RegisterUserParams;
import ke.co.tunda.ApiConnector.Models.UpdateLocationResponse;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.utils.MyLocation;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity implements LocationListener {


    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1234;
    private static final int ACTION_LOCATION_REQUEST = 12345;
    private static final int SPLASH_TIMEOUT = 2000;
    private AlertDialog alertDialog;
//    private RotateLoading newtonCradleLoading;


    SharedPreferences sp;
    SharedPreferences.Editor editor;
    int user_id;


    private AsyncTask mAsyncTask;
    private Bundle args;
    private int position;
    private String TAG = "SPLCL";
    private String countryName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        args = getIntent().getExtras();


        if (args != null && !args.isEmpty()) {
            position = args.getInt("position");

        } else {
            position = 1;

        }

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        user_id = sp.getInt("user_id", 0);

        countryName = sp.getString("country", "");


        if (!Preferences.get(SplashActivity.this).isLocationSet()) {
            mAsyncTask = new BackgroundSplashTask().execute();
        } else {
            openApp();

        }
    }

    @Override
    protected void onDestroy() {
        if (mAsyncTask != null) {
            mAsyncTask.cancel(true);
        }

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            getAddress(location.getLatitude(), location.getLongitude());
        }
    }

    private class BackgroundSplashTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            checkLocationEnabled();
            return null;
        }


    }

    private void checkLocationEnabled() {
        LocationManager lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showAlert();
                }
            });


        } else {


            getLocation();

        }
    }

    private void showAlert() {

        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle("Enable Location");
        builder.setTextGravity(Gravity.CENTER);
        builder.setMessage("To find people near you, Tunda needs to access your location. Enable it in Location Settings");
        builder.setCancelable(false);
        builder.addButton("Enable", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (canToggleGPS()) {
                            turnGPSOn();
                        } else {

                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), ACTION_LOCATION_REQUEST);


                        }


                    }
                });

        builder.show();


    }


    private boolean canToggleGPS() {
        PackageManager pacman = getPackageManager();
        PackageInfo pacInfo;

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
        } catch (PackageManager.NameNotFoundException e) {
            return false; //package not found
        }

        if (pacInfo != null) {
            for (ActivityInfo actInfo : pacInfo.receivers) {
                //test if recevier is exported. if so, we can toggle GPS.
                if (actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported) {
                    return true;
                }
            }
        }

        return false; //default
    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTION_LOCATION_REQUEST) {
                checkLocationEnabled();

        }
    }


    private void getLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            requestPermission();

        } else {
            fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                        getAddress(location.getLatitude(), location.getLongitude());
                        Log.d(TAG, "onSuccess:getLoc lat = "+location.getLatitude()+"lon = "+location.getLongitude());


                    } else {

                        option2();


                    }

                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    option2();

                }
            });

        }

    }


    public void option2() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
                    @Override
                    public void gotLocation(Location loc) {

                        if (loc != null) {

                            getAddress(loc.getLatitude(), loc.getLongitude());
                        } else {
                            openApp();
                        }

                    }
                };
                MyLocation myLocation = new MyLocation();
                myLocation.getLocation(SplashActivity.this, locationResult);


            }
        });

    }


    public void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);

    }


    private class sendLocationToServer extends AsyncTask<Void, Void, Void> {
        Double lat, lng;
        String address;

        public sendLocationToServer(double lat, double lng, String address) {
            this.lat = lat;
            this.lng = lng;
            this.address = address;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "doInBackground: loc");
            ArrayList<String> latlng = new ArrayList<String>();
            latlng.add(String.valueOf(lat));
            latlng.add(String.valueOf(lng));

            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

            RegisterUserParams registerUserParams = new RegisterUserParams();
            registerUserParams.setLatitude(lat);
            registerUserParams.setLongitude(lng);
            registerUserParams.setLast_location(address);
            registerUserParams.setUser_id(user_id);
            registerUserParams.setRequestType("UPDATE_LOCATION");
            retrofit2.Call<UpdateLocationResponse> call = apiCaller.updateLocation(registerUserParams);
            call.enqueue(new Callback<UpdateLocationResponse>() {
                @Override
                public void onResponse(@NonNull retrofit2.Call<UpdateLocationResponse> call, @NonNull Response<UpdateLocationResponse> response) {
                    UpdateLocationResponse updateLocationResponse = response.body();
                    if (updateLocationResponse != null && updateLocationResponse.getBody().equals("SUCCESS")) {
                        Log.d(TAG, "location onResponse: " + updateLocationResponse.getBody());
                        openApp();


                    } else {
                        Log.d(TAG, "response return null: ");
                        openApp();

                    }

                }

                @Override
                public void onFailure(@NonNull retrofit2.Call<UpdateLocationResponse> call, @NonNull Throwable t) {
                    Log.d(TAG, t.getMessage());
                    openApp();

                }
            });
            return null;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkLocationEnabled();
                } else {
                    getLocation();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
        super.onPause();
    }


    private void openApp() {

        Intent i = new Intent(SplashActivity.this,
                MainActivity.class);
        i.putExtra("position", position);
        startActivity(i);
        finish();

    }


    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(SplashActivity.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                Log.d(TAG, "getAddress: addresses");

                Address obj = addresses.get(0);
                String add = obj.getAddressLine(0);
                String locality = obj.getAdminArea();
                String country = obj.getCountryName();

                if (country != null && !country.isEmpty()) {
                    editor.putString("country", country);
                    editor.apply();
                }

                if (add != null) {
                    new sendLocationToServer(lat, lng, add).execute();
                    editor.putString("address", add);
                    editor.apply();
                } else {
                    if (locality != null) {
                        new sendLocationToServer(lat, lng, locality).execute();
                        editor.putString("address", locality);
                        editor.apply();
                    } else {
                        Log.d(TAG, "getAddress: open");
                        openApp();
                    }
                }
            } else {
                Log.d(TAG, "getAddress: addresses empty");
                new sendLocationToServer(lat, lng, "").execute();



            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d(TAG, "getAddress: " + e.getMessage());
            openApp();
        }
    }

}
