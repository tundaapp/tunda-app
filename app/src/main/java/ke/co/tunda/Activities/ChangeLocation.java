/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/22/19 12:34 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.guardanis.applock.AppLock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.RegisterUserParams;
import ke.co.tunda.ApiConnector.Models.UpdateLocationResponse;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.utils.AppUtils;
import retrofit2.Callback;
import retrofit2.Response;

//import com.google.android.libraries.places.api.Places;

public class ChangeLocation extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.edittxt_location)
    EditText mLocationEdittext;
    @BindView(R.id.edit_location)
    ImageView mEditLocation;
    @BindView(R.id.edit_location_check)
    ImageView mEditLocationCheck;


    private static String mUser_Id;
    private ApiCaller apiCaller;
    private static String mAddress;
    RelativeLayout.LayoutParams params;
    RelativeLayout.LayoutParams params1;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    List<Place.Field> fields;
    Place place;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    int user_id;
    private boolean isEdited = false;
    private static final String PLACES_KEY = "AIzaSyDhg8xpEErM9oCkvXMDm22rtoLrjvkRNFs";


    public static void open(Context context, String user_id, String address) {
        context.startActivity(new Intent(context, ChangeLocation.class));
        mUser_Id = user_id;
        mAddress = address;

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_change_location);
        ButterKnife.bind(this);
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        user_id = sp.getInt("user_id", 0);

        apiCaller = Builder.getClient().create(ApiCaller.class);
        // Initialize Places.
        Places.initialize(this, PLACES_KEY);
        PlacesClient placesClient = Places.createClient(this);

        mLocationEdittext.setText(mAddress);

        mBack.setOnClickListener(this);
        mEditLocation.setOnClickListener(this);
        mEditLocationCheck.setOnClickListener(this);

        params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.LEFT_OF, R.id.edit_location_check);


        params1 = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params1.addRule(RelativeLayout.LEFT_OF, R.id.edit_location);
        mLocationEdittext.setLayoutParams(params1);

        fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.ID, Place.Field.NAME);


    }

    @Override
    public void onBackPressed() {

        if (isEdited) {
            updateLocation(place);
        } else {

            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;
            case R.id.edit_location:
                isEdited = true;
                mLocationEdittext.setLayoutParams(params);
                showKeyboard(mLocationEdittext);
                launchPlaces();
                break;
            case R.id.edit_location_check:
                mLocationEdittext.setLayoutParams(params1);

                updateLocation(place);
                break;

        }

    }

    private void updateLocation(Place place) {

        if (place != null) {
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
            builder.setTitle("Change Location");
            builder.setTextGravity(Gravity.CENTER);
            builder.setMessage("Set this as your location on Tunda?");
            builder.setCancelable(false);
            builder.addButton("Sure", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            LatLng latLng = place.getLatLng();
                            Double lat = latLng.latitude;
                            Double lng = latLng.longitude;
                            String address = place.getName();
                            sendlocationtoserver(lat, lng, address);


                        }
                    }).addButton("Cancel", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            isEdited = false;
                            mEditLocation.setVisibility(View.VISIBLE);
                            mEditLocationCheck.setVisibility(View.GONE);

                        }
                    });

            builder.show();
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ChangeLocation.this, R.style.myDialog));
//            alertDialogBuilder.setMessage("Set this as your Location on Tunda");
//            alertDialogBuilder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    LatLng latLng = place.getLatLng();
//                    Double lat = latLng.latitude;
//                    Double lng = latLng.longitude;
//                    String address = place.getName();
//                    sendlocationtoserver(lat, lng, address);
//
//                }
//            });
//            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
////                hideSoftKeyboard(mLocationEdittext);
//                    isEdited = false;
//                    mEditLocation.setVisibility(View.VISIBLE);
//                    mEditLocationCheck.setVisibility(View.GONE);
//                    dialog.dismiss();
//
//                }
//            });
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
        } else {
            isEdited = false;
        }


    }

    private void sendlocationtoserver(double lat, double lng, String address) {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ChangeLocation.this);
        builder.content("Saving ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        ArrayList<String> latlng = new ArrayList<String>();
        latlng.add(String.valueOf(lat));
        latlng.add(String.valueOf(lng));

        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

        RegisterUserParams registerUserParams = new RegisterUserParams();
        registerUserParams.setLatitude(lat);
        registerUserParams.setLongitude(lng);
        registerUserParams.setLast_location(address);
        registerUserParams.setUser_id(user_id);
        registerUserParams.setRequestType("UPDATE_LOCATION");
        retrofit2.Call<UpdateLocationResponse> call = apiCaller.updateLocation(registerUserParams);
        call.enqueue(new Callback<UpdateLocationResponse>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<UpdateLocationResponse> call, @NonNull Response<UpdateLocationResponse> response) {
                UpdateLocationResponse updateLocationResponse = response.body();
                if (updateLocationResponse != null && updateLocationResponse.getBody().equals("SUCCESS")) {
                    dialog.dismiss();
                    Preferences.get(ChangeLocation.this).setDiscoveryChanged(true);
                    Toasty.success(getApplicationContext(), "Location Changed").show();
                    Preferences.get(ChangeLocation.this).setLocationAsSet(true);
                    isEdited = false;

                    mEditLocation.setVisibility(View.VISIBLE);
                    mEditLocationCheck.setVisibility(View.GONE);
                    mLocationEdittext.setText(address);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("address", address);
                    editor.apply();

                    Log.d("LOCATION", "location onResponse: " + updateLocationResponse.getBody());

                } else {
                    dialog.dismiss();
                    Toasty.warning(getApplicationContext(), "Something went wrong,try again").show();
                    isEdited = false;


                }

            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<UpdateLocationResponse> call, @NonNull Throwable t) {
                Log.d("LOCATION", t.getMessage());
                dialog.dismiss();
                Toasty.warning(getApplicationContext(), "Something went wrong,try again").show();
                isEdited = false;
            }
        });
    }

    private void launchPlaces() {
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    public void showKeyboard(final EditText ettext) {

        ettext.setClickable(true);
        ettext.setFocusable(true);
        ettext.setFocusableInTouchMode(true);
        ettext.requestFocus();
        ettext.hasFocus();
        ettext.setSelection(ettext.getText().length());

        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 200);
    }


    private void hideSoftKeyboard(EditText ettext) {
        ettext.setText("");
        ettext.setClickable(false);
        ettext.setFocusable(false);
        ettext.setFocusableInTouchMode(false);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }


    private void saveToSp(String key, String s) {
        SharedPreferences.Editor editor;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        editor.putString(key, s);
        editor.apply();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    place = Autocomplete.getPlaceFromIntent(data);
                    hideSoftKeyboard(mLocationEdittext);
                    mLocationEdittext.setText(place.getName());
                    mEditLocation.setVisibility(View.GONE);
                    mEditLocationCheck.setVisibility(View.VISIBLE);
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                AppUtils.showToast(getApplicationContext(), "Error Occured", false);
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                Toasty.warning(getApplicationContext(), "You Cancelled").show();
                // The user canceled the operation.
            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppLock.onActivityResumed(this);
    }
}
