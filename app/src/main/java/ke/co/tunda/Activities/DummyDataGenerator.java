

package ke.co.tunda.Activities;

import java.util.ArrayList;
import java.util.List;

import ke.co.tunda.ApiConnector.Models.Status;
import ke.co.tunda.ApiConnector.Models.UserStatus;

public class DummyDataGenerator {

    public static final String[] profileUrls = {"https://images.pexels.com/photos/949380/pexels-photo-949380.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
            "https://images.pexels.com/photos/2661255/pexels-photo-2661255.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
            "https://images.pexels.com/photos/2946598/pexels-photo-2946598.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
            "https://images.pexels.com/photos/1019411/pexels-photo-1019411.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"};

    public static final String[] names = {"Trisha",
            "Beryl",
            "Mueni",
            "Pamela"};

    public static final String[] time = {"5 minutes ago",
            "35 minutes ago",
            "Today, 10:15 PM",
            "Today, 6:15 AM"};
    public static ArrayList<UserStatus> generateStatuses() {
        ArrayList<UserStatus> userStatusList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {

            List<Status> statusList = new ArrayList<>();
            for (int j = 0; j < getCount(i); j++) {
                statusList.add(new Status(generateIsSeenForStatus(j, i)));
            }

            UserStatus userStatus = new UserStatus(names[i],profileUrls[i], genereateAreAllSeen(i), statusList,time[i]);
            userStatusList.add(userStatus);

        }
        return userStatusList;
    }


    private static int getCount(int i) {
        switch (i) {
            case 0:
                return 1;
            case 1:
                return 4;

            case 2:
                return 2;

            case 3:
                return 8;
            case 4:
                return 3;
            default:
                return 6;
        }
    }

    private static boolean genereateAreAllSeen(int i) {
        switch (i) {
            case 0:
                return false;
            case 1:
                return false;

            case 2:
                return true;

            case 3:
                return true;

            default:
                return false;
        }
    }

    private static boolean generateIsSeenForStatus(int i, int j) {
        if (j == 0) {
            return false;
        }
        if (j == 1) {
            if (i == 0)
                return true;
            return false;
        }

        return false;
    }

}
