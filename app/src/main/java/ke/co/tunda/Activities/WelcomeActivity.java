

package ke.co.tunda.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.guardanis.applock.dialogs.UnlockDialogBuilder;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder3;
import ke.co.tunda.Models.CountryModel;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private int position = 1;
    private Bundle bundle;
    private final static String TAG = "WEL";
    private ApiCaller apiCaller;

    @BindView(R.id.already_a_user)
    HtmlTextView mAlreadyUser;
    @BindView(R.id.get_started)
    Button mGetStarted;
    @BindView(R.id.loginto)
    Button mLoginto;
    Timer timer;
    int page = 0;


    public static void open(Context context) {
        context.startActivity(new Intent(context, WelcomeActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
        String countryName = sp.getString("country", "");
        apiCaller = Builder3.getClient().create(ApiCaller.class);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("position")) {
            position = getIntent().getExtras().getInt("position");
        } else {
            position = 1;
        }


        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        if (Preferences.get(this).isEnteringNo()) {
            startActivity(new Intent(WelcomeActivity.this, Authentication.class));
            finish();
            return;
        }


        if (countryName != null && countryName.isEmpty()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Call<CountryModel> call = apiCaller.checkCountry();
                    call.enqueue(new Callback<CountryModel>() {
                        @Override
                        public void onResponse(Call<CountryModel> call, Response<CountryModel> response) {
                            if (response.isSuccessful()) {
                                CountryModel countryModel = response.body();
                                if (countryModel != null) {
                                    editor.putString("country", countryModel.getCountry());
                                    editor.apply();


                                } else {

                                    Log.d(TAG, "onResponse: ");
                                }


                            } else {
                                Log.d(TAG, "onResponse: ");
                            }
                        }

                        @Override
                        public void onFailure(Call<CountryModel> call, Throwable t) {
                            Log.d(TAG, "onFailure: ");

                        }
                    });
                }
            }, 100);

        }


        if (!Preferences.get(getApplicationContext()).isFirstLaunch()) {

            if (Preferences.get(getApplicationContext()).isLoggedIn()) {

                if (!((Activity) WelcomeActivity.this).isFinishing()) {
                    new UnlockDialogBuilder(WelcomeActivity.this)
                            .onUnlocked(() -> {
                                launchApp();
                            })

                            .onCanceled(() -> {
                                Toasty.warning(getApplicationContext(), "Cancelled").show();
                            })
                            .showIfRequiredOrSuccess(TimeUnit.MILLISECONDS.toMinutes(1));
                }


            } else {
                if (!((Activity) WelcomeActivity.this).isFinishing()) {
                    new UnlockDialogBuilder(WelcomeActivity.this)
                            .onUnlocked(() -> {
                                launchHomeScreen();
                            })

                            .onCanceled(() -> {
                                Toasty.warning(getApplicationContext(), "Cancelled").show();
                            })
                            .showIfRequiredOrSuccess(TimeUnit.MILLISECONDS.toMinutes(1));
                }

            }

        } else {
            setContentView(R.layout.activity_welcome);
            ButterKnife.bind(this);


            viewPager = (ViewPager) findViewById(R.id.view_pager);
            dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
            btnSkip = (Button) findViewById(R.id.btn_skip);
            btnNext = (Button) findViewById(R.id.btn_next);
            mAlreadyUser.setHtml(getResources().getString(R.string.already_a_user));


            // layouts of all welcome sliders
            // add few more layouts if you want
            layouts = new int[]{
                    R.layout.welcome_slide1,
                    R.layout.welcome_slide2,
                    R.layout.welcome_slide3
            };

            // adding bottom dots
            addBottomDots(0);

            // making notification bar transparent
            changeStatusBarColor();

            myViewPagerAdapter = new MyViewPagerAdapter();
            viewPager.setAdapter(myViewPagerAdapter);
            viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
            pageSwitcher(5);

            btnSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    launchHomeScreen();
                }
            });

            mGetStarted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    launchHomeScreen();
                }
            });

            mLoginto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    launchHomeScreen();
                }
            });
        }


    }

    private void launchApp() {
        Intent router = new Intent(WelcomeActivity.this, SplashActivity.class);
        router.putExtra("position", position);
        startActivity(router);
        finish();
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

    }


    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
//            if (position == layouts.length - 1) {
//                // last page. make button text to GOT IT
//                btnNext.setText(getString(R.string.start));
////                btnSkip.setVisibility(View.GONE);
//            } else {
//                // still pages are left
//                btnNext.setText(getString(R.string.next));
//                btnSkip.setVisibility(View.VISIBLE);
//            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private class RemindTask extends TimerTask {
        @Override
        public void run() {


            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 3) {
                        page = 0;
//                        pageSwitcher(1);
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    public class CardTransformer implements ViewPager.PageTransformer {

        private final float scalingStart;

        public CardTransformer(float scalingStart) {
            super();
            this.scalingStart = 1 - scalingStart;
        }

        @Override
        public void transformPage(View page, float position) {

            if (position >= 0) {
                final int w = page.getWidth();
                float scaleFactor = 1 - scalingStart * position;

                page.setAlpha(1 - position);
                page.setScaleX(scaleFactor);
                page.setScaleY(scaleFactor);
                page.setTranslationX(w * (1 - position) - w);
            }
        }
    }
}
