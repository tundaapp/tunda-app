/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 6/29/19 8:01 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rm.rmswitch.RMSwitch;
import com.shuhart.stepview.StepView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Builder2;
import ke.co.tunda.ApiConnector.Models.RegisterUserResponse;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import com.bumptech.glide.Glide;
import ke.co.tunda.Helpers.ImageCrop.ImagePickerActivity;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.utils.AppUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PesonalInfo extends AppCompatActivity implements TagView.OnTagClickListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.step_view)
    StepView mStepView;
    @BindView(R.id.txt_name)
    TextView mTxtName;
    @BindView(R.id.txt_birthday)
    TextView mBirthday;
    @BindView(R.id.male_container)
    RelativeLayout mChooseFemale;
    @BindView(R.id.female_container)
    RelativeLayout mChooseMale;
    @BindView(R.id.l_layout1)
    RelativeLayout mL1;
    @BindView(R.id.dob_edit)
    EditText mDobEdit;
    @BindView(R.id.btn_submit_date)
    Button mSubmitDate;
    @BindView(R.id.l_layout2)
    RelativeLayout mL2;
    @BindView(R.id.l_layout3)
    RelativeLayout mL3;
    @BindView(R.id.l_layout4)
    RelativeLayout mL4;
    @BindView(R.id.l_layout5)
    RelativeLayout mL5;
    @BindView(R.id.layout6)
    RelativeLayout mL6;
    @BindView(R.id.switch_men)
    RMSwitch rmSwitchMen;
    @BindView(R.id.switch_women)
    RMSwitch rmSwitchWomen;
    @BindView(R.id.btn_submit_all)
    Button mButtonSubmit;
    @BindView(R.id.selected_txt)
    TextView mtxtSelected;
    @BindView(R.id.bio_edit)
    MaterialEditText mBioEdit;
    @BindView(R.id.btn_submit_bio)
    Button mSubmitBio;
    @BindView(R.id.btn_next_tags)
    Button mSubmitTags;
    @BindView(R.id.text_tag)
    MaterialEditText mEditTag;
    @BindView(R.id.btn_add_tag)
    Button mAddTag;
    @BindView(R.id.btn_next_join)
    Button mBtnNextJoin;

    @BindView(R.id.tagcontainerLayout)
    TagContainerLayout mTagContainerLayout;

    @BindView(R.id.img_profile)
    CircleImageView mImgProfile;
    @BindView(R.id.img_plus)
    CircleImageView mImgPlus;


    private String mGender;
    private String date_of_birth;
    private String TAG = "PE";
    private String mGenderIntereset;

    private int user_id;
    private String firstName;
    private ApiCaller apiCaller;
    private int package_id;
    private String image = "";
    private String origin;
    private String bio;
    String jsonInterests;

    public static final int REQUEST_IMAGE = 100;
    private static final int GALLERY_REQUEST_CODE = 1234;
    private static final int CAMERA_REQUEST_CODE = 4321;
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 1234;
    private String filePath = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_pesonal_info);
            ButterKnife.bind(this);

            user_id = getIntent().getIntExtra("id", 0);
            firstName = getIntent().getStringExtra("name");
            package_id = getIntent().getIntExtra("package_id", 1);
            origin = getIntent().getStringExtra("origin");


            Log.d(TAG, "onCreate: user_id" + user_id);
            Log.d(TAG, "onCreate: name" + firstName);
            Log.d(TAG, "onCreate: package_id" + package_id);
            Log.d(TAG, "onCreate: image" + image);

            if (firstName != null && !firstName.isEmpty()) {
                mTxtName.setText(getResources().getString(R.string.hey_don_your_are, firstName));
                mBirthday.setText(getResources().getString(R.string.txt_birthday, firstName));

            } else {
                mTxtName.setText(getResources().getString(R.string.hey_don_your_are, ""));
                mBirthday.setText(getResources().getString(R.string.txt_birthday, ""));
            }


            mChooseFemale.setOnClickListener(this);
            mChooseMale.setOnClickListener(this);
            mStepView.setStepsNumber(6);
            mDobEdit.setOnClickListener(this);
            mtxtSelected.setText("Women");
            mGenderIntereset = "FEMALE";
            mGender = "FEMALE";
            mSubmitDate.setOnClickListener(this);
            mButtonSubmit.setOnClickListener(this);
            mSubmitTags.setOnClickListener(this);
            mSubmitBio.setOnClickListener(this);
            mBtnNextJoin.setOnClickListener(this);
            mImgPlus.setOnClickListener(this);
            mImgProfile.setOnClickListener(this);
            mTagContainerLayout.setOnTagClickListener(this);

            if (Preferences.get(getApplicationContext()).isImageRemainingPes()) {
                mStepView.animate().alpha(1.0f);
                mStepView.setVisibility(View.VISIBLE);
                mStepView.go(5, true);

                mL6.setVisibility(View.VISIBLE);
            } else {
                mStepView.go(0, true);
                mL1.setVisibility(View.VISIBLE);
            }

            if (mDobEdit.getText() != null && !mDobEdit.getText().toString().isEmpty()) {
                mSubmitDate.setVisibility(View.VISIBLE);
            }

            mDobEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    mSubmitDate.setEnabled(false);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mSubmitDate.setEnabled(true);

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() <= 0) {
                        mSubmitDate.setEnabled(false);
                    }

                }
            });

            mEditTag.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    mAddTag.setEnabled(false);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mAddTag.setEnabled(true);
                    mSubmitTags.setEnabled(false);


                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() <= 0) {
                        mAddTag.setEnabled(false);
                        mSubmitTags.setEnabled(true);
                    }

                }
            });


            mAddTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mEditTag.getText() != null && !mEditTag.toString().isEmpty()) {
                        if (mEditTag.getText().toString().length() > 2) {

                            if (mEditTag.getText().toString().length() <= 18) {
                                mTagContainerLayout.addTag(mEditTag.getText().toString());
                                mEditTag.setText("");
                            } else {
                                mEditTag.setError("Be brief, remember you can add as many as you want");
                            }


                        } else {
                            mEditTag.setError("Interest must be more that 2 words");
                        }

                    } else {
                        mEditTag.setError("Add Interest");
                    }
                }
            });


            rmSwitchMen.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
                @Override
                public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                    if (isChecked) {
                        if (rmSwitchWomen.isChecked()) {
                            mtxtSelected.setText("Both");
                            mGenderIntereset = "BOTH";
                        } else {
                            mtxtSelected.setText("MEN");
                            mGenderIntereset = "MALE";
                        }
                    } else {
                        if (rmSwitchWomen.isChecked()) {
                            mtxtSelected.setText("WOMEN");
                            mGenderIntereset = "FEMALE";
                        } else {
                            rmSwitchWomen.setChecked(true);
                            mtxtSelected.setText("WOMEN");
                            mGenderIntereset = "FEMALE";
                        }
                    }
                }
            });

            rmSwitchWomen.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
                @Override
                public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                    if (isChecked) {
                        if (rmSwitchMen.isChecked()) {
                            mtxtSelected.setText("Both");
                            mGenderIntereset = "Both";
                        } else {
                            mtxtSelected.setText("WOMEN");
                            mGenderIntereset = "FEMALE";
                        }
                    } else {
                        if (rmSwitchWomen.isChecked()) {
                            mtxtSelected.setText("MEN");
                            mGenderIntereset = "MALE";
                        } else {
                            rmSwitchMen.setChecked(true);
                            mtxtSelected.setText("MEN");
                            mGenderIntereset = "MALE";
                        }
                    }
                }
            });


        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.male_container:

                mGender = "MALE";
                moveNext(1, mL1, mL2);
                break;
            case R.id.female_container:

                mGender = "FEMALE";
                moveNext(1, mL1, mL2);
                break;
            case R.id.dob_edit:
                openCalender();
                break;
            case R.id.btn_submit_date:

                moveNext(2, mL2, mL3);
                break;
            case R.id.btn_submit_all:
                moveNext(3, mL3, mL4);
                break;
            case R.id.btn_next_tags:
                try {

                    if (mTagContainerLayout.getTags() != null) {
                        if (jsonInterests != null && !jsonInterests.isEmpty()) {
                            jsonInterests = "";
                        }
                        List<String> interests = mTagContainerLayout.getTags();
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        jsonInterests = gson.toJson(interests);
                        Log.d("REG", "onClick: " + jsonInterests);
                        System.out.println(jsonInterests);

                        moveNext(4, mL4, mL5);
                    } else {
                        jsonInterests = null;
                        moveNext(4, mL4, mL5);

                    }


                } catch (Exception e) {
                    Log.d(TAG, "onClick: " + e.getMessage());

                }

                break;
            case R.id.btn_submit_bio:
                if (mBioEdit.getText() != null) {
                    bio = mBioEdit.getText().toString();
                } else {
                    bio = "";
                }
                submit();
                break;
            case R.id.img_plus:
                loadImage();
                break;
            case R.id.img_profile:
                loadImage();
                break;

            case R.id.btn_next_join:
                uploadPhoto();
                break;


        }

    }

    private void loadImage() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(PesonalInfo.this);
            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
            builder.setTitle("Upload Image");
            builder.setCancelable(true);
            builder.setTextGravity(Gravity.CENTER);
            builder.setMessage("Allow Tunda to upload your Image");
            builder.setCancelable(false);
            builder.addButton("Allow", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Dexter.withActivity(PesonalInfo.this)
                                    .withPermissions(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .withListener(new MultiplePermissionsListener() {
                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                                            if (report.areAllPermissionsGranted()) {
                                                showImagePickerOptions();
                                                dialog.dismiss();
                                            }

                                            if (report.isAnyPermissionPermanentlyDenied()) {
                                                dialog.dismiss();
                                                showSettingsDialog();
                                            }
                                        }


                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                            dialog.dismiss();
                                            token.continuePermissionRequest();
                                        }
                                    }).check();


                        }
                    });

            builder.show();
        } else {
            showImagePickerOptions();
        }


    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void showSettingsDialog() {

        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(PesonalInfo.this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle(R.string.dialog_permission_title);
        builder.setCancelable(true);
        builder.setTextGravity(Gravity.CENTER);
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setCancelable(false);
        builder.addButton(getString(R.string.go_to_settings), -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openSettings();
                        dialog.dismiss();
                    }
                });

        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(PesonalInfo.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        //Appending ImageView


        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(PesonalInfo.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void uploadPhoto() {
        if (filePath != null && filePath.isEmpty()) {
            AppUtils.showToast(getApplicationContext(), "No photo uploaded", true);
        } else {

//                    try {
            Log.d("TESTING", "uploadToServer: " + filePath);
            MaterialDialog dialog;
            MaterialDialog.Builder builder = new MaterialDialog.Builder(PesonalInfo.this);
            builder.content("Uploading ....")
                    .widgetColor(getResources().getColor(R.color.colorAccent))
                    .progress(true, 0);
            dialog = builder.build();
            dialog.setCancelable(false);
            dialog.show();
            ApiCaller apiCaller = Builder2.getClient().create(ApiCaller.class);
            //Create a file object using file path
            File file = new File(filePath);
            // Create a request body with file and image media type
            RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
            // Create MultipartBody.Part using file request-body,file name and part name
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("profile_picture", file.getName(), fileReqBody);
            //Create request body with text description and text media type
            RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");
            RequestBody mUserId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(user_id));
            Log.d(TAG, "onClick: " + user_id);
            RequestBody mIsProfile = RequestBody.create(MediaType.parse("text/plain"), "1");


            //


            Call<RegisterUserResponse> call = apiCaller.uploadImage1(fileToUpload, description, mUserId, mIsProfile);
            call.enqueue(new Callback<RegisterUserResponse>() {

                @Override
                public void onResponse(@NonNull Call<RegisterUserResponse> call, @NonNull Response<RegisterUserResponse> response) {
                    if (response.isSuccessful()) {
                        RegisterUserResponse upr = response.body();
                        if (upr.getBody().getStatusMessage().equals("SUCCESS")) {
                            dialog.dismiss();
                            savePhotoPath(upr.getBody().getPhoto_path());
                            Preferences.get(getApplicationContext()).setIsImageRemainingPes(false);
//                                    Glide.with(getApplicationContext()).load(upr.getBody().getPhoto_path()).into(mImgProfile);


                            Toasty.success(getApplicationContext(), "Done").show();
                        } else {
                            dialog.dismiss();
                            AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);
                        }

                    } else {
                        dialog.dismiss();
                        AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);
                    }

                }

                @Override
                public void onFailure(@NonNull Call<RegisterUserResponse> call, @NonNull Throwable throwable) {
                    Log.d("TESTING", "onFailure: " + throwable.getMessage());
                    dialog.dismiss();
                    AppUtils.showToast(getApplicationContext(), "Something went wrong, please try again", false);

                }
            });
//                    }catch (Exception e){
//                        Log.d("TESTING", e.getMessage());
//
//                    }

        }
    }

    private void submit() {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(PesonalInfo.this);
        builder.content("Logging you In ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        if (mGenderIntereset != null && !mGenderIntereset.isEmpty()) {
            if (date_of_birth != null && !date_of_birth.isEmpty()) {
                if (mGender != null && !mGender.isEmpty()) {

                    apiCaller = Builder.getClient().create(ApiCaller.class);


                    UpdateUserProfileRequest params = new UpdateUserProfileRequest();
                    Log.d(TAG, "submit: " + user_id);

                    String id_user = String.valueOf(user_id);

                    params.setGender(mGender);
                    params.setDob(date_of_birth);
                    params.setUserId(id_user);
                    params.setGenderOfInterest(mGenderIntereset);
                    params.setJsonInterest(jsonInterests);
                    params.setAboutMe(bio);
                    params.setRequestType("UPDATE_USER_PROFILE");


                    Call<UpdateUserProfileResponse> call = apiCaller.updateUser(params);
                    call.enqueue(new Callback<UpdateUserProfileResponse>() {
                        @Override
                        public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                            UpdateUserProfileResponse resp = response.body();
                            dialog.dismiss();
                            if (resp != null && resp.getBody().equals("SUCCESS")) {
                                saveToSp();
                                moveNext(5, mL5, mL6);
                                Preferences.get(getApplicationContext()).setIsImageRemainingPes(true);

                            } else {
                                Toasty.warning(getApplicationContext(), "Something Went wrong,try again").show();

                            }


                        }

                        @Override
                        public void onFailure(Call<UpdateUserProfileResponse> call, Throwable t) {
                            dialog.dismiss();
                            if (t.getMessage().contains("Failed to connect")) {
                                Toasty.warning(getApplicationContext(), "Something Went wrong,Check Internet and try again").show();
                            } else {
                                Toasty.warning(getApplicationContext(), "Something Went wrong,try again").show();
                            }


                        }
                    });

                } else {
                    dialog.dismiss();
                    Toasty.error(PesonalInfo.this, "Gender is empty").show();
                }

            } else {
                dialog.dismiss();
                Toasty.error(PesonalInfo.this, "select date of birth").show();
            }

        } else {
            dialog.dismiss();
            Toasty.error(PesonalInfo.this, "Select gender of interest").show();
        }
    }

    private void savePhotoPath(String photo_path) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(PesonalInfo.this);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("image", photo_path);
        editor.apply();
        joinUser();


    }

    private void joinUser() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(PesonalInfo.this);
            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
            builder.setTitle("Welcome on board");
            builder.setCancelable(true);
            builder.setTextGravity(Gravity.CENTER);
            builder.setMessage("To get you started, Allow Tunda to access your location and find people near you.");
            builder.setCancelable(false);
            builder.addButton("Allow", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Dexter.withActivity(PesonalInfo.this)
                                    .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                                    .withListener(new MultiplePermissionsListener() {
                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                                            if (report.areAllPermissionsGranted()) {
                                                Preferences.get(getApplicationContext()).setKeyIsFirstTimeLaunch(false);
                                                Preferences.get(PesonalInfo.this).setKeyIsLoggedIn(true);
                                                Preferences.get(PesonalInfo.this).setKeyIsFirstLogin(false);
                                                Intent intent = new Intent(PesonalInfo.this, SplashActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                                dialog.dismiss();
                                            }

                                            if (report.isAnyPermissionPermanentlyDenied()) {
                                                showSettingsDialog();
                                            }
                                        }


                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                            token.continuePermissionRequest();
                                        }
                                    }).check();


                        }
                    });

            builder.show();

        } else {
            Preferences.get(getApplicationContext()).setKeyIsFirstTimeLaunch(false);
            Preferences.get(PesonalInfo.this).setKeyIsLoggedIn(true);
            Preferences.get(PesonalInfo.this).setKeyIsFirstLogin(false);
            Intent intent = new Intent(PesonalInfo.this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }


    }

    private void saveToSp() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(PesonalInfo.this);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("gender", mGender);
        editor.putString("dob", date_of_birth);
        editor.putString("gender_of_interest", mGenderIntereset);
        editor.putString("interesr", jsonInterests);
        editor.putString("about_me", bio);
        editor.putString("user_status", "ACTIVE");
        if (jsonInterests != null && !jsonInterests.isEmpty()) {
            editor.putString("interest", jsonInterests);
        }
        editor.putInt("package_id", package_id);
        editor.putInt("user_id", user_id);
        editor.putString("first_name", firstName);
        editor.commit();
    }

    private void openCalender() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = null;
        dpd = DatePickerDialog.newInstance(
                PesonalInfo.this, 2000, 01, 01);
        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.setOnDateSetListener(this);
        dpd.setTitle("Pick Date of Birth");
        dpd.setScrollOrientation(DatePickerDialog.ScrollOrientation.HORIZONTAL);

        dpd.showYearPickerFirst(true);
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");


    }

    private void moveNext(int i, RelativeLayout mL1, RelativeLayout mL2) {
        if (mStepView.getVisibility() == View.GONE) {
            mStepView.setVisibility(View.VISIBLE);
        }

        mL1.animate().alpha(0.1f);
        mL1.setVisibility(View.GONE);
        mL2.animate().alpha(1.0f);
        mL2.setVisibility(View.VISIBLE);


        mStepView.go(i, true);

    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, monthOfYear, dayOfMonth);
        Date chosenDate = cal.getTime();

        DateFormat df_medium_us = DateFormat.getDateInstance(DateFormat.SHORT);
        String _date_of_birth = df_medium_us.format(chosenDate);
        String _year = String.valueOf(year);
        int month_selected = monthOfYear + 1;
        String _month = String.valueOf(month_selected);
        if (_month.length() == 1) {
            _month = "0" + _month;
        }
        String _day = String.valueOf(dayOfMonth);
        if (_day.length() == 1) {
            _day = "0" + _day;
        }
        date_of_birth = _year + "-" + _month + "-" + _day;
        Log.d("TESTING", "onDateSet: " + date_of_birth);
        int ageOfUser = calculateAge(date_of_birth);

        if (ageOfUser < 18) {
            mDobEdit.setText("");
            mDobEdit.setError("You have to be over 18");
            mSubmitDate.setEnabled(false);
//            mSubMitDate.setVisibility(View.INVISIBLE);
            Toasty.error(PesonalInfo.this, "Check Date! You have to be over 18", Toasty.LENGTH_LONG).show();
        } else {
            mDobEdit.setText(_date_of_birth);
//            mSubMitDate.animate().alpha(1.0f);
            mDobEdit.setError(null);
            mSubmitDate.setEnabled(true);
        }


    }

    private int calculateAge(String mDob) {
        Calendar calendar = Calendar.getInstance();
        Log.d("TESTING", mDob);
        String DAY;

        String YEAR = mDob.substring(0, 4);
        Log.d("TESTING", "year=" + YEAR);
        String MONTH = mDob.substring(5, 7);
        if (MONTH.contains("-")) {
            MONTH = MONTH.replace("-", "");
            Log.d("TESTING", "month=" + MONTH);
            DAY = mDob.substring(7, mDob.length());
        } else {
            DAY = mDob.substring(8, mDob.length());
        }


        Calendar x = new GregorianCalendar(Integer.valueOf(YEAR), Integer.valueOf(MONTH), Integer.valueOf(DAY));

        Log.d("TESTING", YEAR + "/" + MONTH + "/" + DAY);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));

        Date date = x.getTime();

//        date.setYear();
//        date.setMonth();
//        date.setDate();

        Log.d("TESTING", String.valueOf(date) + ">>" + date.getYear());

        Date currentDate = new Date();


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(date));
        int d2 = Integer.parseInt(formatter.format(currentDate));
        int age = (d2 - d1) / 10000;
        return age;

    }

    @Override
    public void onBackPressed() {


        if (mStepView != null && mStepView.getCurrentStep() == 0) {
            super.onBackPressed();
            return;
        }

        if (mStepView != null) {
            mStepView.go(mStepView.getCurrentStep() - 1, true);
            rewind();
        }


    }

    private void rewind() {

        if (mL6.getVisibility() == View.VISIBLE) {
            mL6.animate().alpha(0.1f);
            mL6.setVisibility(View.GONE);
            mL5.animate().alpha(1.0f);
            mL5.setVisibility(View.VISIBLE);
            return;
        }


        if (mL5.getVisibility() == View.VISIBLE) {
            mL5.animate().alpha(0.1f);
            mL5.setVisibility(View.GONE);
            mL4.animate().alpha(1.0f);
            mL4.setVisibility(View.VISIBLE);
            return;
        }

        if (mL4.getVisibility() == View.VISIBLE) {
            mL4.animate().alpha(0.1f);
            mL4.setVisibility(View.GONE);
            mL3.animate().alpha(1.0f);
            mL3.setVisibility(View.VISIBLE);
            return;
        }
        if (mL3.getVisibility() == View.VISIBLE) {
            mL3.animate().alpha(0.1f);
            mL3.setVisibility(View.GONE);
            mL2.animate().alpha(1.0f);
            mL2.setVisibility(View.VISIBLE);
            return;
        }

        if (mL2.getVisibility() == View.VISIBLE) {
            mL2.animate().alpha(0.1f);
            mL2.setVisibility(View.GONE);
            mL1.animate().alpha(1.0f);
            mL1.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTagClick(int position, String text) {

    }

    @Override
    public void onTagLongClick(int position, String text) {

    }

    @Override
    public void onSelectedTagDrag(int position, String text) {

    }

    @Override
    public void onTagCrossClick(int position) {
        mTagContainerLayout.removeTag(position);

    }

    private void pickFromGallery() {
        //Create an Intent with action as ACTION_PICK
        Intent intent = new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    private void loadProfile(String s, String filePath) {
        Glide.with(PesonalInfo.this).load(s).into(mImgProfile);
        mBtnNextJoin.setEnabled(true);
    }


    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            cursor.close();
            return cursor.getString(idx);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = null;
                if (data != null) {
                    uri = data.getParcelableExtra("path");
                }
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    // loading profile image from local cache
                    if (uri != null) {
                        filePath = getRealPathFromURIPath(uri, PesonalInfo.this);
                        loadProfile(uri.toString(), filePath);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
            }
        }

    }
}
