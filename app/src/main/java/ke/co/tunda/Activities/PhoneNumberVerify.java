

package ke.co.tunda.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;
import com.shuhart.stepview.StepView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.CheckUserExistsParams;
import ke.co.tunda.ApiConnector.Models.CheckUserExistsResponse;
import ke.co.tunda.ApiConnector.Models.OTP;
import ke.co.tunda.ApiConnector.Models.OTPParams;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PhoneNumberVerify extends AppCompatActivity {

    private int currentStep = 0;
    LinearLayout layout1, layout2;
    StepView stepView;
    AlertDialog dialog_verifying, profile_dialog;

    private static String uniqueIdentifier = null;
    private static final String UNIQUE_ID = "UNIQUE_ID";
    private static final long ONE_HOUR_MILLI = 60 * 60 * 1000;

    private static final String TAG = "FirebasePhoneNumAuth";


    private String phoneNumber;
    private Button sendCodeButton;
    private Button verifyCodeButton;
    private Button signOutButton;

    private MaterialEditText phoneNum;
    private PinView verifyCodeET;
    private TextView phonenumberText;
    private TextView textTimer;
    private TextView verifying;
    private HtmlTextView mTextLink;
    private LinearLayout mWrongNumber;
    CountryCodePicker ccp;

    private String mVerificationId;


    private ApiCaller apiCaller;

    private int code_received;

    private MaterialDialog dialog;
    String new_mobile_number;

    SharedPreferences sp;
    int user_id;
    SharedPreferences.Editor editor;
    TextView mNoCode;
    private String mCountryCode;
    private String mPhoneNumberToVerify;


    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    private static boolean CODE_GRABBED = false;


    //    String mPhone = "+16505551234";
//    String smsCode = "123456";

    private FirebaseAuth mAuth;

    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    public static void open(Context context) {
        context.startActivity(new Intent(context, PhoneNumberVerify.class));
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phoneverify);

        //initialize sharedPreferences

        // Restore instance state
        mAuth = FirebaseAuth.getInstance();
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {


                if (dialog.isShowing()) {
                    dialog.dismiss();
                }


                mVerificationInProgress = false;

                if (credential != null && credential.getSmsCode() == null) {
                    signInWithPhoneAuthCredential(credential);
                    return;
                }

                updateUI(credential);


            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Log.w(TAG, "onVerificationFailed", e);
                mVerificationInProgress = false;

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    phoneNum.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Too many tries, Try again later",
                            Snackbar.LENGTH_LONG).show();
                }


            }


            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent: ");
                Preferences.get(PhoneNumberVerify.this).setKeyIsVerifyingNo(true);
                Preferences.get(PhoneNumberVerify.this).setNumber(mPhoneNumberToVerify.trim());
                Preferences.get(PhoneNumberVerify.this).setVerificatioId(verificationId);


                if (dialog.isShowing()) {
                    dialog.dismiss();
                }

                updateUI(null);

                startCountDown();


                mVerificationId = verificationId;
                mResendToken = token;

            }
        };

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        user_id = sp.getInt("user_id", 0);


        apiCaller = Builder.getClient().create(ApiCaller.class);

        layout1 = (LinearLayout) findViewById(R.id.layout1);
        layout2 = (LinearLayout) findViewById(R.id.layout2);
        sendCodeButton = (Button) findViewById(R.id.submit1);
        verifyCodeButton = (Button) findViewById(R.id.submit2);
        phoneNum = (MaterialEditText) findViewById(R.id.phonenumber);
        verifyCodeET = (PinView) findViewById(R.id.pinView);
        phonenumberText = (TextView) findViewById(R.id.phonenumberText);
        mNoCode = findViewById(R.id.no_code);
        mTextLink = findViewById(R.id.privacy_link);
        mTextLink.setHtml(this.getResources().getString(R.string.accept_policy));
        ccp = (CountryCodePicker) findViewById(R.id.ccp_view);
        textTimer = (TextView) findViewById(R.id.timer);
        verifying = findViewById(R.id.verifying);
        mWrongNumber = findViewById(R.id.wrong_number_holder);
        TextView wrong_number = findViewById(R.id.wrong_number);

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                mCountryCode = selectedCountry.getPhoneCode();
            }


        });

        mCountryCode = ccp.getSelectedCountryCodeWithPlus();

        phoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                sendCodeButton.setEnabled(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isPhoneNumberValid(s.toString())) {
                    sendCodeButton.setEnabled(true);
                    phoneNum.setError(null);
                } else {
                    sendCodeButton.setEnabled(false);
                    phoneNum.setError("Enter a valid phone number");
                }


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isPhoneNumberValid(s.toString())) {
                    sendCodeButton.setEnabled(true);
                    phoneNum.setError(null);
                } else {
                    sendCodeButton.setEnabled(false);
                    phoneNum.setError("Enter a valid phone number");
                }

            }
        });

        verifyCodeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                verifyCodeButton.setEnabled(false);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                verifyCodeButton.setEnabled(true);

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() <= 0) {
                    verifyCodeButton.setEnabled(false);

                }

            }
        });


        mNoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.get(getApplicationContext()).setNumber("");
                Preferences.get(getApplicationContext()).setCode(0);
                resendVerificationCode(mPhoneNumberToVerify, mResendToken);
                textTimer.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Code Resent", Toast.LENGTH_SHORT).show();

            }
        });

        wrong_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentStep = 0;
                stepView.go(0, true);
                layout2.setVisibility(View.GONE);
                layout1.setVisibility(View.VISIBLE);

            }
        });

        stepView = findViewById(R.id.step_view);
        stepView.setStepsNumber(2);


        if (Preferences.get(this).isVerifyingNo()) {
            stepView.go(1, true);
            currentStep = 1;
            layout1.setVisibility(View.GONE);
            layout2.setVisibility(View.VISIBLE);
            verifying.setText(getResources().getString(R.string.verifying,
                    Preferences.get(getApplicationContext()).getNumber()));
            phonenumberText.setText(getResources().getString(R.string.your_number,
                    Preferences.get(getApplicationContext()).getNumber()));
            mVerificationId = Preferences.get(PhoneNumberVerify.this).getKeyVerificationId();


        } else {
            stepView.go(0, true);
            layout1.setVisibility(View.VISIBLE);
            layout2.setVisibility(View.GONE);
        }


        sendCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendCodeButton.setEnabled(false);

                if (phoneNum.getText() != null) {
                    phoneNumber = phoneNum.getText().toString();
                    if (PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
                        if (phoneNumber.startsWith("0")) {
                            phoneNumber = phoneNumber.substring(1, phoneNumber.length());
                        }
                        MaterialDialog.Builder builder = new MaterialDialog.Builder(PhoneNumberVerify.this);
                        builder.content("verifying phone number....")
                                .widgetColor(getResources().getColor(R.color.colorAccent))
                                .progress(true, 0);
                        dialog = builder.build();
                        dialog.setCancelable(false);
                        dialog.show();

                        mPhoneNumberToVerify = ccp.getSelectedCountryCodeWithPlus() + phoneNumber;
                        if (!mPhoneNumberToVerify.isEmpty()) {
                            startPhoneNumberVerification(mPhoneNumberToVerify);
                            verifying.setText(getResources().getString(R.string.verifying, mPhoneNumberToVerify));
                            phonenumberText.setText(getResources().getString(R.string.your_number, mPhoneNumberToVerify));
                        }


                    } else {
                        phoneNum.setError("Please enter a valid phone");
                        phoneNum.requestFocus();
                    }
                }


            }
        });


        verifyCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String code = verifyCodeET.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    Toast.makeText(getApplicationContext(), "Enter code", Toast.LENGTH_SHORT).show();
                    return;
                }

                MaterialDialog.Builder builder = new MaterialDialog.Builder(PhoneNumberVerify.this);
                builder.content("verifying code....")
                        .widgetColor(getResources().getColor(R.color.colorAccent))
                        .progress(true, 0);
                dialog = builder.build();
                dialog.setCancelable(false);
                dialog.show();


                verifyPhoneNumberWithCode(mVerificationId, code);
//
//                String verificationCode = verifyCodeET.getText().toString();
//
//                if (verificationCode.isEmpty()) {
//                    Toasty.warning(getApplicationContext(), "Enter verification code").show();
//                } else if (verificationCode.length() != 4) {
//                    Toasty.warning(getApplicationContext(), "Enter a valid verification code").show();
//
//
//                } else {
//
//                    MaterialDialog.Builder builder = new MaterialDialog.Builder(PhoneNumberVerify.this);
//                    builder.content("verifying....")
//                            .widgetColor(getResources().getColor(R.color.colorAccent))
//                            .progress(true, 0);
//                    dialog = builder.build();
//                    dialog.setCancelable(false);
//                    dialog.show();
//
//
//                    verifyCode(Integer.parseInt(verificationCode));
//
//
//                }
            }
        });


    }

    private void updateUI(PhoneAuthCredential cred) {

        if (currentStep < stepView.getStepCount() - 1) {
            currentStep++;
            stepView.go(currentStep, true);
        } else {
            stepView.done(true);
        }
        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.VISIBLE);
        if (cred != null) {
            Log.d(TAG, "updateUI: cred" + cred.getSmsCode());
            if (cred.getSmsCode() != null) {
                verifyCodeET.setText(cred.getSmsCode());
                signInWithPhoneAuthCredential(cred);

            }
        } else {
            Log.d(TAG, "updateUI: cred null");
        }

    }


    private void loginUserWithPhoneNumber() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        Preferences.get(this).setKeyIsFirstTimeLaunch(false);
        Preferences.get(this).setKeyIsLoggedIn(true);


        Intent intent = new Intent(PhoneNumberVerify.this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        startActivity(intent);
        finish();
    }

    private void saveUserDetails(CheckUserExistsResponse.UserData userData) {

        try {
            Log.d(TAG, "saveUserDetails: " + userData.getPhotoPath());
            editor = sp.edit();
            editor.putInt("user_id", userData.getUserId());
            editor.putString("profile_photo_id", userData.getPhotoPath());
            editor.putInt("package_id", userData.getPackageId());
            editor.putString("first_name", userData.getFirstName());
            editor.putString("gender", userData.getGender());
            editor.putString("gender_of_interest", userData.getGenderOfInterest());
            editor.putString("dob", userData.getDob());
            editor.putString("interest", userData.getInterest());
            editor.putString("source", userData.getSource());
            editor.putString("about_me", userData.getAboutMe());
            editor.putString("job_title", userData.getJobTitle());
            editor.putString("company", userData.getCompany());
            editor.putString("school", userData.getSchool());
            editor.putString("show_age", userData.getShowAge());
            editor.putString("show_location", userData.getShowLocation());
            editor.putString("last_location", userData.getLastLocation());
            editor.putString("user_status", userData.getUserStatus());
            editor.putString("image", userData.getPhotoPath());
            editor.putInt("age_from", userData.getAgeFrom());
            editor.putInt("age_to", userData.getAgeTo());
            editor.putInt("distance", userData.getDistance());
            editor.putString("package_expiry_date", userData.getPackageExpiryDate());
            editor.putInt("photo_count", userData.getPhoto_count());
            editor.putString("show_age", userData.getShowAge());
            editor.putString("show_location", userData.getShowLocation());
            editor.apply();

        } catch (Exception e) {
            Log.d(TAG, "saveUserDetails: ");
        }


    }


    private void delayAndSetCode(int code_received) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (code_received > 0) {
                    verifyCodeET.setText(String.valueOf(code_received));
                }

            }
        }, 1000);

    }

    private void startCountDown() {
        if (mNoCode.getVisibility() == View.VISIBLE) {
            mNoCode.setVisibility(View.GONE);
        }

        if (textTimer.getVisibility() == View.GONE) {
            textTimer.animate().alpha(1.0f);
            textTimer.setVisibility(View.VISIBLE);

        }


        new CountDownTimer(60000, 1000) {
            int time = 60;

            public void onTick(long millisUntilFinished) {
                textTimer.setText("Resend in " + checkDigit(time));
                time--;
            }

            public void onFinish() {
                textTimer.animate().alpha(0.1f);
                textTimer.setVisibility(View.GONE);
                mNoCode.animate().alpha(1.0f);
                mNoCode.setVisibility(View.VISIBLE);
                mNoCode.setText("Resend");
                sendCodeButton.setEnabled(true);

            }

        }.start();

    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }


    @Override
    public void onBackPressed() {


        if (currentStep == 1) {
            currentStep--;
            stepView.go(currentStep, true);
            layout2.setVisibility(View.GONE);
            layout1.setVisibility(View.VISIBLE);
        } else if (currentStep == 0) {
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }

    }

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        mVerificationInProgress = true;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        if (!dialog.isShowing()) {
            MaterialDialog.Builder builder = new MaterialDialog.Builder(PhoneNumberVerify.this);
            builder.content("verifying....")
                    .widgetColor(getResources().getColor(R.color.colorAccent))
                    .progress(true, 0);
            dialog = builder.build();
            dialog.setCancelable(false);
            dialog.show();
        }
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            if (task.getResult() != null) {
                                FirebaseUser user = task.getResult().getUser();
                                savePhoneNumber(user.getPhoneNumber());
                                Preferences.get(PhoneNumberVerify.this).setKeyIsVerifyingNo(false);
                            }


                            // [START_EXCLUDE]
//                            updateUI(STATE_SIGNIN_SUCCESS, user);
                            // [END_EXCLUDE]
                        } else {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                verifyCodeET.setError("Invalid code.");
                                Toast.makeText(getApplicationContext(), "Invalid code", Toast.LENGTH_SHORT).show();
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
//                            updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }
                    }
                });
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = phoneNum.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            phoneNum.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);
        startCountDown();
    }


    private void savePhoneNumber(String phoneNumber) {

        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(PhoneNumberVerify.this);
        builder.content("Updating ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();

        UpdateUserProfileRequest updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setPhone_number(phoneNumber);
        updateUserProfileRequest.setUserId(String.valueOf(user_id));
        updateUserProfileRequest.setRequestType("UPDATE_USER_PROFILE");

        Call<UpdateUserProfileResponse> call = apiCaller.updateUser(updateUserProfileRequest);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse upr = response.body();
                    if (upr.getBody().equals("SUCCESS")) {
                        dialog.dismiss();
                        saveToSp(mPhoneNumberToVerify);
                        Preferences.get(getApplicationContext()).setKeyIsVerifyingNo(false);

                    } else {
                        Preferences.get(getApplicationContext()).setKeyIsVerifyingNo(false);
                        dialog.dismiss();
                        Toasty.warning(getApplicationContext(), "Something Went Wrong, Try Again").show();


                    }
                } else {
                    Preferences.get(getApplicationContext()).setKeyIsVerifyingNo(false);
                    dialog.dismiss();
                    Toasty.warning(getApplicationContext(), "Something Went Wrong, Try Again").show();

                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable t) {
                Preferences.get(getApplicationContext()).setKeyIsVerifyingNo(false);
                dialog.dismiss();
                Toasty.warning(getApplicationContext(), "Something Went Wrong, Try Again").show();

            }
        });


    }

    private void saveToSp(String new_mobile_number) {
        editor = sp.edit();
        editor.putString("phone_number", new_mobile_number);
        editor.putBoolean("verified", true);
        editor.apply();
        SettingsActivity.open(PhoneNumberVerify.this);
        finish();
    }

    private boolean isPhoneNumberValid(String phoneNumber) {
        String phonenumberregex = "^[0-9]{9,13}$";
        return phoneNumber.matches(phonenumberregex);

    }

}

