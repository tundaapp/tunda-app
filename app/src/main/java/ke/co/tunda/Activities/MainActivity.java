/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/22/19 12:19 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.transition.Explode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.guardanis.applock.AppLock;
import com.polyak.iconswitch.IconSwitch;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.ApiConnector.Models.GetUsersNearbyResponse;
import ke.co.tunda.ApiConnector.Models.RegisterUserParams;
import ke.co.tunda.ApiConnector.Models.UpdateLocationResponse;
import ke.co.tunda.Firebase.FireBaseMessagingService;
import ke.co.tunda.Helpers.MyBounceInterpolator;
import ke.co.tunda.Models.DefaultRequest;
import ke.co.tunda.Models.GetBoostedparams;
import ke.co.tunda.Models.LikesResponse;
import ke.co.tunda.Models.MoodsRequest;
import ke.co.tunda.Models.MoodsResponse;
import ke.co.tunda.R;
import ke.co.tunda.fragments.ChatsStatusFragment;
import ke.co.tunda.fragments.ConnectionsFragment;
import ke.co.tunda.fragments.MainHomeFragment;
import ke.co.tunda.fragments.MyProfileFragment;
import ke.co.tunda.fragments.TopPicksFragment;
import ke.co.tunda.utils.MyLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements LocationListener, View.OnClickListener,
        MainHomeFragment.OnHeadlineSelectedListener, TopPicksFragment.onDataLoadedListener, ChatsStatusFragment.OnChatStatusPstnChanged {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1234;
    private static final int ACTION_LOCATION_REQUEST = 12345;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    private NonSwipeableViewPager mViewPager;
    Intent fcmService;

    public static double longitude = 0.0;
    public static double latitude = 0.0;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private int user_id;
    private String imageProfile;
    private static MainActivity instance;
    private FusedLocationProviderClient fusedLocationClient;
    private Snackbar snackbar;
    private Fragment mHomeFrag = new MainHomeFragment();
    private Fragment mProfile = new MyProfileFragment();
    private Fragment mConnections = new ConnectionsFragment();
    Fragment active = mHomeFrag;
    final FragmentManager fm = getSupportFragmentManager();
    private final static int REQUEST_CODE_PICKER = 2000;

    @BindView(R.id.home)
    RelativeLayout mHome;
    @BindView(R.id.profile)
    RelativeLayout myProfile;
    @BindView(R.id.chats)
    RelativeLayout chats;

    @BindView(R.id.profileImg)
    ImageView myProfileImg;
    @BindView(R.id.homeImg)
    ImageView homeImg;
    @BindView(R.id.chatsImg)
    ImageView chatsImg;
    @BindView(R.id.badge_notification_1)
    TextView mBadger;

    @BindView(R.id.icon_switch)
    IconSwitch mTopSwitch;
//    @BindView(R.id.icon_switch_end)
//    IconSwitch mEndSwitch;
//    @BindView(R.id.rm_triswitch)
//    RMTristateSwitch mRmSwitch;

    private Bundle args;
    private int mPosition;
    private int position;
    String payload;

    private static MainActivity mInstance;
    private String gender;
    private String dob;
    private String mInterest;
    public boolean isBoostsAvailable = false;
    private String TAG = "MN";
    private String TAG1 = "CHM";
    private Body[] mBody;
    private ViewPager.OnPageChangeListener mPagerChager;
    private View mHomeView;
    private int number_of_likes;
    private boolean is_likes_available = false;
    private GetUsersNearbyResponse boostPayload;
    private LikesResponse likesPayload;


    public static synchronized MainActivity getInstance() {
        return mInstance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setExitTransition(new Explode());
        }


        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        args = getIntent().getExtras();
        Log.d("Tunda", "onCreate: ");


        mTopSwitch.setCheckedChangeListener(new IconSwitch.CheckedChangeListener() {
            @Override
            public void onCheckChanged(IconSwitch.Checked current) {
                if (current == IconSwitch.Checked.LEFT) {
                    onArticleSelected(0);
                    Log.d(TAG, "onCheckChanged: left");

                } else {
                    Log.d(TAG, "onCheckChanged: right");
                    onArticleSelected(1);

                }
            }
        });


        try {
            new sendCampaignInfo().execute();


            if (args != null && !args.isEmpty() && args.containsKey("position")) {

                payload = getIntent().getStringExtra("payload");
                mPosition = args.getInt("position");
                Log.d("TAG1", "onCreate: position" + mPosition);
                position = mPosition;
                getIntent().removeExtra("position");

            } else {
                position = 1;

            }

            InitializeFirebase();
            instance = this;


            sp = PreferenceManager.getDefaultSharedPreferences(this);
            editor = sp.edit();
            user_id = sp.getInt("user_id", 0);
            imageProfile = sp.getString("image", "");
            gender = sp.getString("gender", "");
            dob = sp.getString("dob", "");
            mInterest = sp.getString("gender_of_interest", "");


            new getstatuses().execute();


            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
            GetBoostedparams params = new GetBoostedparams();
            params.setRequestType("GET_BOOSTED_USERS");
            params.setUserId(user_id);
            params.setPage(1);
            Call<GetUsersNearbyResponse> mCall = apiCaller.getBoosted(params);


            DefaultRequest request = new DefaultRequest();
            request.setRequestType("GET_USER_LIKES");
            request.setUserId(user_id);
            Call<LikesResponse> mCall2 = apiCaller.getLikes(request);


            if (gender != null && gender.isEmpty()) {
                position = 0;
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                builder.setTitle("Incomplete Profile");
                builder.setTextGravity(Gravity.CENTER);
                builder.setMessage("Your profile is incomplete.Add your gender to get matches");
                builder.setCancelable(false);
                builder.addButton("Add Now", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();


                            }
                        }).addButton("Am Good (Not Recommended)", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });

                builder.show();


            }

            if (dob != null && dob.isEmpty()) {
                position = 0;
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                builder.setTitle("Incomplete Profile");
                builder.setTextGravity(Gravity.CENTER);
                builder.setMessage("To find People and be visible on Tunda, Please add your Date of Birth");
                builder.setCancelable(false);
                builder.addButton("Add Now", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).addButton("Am Good (Not Recommended)", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                builder.show();

            }


            if (mInterest != null && mInterest.isEmpty()) {
                position = 0;
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                builder.setTitle("Incomplete Profile");
                builder.setTextGravity(Gravity.CENTER);
                builder.setMessage("To find People and be visible on Tunda, Please add your Gender of Interest");
                builder.setCancelable(false);
                builder.addButton("Add Now", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        }).addButton("Am Good (Not Recommended)", -1, getResources().getColor(R.color.colorPrimary), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });

                builder.show();


            }

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mViewPager = (NonSwipeableViewPager) findViewById(R.id.container);
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.setCurrentItem(position);

            mPagerChager = new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {


                    if (position == 0) {

                        Log.d(TAG1, "onPageSelected 0: " + position);

                        setDefaultHome();
//                        setDefaultChatsInactive();

                        chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                        myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile));

                        mViewPager.setPagingEnabled(true);
//                        ImageViewAnimatedChange(MainActivity.this, myProfileImg, getResources().getDrawable(R.drawable.ic_my_profile));
                        return;

                    }

                    if (position == 1) {

                        Log.d(TAG1, "onPageSelected 1: " + position);

                        if (isBoostsAvailable) {
                            myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                            chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                            mViewPager.setPagingEnabled(false);
                            setDefaultHomeWithBoosts();
//                            setDefaultChatsInactive();

                        } else if (is_likes_available) {
                            setDefaultHomeWithLikes();
//                            setDefaultChatsInactive();
                            myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                            chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                            mViewPager.setPagingEnabled(false);

                        } else {
                            setDefaultHomeActive();
//                            setDefaultChatsInactive();
                            myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                            chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                            mViewPager.setPagingEnabled(false);


                        }

                        return;


                    }

                    if (position == 2) {
                        Log.d(TAG1, "onPageSelected 2: " + position);
                        setDefaultHome();
//                        setDefaultChats();
                        myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                        chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img));
                        mViewPager.setPagingEnabled(true);
//                        ImageViewAnimatedChange(MainActivity.this, chatsImg, getResources().getDrawable(R.drawable.ic_chats_img));


                    }

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            };
            mPagerChager.onPageSelected(mViewPager.getCurrentItem());


            mViewPager.addOnPageChangeListener(mPagerChager);


            mViewPager.setOffscreenPageLimit(2);


            mViewPager.setEnabled(false);

            new getTopPicks(mCall).execute();
            new getLikes(mCall2).execute();


            myProfile.setOnClickListener(this);
            chats.setOnClickListener(this);
            homeImg.setOnClickListener(this);
            chatsImg.setOnClickListener(this);


            if (position == 0) {


                setDefaultHome();
//                setDefaultChatsInactive();
//                ImageViewAnimatedChange(MainActivity.this, myProfileImg, getResources().getDrawable(R.drawable.ic_my_profile));
                chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile));
                mViewPager.setPagingEnabled(true);
                return;


            }

            if (position == 2) {

                setDefaultHome();
//                setDefaultChats();

//                ImageViewAnimatedChange(MainActivity.this, chatsImg, getResources().getDrawable(R.drawable.ic_chats_img));
                mViewPager.setPagingEnabled(true);
                myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img));

                return;

            }

            if (position == 1) {


                if (isBoostsAvailable) {
                    setDefaultHomeWithBoosts();
//                    setDefaultChatsInactive();
                    myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                    chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                    mViewPager.setPagingEnabled(false);
                } else if (is_likes_available) {
                    setDefaultHomeWithLikes();
//                    setDefaultChatsInactive();
                    myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                    chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                    mViewPager.setPagingEnabled(false);
                } else {
//                    setDefaultChatsInactive();
                    homeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_active));
                    myProfileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_profile_inactive));
                    chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
                    mViewPager.setPagingEnabled(false);
                }


            }


        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());
        }


    }

    private void setDefaultHomeActive() {
        homeImg.setImageDrawable(null);
        homeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_active));
        mTopSwitch.animate().alpha(0.1f);
        mTopSwitch.setVisibility(View.GONE);
        mHome.animate().alpha(1.0f);
        mHome.setVisibility(View.VISIBLE);
        homeImg.setClickable(true);
        homeImg.setFocusable(true);
    }

    private void setDefaultHomeWithLikes() {
        mTopSwitch.animate().alpha(1.0f);
        mTopSwitch.setVisibility(View.VISIBLE);
        mHome.animate().alpha(0.1f);
        mHome.setVisibility(View.GONE);
        mTopSwitch.getRightIcon().setImageDrawable(getResources().getDrawable(R.drawable.ic_likes));
    }

    private void setDefaultChats() {
        chats.animate().alpha(0.1f);
        chats.setVisibility(View.GONE);
//        mEndSwitch.animate().alpha(1.0f);
//        mEndSwitch.setVisibility(View.VISIBLE);


    }

    private void setDefaultChatsInactive() {
        chats.animate().alpha(1.0f);
        chats.setVisibility(View.VISIBLE);
        chatsImg.setImageDrawable(null);
        chatsImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_chats_img_inactive));
//        mEndSwitch.animate().alpha(0.1f);
//        mEndSwitch.setVisibility(View.GONE);


    }


    public void setDefaultHomeWithBoosts() {
        homeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_active));
        mTopSwitch.animate().alpha(1.0f);
        mTopSwitch.setVisibility(View.VISIBLE);
        mHome.animate().alpha(0.1f);
        mHome.setVisibility(View.GONE);

    }

    //
    private void setDefaultHome() {
        homeImg.setImageDrawable(null);
        homeImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_inactive));
        mTopSwitch.animate().alpha(0.1f);
        mTopSwitch.setVisibility(View.GONE);
        mHome.animate().alpha(1.0f);
        mHome.setVisibility(View.VISIBLE);
        homeImg.setClickable(true);
        homeImg.setFocusable(true);


    }


    private void InitializeFirebase() {
        fcmService = new Intent(MainActivity.this, FireBaseMessagingService.class);
        startService(fcmService);
    }


    public void checkLocationEnabled() {
        LocationManager lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showAlert();
                }
            });


        } else {


            getLocation();

        }


    }


    private void showAlert() {

        new AlertDialog.Builder(this)
                .setMessage("For a better experience, Tunda needs to access your location. Enable it in Location Settings")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (canToggleGPS()) {
                            turnGPSOn();
                        } else {

                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), ACTION_LOCATION_REQUEST);


                        }

                    }
                })
                .setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .show();


    }


    private void getLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            requestPermission();
            Log.d("HTTP", "getLocation: Denied permissio");
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        } else {
            fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location != null) {
                        Log.d("HTTP", "onLocation: ");
                        Log.d("HTTP", "onSuccess: ");
                        sendlocationtoserver(location.getLatitude(), location.getLongitude());


                    } else {
                        Log.d("HTTP", "location return null: ");

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {

                                MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
                                    @Override
                                    public void gotLocation(Location location) {
                                        sendlocationtoserver(location.getLatitude(), location.getLongitude());
                                        //Got the location!
                                    }
                                };
                                MyLocation myLocation = new MyLocation();
                                myLocation.getLocation(MainActivity.this, locationResult);


                            }
                        });


                    }

                }
            });

            fusedLocationClient.getLastLocation().addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("HTTP", "fusedLocationClient onFailure: " + e.getMessage());
                }
            });

            fusedLocationClient.getLastLocation().addOnCanceledListener(new OnCanceledListener() {
                @Override
                public void onCanceled() {
                    Log.d("HTTP", "fusedLocationClient onCanceled: ");
                }
            });

            fusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Log.d("HTTP", "fusedLocationClient onComplete: ");
                }
            });

            fusedLocationClient.getLocationAvailability().addOnSuccessListener(new OnSuccessListener<LocationAvailability>() {
                @Override
                public void onSuccess(LocationAvailability locationAvailability) {
                    Log.d("HTTP", "LocationAvailability onSuccess: " + locationAvailability.toString());
                }
            });
        }

    }


    public void requestPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);

    }


    private void sendlocationtoserver(double lat, double lng) {
        ArrayList<String> latlng = new ArrayList<String>();
        latlng.add(String.valueOf(lat));
        latlng.add(String.valueOf(lng));

        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

        RegisterUserParams registerUserParams = new RegisterUserParams();
        registerUserParams.setLatitude(lat);
        registerUserParams.setLongitude(lng);
        registerUserParams.setUser_id(user_id);
        registerUserParams.setRequestType("UPDATE_LOCATION");
        retrofit2.Call<UpdateLocationResponse> call = apiCaller.updateLocation(registerUserParams);
        call.enqueue(new Callback<UpdateLocationResponse>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<UpdateLocationResponse> call, @NonNull Response<UpdateLocationResponse> response) {
                UpdateLocationResponse updateLocationResponse = response.body();
                if (!updateLocationResponse.getBody().equals("SUCCESS")) {
                    Log.d("LOCATION", "location onResponse: " + updateLocationResponse.getBody());
                }


            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<UpdateLocationResponse> call, @NonNull Throwable t) {
                Log.d("LOCATION", t.getMessage());

            }
        });


    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile:
                didTapButton(myProfileImg);
                mViewPager.setCurrentItem(0);
                mPagerChager.onPageSelected(0);

                break;
            case R.id.homeImg:
                didTapButton(homeImg);
                mViewPager.setCurrentItem(1);
                mPagerChager.onPageSelected(1);
                break;
            case R.id.chatsImg:
                didTapButton(chatsImg);
                mViewPager.setCurrentItem(2);
                mPagerChager.onPageSelected(2);
                break;


        }
    }

    @Override
    public void onArticleSelected(int position) {
        MainHomeFragment mMainHome = (MainHomeFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + mViewPager.getId() + ":" + 1);
        if (mMainHome != null) {
            if (position == 1) {
                mMainHome.setPosition(position, false, null,
                        false, null);
            } else if (position == 2) {
                mMainHome.setPosition(position, isBoostsAvailable, boostPayload,
                        is_likes_available, likesPayload);
            } else {
                mMainHome.setPosition(position, false, null,
                        false, null);
            }

        }


    }

    @Override
    public Void onDataLoaded(int position) {
        return null;
    }

    @Override
    public void onPositionChanged(int position) {
        ChatsStatusFragment mChatStatusFrag = (ChatsStatusFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + mViewPager.getId() + ":" + 2);
        if (mChatStatusFrag != null) {
            if (position == 0) {
                mChatStatusFrag.setPosition(position);
            } else if (position == 1) {
                mChatStatusFrag.setPosition(position);
            } else {
                mChatStatusFrag.setPosition(position);
            }

        }


    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new MyProfileFragment();
                case 1:
                    return new MainHomeFragment();
                case 2:
                    return new ConnectionsFragment();
                default:
                    return null;

            }

        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkLocationEnabled();
                }
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                else {
                    Snackbar.make(findViewById(R.id.header), "Please Enable Permission for a good Tunda Experience", Snackbar.LENGTH_LONG).show();

                }
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }
    }

    private boolean canToggleGPS() {
        PackageManager pacman = getPackageManager();
        PackageInfo pacInfo = null;

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
        } catch (PackageManager.NameNotFoundException e) {
            return false; //package not found
        }

        if (pacInfo != null) {
            for (ActivityInfo actInfo : pacInfo.receivers) {
                //test if recevier is exported. if so, we can toggle GPS.
                if (actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported) {
                    return true;
                }
            }
        }

        return false; //default
    }

    private void turnGPSOn() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    private void turnGPSOff() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ACTION_LOCATION_REQUEST:
                Log.d("HTTP", "onActivityResult:ok ");
                checkLocationEnabled();


                break;

            case REQUEST_CODE_PICKER:
                ChatsStatusFragment mChatStatusFrag = (ChatsStatusFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + mViewPager.getId() + ":" + 2);
                mChatStatusFrag.onActivityResult(requestCode, resultCode, data);
                break;
//            case ImageEditor.RC_IMAGE_EDITOR:
//                ChatsStatusFragment mChatStatusFrag2 = (ChatsStatusFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + mViewPager.getId() + ":" + 2);
//                mChatStatusFrag2.onActivityResult(requestCode, resultCode, data);
//                break;

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(fcmService);
    }


    @Override
    public void onAttachFragment(Fragment fragment) {
        if (fragment instanceof MainHomeFragment) {
            MainHomeFragment headlinesFragment = (MainHomeFragment) fragment;
            headlinesFragment.setOnHeadlineSelectedListener(this);
            Log.d(TAG, "onAttachFragment: mainhome");
            return;
        }

//        if (fragment instanceof ConnectionsFragment) {
//            Log.d(TAG, "onAttachFragment: chatstatus");
//            ChatsStatusFragment chatsStatusFragment = (ChatsStatusFragment) fragment;
//            chatsStatusFragment.setOnChatStatusPstnChangedListener(this);
//        }

    }


    private class getTopPicks extends AsyncTask<Void, Void, Void> {
        Call<GetUsersNearbyResponse> call;

        getTopPicks(Call<GetUsersNearbyResponse> mCall) {
            this.call = mCall;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            call.enqueue(new Callback<GetUsersNearbyResponse>() {
                @Override
                public void onResponse(Call<GetUsersNearbyResponse> call, Response<GetUsersNearbyResponse> response) {
                    if (response.isSuccessful()) {
                        GetUsersNearbyResponse resp = response.body();
                        if (resp != null && resp.getStatusCode().equals("00") && resp.getBody() != null) {
                            mBody = resp.getBody();
                            if (mBody.length > 0) {
                                isBoostsAvailable = true;
                                if (position == 1) {
                                    setDefaultHomeWithBoosts();
                                }

                                mBody = resp.getBody();
                                boostPayload = resp;

                            }


                        } else if (resp != null && resp.getStatusCode().equals("99")) {
                            Log.d(TAG, "onResponse: 99");
                            isBoostsAvailable = false;
                        }
                    } else {
                        isBoostsAvailable = false;
                        Log.d(TAG, "onResponse: ");
                    }
                }

                @Override
                public void onFailure(Call<GetUsersNearbyResponse> call, Throwable t) {
                    isBoostsAvailable = false;
                    Log.d(TAG, "onFailure: ");
                }
            });
            return null;
        }


    }

    public Body[] getData() {
        if (mBody != null) {
            return mBody;
        }
        return null;

    }

    @Override
    protected void onPostResume() {
        Log.d(TAG, "onPostResume: ");
        super.onPostResume();
        AppLock.onActivityResumed(this);


    }


    private class sendCampaignInfo extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            // Get tracker.


            Tracker t = new Analytics(MainActivity.this).getTracker(Analytics.TrackerName.APP_TRACKER);

// Set screen name.
            t.setScreenName("MainActivity");

// Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder()
                    .build());
            return null;
        }
    }

    private class getLikes extends AsyncTask<Void, Void, Void> {
        Call<LikesResponse> mCall;

        public getLikes(Call<LikesResponse> mCall2) {
            this.mCall = mCall2;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mCall.enqueue(new Callback<LikesResponse>() {
                @Override
                public void onResponse(Call<LikesResponse> call, Response<LikesResponse> response) {
                    if (response.isSuccessful()) {
                        LikesResponse likesResponse = response.body();
                        if (likesResponse != null) {
                            if (likesResponse.getCount() != null && likesResponse.getCount() > 0) {
                                number_of_likes = likesResponse.getCount();
                                is_likes_available = true;
                                if (position == 1) {
                                    setDefaultHomeWithLikes();
                                }

                                likesPayload = likesResponse;
                                editor.putInt("likes_number", likesResponse.getCount());
                                editor.commit();
                            } else {
                                editor.putInt("likes_number", 0);
                                editor.apply();
                            }

                        } else {
                            editor.putInt("likes_number", 0);
                            editor.apply();
                            number_of_likes = 0;
                            is_likes_available = false;
                        }
                    } else {
                        editor.putInt("likes_number", 0);
                        editor.apply();
                        number_of_likes = 0;
                        is_likes_available = false;
                    }
                }

                @Override
                public void onFailure(Call<LikesResponse> call, Throwable t) {
                    editor.putInt("likes_number", 0);
                    editor.apply();
                    number_of_likes = 0;
                    is_likes_available = false;
                    Log.d(TAG, "onFailure: get likes");

                }
            });
            return null;
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
    }

    public void didTapButton(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(MainActivity.this, R.anim.bounce);
//        view.startAnimation(myAnim);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        view.startAnimation(myAnim);
    }


    private class getstatuses extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

            MoodsRequest moodsRequest = new MoodsRequest();
            moodsRequest.setRequestType("GET_MY_LIKE_MOODS");
            moodsRequest.setUserId(user_id);
            Call<MoodsResponse> moodsResponseCall = apiCaller.getMoods(moodsRequest);
            moodsResponseCall.enqueue(new Callback<MoodsResponse>() {
                @Override
                public void onResponse(@NonNull Call<MoodsResponse> call, @NonNull Response<MoodsResponse> response) {
                    if (response.isSuccessful()) {
                        MoodsResponse moodsResponse = response.body();
                        if (moodsResponse != null && moodsResponse.getStatusCode().equals("00") && moodsResponse.getBody() != null) {

                            if (isNotViewed(moodsResponse)) {

                                mBadger.setVisibility(View.VISIBLE);
                            } else {
                                mBadger.setVisibility(View.GONE);
                            }


                        } else if (moodsResponse != null && moodsResponse.getStatusCode().equals("99")) {
                            mBadger.setVisibility(View.GONE);

                        }
                    }


                }

                @Override
                public void onFailure(@NonNull Call<MoodsResponse> call, @NonNull Throwable t) {
                    mBadger.setVisibility(View.GONE);


                }
            });

            return null;
        }
    }

    private boolean isNotViewed(MoodsResponse moodsResponse) {
        for (int i = 0; i < moodsResponse.getBody().size(); i++) {
            if (moodsResponse.getBody().get(i).getMoodCount() > moodsResponse.getBody().get(i).getViewedMoods()) {
                return true;

            }
        }
        return false;
    }


}