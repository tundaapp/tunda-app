

package ke.co.tunda.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.guardanis.applock.AppLock;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BioDetails extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.settings_back)
    ImageView mBack;
    @BindView(R.id.btn_save)
    Button mSave;

    @BindView(R.id.edittxt_bio)
    EditText mEditTextBio;
    SharedPreferences sp;
    String about_me = "";

    private static String mBio, mUser_Id;
    UpdateUserProfileRequest updateUserProfileRequest;
    private ApiCaller apiCaller;
    private boolean isEdited = false;


    public static void open(Context context, String bio, String user_id) {
        context.startActivity(new Intent(context, BioDetails.class));
        mBio = bio;
        mUser_Id = user_id;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_details);
        ButterKnife.bind(this);


        sp = PreferenceManager.getDefaultSharedPreferences(this);
        about_me = sp.getString("about_me", "");

        if (about_me != null && !about_me.isEmpty()) {
            mEditTextBio.setText(about_me);
        }

        mBack.setOnClickListener(this);
        updateUserProfileRequest = new UpdateUserProfileRequest();
        updateUserProfileRequest.setUserId(mUser_Id);
        updateUserProfileRequest.setRequestType("UPDATE_USER_PROFILE");
        apiCaller = Builder.getClient().create(ApiCaller.class);


        mEditTextBio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEdited = true;
                mSave.setEnabled(true);
            }
        });

        mEditTextBio.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                isEdited = true;
                mSave.setEnabled(true);
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEdited){
                    updatechanges();
                }

            }
        });

    }

    private void updatechanges() {
        if (!mEditTextBio.getText().toString().isEmpty()) {
            about_me = mEditTextBio.getText().toString();
            updateUserProfileRequest.setAboutMe(mEditTextBio.getText().toString());
            uploadBio(updateUserProfileRequest);

        } else {
            Toasty.warning(getApplicationContext(), "No Bio").show();
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (isEdited) {
            updatechanges();
        } else {
            super.onBackPressed();
        }


    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_back:
                onBackPressed();
                break;

        }
    }

    private void saveToSp(String s) {
        SharedPreferences.Editor editor;

        editor = sp.edit();
        editor.putString("about_me", s);
        editor.apply();

    }

    private void uploadBio(UpdateUserProfileRequest updateUserProfileRequest) {

        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(BioDetails.this);
        builder.content("Updating ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();
        Call<UpdateUserProfileResponse> call = apiCaller.updateUser(updateUserProfileRequest);
        call.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateUserProfileResponse> call, Response<UpdateUserProfileResponse> response) {
                if (response.isSuccessful()) {
                    UpdateUserProfileResponse upr = response.body();
                    if (upr != null) {
                        if (upr.getBody().equals("SUCCESS")) {
                            dialog.dismiss();
                            saveToSp(about_me);
                            Toasty.success(getApplicationContext(), "updated").show();
                            finish();


                        } else {
                            dialog.dismiss();
                            Toasty.warning(getApplicationContext(), "Something went wrong,try again").show();
                            finish();

                        }
                    }
                } else {
                    dialog.dismiss();
                    Toasty.warning(getApplicationContext(), "Something went wrong,try again").show();
                    finish();

                }
            }

            @Override
            public void onFailure(Call<UpdateUserProfileResponse> call, Throwable throwable) {
                dialog.dismiss();
                Toasty.warning(getApplicationContext(), "Something went wrong,try again").show();
                finish();


            }
        });

    }

    public void showKeyboard(final EditText ettext) {

        ettext.setClickable(false);
        ettext.setFocusable(true);
        ettext.setFocusableInTouchMode(true);
        ettext.requestFocus();
        ettext.setSelection(ettext.getText().length());

        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 200);
    }


    private void hideSoftKeyboard(EditText ettext) {
        ettext.setClickable(false);
        ettext.setFocusable(false);
        ettext.setFocusableInTouchMode(false);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppLock.onActivityResumed(this);
    }
}
