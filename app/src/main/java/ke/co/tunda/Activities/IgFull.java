package ke.co.tunda.Activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.R;

public class IgFull extends AppCompatActivity {
    @BindView(R.id.back_nav)
    ImageView mBackImg;
    @BindView(R.id.profile_image)
    ImageView mProfileImage;
    @BindView(R.id.title)
    TextView mUserName;
    @BindView(R.id.menu_options)
    TextView mMenu;
    @BindView(R.id.image_ig)
    ImageView mImage;
    private SharedPreferences sp;
    private static String user_name;
    private  static String image;
    private static String image_url;

    public static void open(Context context, String url, ActivityOptions options,String name,String photo_path) {
        Intent intent = new Intent(context, IgFull.class);
        if (options != null) {
            // Check if we're running on Android 5.0 or higher
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.startActivity(intent, options.toBundle());
            } else {
                context.startActivity(intent);
            }

        } else {
            context.startActivity(intent);
        }
        image_url = url;
        user_name = name;
        image = photo_path;


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ig_full);
        ButterKnife.bind(this);


        if (user_name != null && user_name.contains(" ")) {
            String[] parts = user_name.split(" ");
            user_name = parts[0];

        }

        String [] parts = image_url.split("~~");


        Glide.with(this).load(image).into(mProfileImage);
        Glide.with(this).load(parts[0]).into(mImage);


        mMenu.setText(getResources().getString(R.string.time_past,calculatetoNow(parts[1])));
        Log.d("IGF", "onCreate: "+parts[1]);
        mUserName.setText(getResources().getString(R.string.user_name_ig, user_name));
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    private String calculatetoNow(String date){
        try {
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            Date past = format.parse(date);
            Date now = new Date();

//            System.out.println(TimeUnit.MILLISECONDS.toMillis(now.getTime() - past.getTime()) + " milliseconds ago");
//            System.out.println(TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) + " minutes ago");
//            System.out.println(TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) + " hours ago");
//            System.out.println(TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) + " days ago");
            return String.valueOf(TimeUnit.MILLISECONDS.toDays(now.getTime()-past.getTime()));
        }
        catch (Exception j){
            Log.d("IGF", "calculatetoNow: "+j.getMessage());
        }
        return null;
    }
}
