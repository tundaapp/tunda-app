

package ke.co.tunda.Views.graphic;

import android.graphics.Bitmap;

public interface IBitmapDrawable {

	Bitmap getBitmap();
}
