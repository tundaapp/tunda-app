

package ke.co.tunda.Views;

import android.view.View;

public interface ViewTouchListener {
  void onStartViewChangeListener(View view);
  void onStopViewChangeListener(View view);
}
