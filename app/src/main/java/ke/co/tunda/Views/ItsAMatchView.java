

package ke.co.tunda.Views;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.preference.PreferenceManager;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Callbacks.ExitMatchView;
import com.bumptech.glide.Glide;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Matches;

/**
 * Created by yarolegovich on 08.03.2017.
 */

public class ItsAMatchView extends LinearLayout {


    CircleImageView user_profile_photo;
    CircleImageView match_profile_photo;
    TextView itsAmatchText;
    Button keep_swiping;
    Button message_match;

    private Paint gradientPaint;
    private ExitMatchView callback;


    public ItsAMatchView(Context context) {
        super(context);
    }

    public ItsAMatchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ItsAMatchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ItsAMatchView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    {

        gradientPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        setWillNotDraw(false);

        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);
        inflate(getContext(), R.layout.view_forecast, this);


        user_profile_photo=findViewById(R.id.user_profile_photo);
        match_profile_photo=findViewById(R.id.match_profile_photo);
        itsAmatchText=findViewById(R.id.itsAmatchText);
        keep_swiping=findViewById(R.id.keep_swiping);
        message_match=findViewById(R.id.message_match);

    }



    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRect(0, 0, getWidth(), getHeight(), gradientPaint);
        super.onDraw(canvas);
    }

    public void setForecast(Matches forecast,Context context,ExitMatchView mCallback) {
        this.callback=mCallback;
        Log.d("HTTP", "setForecast: "+forecast.getPhotoPath());

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        String imageProfile = sp.getString("image", "");
        int user_id = sp.getInt("user_id", 0);


        Glide.with(getContext()).load(forecast.getPhotoPath()).into(match_profile_photo);
        itsAmatchText.setText("You and " + forecast.getFullName() + " like each other");
        Glide.with(getContext()).load(imageProfile).into(user_profile_photo);
        keep_swiping.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callback.keepSwiping();
                }catch (Exception e){
                    Log.d("TEST", e.getMessage());
                }


            }
        });

        message_match.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callback.sendMessage(forecast);
                }catch ( Exception e){
                    Log.d("TEST", e.getMessage());
                }




            }
        });

    }




}
