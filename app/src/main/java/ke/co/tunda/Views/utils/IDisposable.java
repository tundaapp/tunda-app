

package ke.co.tunda.Views.utils;

public interface IDisposable {
	void dispose();
}
