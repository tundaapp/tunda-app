

package ke.co.tunda.multimediapicker.listeners;

import ke.co.tunda.multimediapicker.model.Folder;

/**
 * Created by admin on 03/02/2017.
 */

public interface OnFolderClickListener {

    void onFolderClick(Folder bucket);
}
