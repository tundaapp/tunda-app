package ke.co.tunda.ChatKit;

import com.stfalcon.chatkit.messages.MessagesListAdapter;

import androidx.appcompat.app.AppCompatActivity;

public class MessagesPage extends AppCompatActivity implements MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnLoadMoreListener {
    @Override
    public void onLoadMore(int page, int totalItemsCount) {

    }

    @Override
    public void onSelectionChanged(int count) {

    }
}
