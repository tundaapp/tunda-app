

package ke.co.tunda.ChatKit;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.guardanis.applock.AppLock;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.utils.DateFormatter;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.Activities.MainActivity;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Builder4;
import ke.co.tunda.ApiConnector.Models.ChatHistoryParams;
import ke.co.tunda.ApiConnector.Models.ChatHistoryResponse;
import ke.co.tunda.ApiConnector.Models.ChatInfo;
import ke.co.tunda.ApiConnector.Models.ChatKey;
import ke.co.tunda.ApiConnector.Models.LogOutUserParams;
import ke.co.tunda.ApiConnector.Models.ReportUserParams;
import ke.co.tunda.ApiConnector.Models.RequestCallParams;
import ke.co.tunda.Callbacks.RetrySendingMessage;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.ChatKit.ChatKitModels.Message;
import ke.co.tunda.ChatKit.ChatKitModels.User;
import ke.co.tunda.Constants.Extras;

import com.bumptech.glide.Glide;

import ke.co.tunda.Models.DefaultResponse;
import ke.co.tunda.Models.DeleteMessageParams;
import ke.co.tunda.Models.MyMatches;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.Room.ChatDialogsTable;
import ke.co.tunda.Room.ChatHistoryTable;
import ke.co.tunda.Room.ChatsViewModel;
import ke.co.tunda.Room.DatabaseRoom;
import ke.co.tunda.Room.TundaRoomDao;
import ke.co.tunda.SqLite.DatabaseHandler;
import ke.co.tunda.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomHolderMessagesActivity extends AppCompatActivity
        implements
        MessageInput.InputListener,
        MessageInput.AttachmentsListener, MessageInput.TypingListener, RetrySendingMessage,
        MessagesListAdapter.SelectionListener, DateFormatter.Formatter {
    private static final int MY_PERMISSIONS_REQUEST_CALL = 101;
    private String[] starters = {

            "Live as if you die today , dream as if you live forever.", "Are your hands tied or something?",
            "if only there was a a way to start a conversation . Oh wait…", "You don't have to be lonely .. but you have to say hi",
            "No one likes a bore", "if you don’t ask , the answer is always no", "What are you acting shy for?", "Are you waiting for an invitation?!\uD83E\uDD23\uD83E\uDD23",
            "Say thanks for swiping right.hehehe!", "Have some courtesy,say “Hello”", "How long are you going to stare at this screen?",
            "No one likes a bore", "You’re not getting any younger"
    };
    static Dialog dialogue;
    Dialog convo;
    String json_response = "";
    User Me, myMatch;
    ArrayList<Message> messages = new ArrayList<>();
    ArrayList<Message> new_messages = new ArrayList<>();
    RelativeLayout relativeLayout;
    String typed_message = "";
    private ChatHistory[] history = null;
    private int user_id;
    private ApiCaller apicaller;
    static int position;
    @BindView(R.id.cm_holder)
    RelativeLayout mMainHolder;
    @BindView(R.id.txtMessagesError)
    TextView mErrorMsg;
    @BindView(R.id.btn_retry_user_chats)
    Button mRetryMessages;
    @BindView(R.id.error_layout)
    RelativeLayout mErrorLayout;
    @BindView(R.id.match_to_chat)
    CircleImageView mMatchTochat;
    @BindView(R.id.no_chats)
    RelativeLayout mNoChats;
    @BindView(R.id.no_chats_msg)
    TextView mNoChatsMsg;
    @BindView(R.id.startChatting)
    TextView mStartChatting;
    @BindView(R.id.cause)
    EditText mEdit;
    //    @BindView(R.id.messageRecycler)
//    RecyclerView messageRecycler;
    @BindView(R.id.loadingIndicator)
    AVLoadingIndicatorView mLoader;
    @BindView(R.id.profile_image)
    CircleImageView profile_image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.profile_details_click)
    RelativeLayout mProfileDetails;
    @BindView(R.id.back_nav)
    ImageView mBackNav;
    @BindView(R.id.menu_options)
    ImageView mMenuOptions;
    @BindView(R.id.messagesList)
    MessagesList mMessageList;
    @BindView(R.id.input)
    MessageInput mInput;
    @BindView(R.id.me)
    CircleImageView mMe;
    @BindView(R.id.my_toolbar)
    Toolbar mToolbar;
    private boolean isWaitingForNetwork = false;
    private NetworkChangeReceiver networkChangeReceiver;


    private String uniqueID;
    private String report_type = "Inappropriate Messages";
    private SharedPreferences sp;
    private MessagesListAdapter<Message> adapter;
    public boolean socketstarted = false;
    private static String TAG = "CHM";
    private String mProfilePhoto;
    private Snackbar mSnackbar;
    private int selectionCount;
    private Menu mMenu;

    private Animation animFadeIn;
    private Animation animFadeOut;

    private boolean isDataChanges = false;


    private static CustomHolderMessagesActivity mInstance;
    private static final int MY_PERMISSIONS3_REQUEST_CALL = 1234;


    private Socket mSocket;
    private String mPhoneNumber;
    private TundaRoomDao tundaRoomDao;
    private ChatsViewModel chatsViewModel;
    ArrayList<ChatKey> chats;

    {
        try {
//            mSocket = IO.socket("https://api.tunda.mobi/tunda-chat");
            mSocket = IO.socket("http://207.180.212.198:3000");

        } catch (URISyntaxException e) {
            Log.d(TAG, "instance initializer: " + e.getMessage());
        }
    }


    public static synchronized CustomHolderMessagesActivity getInstance() {
        return mInstance;
    }

    public static void open(Context context, Dialog dialog, int mPosition, ActivityOptions options) {
        Intent intent = new Intent(context, CustomHolderMessagesActivity.class);
        if (options != null) {
            // Check if we're running on Android 5.0 or higher
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.startActivity(intent);
            } else {
                context.startActivity(intent);
            }

        } else {
            context.startActivity(intent);
        }
        dialogue = dialog;
        position = mPosition;
    }


    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        networkChangeReceiver = new NetworkChangeReceiver();
        registerReceiver(networkChangeReceiver, filter);

        animFadeIn = AnimationUtils.loadAnimation(this,
                R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(this,
                R.anim.fade_out);


        Log.d("SOCKET", "onCreate: ");
        setContentView(R.layout.activity_custom_holder_messages);
        ButterKnife.bind(this);

        Log.d(TAG, "onCreate ID: " + dialogue.getId());

        if (dialogue.getId() != null && dialogue.getId().equals("0")) {
            mInput.setVisibility(View.GONE);
            mProfileDetails.setEnabled(false);
        } else {
            mProfileDetails.setEnabled(true);
            mInput.setVisibility(View.VISIBLE);
        }


        setSupportActionBar(mToolbar);
        mToolbar.setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        tundaRoomDao = DatabaseRoom.getDatabase(this).tundaRoomDao();
        chats = new ArrayList<>();


        apicaller = Builder.getClient().create(ApiCaller.class);
        mSnackbar = Snackbar.make(mMainHolder, "You are offline, waiting for network ...", Snackbar.LENGTH_INDEFINITE);


        try {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            user_id = sp.getInt("user_id", 0);
            mProfilePhoto = sp.getString("image", "");
            mPhoneNumber = sp.getString("phone_number", "");

            JSONObject requestParams = new JSONObject();
            try {
                requestParams.put("request_type", "GET_USER_CHATS");
                requestParams.put("match_id", dialogue.getId());
                requestParams.put("user_id", user_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            initAdapter();


        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e.getMessage());

        }


        convo = dialogue;


        Glide.with(this)
                .load(convo.getDialogPhoto())
                .into(profile_image);
        chatsViewModel = ViewModelProviders.of(this).get(ChatsViewModel.class);
        mLoader.show();

        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("request_type", "GET_USER_CHATS");
            requestParams.put("match_id", dialogue.getId());
            requestParams.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.connect().emit("join", requestParams).on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "call: room joined");
            }
        }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("Error connecting", args[0].toString());
            }
        });


        if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
            tundaRoomDao.getAllChatsLive(dialogue.getId()).observe(this, new Observer<List<ChatHistoryTable>>() {
                @Override
                public void onChanged(List<ChatHistoryTable> chatHistoryTables) {
                    if (chatHistoryTables != null && chatHistoryTables.size() > 0) {
                        Log.d(TAG, "onChanged: " + chatHistoryTables.size());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mLoader.hide();
                                if (mNoChats.getVisibility() == View.VISIBLE) {
                                    mNoChats.animate().alpha(1.0f);
                                    mNoChats.setVisibility(View.GONE);
                                }
                                if (mRetryMessages.getVisibility() == View.VISIBLE) {
                                    mRetryMessages.animate().alpha(0.1f);
                                    mRetryMessages.setVisibility(View.GONE);
                                }

                                if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
                                    mInput.animate().alpha(1.0f);
                                    mInput.setVisibility(View.VISIBLE);
                                }

                                mMessageList.animate().alpha(1.0f);
                                mMessageList.setVisibility(View.VISIBLE);
//
                                if (adapter != null && !adapter.isEmpty()) {
                                    adapter.clear();
                                }

                                for (int i = 0; i < chatHistoryTables.size(); i++) {
                                    ChatHistoryTable mChat = chatHistoryTables.get(i);
                                    Date date_created = null;
                                    SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
                                    String strDate = mChat.date_created;
                                    try {
                                        date_created = formatter.parse(strDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    User user;
                                    Message msg;
                                    uniqueID = UUID.randomUUID().toString();

                                    user = new User(String.valueOf(mChat.user_id), "", null, false);
                                    msg = new Message(String.valueOf(mChat.chat_id), user, mChat.message,
                                            date_created);
                                    adapter.addToStart(msg, true);


                                }


                            }
                        });
                    } else {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                mNoChats.animate().alpha(1.0f);
                                mNoChats.setVisibility(View.VISIBLE);

                                mMessageList.animate().alpha(0.1f);
                                mMessageList.setVisibility(View.GONE);
                                mLoader.hide();

                                if (dialogue.getId() != null && dialogue.getId().equals("0")) {
                                    Glide.with(getApplicationContext())
                                            .load(dialogue.getDialogPhoto())
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(mMatchTochat);
                                } else {
                                    Glide.with(getApplicationContext())
                                            .load(dialogue.getDialogPhoto())
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(mMatchTochat);
                                    Glide.with(getApplicationContext()).
                                            load(mProfilePhoto
                                            ).into(mMe);
                                }


                                if (dialogue.getId() != null && dialogue.getId().equals("0")) {
                                    mNoChatsMsg.setText("");

                                } else {
                                    mNoChatsMsg.setText(getResources().getString(R.string.you_and_so, dialogue.getDialogName()));

                                }

                                if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
                                    mInput.animate().alpha(1.0f);
                                    mInput.setVisibility(View.VISIBLE);
                                }

                                if (isInternetOn()) {
                                    mRetryMessages.animate().alpha(0.1f);
                                    mRetryMessages.setVisibility(View.GONE);
                                    Random r = new Random();
                                    int randomNumber = r.nextInt(starters.length);
                                    mStartChatting.animate().alpha(1.0f);
                                    mStartChatting.setVisibility(View.VISIBLE);
                                    mStartChatting.setText(starters[randomNumber]);
                                } else {
                                    mRetryMessages.animate().alpha(1.0f);
                                    mRetryMessages.setVisibility(View.VISIBLE);
                                    mStartChatting.setVisibility(View.VISIBLE);
                                    mStartChatting.setText("Connection Problem");
                                    mInput.setVisibility(View.GONE);

                                }


                            }
                        });

                        Log.d(TAG, "call: No history");
                    }

                }
            });

        }


        try {
            new getChatHistory().execute();
        } catch (Exception e) {
            Log.d(TAG, "ch: " + e.getMessage());
        }


        mSocket.on("message", new Emitter.Listener() {

            @Override
            public void call(final Object... args) {
                Log.d(TAG, "call: onMessage");
                Preferences.get(CustomHolderMessagesActivity.this).setIsTimeToRefreshCDialogss(true);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject response = (JSONObject) args[0];

                            if (response.getString("status_code").equals("00")) {
                                JSONObject body = response.getJSONObject("body");
                                String messagebody = body.getString("message");
                                String date = body.getString("date_created");
                                int recipient_id = body.getInt("recipient_id");
                                int user_id_sender = body.getInt("user_id");
                                int chat_id = body.getInt("chat_id");
                                String uniqueId = body.getString("message_id");
                                Log.d(TAG, "run: " + date);

                                Log.d(TAG, "run:chat id " + chat_id);
                                Log.d(TAG, "run: unique id" + uniqueId);
                                Log.d(TAG, "run: " + user_id_sender);


                                SimpleDateFormat formatterX = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss");
                                String strDate = null;
                                try {
                                    Date date1 = formatterX.parse(date);
                                    DateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
                                    strDate = dateFormat.format(date1);

                                } catch (ParseException e) {
                                    Log.d(TAG, "onChanged: " + e.getMessage());
                                }


                                ChatHistoryTable chatHistoryTable = new ChatHistoryTable(chat_id, user_id_sender,
                                        recipient_id, messagebody, strDate, "",
                                        dialogue.getId());
                                tundaRoomDao.insertChat(chatHistoryTable);
                                ChatDialogsTable chatDialogsTable = tundaRoomDao.findChatDialogById(Integer.valueOf(dialogue.getId()));
                                if (chatDialogsTable != null) {
                                    tundaRoomDao.updateChatDialogLastMessage(Integer.valueOf(dialogue.getId()), messagebody);
                                } else {
                                    Integer has_insta = dialogue.isHas_insta_photo() ? 1 : 0;
                                    ChatDialogsTable chatDialogsTable1 = new ChatDialogsTable(
                                            Integer.valueOf(dialogue.getId()),
                                            dialogue.getDialogName(),
                                            dialogue.getDialogPhoto(),
                                            null,
                                            "",
                                            messagebody,
                                            "",
                                            dialogue.getUnreadCount(),
                                            dialogue.getInterest(),
                                            Integer.valueOf(dialogue.getRecipient_id()),
                                            dialogue.getDob(),
                                            dialogue.getBio(),
                                            dialogue.getJob(),
                                            "",
                                            dialogue.getDistance(),
                                            dialogue.getPhoto_count(),
                                            dialogue.getVerfication_status(),
                                            dialogue.getLikes_count(), has_insta);
                                    tundaRoomDao.insertDialog(chatDialogsTable1);


                                }


                            } else {
                                Log.d(TAG, "run: else");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            }
        });


        mSocket.on("on_typing", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject response = (JSONObject) args[0];


                    int user_id_sender = response.getInt("user_id");


                    if (user_id != user_id_sender) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                title.setText(getString(R.string.typing));
                                title.startAnimation(animFadeIn);
                            }
                        });

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        mSocket.on("stop_typing", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject response = (JSONObject) args[0];

//                        JSONObject body = response.getJSONObject("body");

                    int user_id_sender = response.getInt("user_id");


                    if (user_id != user_id_sender) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                title.setText(dialogue.getDialogName());
                                title.startAnimation(animFadeIn);
                            }
                        });

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });


        mBackNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        mMenuOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptions();
            }
        });


        mProfileDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("CLICK", "onClick: ");
                try {
                    boolean hasMultipleImages = false;
                    if (dialogue.getPhoto_count() > 0) {

                        hasMultipleImages = dialogue.getPhoto_count() > 1;

                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptions options = ActivityOptions
                                .makeSceneTransitionAnimation(CustomHolderMessagesActivity.this, profile_image, "match_photo");
                        FullProfile.open(CustomHolderMessagesActivity.this, dialogue,
                                hasMultipleImages, false, Extras.ORIGIN_MESSAGES, 1234, options);
                    } else {
                        FullProfile.open(CustomHolderMessagesActivity.this, dialogue,
                                hasMultipleImages, false, Extras.ORIGIN_MESSAGES, 1234, null);
                    }

                } catch (Exception e) {
                    Log.d("FULLP", "onClick: " + e.getMessage());

                }

            }
        });


        title.setText(convo.getDialogName());


        mRetryMessages.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mNoChats.animate().alpha(0.1f);
                mNoChats.setVisibility(View.GONE);
                Log.d(TAG, "onClick: retrying /...");
                new getChatHistory().execute();
                if (mSnackbar.isShown()) {
                    mSnackbar.dismiss();
                }
            }
        });


        mInput.setInputListener(this);
        mInput.setAttachmentsListener(this);
        mInput.setTypingListener(this);


    }


    private void initAdapter() {

        adapter = new MessagesListAdapter<Message>(String.valueOf(user_id), null);
        if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
            adapter.enableSelectionMode(this);
        } else {
            adapter.disableSelectionMode();
        }

        adapter.setDateHeadersFormatter(this);
        mMessageList.setAdapter(adapter);

    }

    private void showOptions() {
        PopupMenu menu = new PopupMenu(CustomHolderMessagesActivity.this, mMenuOptions);
        menu.getMenuInflater().inflate(R.menu.chat_actions_menu, menu.getMenu());
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_report:
                        reportUser();
                        break;
                    case R.id.action_unmatch:
                        unMatch(dialogue.getRecipient_id());
                        break;
                }

                return false;
            }
        });
        menu.show();

    }


    @Override
    public boolean onSubmit(CharSequence input) {
        mNoChats.setVisibility(View.GONE);
        uniqueID = UUID.randomUUID().toString();


        typed_message = input.toString();
        sendMessage(user_id, Integer.valueOf(dialogue.getRecipient_id()), uniqueID, typed_message, "sent");
        return true;
    }

    private void sendMessage(int user_id, int recipient_id, String uniqueIDx, String typed_message, String status) {
        Log.d(TAG, "sendMessage: sending ...");


        ChatHistory history = new ChatHistory();
        history.setUniqueId(uniqueIDx);
        history.setMessage(typed_message);
        history.setUserId(user_id);
        history.setRecipientId(recipient_id);
        history.setmStatus(status);
        history.setDateCreated(new Date());
//        setUpRecyclerChat();


        if (mNoChats.getVisibility() == View.VISIBLE) {
            mNoChats.setVisibility(View.GONE);
        }

        if (mMessageList.getVisibility() == View.GONE) {
            mMessageList.setVisibility(View.VISIBLE);
        }

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        String strDate = dateFormat.format(date);


        JSONObject requestObject = new JSONObject();
        try {
            requestObject.put("request_type", "POST_MESSAGE");
            requestObject.put("recipient_id", dialogue.getRecipient_id());
            requestObject.put("user_id", user_id);
            requestObject.put("match_id", dialogue.getId());
            requestObject.put("message", typed_message);
            requestObject.put("message_id", uniqueID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "call: sending" + requestObject.toString());


        Preferences.get(CustomHolderMessagesActivity.this).setIsTimeToRefreshCDialogss(true);


        mSocket.emit("sendmessage", requestObject)
                .on(Socket.EVENT_CONNECT, new Emitter.Listener() {


                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "call: sendmesage conneted");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mSnackbar.isShown()) {
                                    mSnackbar.dismiss();
                                }
                            }
                        });
                    }
                })

                .on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "call: sendmessage Failed");
                        if (!mSnackbar.isShown()) {
                            mSnackbar.setText("Failed! You are offline");
                            mSnackbar.show();
                            mSnackbar.setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE);
                        }

                    }
                })

                .on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Log.d(TAG, "call: sendmessage Failed");
                        if (!mSnackbar.isShown()) {
                            mSnackbar.setText("Failed! You are offline");
                            mSnackbar.show();
                            mSnackbar.setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE);
                        }

                    }
                });

    }


    @Override
    public void onAddAttachments() {
    }


    private ChatHistory[] fetchResultsFrom(GetUserMatchesResponse getUserMatchesResponse) {
        return getUserMatchesResponse.getHistory();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.chat_actions_menu, menu);
            this.mMenu = menu;
            onSelectionChanged(0);

        }

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_report:
                Log.d(TAG, "onOptionsItemSelected: 1");
                try {
                    reportUser();
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                }

                break;
            case R.id.action_unmatch:
                Log.d(TAG, "onOptionsItemSelected: 1");
                unMatch(dialogue.getRecipient_id());
                break;
            case R.id.action_delete:
                Log.d(TAG, "onOptionsItemSelected: " + adapter.getSelectedMessages().get(0).getUser().getId());
                Log.d(TAG, "onOptionsItemSelected msg: " + adapter.getSelectedMessages().get(0).getId());
                DeleteMessageParams params = new DeleteMessageParams();
                params.setRequestType("DELETE_USER_CHAT");
                if (chats.size() > 0) {
                    chats.clear();
                }
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                builder.setTitle("Delete Messages");

                builder.addButton("Delete for me", -1, getResources().getColor(R.color.colorAccent),
                        CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.d(TAG, "onClicksize: " + adapter.getSelectedMessages().size());
                                for (int x = 0; x < adapter.getSelectedMessages().size(); x++) {
                                    Log.d(TAG, "message ID: " + adapter.getSelectedMessages().get(x).getId());
//                                    chatKey.setId(Integer.valueOf(adapter.getSelectedMessages().get(x).getId()));
                                    chats.add(new ChatKey(Integer.valueOf(adapter.getSelectedMessages().get(x).getId())));
                                }
                                params.setChats(chats);
                                params.setUserId(user_id);
                                Call<DefaultResponse> call = apicaller.deleteMessages(params);
                                call.enqueue(new Callback<DefaultResponse>() {
                                    @Override
                                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                                        if (response.isSuccessful()) {
                                            DefaultResponse defaultResponse = response.body();
                                            if (defaultResponse != null && defaultResponse.getStatusMessage().equals("SUCCESS")) {
                                                Toasty.success(CustomHolderMessagesActivity.this, "messages deleted", Toasty.LENGTH_SHORT).show();
                                                adapter.deleteSelectedMessages();
                                                dialogInterface.dismiss();
                                            } else {
                                                Toasty.warning(CustomHolderMessagesActivity.this, "messages not deleted fully", Toasty.LENGTH_SHORT).show();
                                                dialogInterface.dismiss();

                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                                        Toasty.error(CustomHolderMessagesActivity.this, "messages not deleted, check internet", Toasty.LENGTH_SHORT).show();
                                        dialogInterface.dismiss();


                                    }
                                });


                            }
                        });
                builder.addButton("Delete for Everyone", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                for (int x = 0; x < adapter.getSelectedMessages().size(); x++) {
                                    chats.add(new ChatKey(Integer.valueOf(adapter.getSelectedMessages().get(x).getId())));
                                }
                                params.setChats(chats);
                                params.setUserId(0);
                                Call<DefaultResponse> call = apicaller.deleteMessages(params);
                                call.enqueue(new Callback<DefaultResponse>() {
                                    @Override
                                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                                        if (response.isSuccessful()) {
                                            DefaultResponse defaultResponse = response.body();
                                            if (defaultResponse != null && defaultResponse.getStatusMessage().equals("SUCCESS")) {
                                                Toasty.success(CustomHolderMessagesActivity.this, "messages deleted", Toasty.LENGTH_SHORT).show();
                                                adapter.deleteSelectedMessages();
                                                dialogInterface.dismiss();
                                            } else {
                                                Toasty.warning(CustomHolderMessagesActivity.this, "messages not deleted fully", Toasty.LENGTH_SHORT).show();
                                                dialogInterface.dismiss();

                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                                        Toasty.error(CustomHolderMessagesActivity.this, "messages not deleted, check internet", Toasty.LENGTH_SHORT).show();
                                        dialogInterface.dismiss();


                                    }
                                });

                                dialogInterface.dismiss();


                            }
                        });

                builder.show();


                break;
            case R.id.action_copy:
                adapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
                AppUtils.showToast(this, R.string.copied_message, true);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.call:
                if (mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
                    if (ActivityCompat.checkSelfPermission(CustomHolderMessagesActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        CFAlertDialog.Builder builderX = new CFAlertDialog.Builder(CustomHolderMessagesActivity.this);
                        builderX.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                        builderX.setTitle("Make call");
                        builderX.setCancelable(true);
                        builderX.setTextGravity(Gravity.CENTER);
                        builderX.setMessage("Allow Tunda to make a call");
                        builderX.addButton("Allow", -1, getResources().getColor(R.color.colorAccent), CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        Dexter.withActivity(CustomHolderMessagesActivity.this)
                                                .withPermissions(Manifest.permission.CALL_PHONE)
                                                .withListener(new MultiplePermissionsListener() {
                                                    @Override
                                                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                                                        if (report.areAllPermissionsGranted()) {
                                                            dialog.dismiss();
                                                            openCall();
                                                        }

                                                        if (report.isAnyPermissionPermanentlyDenied()) {
                                                            dialog.dismiss();
                                                            AppUtils.showToast(CustomHolderMessagesActivity.this, "Tunda needs your permission to call", false);

                                                        }
                                                    }


                                                    @Override
                                                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                                        dialog.dismiss();
                                                        token.continuePermissionRequest();
                                                    }
                                                }).check();


                                    }
                                });

                        builderX.show();
                    } else {
                        openCall();
                    }
                } else {
                    Toasty.warning(getApplicationContext(), "Add and verify your phone number", Toasty.LENGTH_SHORT).show();
                }

                break;


        }
        return super.onOptionsItemSelected(item);
    }

    private void openCall() {
        MaterialDialog dialog;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(CustomHolderMessagesActivity.this);
        builder.content("Connecting ....")
                .widgetColor(getResources().getColor(R.color.colorAccent))
                .progress(true, 0);
        dialog = builder.build();
        dialog.setCancelable(false);
        dialog.show();


        RequestCallParams params = new RequestCallParams();
        params.setRequestType("REQUEST_CALL");
        params.setUserId(user_id);
        params.setmPhoneNumber(mPhoneNumber);

        params.setRecipient_id(Integer.valueOf(dialogue.getRecipient_id()));
        ApiCaller mx = Builder4.getClient().create(ApiCaller.class);

        Call<ke.co.tunda.ApiConnector.Models.Response> call = mx.sendCall(params);
        call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
            @Override
            public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                if (response.isSuccessful()) {
                    ke.co.tunda.ApiConnector.Models.Response response1 = response.body();
                    if (response1 != null && response1.getStatusCode() != null && response1.getStatusCode().equals("0")) {
                        dialog.dismiss();
                        Toasty.success(getApplicationContext(), response1.getBody(), Toasty.LENGTH_SHORT).show();

                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + "0900 620 470"));
                        CustomHolderMessagesActivity.this.startActivity(intent);


                    } else {
                        dialog.dismiss();
                        if (response1 != null) {
                            Toasty.normal(getApplicationContext(), response1.getBody(), Toasty.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    dialog.dismiss();
                    Toasty.error(getApplicationContext(), "Something went wrong, its our fault", Toasty.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                dialog.dismiss();
                Toasty.normal(getApplicationContext(), "problem connecting. check internet and try again", Toasty.LENGTH_SHORT).show();

            }
        });
    }

    private void reportUser() {


        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle("What seems to be the issue");
        builder.setSingleChoiceItems(new String[]{"Inappropriate Images", "Inappropriate Messages", "Other"}, 1,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index) {
                        if (index == 2) {
                            dialogInterface.dismiss();
                            CFAlertDialog.Builder builder = new CFAlertDialog.Builder(CustomHolderMessagesActivity.this);
                            builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                            builder.setTitle("Other");
                            builder.setTextGravity(Gravity.CENTER_HORIZONTAL);
                            mEdit.setVisibility(View.VISIBLE);
                            builder.setFooterView(mEdit);
                            builder.addButton("Report", -1, getResources().getColor(R.color.colorAccent),
                                    CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            String text = mEdit.getText().toString();
                                            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

                                            ReportUserParams params = new ReportUserParams();
                                            params.setSuspectId(dialogue.getRecipient_id());
                                            params.setUserId(String.valueOf(user_id));
                                            params.setRequestType("REPORT_USER");
                                            params.setReportType(text);

                                            Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.reportUser(params);
                                            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                                                @Override
                                                public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                                                    if (response.isSuccessful()) {
                                                        ke.co.tunda.ApiConnector.Models.Response resp = response.body();
                                                        if (resp.getBody().equals("SUCCESS")) {
                                                            AppUtils.showToast(getApplicationContext(), "Reported", false);
                                                        } else {
                                                            AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                                        }
                                                    } else {
                                                        AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                                                    AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);

                                                }
                                            });

                                        }
                                    });
                            if (mEdit.getParent() != null) {
                                ((ViewGroup) mEdit.getParent()).removeView(mEdit);
                            }


                            builder.show();

                        } else if (index == 0) {
                            report_type = "Inappropriate Images";
                        } else if (index == 1) {
                            report_type = "Inappropriate Messges";
                        }
                    }
                });
        builder.addButton("Report", -1, getResources().getColor(R.color.colorAccent),
                CFAlertDialog.CFAlertActionStyle.POSITIVE,
                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);

                        ReportUserParams params = new ReportUserParams();
                        params.setSuspectId(dialogue.getRecipient_id());
                        params.setUserId(String.valueOf(user_id));
                        params.setRequestType("REPORT_USER");
                        params.setReportType(report_type);

                        Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.reportUser(params);
                        call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                            @Override
                            public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                                if (response.isSuccessful()) {
                                    ke.co.tunda.ApiConnector.Models.Response resp = response.body();
                                    if (resp.getBody().equals("SUCCESS")) {
                                        AppUtils.showToast(getApplicationContext(), "Reported", false);
                                    } else {
                                        AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                    }
                                } else {
                                    AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);
                                }
                            }

                            @Override
                            public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                                AppUtils.showToast(getApplicationContext(), "Something went wrong, try Again", false);

                            }
                        });
                    }
                });

        builder.show();


    }

    private void unMatch(String recipient_id) {

        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
        builder.setTitle("Unmatch " + dialogue.getDialogName());
        builder.setTextGravity(Gravity.CENTER);
        builder.setMessage("Are you Sure?");
        builder.addButton("Yes", -1, getResources().getColor(R.color.colorAccent),
                CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UnmatchParams params = new UnmatchParams();
                        params.setMatchId(dialogue.getId());
                        params.setRequestType("UNMATCH_USER");
                        params.setUserId(String.valueOf(user_id));
                        Call<UnmatchResponse> call = apicaller.unmatchUser(params);
                        call.enqueue(new Callback<UnmatchResponse>() {
                            @Override
                            public void onResponse(Call<UnmatchResponse> call, Response<UnmatchResponse> response) {
                                if (response.isSuccessful()) {
                                    UnmatchResponse unmatchResponse = response.body();
                                    if (unmatchResponse.getStatusCode().equals("00")) {
                                        dialog.dismiss();
                                        Toasty.success(getApplicationContext(), "Unmatched").show();
                                        Intent intent = new Intent(CustomHolderMessagesActivity.this, MainActivity.class);
                                        intent.putExtra("position", 2);
                                        removeMatchFromdatabase(dialogue.getRecipient_id());
                                        removeDialogFromdatabase(dialogue.getRecipient_id());
                                        startActivity(intent);


                                        finish();
                                    } else {
                                        dialog.dismiss();
                                        Toasty.warning(getApplicationContext(), "Something went wrong, Try Again").show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<UnmatchResponse> call, Throwable t) {
                                dialog.dismiss();
                                Toasty.warning(getApplicationContext(), "Something went wrong, Try Again").show();

                            }
                        });

                    }
                }).addButton("Cancel", -1, getResources().getColor(R.color.colorPrimary),
                CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        builder.show();


    }

    private void removeMatchFromdatabase(String recipient_id) {
        DatabaseHandler handler = new DatabaseHandler(getApplicationContext());
        MyMatches myMatches = new MyMatches(Integer.valueOf(recipient_id), dialogue.getDialogName(), dialogue.getDialogPhoto(), 0, "56");

        handler.deleteFavourite(myMatches);
    }

    private void removeDialogFromdatabase(String recipient_id) {
        DatabaseHandler handler = new DatabaseHandler(getApplicationContext());
        ChatInfo chatInfo = new ChatInfo(Integer.parseInt(dialogue.getId()), dialogue.getDialogName(), dialogue.getDialogPhoto(),
                0, "67",
                Integer.parseInt(dialogue.getRecipient_id()), "jb", "434", 6, dialogue.getInterest());

        handler.deleteDialog(chatInfo);
    }


    @Override
    public void onBackPressed() {
        if (selectionCount == 0) {
            if (position == 1) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();


                } else {


                    finish();
                }

            }
        } else {
            adapter.unselectAllItems();
        }


    }

    @Override
    public void onStartTyping() {
        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("match_id", dialogue.getId());
            requestParams.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("on_typing", requestParams);


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!socketstarted) {
//            networkRequest("on resume");
//            initializeChat();
            socketstarted = true;
        }
    }

    @Override
    public void onStopTyping() {
        JSONObject requestParams = new JSONObject();
        try {
            requestParams.put("match_id", dialogue.getId());
            requestParams.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("stop_typing", requestParams);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d("SOCKET", "onDestroy: ");
        mSocket.disconnect();
        mSocket.off("message");
        mSocket.off("on_typing");
        mSocket.off("stop_typing");
        if (networkChangeReceiver != null) {
            unregisterReceiver(networkChangeReceiver);

            new LogoutUser().execute();
        }

        super.onDestroy();
        finish();

    }

    private void disconnectSocket() {
        mSocket.off("message");
        mSocket.off("join");
        mSocket.off("messagehistory");
        mSocket.off("sendmessage");
        mSocket.disconnect();
    }

    @Override
    public void retrySendMessage(ChatHistory history) {
        sendMessage(user_id, Integer.valueOf(dialogue.getRecipient_id()), history.getUniqueId(), history.getMessage(), "retrying..");
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("SOCKET", "onStart: ");

    }


    @Override
    protected void onStop() {
        super.onStop();
//        disconnectSocket();
    }

    @Override
    public void onSelectionChanged(int count) {
        this.selectionCount = count;
        if (mMenu != null) {

            adapter.getSelectedMessages();
            mMenu.findItem(R.id.action_copy).setVisible(count > 0);
            mMenu.findItem(R.id.action_delete).setVisible(count > 0);
        } else {
            Log.d(TAG, "onSelectionChanged: Mmenu is null");
        }


    }

    @Override
    public String format(Date date) {
        if (DateFormatter.isToday(date)) {
            return getString(R.string.date_header_today);
        } else if (DateFormatter.isYesterday(date)) {
            return getString(R.string.date_header_yesterday);
        } else {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR);
        }
    }

    protected class getChatHistory extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... voids) {


            ChatHistoryParams params = new ChatHistoryParams();
            params.setRequestType("GET_USER_CHATS");
            params.setUser_id(user_id);
            params.setMatch_id(dialogue.getId());
            Call<ChatHistoryResponse> mCall = apicaller.getChatHistory(params);
            mCall.enqueue(new Callback<ChatHistoryResponse>() {
                @Override
                public void onResponse(Call<ChatHistoryResponse> call, Response<ChatHistoryResponse> response) {
                    if (response.isSuccessful()) {
                        ChatHistoryResponse mResp = response.body();
                        if (mResp != null) {
                            if (mResp.getStatusCode().equals("00")) {
                                history = mResp.getBody();


                                if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
                                    tundaRoomDao.deleteAllChats(dialogue.getId());

                                    for (ChatHistory aHistory : history) {

                                        SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
                                        String strDate = formatter.format(aHistory.getDateCreated());

                                        ChatHistoryTable chatHistoryTable = new ChatHistoryTable(aHistory.getChatId(), aHistory.getUserId(),
                                                aHistory.getRecipientId(), aHistory.getMessage(), strDate, aHistory.getmStatus(),
                                                dialogue.getId());
                                        tundaRoomDao.insertChat(chatHistoryTable);

                                    }
                                } else {


                                    for (ChatHistory aHistory : history) {
                                        Date date_created = null;
                                        SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
                                        String strDate = formatter.format(aHistory.getDateCreated());
                                        try {
                                            date_created = formatter.parse(strDate);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        User user;
                                        Message msg;
                                        uniqueID = UUID.randomUUID().toString();

                                        user = new User(String.valueOf(aHistory.getUserId()), "", null, false);
                                        msg = new Message(String.valueOf(aHistory.getUserId()), user, aHistory.getMessage(),
                                                date_created);
                                        adapter.addToStart(msg, true);
                                    }


                                }


                            } else {
                                if (dialogue.getId() != null && !dialogue.getId().equals("0")) {
                                    tundaRoomDao.deleteAllChats(dialogue.getId());
                                }


                                Log.d(TAG, "call: No history");
                            }
                        } else {
                            Log.d(TAG, "response null onResponse: ");
                        }
                    }

                }

                @Override
                public void onFailure(Call<ChatHistoryResponse> call, Throwable t) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSnackbar.show();
///
                        }
                    });


                }
            });


            return null;
        }
    }

    private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
        return new MessagesListAdapter.Formatter<Message>() {
            @Override
            public String format(Message message) {

                String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
                        .format(message.getCreatedAt());

                String text = message.getText();
                if (text == null) text = "[attachment]";

                return String.format(Locale.getDefault(), "%s: %s (%s)",
                        message.getUser().getName(), text, createdAt);
            }
        };
    }

    public boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {


            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {


            return false;
        }
        return false;
    }


    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (isInternetOn()) {
                if (mSnackbar.isShown()) {
                    mSnackbar.dismiss();
                    try {
//                        mSocket = IO.socket("https://api.tunda.mobi/tunda-chat");
                        mSocket = IO.socket("http://207.180.212.198:3000");
                        JSONObject requestParams = new JSONObject();
                        try {
                            requestParams.put("request_type", "GET_USER_CHATS");
                            requestParams.put("match_id", dialogue.getId());
                            requestParams.put("user_id", user_id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSocket.connect().emit("join", requestParams).on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                Log.d(TAG, "call: room joined");
                            }
                        });
                    } catch (URISyntaxException e) {
                        Log.d(TAG, "instance initializer: " + e.getMessage());
                    }

                }
            } else {
                if (!mSnackbar.isShown()) {
                    mSnackbar.show();
                }

            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppLock.onActivityResumed(this);
    }


    private class LogoutUser extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            LogOutUserParams params = new LogOutUserParams();
            params.setRequestType("USER_LOGOUT");
            params.setUserId(user_id);
            Call<ke.co.tunda.ApiConnector.Models.Response> call = apicaller.logOut(params);
            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                @Override
                public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        ke.co.tunda.ApiConnector.Models.Response mResponse = response.body();

                        if (mResponse.getBody().equals("SUCCESS")) {
                            Log.d(TAG, "onResponse: logged out");
                        } else {
                            Log.d(TAG, "onFailure: no logout" + mResponse.getBody());

                        }
                    } else {
                        Log.d(TAG, "onFailure: no logout");

                    }
                }

                @Override
                public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: no logout");

                }
            });
            return null;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + "0900 620 470"));
                    CustomHolderMessagesActivity.this.startActivity(intent);
                } else {
                    Toasty.warning(getApplicationContext(), "We need your permission to enable call", Toasty.LENGTH_SHORT).show();
                }
            }
        }
    }


    private class deleteChat extends AsyncTask<Void, Void, Void> {
        DeleteMessageParams mParams;

        public deleteChat(DeleteMessageParams params) {
            this.mParams = params;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<DefaultResponse> call = apicaller.deleteMessages(mParams);
            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    if (response.isSuccessful()) {
                        DefaultResponse defaultResponse = response.body();
                        if (defaultResponse != null && defaultResponse.getStatusMessage().equals("SUCCESS")) {
                            Toasty.success(CustomHolderMessagesActivity.this, "messages deleted", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(CustomHolderMessagesActivity.this, "messages not deleted fully", Toasty.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {

                }
            });
            return null;
        }
    }
}




