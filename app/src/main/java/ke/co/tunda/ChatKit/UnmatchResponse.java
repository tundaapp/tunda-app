

package ke.co.tunda.ChatKit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnmatchResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private String body;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
