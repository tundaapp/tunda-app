

package ke.co.tunda.ChatKit.ChatKitModels;

import com.stfalcon.chatkit.commons.models.IDialog;

import java.util.ArrayList;


public class Dialog implements IDialog<Message> {

    private String id;
    private String dialogPhoto;
    private String dialogName;
    private ArrayList<User> users;
    private Message lastMessage;
    private String recipient_id;

    private int unreadCount;
    private String dob;
    private String distance;
    private String bio;
    private String job;
    private String education;
    private int photo_count;
    private String interest;
    private Integer verfication_status;
    private Integer likes_count;
    private boolean has_insta_photo;

    public Dialog(String id, String name, String photo,
                  ArrayList<User> users, Message lastMessage, int unreadCount, String recipient_id, String dob,
                  String distance, String bio, String job, String education, int photo_count,
                  String interest, Integer verfication_status, Integer likes_count, boolean has_insta_photo) {

        this.id = id;
        this.dialogName = name;
        this.dialogPhoto = photo;
        this.users = users;
        this.lastMessage = lastMessage;
        this.unreadCount = unreadCount;
        this.recipient_id = recipient_id;
        this.dob = dob;
        this.distance = distance;
        this.bio = bio;
        this.job = job;
        this.education = education;
        this.photo_count = photo_count;
        this.interest = interest;
        this.verfication_status = verfication_status;
        this.likes_count = likes_count;
        this.has_insta_photo = has_insta_photo;

    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getDialogPhoto() {
        return dialogPhoto;
    }

    @Override
    public String getDialogName() {
        return dialogName;
    }

    @Override
    public ArrayList<User> getUsers() {
        return users;
    }

    @Override
    public Message getLastMessage() {
        return lastMessage;
    }

    @Override
    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }

    @Override
    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }


    public String getRecipient_id() {
        return recipient_id;
    }

    public void setRecipient_id(String recipient_id) {
        this.recipient_id = recipient_id;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDob() {
        return dob;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDistance() {
        return distance;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getBio() {
        return bio;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }

    public Integer getVerfication_status() {
        return verfication_status;
    }

    public void setVerfication_status(Integer verfication_status) {
        this.verfication_status = verfication_status;
    }

    public Integer getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(Integer likes_count) {
        this.likes_count = likes_count;
    }

    public void setHas_insta_photo(boolean has_insta_photo) {
        this.has_insta_photo = has_insta_photo;
    }

    public boolean isHas_insta_photo() {
        return has_insta_photo;
    }
}
