

package ke.co.tunda.ChatKit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GetUserMatchesResponse {



    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private ChatHistory[] history = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ChatHistory[] getHistory() {
        return history;
    }

    public void setHistory(ChatHistory[] history) {
        this.history = history;
    }
}
