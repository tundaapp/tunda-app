

package ke.co.tunda.ChatKit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ChatHistory {
    @SerializedName("chat_id")
    @Expose
    private Integer chatId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("recipient_id")
    @Expose
    private Integer recipientId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("date_created")
    @Expose
    private Date dateCreated;

    public Integer getUserId() {
        return userId;
    }

    @SerializedName("unique_id")
    @Expose
    String uniqueId;

    @SerializedName("status")
    @Expose
    String mStatus;


    public Integer getChatId() {
        return chatId;
    }

    public void setChatId(Integer chatId) {
        this.chatId = chatId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(Integer recipientId) {
        this.recipientId = recipientId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }


    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmStatus() {
        return mStatus;
    }
}


