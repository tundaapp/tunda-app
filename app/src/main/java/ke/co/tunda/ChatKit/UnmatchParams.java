/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 4/12/19 11:08 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ChatKit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnmatchParams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("match_id")
    @Expose
    private String matchId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }
}
