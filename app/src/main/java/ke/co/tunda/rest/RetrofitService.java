package ke.co.tunda.rest;


import ke.co.tunda.POJO.AccessTokenResponse;
import ke.co.tunda.POJO.InstagramResponse;
import ke.co.tunda.POJO.MediaRepsonse;
import ke.co.tunda.POJO.PhotoResponse;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by torzsacristian on 26/02/2017.
 */

public interface RetrofitService {

    @GET("v1/tags/{tag_name}/media/recent")
    Call<InstagramResponse> getTagPhotos(@Path("tag_name") String tag_name,
                                         @Query("access_token") String access_token);

    @FormUrlEncoded
    @POST("/oauth/access_token")
    Call<AccessTokenResponse> getAccessToken(
            @Field(("app_id")) String app_id,
            @Field("app_secret") String app_secret,
            @Field("grant_type") String grant_type,
            @Field("redirect_uri") String redirect_uri,
            @Field("code") String code);

    @Multipart
    @POST("/oauth/access_token")
    Call<AccessTokenResponse> getAccessToken(
            @Part(("app_id")) RequestBody app_id,
            @Part("app_secret") RequestBody app_secret,
            @Part("grant_type") RequestBody grant_type,
            @Part("redirect_uri") RequestBody redirect_uri,
            @Part("code") RequestBody code);


    @GET("/me/media")
    Call<MediaRepsonse> getUserMedia(
            @Query(value = "fields", encoded = true) String fields,
            @Query(("access_token")) String access_token);

    @GET
    Call<PhotoResponse> getMediaInfo(@Url String url,
                                     @Query(value = "fields", encoded = true) String fields,
                                     @Query("access_token") String access_token);

}

