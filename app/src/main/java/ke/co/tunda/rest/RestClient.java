package ke.co.tunda.rest;


import java.util.concurrent.TimeUnit;

import ke.co.tunda.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by torzsacristian on 29/06/2017.
 */

public class RestClient {

    private static Retrofit retrofit = null;
    private static Retrofit retrofit_graph = null;

    

    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .callTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL)
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClientGraph() {
        if (retrofit_graph == null) {
            retrofit_graph = new Retrofit.Builder()
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.BASE_URL_GRAPH)
                    .build();
        }
        return retrofit_graph;
    }

}