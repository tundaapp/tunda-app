

package ke.co.tunda.Constants;

public class Extras {
    public static final String PRIMARY_URL="https://api.tunda.mobi";
    public static final String PRIMARY_URL_1="http://207.180.212.198:3335/";
    public static final String PRIMARY_URL_IMAGE="https://api.tunda.mobi/";
    public static final String facebook_app_id="388303251964809";
    public static final String fb_login_protocol_scheme="fb388303251964809";
    public static final String PHOTOS_URL="207.180.212.198:1986/var/www/html/tunda-portal/web/user-photos/";
    public static final String GOOGLE_CLIENT_ID="175838244144-vf6sor4j6ng62dg18l5va8c2618dbhk6.apps.googleusercontent.com";


    public static final String TOP_PICKS = "top_picks";
    public static final  String USER_ID="user_id";
    public static final String NAME = "name";
    public static final String ABOUT_ME = "about_me";
    public static final String SHOW_AGE = "show_age";
    public static final String SHOW_LOCATION = "show_location";
    public static final String AGE = "my_age";
    public static final String DIST_KM = "dist_km";
    public static final String PHOTO_PATH = "photo_path";
    public static final String PHOTO_COUNT = "photo_count";
    public static final String JOB_TITLE = "job_title";
    public static final String SCHOOL = "school";
    public static final String INTEREST = "interest";
    public static final String HRS_LEFT = "hours_left";
    public static final String BOOST_STATUS_REG_TYPE = "QUERY_USER_BOOST";
    public static final String RECIPIENT_ID = "recipient_id";
    public static final String MESSAGE = "message";
    public static final String DATE_CREATED = "date_created";
    public static final String FULL_NAME = "full_name";
    public static final int ORIGIN_TOP_PICKS = 5;
    public static final int ORIGIN_LIKES = 4;
    public static final int ORIGIN_USERLIKES = 7;
    public static final String TIME_POSTED = "time_posted";
    public static final String MOOD_ID = "mood_id";
    public static final String MOOD_TYPE = "mood_type";
    public static final String MOOD_CAPTION = "mood_caption";
    public static final String MOOD_TEXT ="mood_text" ;
    public static final String MOOD_STATUS = "mood_status";
    public static final String VIEW_COUNT = "view_count";
    public static final String FONT_COLOR = "font_color";
    public static final String BACKGROUND_COLOR = "background_color";
    public static final String VIEW_STATUS = "view_status";
    public static final String MOOD_COUNT = "mood_count";
    public static final String VIEWED_MOODS = "viewed_moods";
    public static final String USER_PHOTO = "user_photo";
    public static final String USER_MOODS = "user_moods";
    public static final String STATUS = "status";
    public static final String UNIQUE_ID = "unique_id";
    public static final String MATCH_ID = "match_id";
    public static final String CHAT_ID = "chat_id";
    public static final int ORIGIN_MAIN = 1;
    public static final int ORIGIN_MESSAGES = 3;
    public static final String KEY_MATCH_LAST_SEEN_DATE = "last_seen_date";
    public static final String KEY_LAST_MESSAGE = "last_message";
    public static final String KEY_MATCH_LAST_CHAT_DATE = "last_chat_date";
    public static final String KEY_MATCH_UNREAD_MESSAGES = "unread_messages";
    public static final String KEY_INTEREST_DIALOGS = "interest";
    public static final String KEY_MATCH_VERIFIED = "verified";
    public static final String UNREAD_MESGS_COUNT = "unread_messages";

    public static final String DOB ="dob" ;
    public static final String VERIFICATION_STATUS = "verification_status";
    public static final String LIKES_COUNT = "likes";
    public static final String HAS_INSTAGRAM ="has_insta_photo" ;
}
