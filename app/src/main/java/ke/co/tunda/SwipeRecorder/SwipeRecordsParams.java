

package ke.co.tunda.SwipeRecorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SwipeRecordsParams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("swipe_data")
    @Expose
    private ArrayList<Swipe> arrayList;
    @SerializedName("user_id")
    @Expose
    private int userId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public ArrayList<Swipe> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Swipe> arrayList) {
        this.arrayList = arrayList;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
