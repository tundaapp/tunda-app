

package ke.co.tunda.SwipeRecorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Swipe {
    @SerializedName("recipient_id")
    @Expose
    private int recipient_id;
    @SerializedName("swipe_action")
    @Expose
    private String swipe_action;

    public Swipe(int recipient_id,String swipe_action){
        this.recipient_id=recipient_id;
        this.swipe_action=swipe_action;
    }

    public Swipe(){
    }


    public int getRecipient_id() {
        return recipient_id;
    }

    public void setRecipient_id(int recipient_id) {
        this.recipient_id = recipient_id;
    }


    public String getSwipe_action() {
        return swipe_action;
    }


    public void setSwipe_action(String swipe_action) {
        this.swipe_action = swipe_action;
    }


}
