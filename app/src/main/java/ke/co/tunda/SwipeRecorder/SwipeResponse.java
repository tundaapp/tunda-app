

package ke.co.tunda.SwipeRecorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ke.co.tunda.ApiConnector.Models.Body;

public class SwipeResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private Matches[] matches;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Matches[] getBody() {
        return matches;
    }

    public void setMatches(Body[] body) {
        this.matches = matches;
    }
}
