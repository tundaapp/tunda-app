

package ke.co.tunda.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import ke.co.tunda.Activities.WelcomeActivity;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.Response;
import ke.co.tunda.Models.FcmPayload;
import ke.co.tunda.Models.SendFcmToken;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;

public class FireBaseMessagingService extends FirebaseMessagingService {
    private static final String CHANNEL_ID = "254";
    private static String TAG = "FCM";
    private int user_id;
    private NotificationChannel channel;
    Intent router;

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        user_id = sp.getInt("user_id", 0);


        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w("FCM", "getInstanceId failed", task.getException());
                    return;
                }

                // Get new Instance ID token
                String token = task.getResult().getToken();
                sendRegistrationTokenToServer(token);

                // Log and toast


            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w("FCM", "onFailure: ");
            }
        });


    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        sendRegistrationTokenToServer(s);
    }

    private void sendRegistrationTokenToServer(String token) {

        if (user_id > 0) {
            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
            SendFcmToken sendFcmToken = new SendFcmToken();
            sendFcmToken.setRegistrationToken(token);
            sendFcmToken.setRequestType("SEND_REGISTRATION_TOKEN");
            sendFcmToken.setUserId(user_id);

            Call<Response> call = apiCaller.sendToken(sendFcmToken);
            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    if (response.isSuccessful()) {
                        Response resp = response.body();
                        if (resp.getBody().equals("SUCCESS")) {
                            Log.d(TAG, "onResponse: Success");
                        } else {
                            Log.d(TAG, resp.getBody());
                        }

                    }
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    Log.d(TAG, "Fcm Send failed   " + t.getMessage());

                }
            });

        }


    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "onMessageReceived: " + remoteMessage.toString());
//        Toasty.success(getApplicationContext(),remoteMessage.toString()).show();
        String description = "";
        String title = "";
        String action_s = "";
        int action = 10;
        JSONObject body = new JSONObject();

        Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);

        Log.e("FCM_OBJECT", object.toString());
        try {
            description = object.getString("description");
            title = object.getString("title");
            action_s = object.getString("action");
            if (action_s != null && !action_s.isEmpty()) {
                action = Integer.parseInt(action_s);
            }

            Log.e("FCM_OBJECT", description);
            Log.e("FCM_OBJECT", title);
            Log.e("FCM_OBJECT", "" + action);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String json_string = new Gson().toJson(object);
        FcmPayload payload = new Gson().fromJson(json_string, FcmPayload.class);

        if (description != null && title != null && action > 0) {
            Log.d(TAG, "onMessageReceived: ");

            switch (action) {
                case 10:
                    Log.d(TAG, "null: ");
                    router = new Intent(this, WelcomeActivity.class);
                    router.putExtra("position", 1);
                    notifyFcm(router,title,description);
                    break;

                case 1:
                    Log.d(TAG, "user_match: ");
                    router = new Intent(this, WelcomeActivity.class);
                    router.putExtra("position", 2);
                    notifyFcm(router,title,description);
                    break;
                case 2:
                    Log.d(TAG, "pamsa: ");
                    router = new Intent(this, WelcomeActivity.class);
                    router.putExtra("position", 0);
                    notifyFcm(router,title,description);
                    break;
                case 3:
                    Log.d(TAG, "lstmsg: ");
                    router = new Intent(this, WelcomeActivity.class);
                    router.putExtra("position", 2);
                    notifyFcm(router,title,description);
                    break;
                default:
                    router = new Intent(this, WelcomeActivity.class);
                    router.putExtra("position", 1);
                    notifyFcm(router,title,description);
                    break;


            }




        }


    }

    private void notifyFcm(Intent router,String title,String description) {
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, router, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat
                .Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(description)
                .setTimeoutAfter(3600000)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Random rand = new Random();

        int n = rand.nextInt(50000);


        n += 1;

        notificationManager.notify(n + 1, notificationBuilder.build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


}
