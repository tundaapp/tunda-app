package ke.co.tunda.utils;

/**
 * Created by torzsacristian on 29/06/2017.
 */

public final class Constants {
    public static final String BASE_URL = "https://api.instagram.com";
    public static final String BASE_URL_GRAPH = "https://graph.instagram.com";
    public static final String CLIENT_ID = "994092430939834";
    public static final String REDIRECT_URI = "https://tunda.mobi/";
    public static final String APP_SECRET = "a4de1c0a45a324c51c87b438e9ae0bda";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String FIELDS_ID = "id";
    public static final String FIELDS_CAPTION = "caption";
    public static final String FIELDS_PHOTOS = "id,media_type,media_url,username,timestamp";
    public static final String URL = "image_url";
}
