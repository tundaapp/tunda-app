package ke.co.tunda.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MediaRepsonse {

    @SerializedName("data")
    @Expose
    private Datum[] data;
    @SerializedName("paging")
    @Expose
    private Paging paging;

    public Datum[] getData() {
        return data;
    }

    public void setData(Datum[] data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
