package ke.co.tunda.POJO;


import androidx.annotation.NonNull;

public class FieldGen {
    private String id;
    private String caption;

    public FieldGen(String id, String caption) {
        this.id = id;
        this.caption = caption;
    }

    @NonNull
    public String toString() {
        return String.format("%.1f,%.1f", id, caption);
    }
}
