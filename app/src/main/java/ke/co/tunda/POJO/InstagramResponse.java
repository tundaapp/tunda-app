package ke.co.tunda.POJO;

/**
 * Created by torzsacristian on 29/06/2017.
 */

public class InstagramResponse {

    private Datum[] data;

    public Datum[] getData() {
        return data;
    }

    public void setData(Datum[] data) {
        this.data = data;
    }

}
