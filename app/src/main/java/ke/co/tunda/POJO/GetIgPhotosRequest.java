package ke.co.tunda.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetIgPhotosRequest {
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("request_type")
    @Expose
    private String requestType;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
