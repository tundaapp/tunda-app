/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/9/19 4:28 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import ke.co.tunda.Constants.Extras;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Builder {
    private static Retrofit retrofit = null;


//    static ConnectionSpec spec = new
//            ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//            .tlsVersions(TlsVersion.TLS_1_2)
//            .cipherSuites(
//                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
//                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
//                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
//            .build();

    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .callTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Extras.PRIMARY_URL)
                    .build();
        }
        return retrofit;
    }
}
