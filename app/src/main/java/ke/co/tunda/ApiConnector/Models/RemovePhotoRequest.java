/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 4/25/19 12:17 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemovePhotoRequest {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("photo_path")
    @Expose
    private String photoPath;
    @SerializedName("user_id")
    @Expose
    private String user_id;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getPhotoPath() {
        return photoPath;
    }
}
