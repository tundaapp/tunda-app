

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserProfileRequest {

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("gender_of_interest")
    @Expose
    private String genderOfInterest;


    @SerializedName("interest")
    @Expose
    private String jsonInterest;


    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("user_id")
    @Expose
    private String userId;



    @SerializedName("age_from")
    @Expose
    private String minAge;
    @SerializedName("age_to")
    @Expose
    private String maxAge;
    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("user_status")
    @Expose
    private String userStatus;


    @SerializedName("show_age")
    @Expose
    private String show_age;

    @SerializedName("show_location")
    @Expose
    private String show_location;

    @SerializedName("phone_number")
    @Expose
    private String phone_number;

    @SerializedName("show_my_likes")
    @Expose
    private Integer show_my_likes;


    public void setJsonInterest(String jsonInterest) {
        this.jsonInterest = jsonInterest;
    }

    public String getJsonInterest() {
        return jsonInterest;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getGenderOfInterest() {
        return genderOfInterest;
    }

    public void setGenderOfInterest(String genderOfInterest) {
        this.genderOfInterest = genderOfInterest;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getMinAge() {
        return minAge;
    }

    public void setMinAge(String minAge) {
        this.minAge = minAge;
    }

    public String getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(String maxAge) {
        this.maxAge = maxAge;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getShow_age() {
        return show_age;
    }

    public void setShow_age(String show_age) {
        this.show_age = show_age;
    }

    public void setShow_location(String show_location) {
        this.show_location = show_location;
    }

    public String getShow_location() {
        return show_location;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setShow_my_likes(Integer show_my_likes) {
        this.show_my_likes = show_my_likes;
    }

    public Integer getShow_my_likes() {
        return show_my_likes;
    }
}
