

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Progress {

    @SerializedName("size")
    @Expose
    private Integer progress_size;

    public Progress(Integer progress_size){
        this.progress_size = progress_size;
    }

    public Integer getProgress_size() {
        return progress_size;
    }

    public void setProgress_size(Integer progress_size) {
        this.progress_size = progress_size;
    }
}
