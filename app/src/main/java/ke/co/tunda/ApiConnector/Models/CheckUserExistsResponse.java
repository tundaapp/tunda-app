

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckUserExistsResponse {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("body")
    @Expose
    private Body body;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }


    public class Body {

        @SerializedName("user_exist")
        @Expose
        private Boolean userExist;
        @SerializedName("user_data")
        @Expose
        private UserData userData;

        public Boolean getUserExist() {
            return userExist;
        }

        public void setUserExist(Boolean userExist) {
            this.userExist = userExist;
        }

        public UserData getUserData() {
            return userData;
        }

        public void setUserData(UserData userData) {
            this.userData = userData;
        }
    }

    public class UserData {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("photo_path")
        @Expose
        private String photoPath;

        @SerializedName("photo_count")
        @Expose
        private int photo_count;

        @SerializedName("package_id")
        @Expose
        private Integer packageId;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("interest")
        @Expose
        private String interest;
        @SerializedName("source")
        @Expose
        private String source;
        @SerializedName("about_me")
        @Expose
        private String aboutMe;
        @SerializedName("job_title")
        @Expose
        private String jobTitle;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("school")
        @Expose
        private String school;
        @SerializedName("show_age")
        @Expose
        private String showAge;
        @SerializedName("show_location")
        @Expose
        private String showLocation;
        @SerializedName("last_location")
        @Expose
        private String lastLocation;
        @SerializedName("user_status")
        @Expose
        private String userStatus;
        @SerializedName("gender_of_interest")
        @Expose
        private String genderOfInterest;
        @SerializedName("age_from")
        @Expose
        private Integer ageFrom;
        @SerializedName("age_to")
        @Expose
        private Integer ageTo;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("package_expiry_date")
        @Expose
        private String packageExpiryDate;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getPhotoPath() {
            return photoPath;
        }

        public void setPhotoPath(String photoPath) {
            this.photoPath = photoPath;
        }

        public Integer getPackageId() {
            return packageId;
        }

        public void setPackageId(Integer packageId) {
            this.packageId = packageId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getInterest() {
            return interest;
        }

        public void setInterest(String interest) {
            this.interest = interest;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getAboutMe() {
            return aboutMe;
        }

        public void setAboutMe(String aboutMe) {
            this.aboutMe = aboutMe;
        }

        public String getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(String jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getSchool() {
            return school;
        }

        public void setSchool(String school) {
            this.school = school;
        }

        public String getShowAge() {
            return showAge;
        }

        public void setShowAge(String showAge) {
            this.showAge = showAge;
        }

        public String getShowLocation() {
            return showLocation;
        }

        public void setShowLocation(String showLocation) {
            this.showLocation = showLocation;
        }

        public String getLastLocation() {
            return lastLocation;
        }

        public void setLastLocation(String lastLocation) {
            this.lastLocation = lastLocation;
        }

        public String getUserStatus() {
            return userStatus;
        }

        public void setUserStatus(String userStatus) {
            this.userStatus = userStatus;
        }

        public String getGenderOfInterest() {
            return genderOfInterest;
        }

        public void setGenderOfInterest(String genderOfInterest) {
            this.genderOfInterest = genderOfInterest;
        }

        public Integer getAgeFrom() {
            return ageFrom;
        }

        public void setAgeFrom(Integer ageFrom) {
            this.ageFrom = ageFrom;
        }

        public Integer getAgeTo() {
            return ageTo;
        }

        public void setAgeTo(Integer ageTo) {
            this.ageTo = ageTo;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public String getPackageExpiryDate() {
            return packageExpiryDate;
        }

        public void setPackageExpiryDate(String packageExpiryDate) {
            this.packageExpiryDate = packageExpiryDate;
        }

        public int getPhoto_count() {
            return photo_count;
        }

        public void setPhoto_count(int photo_count) {
            this.photo_count = photo_count;
        }
    }



}
