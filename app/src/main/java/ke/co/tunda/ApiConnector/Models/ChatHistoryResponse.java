

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ke.co.tunda.ChatKit.ChatHistory;

public class ChatHistoryResponse {

    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private ChatHistory[] body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ChatHistory[] getBody() {
        return body;
    }

    public void setBody(ChatHistory[] body) {
        this.body = body;
    }

}
