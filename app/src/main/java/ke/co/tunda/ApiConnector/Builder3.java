/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 6/24/19 10:40 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector;

import java.util.concurrent.TimeUnit;

import ke.co.tunda.Constants.Extras;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Builder3 {
    private static Retrofit retrofit = null;


//    static ConnectionSpec spec = new
//            ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//            .tlsVersions(TlsVersion.TLS_1_2)
//            .cipherSuites(
//                    CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
//                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
//                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
//            .build();

    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()

                .callTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://ip-api.com/")
                    .build();
        }
        return retrofit;
    }
}
