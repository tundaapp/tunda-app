/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 6/26/19 5:56 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RegisterUserParams {

    @SerializedName("last_location")
    @Expose
    private String last_location;

    @SerializedName("user_id")
    @Expose
    private Integer user_id;

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("send_alerts")
    @Expose
    private String sendAlerts;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("gender_of_interest")
    @Expose
    private String genderOfInterest;
    @SerializedName("origin")
    private String origin;

    @SerializedName("latitude")
    @Expose
    private Double latitude;

    @SerializedName("longitude")
    @Expose
    private Double longitude;

    @SerializedName("interest")
    @Expose
    private String jsonInterest;


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSendAlerts() {
        return sendAlerts;
    }

    public void setSendAlerts(String sendAlerts) {
        this.sendAlerts = sendAlerts;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getGenderOfInterest() {
        return genderOfInterest;
    }

    public void setGenderOfInterest(String genderOfInterest) {
        this.genderOfInterest = genderOfInterest;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;

    }

    public Double getLatitude(){return latitude;}

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


    public Double getLongitude(){return longitude;}

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setLast_location(String last_location) {
        this.last_location = last_location;
    }

    public String getLast_location() {
        return last_location;
    }

    public String getJsonInterest() {
        return jsonInterest;
    }

    public void setJsonInterest(String jsonInterest) {
        this.jsonInterest = jsonInterest;
    }
}
