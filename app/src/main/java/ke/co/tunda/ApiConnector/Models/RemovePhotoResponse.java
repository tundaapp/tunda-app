/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 4/25/19 12:17 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemovePhotoResponse {
    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("status_code")
    @Expose
    private String statusCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }


    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public class Body {

        @SerializedName("status_code")
        @Expose
        private String statusCode;
        @SerializedName("status_message")
        @Expose
        private String statusMessage;


        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        public void setStatusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
        }



    }
}
