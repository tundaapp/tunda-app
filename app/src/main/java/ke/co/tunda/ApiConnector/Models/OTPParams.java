/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 3/20/19 2:11 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPParams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
