/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 3/20/19 12:04 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTP {
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("body")
    @Expose
    private Integer body;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getBody() {
        return body;
    }

    public void setBody(Integer body) {
        this.body = body;
    }
}
