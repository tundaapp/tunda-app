/*
 * Creator: Donbosco Muthiani on 9/25/19 11:17 AM Last modified: 9/25/19 9:55 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import java.util.List;

public class UserStatus {
    private String userName;
    private String imageUrl;
    private boolean allSeen;
    private String time;
    private List<Status> statusList;

    public UserStatus(String userName,String imageUrl, boolean areAllSeen, List<Status> statusList,String time) {
        this.userName = userName;
        this.allSeen = areAllSeen;
        this.statusList = statusList;
        this.imageUrl = imageUrl;
        this.time = time;

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean areAllSeen() {
        return allSeen;
    }

    public void setAllSeen(boolean allSeen) {
        this.allSeen = allSeen;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
