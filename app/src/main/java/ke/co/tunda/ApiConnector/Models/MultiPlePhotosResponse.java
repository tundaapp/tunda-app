/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/7/19 4:56 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MultiPlePhotosResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private List<Photos> body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<Photos> getBody() {
        return body;
    }

    public void setBody(List<Photos> body) {
        this.body = body;
    }
}
