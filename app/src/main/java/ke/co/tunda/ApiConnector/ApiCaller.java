

package ke.co.tunda.ApiConnector;


import ke.co.tunda.ApiConnector.Models.ChatHistoryParams;
import ke.co.tunda.ApiConnector.Models.ChatHistoryResponse;
import ke.co.tunda.ApiConnector.Models.CheckUserExistsParams;
import ke.co.tunda.ApiConnector.Models.CheckUserExistsResponse;
import ke.co.tunda.ApiConnector.Models.GetLikesparams;
import ke.co.tunda.ApiConnector.Models.GetMyMoodRequest;
import ke.co.tunda.ApiConnector.Models.GetMyMoodResponse;
import ke.co.tunda.ApiConnector.Models.GetNearbyUsersParams;
import ke.co.tunda.ApiConnector.Models.GetUserChatGroupParams;
import ke.co.tunda.ApiConnector.Models.GetUserChatGroupsResponse;
import ke.co.tunda.ApiConnector.Models.GetUsersNearbyResponse;
import ke.co.tunda.ApiConnector.Models.ImageUploadResponse;
import ke.co.tunda.ApiConnector.Models.LogOutUserParams;
import ke.co.tunda.ApiConnector.Models.MultiPlePhotosResponse;
import ke.co.tunda.ApiConnector.Models.MultiplePhotoRequest;
import ke.co.tunda.ApiConnector.Models.OTP;
import ke.co.tunda.ApiConnector.Models.OTPParams;
import ke.co.tunda.ApiConnector.Models.PackageIdParams;
import ke.co.tunda.ApiConnector.Models.PackageResponse;
import ke.co.tunda.ApiConnector.Models.RegisterUserParams;
import ke.co.tunda.ApiConnector.Models.RegisterUserResponse;
import ke.co.tunda.ApiConnector.Models.RemovePhotoRequest;
import ke.co.tunda.ApiConnector.Models.RemovePhotoResponse;
import ke.co.tunda.ApiConnector.Models.ReportUserParams;
import ke.co.tunda.ApiConnector.Models.RequestCallParams;
import ke.co.tunda.ApiConnector.Models.Response;
import ke.co.tunda.ApiConnector.Models.SendPurchaseTokenParams;
import ke.co.tunda.ApiConnector.Models.UpdateLocationResponse;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileRequest;
import ke.co.tunda.ApiConnector.Models.UpdateUserProfileResponse;
import ke.co.tunda.ChatKit.ChatHistory;
import ke.co.tunda.ChatKit.UnmatchParams;
import ke.co.tunda.ChatKit.UnmatchResponse;
import ke.co.tunda.Models.ActivateBoostParams;
import ke.co.tunda.Models.BaseResponse;
import ke.co.tunda.Models.BoostStatusParams;
import ke.co.tunda.Models.BoostStatusResp;
import ke.co.tunda.Models.CountryModel;
import ke.co.tunda.Models.DefaultRequest;
import ke.co.tunda.Models.DefaultResp;
import ke.co.tunda.Models.DefaultResponse;
import ke.co.tunda.Models.DeleteAccountParams;
import ke.co.tunda.Models.DeleteAccountResponse;
import ke.co.tunda.Models.DeleteBulkParams;
import ke.co.tunda.Models.DeleteMessageParams;
import ke.co.tunda.Models.GetBoostedparams;
import ke.co.tunda.Models.GetBoostsParams;
import ke.co.tunda.Models.GetBoostsResp;
import ke.co.tunda.Models.GetPackagesResp;
import ke.co.tunda.Models.GetpackageParams;
import ke.co.tunda.Models.GoogleSignParams;
import ke.co.tunda.Models.LikesResponse;
import ke.co.tunda.Models.MoodsRequest;
import ke.co.tunda.Models.MoodsResponse;
import ke.co.tunda.Models.MyMatchesParams;
import ke.co.tunda.Models.MyMatchesResponse;
import ke.co.tunda.Models.PayMpesaParams;
import ke.co.tunda.Models.PurchasePackageParams;
import ke.co.tunda.Models.PurchasePackageResp;
import ke.co.tunda.Models.PurchaseSpotParams;
import ke.co.tunda.Models.RequestViewStatus;
import ke.co.tunda.Models.SendFcmToken;
import ke.co.tunda.Models.SendViewParams;
import ke.co.tunda.Models.UserLikesModel;
import ke.co.tunda.Models.ViewMoodRequest;
import ke.co.tunda.Models.ViewMoodResponse;
import ke.co.tunda.Models.WalletBalReq;
import ke.co.tunda.Models.WalletBalResp;
import ke.co.tunda.POJO.GetIgPhotoResponse;
import ke.co.tunda.POJO.GetIgPhotosRequest;
import ke.co.tunda.POJO.SendIgPhotosRequest;
import ke.co.tunda.SwipeRecorder.SwipeRecordsParams;
import ke.co.tunda.SwipeRecorder.SwipeResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by Don on 03/20/19.
 */

public interface ApiCaller {

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<OTP> sendOtp(@Body OTPParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<RegisterUserResponse> registerUser(@Body RegisterUserParams params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<CheckUserExistsResponse> checkIfUserExists(@Body CheckUserExistsParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetUsersNearbyResponse> getNearbyUsers(@Body GetNearbyUsersParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<UpdateLocationResponse> updateLocation(@Body RegisterUserParams params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<SwipeResponse> sendSwipeRecords(@Body SwipeRecordsParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<MyMatchesResponse> getMatches(@Body MyMatchesParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetUserChatGroupsResponse> getChatDialogs(@Body GetUserChatGroupParams params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<UnmatchResponse> unmatchUser(@Body UnmatchParams params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<UpdateUserProfileResponse> updateUser(@Body UpdateUserProfileRequest params);


    @Multipart
    @POST("tunda-uploads-api")
    Call<ImageUploadResponse> uploadImage(@Part MultipartBody.Part file, @Part("profile_picture")
            RequestBody requestBody, @Part("user_id") RequestBody userId,
                                          @Part("is_profile") RequestBody is_profile);

    @Multipart
    @POST("/tunda-uploads-api")
    Call<RegisterUserResponse> uploadImage1(@Part MultipartBody.Part file, @Part("profile_picture")
            RequestBody requestBody, @Part("user_id") RequestBody userId, @Part("is_profile") RequestBody is_profile);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<RemovePhotoResponse> removePhoto(@Body RemovePhotoRequest params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<DeleteAccountResponse> deleteAccount(@Body DeleteAccountParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<MultiPlePhotosResponse> getUserPhotos(@Body MultiplePhotoRequest params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<Response> reportUser(@Body ReportUserParams params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetPackagesResp> getPackages(@Body GetpackageParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetBoostsResp> getBoosts(@Body GetBoostsParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<PurchasePackageResp> loadWallet(@Body PayMpesaParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<PurchasePackageResp> buyPackage(@Body PurchasePackageParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<PurchasePackageResp> buyBoost(@Body PurchaseSpotParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<Response> sendToken(@Body SendFcmToken params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<WalletBalResp> checkBalance(@Body WalletBalReq params);

    @GET("/json")
    Call<CountryModel> checkCountry();

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<RegisterUserResponse> googleSignIn(@Body GoogleSignParams parama);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<PackageResponse> getPackageId(@Body PackageIdParams parama);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetUsersNearbyResponse> getBoosted(@Body GetBoostedparams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<BoostStatusResp> getBoostStatus(@Body BoostStatusParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<DefaultResp> activateBoost(@Body ActivateBoostParams params);


    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<ChatHistoryResponse> getChatHistory(@Body ChatHistoryParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<Response> logOut(@Body LogOutUserParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<Response> sendPurchaseToken(@Body SendPurchaseTokenParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<Response> sendViewStatus(@Body RequestViewStatus params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<LikesResponse> getLikes(@Body DefaultRequest params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<BaseResponse> sendViews(@Body SendViewParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<UserLikesModel> getUserLikes(@Body GetLikesparams params);

    @Headers("Content-Type: application/json")
    @POST("appcall")
    Call<Response> sendCall(@Body RequestCallParams params);

    @Multipart
    @POST("/tunda-uploads-api/tunda-mood")
    Call<Response> uploadMoodImage(@Part MultipartBody.Part file, @Part("mood_media")
            RequestBody requestBody, @Part("user_id") RequestBody userId,
                                          @Part("mood_type") RequestBody mood_type,
                                              @Part("mood_caption") RequestBody mood_caption,
                                              @Part("mood_text") RequestBody mood_text);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetMyMoodResponse> getMyMood(@Body GetMyMoodRequest params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<MoodsResponse> getMoods(@Body MoodsRequest params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<ViewMoodResponse> updateMoodView(@Body ViewMoodRequest params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<DefaultResponse> deleteMessages(@Body DeleteMessageParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<DefaultResponse> deleteBulk(@Body DeleteBulkParams params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<GetIgPhotoResponse> getIgPhotos(@Body GetIgPhotosRequest params);

    @Headers("Content-Type: application/json")
    @POST("tunda-app-api")
    Call<BaseResponse> sendIgPhotos(@Body SendIgPhotosRequest params);


}



