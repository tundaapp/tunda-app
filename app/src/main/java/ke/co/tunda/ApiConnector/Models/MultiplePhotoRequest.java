/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/7/19 4:57 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MultiplePhotoRequest {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
