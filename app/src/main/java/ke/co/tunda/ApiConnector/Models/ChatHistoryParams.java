

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatHistoryParams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private int user_id;
    @SerializedName("match_id")
    @Expose
    private String match_id;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getMatch_id() {
        return match_id;
    }
}
