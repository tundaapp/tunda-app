

package ke.co.tunda.ApiConnector.Models;

import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Body {

    @SerializedName("v_last_page")
    @Expose
    private Integer vLastPage;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("show_age")
    @Expose
    private boolean showAge;
    @SerializedName("show_location")
    @Expose
    private boolean showLocation;
    @SerializedName("my_age")
    @Expose
    private Integer myAge;
    @SerializedName("dist_km")
    @Expose
    private Double distKm;
    @SerializedName("photo_path")
    @Expose
    private String photoPath;
    @SerializedName("photo_count")
    @Expose
    private Integer photoCount;

    @SerializedName("job_title")
    @Expose
    private String job_title;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("interest")
    @Expose
    private String interests;

    @SerializedName("hours_left")
    @Expose
    private Integer hrs_left;

    @SerializedName("verification_status")
    @Expose
    private Integer verification_status;

    @SerializedName("likes")
    @Expose
    private Integer likes_count;

    @SerializedName("views")
    @Expose
    private Integer views_count;

    @SerializedName("has_insta_photo")
    @Expose
    private Integer has_insta_photo;


    public Integer getHrs_left() {
        return hrs_left;
    }

    public void setHrs_left(int hrs_left) {
        this.hrs_left = hrs_left;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public Integer getVLastPage() {
        return vLastPage;
    }

    public void setVLastPage(Integer vLastPage) {
        this.vLastPage = vLastPage;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public boolean getShowAge() {
        return showAge;
    }

    public void setShowAge(boolean showAge) {
        this.showAge = showAge;
    }

    public boolean getShowLocation() {
        return showLocation;
    }

    public void setShowLocation(boolean showLocation) {
        this.showLocation = showLocation;
    }

    public Integer getMyAge() {
        return myAge;
    }

    public void setMyAge(Integer myAge) {
        this.myAge = myAge;
    }

    public Double getDistKm() {
        return distKm;
    }

    public void setDistKm(Double distKm) {
        this.distKm = distKm;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public Integer getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(Integer photoCount) {
        this.photoCount = photoCount;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSchool() {
        return school;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(Integer verification_status) {
        this.verification_status = verification_status;
    }

    public void setvLastPage(Integer vLastPage) {
        this.vLastPage = vLastPage;
    }

    public Integer getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(Integer likes_count) {
        this.likes_count = likes_count;
    }

    public Integer getvLastPage() {
        return vLastPage;
    }

    public void setViews_count(Integer views_count) {
        this.views_count = views_count;
    }

    public Integer getViews_count() {
        return views_count;
    }

    public void setHas_insta_photo(Integer has_insta_photo) {
        this.has_insta_photo = has_insta_photo;
    }

    public Integer getHas_insta_photo() {
        return has_insta_photo;
    }
}
