

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetUserChatGroupsResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private ChatInfo[] info = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ChatInfo[] getBody() {
        return info;
    }

    public void setBody(ChatInfo[] info) {
        this.info = info;
    }

}
