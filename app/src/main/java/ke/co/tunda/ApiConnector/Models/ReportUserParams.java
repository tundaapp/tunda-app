

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportUserParams {

        @SerializedName("request_type")
        @Expose
        private String requestType;
        @SerializedName("suspect_id")
        @Expose
        private String suspectId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("report_type")
        @Expose
        private String reportType;
        @SerializedName("report_details")
        @Expose
        private String reportDetails;

        public String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public String getSuspectId() {
            return suspectId;
        }

        public void setSuspectId(String suspectId) {
            this.suspectId = suspectId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getReportType() {
            return reportType;
        }

        public void setReportType(String reportType) {
            this.reportType = reportType;
        }

        public String getReportDetails() {
            return reportDetails;
        }

        public void setReportDetails(String reportDetails) {
            this.reportDetails = reportDetails;
        }
}
