/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/19/19 5:55 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PackageResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private List<PackageId> body = null;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PackageId> getBody() {
        return body;
    }

    public void setBody(List<PackageId> body) {
        this.body = body;
    }

    public class PackageId {

        @SerializedName("package_id")
        @Expose
        private Integer packageId;


        @SerializedName("share_url")
        @Expose
        private String share_url;

        @SerializedName("likes")
        @Expose
        private Integer likes_count;

        public Integer getPackageId() {
            return packageId;
        }

        public void setPackageId(Integer packageId) {
            this.packageId = packageId;
        }

        public void setShare_url(String share_url) {
            this.share_url = share_url;
        }

        public String getShare_url() {
            return share_url;
        }

        public Integer getLikes_count() {
            return likes_count;
        }

        public void setLikes_count(Integer likes_count) {
            this.likes_count = likes_count;
        }
    }


}
