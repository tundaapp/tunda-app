package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ke.co.tunda.Models.Mood;

public class GetMyMoodResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private ArrayList<Mood> body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ArrayList<Mood> getBody() {
        return body;
    }

    public void setBody(ArrayList<Mood> body) {
        this.body = body;
    }


}
