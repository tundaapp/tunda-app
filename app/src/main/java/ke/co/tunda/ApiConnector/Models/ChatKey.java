

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatKey {

    @SerializedName("chat_id")
    @Expose
    private Integer chat_id;

    public ChatKey(Integer chat_id){
        this.chat_id = chat_id;
    }


    public void setId(Integer chat_id) {
        this.chat_id = chat_id;
    }

    public int getChat_id() {
        return chat_id;
    }
}
