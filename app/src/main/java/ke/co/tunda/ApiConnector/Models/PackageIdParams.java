

package ke.co.tunda.ApiConnector.Models;

public class PackageIdParams {
    private int user_id;
    private String request_type;

    public String getRequest_type() {
        return request_type;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

}
