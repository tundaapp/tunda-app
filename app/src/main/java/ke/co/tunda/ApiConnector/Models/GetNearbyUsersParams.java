

package ke.co.tunda.ApiConnector.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetNearbyUsersParams {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("user_id")
    @Expose
    private int userId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
