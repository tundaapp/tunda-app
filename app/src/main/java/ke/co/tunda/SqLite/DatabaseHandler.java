

package ke.co.tunda.SqLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.ApiConnector.Models.ChatInfo;
import ke.co.tunda.ApiConnector.Models.GetUsersNearbyResponse;
import ke.co.tunda.Models.MyMatches;
import ke.co.tunda.SwipeRecorder.Swipe;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 13;

    // Database Name
    private static final String DATABASE_NAME = "tunda_database";

    // Tunda table names
    public static final String TABLE_MATCHES = "matches";
    public static final String TABLE_DIALOGS = "dialogs";
    public static final String TABLE_SWIPES = "swipes";
    public static final String TABLE_TOP_PICKS = "top_picks";

    // TABLE_MATCHES Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_FULL_NAME = "full_name";
    public static final String KEY_PHOTO_PATH = "photo_path";
    public static final String KEY_VIEW_STATUS = "view_status";
    public static final String KEY_LAST_SEEN_DATE = "last_seen_date";
    public static final String KEY_BIO = "bio";
    public static final String KEY_DISTANCE = "distance";
    public static final String KEY_JOB = "job";
    public static final String KEY_SCHOOL = "school";
    public static final String KEY_DOB = "dob";
    public static final String KEY_INTEREST_MATCHES = "interest";


    // TABLE_DIALOGS Table Columns names
    public static final String KEY_MATCH_ID = "match_id";
    public static final String KEY_ID_DIALOGS = "id";
    public static final String KEY_RECIPIENT_ID = "user_id";
    public static final String KEY_MATCH_FULL_NAME = "full_name";
    public static final String KEY_MATCH_PHOTO_PATH = "photo_path";
    public static final String KEY_MATCH_VIEW_STATUS = "view_status";
    public static final String KEY_MATCH_LAST_SEEN_DATE = "last_seen_date";
    public static final String KEY_LAST_MESSAGE = "last_message";
    public static final String KEY_MATCH_LAST_CHAT_DATE = "last_chat_date";
    public static final String KEY_MATCH_UNREAD_MESSAGES = "unread_messages";
    public static final String KEY_INTEREST_DIALOGS = "interest";
    public static final String KEY_MATCH_VERIFIED = "verified";


    //SWIPES COLUMN
    public static final String KEY_REC_ID = "recipient_id";
    public static final String KEY_SWIPE_ACTION = "swipe_action";


    //TOP_PICKS COLUMNS
    public static final String KEY_TOP_PICKS = "top_picks_list";


    public static final String TAG = "db_sq";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MATCH_TABLE = "CREATE TABLE " + TABLE_MATCHES + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_MATCH_ID + " INTEGER," +
                KEY_USER_ID + " INTEGER," +
                KEY_FULL_NAME + " TEXT," +
                KEY_PHOTO_PATH + " TEXT," +
                KEY_VIEW_STATUS + " INTEGER," +
                KEY_BIO + " TEXT," +
                KEY_DISTANCE + " TEXT," +
                KEY_JOB + " TEXT," +
                KEY_SCHOOL + " TEXT," +
                KEY_DOB + " TEXT," +
                KEY_LAST_SEEN_DATE + " TEXT," +
                KEY_MATCH_VERIFIED + " INTEGER," +
                KEY_INTEREST_MATCHES + " TEXT" + ")";

        String CREATE_DIALOGS_TABLE = "CREATE TABLE " + TABLE_DIALOGS + "(" +
                KEY_ID_DIALOGS + " INTEGER PRIMARY KEY," +
                KEY_MATCH_ID + " INTEGER," +
                KEY_MATCH_FULL_NAME + " TEXT," +
                KEY_MATCH_PHOTO_PATH + " TEXT," +
                KEY_MATCH_VIEW_STATUS + " INTEGER," +
                KEY_MATCH_LAST_SEEN_DATE + " TEXT," +
                KEY_RECIPIENT_ID + " INTEGER," +
                KEY_LAST_MESSAGE + " TEXT," +
                KEY_MATCH_LAST_CHAT_DATE + " TEXT," +
                KEY_BIO + " TEXT," +
                KEY_DISTANCE + " TEXT," +
                KEY_JOB + " TEXT," +
                KEY_SCHOOL + " TEXT," +
                KEY_DOB + " TEXT," +
                KEY_MATCH_UNREAD_MESSAGES + " INTEGER," +
                KEY_MATCH_VERIFIED + " INTEGER," +
                KEY_INTEREST_DIALOGS + " TEXT" + ")";

        String CREATE_TABLE_SWIPES = "CREATE TABLE " + TABLE_SWIPES + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_REC_ID + " INTEGER," +
                KEY_SWIPE_ACTION + " TEXT" + ")";

        String CREATE_TABLE_TOP_PICKS = "CREATE TABLE " + TABLE_TOP_PICKS + "(" +
                KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_TOP_PICKS + " TEXT" + ")";


        db.execSQL(CREATE_MATCH_TABLE);
        db.execSQL(CREATE_DIALOGS_TABLE);
        db.execSQL(CREATE_TABLE_SWIPES);
        db.execSQL(CREATE_TABLE_TOP_PICKS);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MATCHES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIALOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SWIPES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOP_PICKS);

        // Create tables again
        onCreate(db);
    }


    public void addTopPicks(GetUsersNearbyResponse response) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TOP_PICKS, Arrays.toString(response.getBody()));
        try {
            Log.d(TAG, "addTopPicks: "+values.size());

            db.insert(KEY_TOP_PICKS, null, values);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

    }

    public ArrayList<GetUsersNearbyResponse> getTopPicks() {
        String QUERY_TOP_PICKS = "SELECT * FROM " + TABLE_TOP_PICKS;
        ArrayList<GetUsersNearbyResponse> pb = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(QUERY_TOP_PICKS, null);
        cursor.moveToFirst();
        final List<Body> topPicks = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    Log.d(TAG, "getTopPicks: cursor");
                    Body body = new Body();
                    String s = cursor.getString(1);
                    Gson gson = new Gson();
                    TypeToken<ArrayList<GetUsersNearbyResponse>> token = new TypeToken<ArrayList<GetUsersNearbyResponse>>() {
                    };
                    pb = gson.fromJson(s, token.getType());

                }
                while (cursor.moveToFirst());

            }
        return pb;
    }

    public void deleteAllMactches() {
        SQLiteDatabase db = this.getWritableDatabase();

        String count = "SELECT count(*) FROM " + TABLE_MATCHES;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0) {
            db.execSQL("DELETE from " + TABLE_MATCHES);

        } else {
            Log.d("DATABASE", "deleteAllMactches: null");
        }

    }

    public void deleteAllSwipes() {
        SQLiteDatabase db = this.getWritableDatabase();

        String count = "SELECT count(*) FROM " + TABLE_SWIPES;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0) {
            db.execSQL("DELETE from " + TABLE_SWIPES);
            Log.d("DATABASE", "deleeallswipes: deleted");

        } else {
            Log.d("DATABASE", "deleeallswipes: null");
        }

    }

    public void deleteAllDialogs() {
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT count(*) FROM " + TABLE_DIALOGS;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0) {
            db.execSQL("DELETE from " + TABLE_DIALOGS);

        } else {
            Log.d("DATABASE", "deleteAllDialogs: null");
        }
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void addMatches(MyMatches myMatches) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(KEY_MATCH_ID, myMatches.getMatch_id());
        Log.d("DATABASE", "addMatches: user_id" + myMatches.getUser_id());
        values.put(KEY_USER_ID, myMatches.getUser_id());
        values.put(KEY_FULL_NAME, myMatches.getFullName());
        values.put(KEY_PHOTO_PATH, myMatches.getPhotoPath());
        values.put(KEY_VIEW_STATUS, myMatches.getViewStatus());
        values.put(KEY_BIO, myMatches.getBio());
        values.put(KEY_DISTANCE, myMatches.getDistance());
        values.put(KEY_JOB, myMatches.getJob());
        values.put(KEY_SCHOOL, myMatches.getSchool());
        values.put(KEY_DOB, myMatches.getDob());
        values.put(KEY_LAST_SEEN_DATE, myMatches.getLastSeenDate());
        values.put(KEY_INTEREST_MATCHES, myMatches.getInterest());
        values.put(KEY_MATCH_VERIFIED, myMatches.getVerification_status());


        // Inserting Row
        db.insert(TABLE_MATCHES, null, values);
        db.close(); // Closing database connection
    }

    public void addDialogs(ChatInfo chatInfo) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(KEY_MATCH_ID, chatInfo.getMatchId());
        values.put(KEY_MATCH_FULL_NAME, chatInfo.getFullName());
        values.put(KEY_MATCH_PHOTO_PATH, chatInfo.getPhotoPath());
        values.put(KEY_MATCH_VIEW_STATUS, chatInfo.getViewStatus());
        values.put(KEY_MATCH_LAST_SEEN_DATE, chatInfo.getLastSeenDate());
        values.put(KEY_RECIPIENT_ID, chatInfo.getRecipientId());
        values.put(KEY_LAST_MESSAGE, chatInfo.getLastMessage());
        values.put(KEY_MATCH_LAST_CHAT_DATE, String.valueOf(chatInfo.getLastChatDate()));
        values.put(KEY_BIO, chatInfo.getBio());
        values.put(KEY_DISTANCE, chatInfo.getDistance());
        Log.d("DATABASE", "put Distance" + chatInfo.getDistance());
        values.put(KEY_JOB, chatInfo.getJob());
        values.put(KEY_SCHOOL, chatInfo.getSchool());
        values.put(KEY_DOB, chatInfo.getDob());
        values.put(KEY_MATCH_UNREAD_MESSAGES, chatInfo.getUnreadMsgCount());
        values.put(KEY_INTEREST_DIALOGS, chatInfo.getInterest());
        values.put(KEY_MATCH_VERIFIED,chatInfo.getVerification_status());


        // Inserting Row
        db.insert(TABLE_DIALOGS, null, values);

        db.close(); // Closing database connection
    }

    public void adSwipes(Swipe swipe) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(KEY_REC_ID, swipe.getRecipient_id());
        values.put(KEY_SWIPE_ACTION, swipe.getSwipe_action());
        Log.d("DATABASE", "adSwipes: " + swipe.getRecipient_id());
        Log.d("DATABASE", "adSwipes: " + swipe.getSwipe_action());

        db.insert(TABLE_SWIPES, null, values);
        db.close();

    }


    public List<Swipe> getAllSwipes() {
        List<Swipe> swipeArrayList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SWIPES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Swipe swipe = new Swipe();
                swipe.setRecipient_id(cursor.getInt(1));
                swipe.setSwipe_action(cursor.getString(2));
                Log.d("DATABASE", "rec_id : " + cursor.getInt(1));
                Log.d("DATABASE", "swipe_action: " + cursor.getString(2));

                swipeArrayList.add(swipe);
            }
            while (cursor.moveToNext());
        }

        // return contact list
        return swipeArrayList;

    }


    // Getting All Contacts
    public List<MyMatches> getAllMatches() {
        List<MyMatches> matchesList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_MATCHES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MyMatches myMatches = new MyMatches();
                myMatches.setMatch_id(cursor.getInt(1));
                myMatches.setUser_id(cursor.getInt(2));
                myMatches.setFullName(cursor.getString(3));
                myMatches.setPhotoPath(cursor.getString(4));
                myMatches.setViewStatus(cursor.getInt(5));
                myMatches.setBio(cursor.getString(6));
                myMatches.setDistance(cursor.getString(7));
                myMatches.setJob(cursor.getString(8));
                myMatches.setSchool(cursor.getString(9));
                myMatches.setDob(cursor.getString(10));
                myMatches.setLastSeenDate(cursor.getString(11));
                myMatches.setVerification_status(cursor.getInt(12));
                myMatches.setInterest(cursor.getString(13));
                // Adding contact to list
                matchesList.add(myMatches);
            }
            while (cursor.moveToNext());
        }

        // return contact list
        return matchesList;
    }


    public List<ChatInfo> getAllDialogs() {
        List<ChatInfo> dialogList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_DIALOGS;
        Log.d("TEST", "unread" + selectQuery);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChatInfo chatInfo = new ChatInfo();
                chatInfo.setMatchId(cursor.getInt(1));
                chatInfo.setFullName(cursor.getString(2));
                chatInfo.setPhotoPath(cursor.getString(3));
                chatInfo.setViewStatus(cursor.getInt(4));
                chatInfo.setLastSeenDate(cursor.getString(5));
                chatInfo.setRecipientId(cursor.getInt(6));
                chatInfo.setLastMessage(cursor.getString(7));
                chatInfo.setLastChatDate(cursor.getString(8));
                chatInfo.setBio(cursor.getString(9));
                chatInfo.setDistance(cursor.getString(10));
                chatInfo.setJob(cursor.getString(11));
                chatInfo.setSchool(cursor.getString(12));
                chatInfo.setDob(cursor.getString(13));
                chatInfo.setUnreadMsgCount(cursor.getInt(14));
                chatInfo.setVerification_status(cursor.getInt(15));
                chatInfo.setInterest(cursor.getString(16));
                Log.d("DATABASE", "unread" + cursor.getInt(14));
                Log.d("DATABASE", "Name dialog" + cursor.getString(2));
                Log.d("DATABASE", "Name distance" + cursor.getString(10));
                // Adding contact to list
                dialogList.add(chatInfo);
            }
            while (cursor.moveToNext());
        }

        // return contact list
        return dialogList;
    }


    public boolean Exists(String _id) {
        String query = "SELECT 1 FROM " + TABLE_MATCHES + " WHERE " + KEY_USER_ID + "=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{_id});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public boolean SwipeExists(String _id) {
        String query = "SELECT 1 FROM " + TABLE_SWIPES + " WHERE " + KEY_REC_ID + "=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{_id});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        Log.d("DATABASE", "SwipeExists: " + exists);
        return exists;
    }

    public boolean Exists_DIALOG(String _id) {
        String query = "SELECT 1 FROM " + TABLE_DIALOGS + " WHERE " + KEY_RECIPIENT_ID + "=?";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{_id});
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    // Deleting single contact
    public void deleteFavourite(MyMatches myMatches) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MATCHES, KEY_USER_ID + " = ?", new String[]{String.valueOf(myMatches.getUser_id())});
        db.close();
    }


    public void deleteDialog(ChatInfo chatInfo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DIALOGS, KEY_RECIPIENT_ID + " = ?", new String[]{String.valueOf(chatInfo.getRecipientId())});
        db.close();
    }


    // Getting contacts Count
    public int getFavouritesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_MATCHES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}
