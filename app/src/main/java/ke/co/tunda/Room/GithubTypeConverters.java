package ke.co.tunda.Room;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import androidx.room.TypeConverter;
import ke.co.tunda.Models.Mood;

class GithubTypeConverters {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<Mood> stringToSomeObjectList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Mood>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(List<Mood> someObjects) {
        return gson.toJson(someObjects);
    }
}
