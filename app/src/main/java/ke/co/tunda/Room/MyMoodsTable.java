/*
 * Creator: Louis on 6/19/19 2:50 PM Last modified: 6/19/19 2:50 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import ke.co.tunda.Constants.Extras;


@Entity(tableName = "MyMoodsTable",
        indices = {@Index(value = "mood_id", unique = true)})

public class MyMoodsTable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "did")
    public int id;

    @ColumnInfo(name = Extras.TIME_POSTED)
    @NonNull
    public String time_posted;

    @ColumnInfo(name = Extras.MOOD_ID)
    @NonNull
    public int mood_id;

    @ColumnInfo(name = Extras.MOOD_TYPE)
    @NonNull
    public int mood_type;

    @ColumnInfo(name = Extras.MOOD_CAPTION)
    public String mood_caption;

    @ColumnInfo(name = Extras.MOOD_TEXT)
    public String mood_text;

    @ColumnInfo(name = Extras.MOOD_STATUS)
    public int mood_status;

    @ColumnInfo(name = Extras.VIEW_COUNT)
    @NonNull
    public int view_count;

    @ColumnInfo(name = Extras.FONT_COLOR)
    public String font_color;

    @ColumnInfo(name = Extras.BACKGROUND_COLOR)
    public String background_color;

    @ColumnInfo(name = Extras.PHOTO_PATH)
    public String photo_path;

    @ColumnInfo(name = Extras.VIEW_STATUS)
    @NonNull
    public int view_status;

    @ColumnInfo(name = Extras.FULL_NAME)
    public String full_name;

    @ColumnInfo(name = Extras.MOOD_COUNT)
    public Integer mood_count;

    @ColumnInfo(name = Extras.VIEWED_MOODS)
    public Integer viewed_moods;

    @ColumnInfo(name = Extras.USER_PHOTO)
    public String user_photo;

    @ColumnInfo(name =  Extras.DATE_CREATED)
    public String created_on;



    public MyMoodsTable(@NonNull String time_posted, @NonNull int mood_id, @NonNull int mood_type,
                        @NonNull String mood_caption, String mood_text,
                         int mood_status, @NonNull int view_count,
                         String font_color,  String background_color, @NonNull String photo_path,@NonNull int view_status,
                        String full_name,Integer mood_count,Integer viewed_moods,String user_photo,String created_on) {

        this.time_posted = time_posted;
        this.mood_id = mood_id;
        this.mood_type = mood_type;
        this.mood_caption = mood_caption;
        this.mood_text = mood_text;
        this.mood_status = mood_status;
        this.view_count = view_count;
        this.font_color = font_color;
        this.background_color = background_color;
        this.photo_path = photo_path;
        this.view_status = view_status;
        this.full_name = full_name;
        this.mood_count = mood_count;
        this.viewed_moods= viewed_moods;
        this.user_photo = user_photo;
        this.created_on = created_on;

    }


}
