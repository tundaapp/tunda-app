/*
 * Creator: Louis on 6/19/19 2:50 PM Last modified: 6/19/19 2:50 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import ke.co.tunda.Constants.Extras;


@Entity(tableName = "ChatDialogsTable",
        indices = {@Index(value = Extras.MATCH_ID, unique = true)})

public class ChatDialogsTable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "did")
    public int id;

    @ColumnInfo(name = Extras.MATCH_ID)
    @NonNull
    public Integer match_id;

    @ColumnInfo(name = Extras.FULL_NAME)
    @NonNull
    public String full_name;


    @ColumnInfo(name = Extras.PHOTO_PATH)
    @NonNull
    public String photo_path;

    @ColumnInfo(name = Extras.VIEW_STATUS)
    public Integer view_status;

    @ColumnInfo(name = Extras.KEY_MATCH_LAST_SEEN_DATE)
    public String last_seen_date;

    @ColumnInfo(name = Extras.KEY_LAST_MESSAGE)
    public String last_message;

    @ColumnInfo(name = Extras.KEY_MATCH_LAST_CHAT_DATE)
    public String last_chat_date;

    @ColumnInfo(name = Extras.KEY_MATCH_UNREAD_MESSAGES)
    public Integer unread_messages;

    @ColumnInfo(name = Extras.KEY_INTEREST_DIALOGS)
    public String interests;



    @ColumnInfo(name = Extras.RECIPIENT_ID)
    public Integer recipient_id;

    @ColumnInfo(name = Extras.DOB)
    public String dob;

    @ColumnInfo(name = Extras.ABOUT_ME)
    public String about_me;


    @ColumnInfo(name = Extras.JOB_TITLE)
    public String job_title;

    @ColumnInfo(name = Extras.SCHOOL)
    public String getSchool;

    @ColumnInfo(name = Extras.DIST_KM)
    public String getDistance;

    @ColumnInfo(name = Extras.PHOTO_COUNT)
    public Integer getPhoto_Count;

    @ColumnInfo(name = Extras.VERIFICATION_STATUS)
    public Integer getVerificatonStatus;

    @ColumnInfo(name = Extras.LIKES_COUNT)
    public Integer getLikesCount;

    @ColumnInfo(name = Extras.HAS_INSTAGRAM)
    public Integer has_insta_photo;


    public ChatDialogsTable(@NonNull Integer match_id, @NonNull String full_name, @NonNull String photo_path,
                             Integer view_status, String last_seen_date,
                            String last_message, @NonNull String last_chat_date,
                            int unread_messages, String interests,
                            int recipient_id, String dob, String about_me, String job_title, String getSchool, String getDistance,
                            Integer getPhoto_Count, Integer getVerificatonStatus, Integer getLikesCount,Integer has_insta_photo) {

        this.match_id = match_id;
        this.full_name = full_name;
        this.photo_path = photo_path;
        this.view_status = view_status;
        this.last_seen_date = last_seen_date;
        this.last_message = last_message;
        this.last_chat_date = last_chat_date;
        this.unread_messages = unread_messages;
        this.interests = interests;
        this.recipient_id = recipient_id;
        this.dob = dob;
        this.about_me = about_me;
        this.job_title = job_title;
        this.getSchool = getSchool;
        this.getDistance = getDistance;
        this.getPhoto_Count = getPhoto_Count;
        this.getLikesCount = getLikesCount;
        this.getVerificatonStatus=getVerificatonStatus;
        this.has_insta_photo = has_insta_photo;

    }


}
