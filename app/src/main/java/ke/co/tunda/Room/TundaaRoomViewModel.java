/*
 * Creator: Louis on 6/19/19 4:37 PM Last modified: 6/19/19 4:37 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class TundaaRoomViewModel extends AndroidViewModel {
    private TundaRoomDao tundaRoomDao;
    private LiveData<List<MyMoodsTable>> myMoodsTableLiveData;
    private LiveData<List<MainMoodsTable>> mainMoodsTableLiveData;
    private LiveData<List<ChatDialogsTable>> chatDialogsLiveData;


    public TundaaRoomViewModel(@NonNull Application application) {
        super(application);

        tundaRoomDao = DatabaseRoom.getDatabase(application.getApplicationContext()).tundaRoomDao();
        myMoodsTableLiveData = tundaRoomDao.getAllMyMoods();
        mainMoodsTableLiveData = tundaRoomDao.getAllMainMoods();
        chatDialogsLiveData = tundaRoomDao.getAllChatDialogs();

    }

    public LiveData<List<MyMoodsTable>> getMyMoodsTableLiveData() {
        return myMoodsTableLiveData;
    }

    public LiveData<List<MainMoodsTable>> getMainMoodsTableLiveData() {
        return mainMoodsTableLiveData;
    }


    public void insert(MyMoodsTable myMoodsTable) {
        tundaRoomDao.insert(myMoodsTable);
    }

    public void insert(MainMoodsTable mainMoodsTable) {
        tundaRoomDao.insert(mainMoodsTable);
    }

    public void update(MyMoodsTable myMoodsTable) {
        tundaRoomDao.update(myMoodsTable);
    }

    public void update(MainMoodsTable mainMoodsTable) {
        tundaRoomDao.update(mainMoodsTable);
    }

    public void deleteAllMyMoods() {
        tundaRoomDao.deleteAllMyMoods();
    }

    public void deleteAllMainMoods() {
        tundaRoomDao.deleteAllMainMoods();
    }

    public void deleteSingleMood(MyMoodsTable myMoodsTable) {
        tundaRoomDao.deleteMoodById(myMoodsTable.mood_id);
    }

    public void deleteSingleMainMood(MainMoodsTable mainMoodsTable) {
        tundaRoomDao.deleteMainMoodById(mainMoodsTable.user_id);
    }

    public LiveData<List<ChatDialogsTable>> getChatDialogsLiveData() {
        return chatDialogsLiveData;
    }
}
