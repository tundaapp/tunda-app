/*
 * Creator: Louis on 6/19/19 4:37 PM Last modified: 6/19/19 4:37 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import android.app.Application;
import android.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class ChatsViewModel extends AndroidViewModel {
    private TundaRoomDao tundaRoomDao;
    private LiveData<List<ChatHistoryTable>> chatHistoryLiveData;
    private List<ChatHistoryTable> chatHistoryTableList;
    private String match_id;
    private static String TAG = "CHATVM";


    public ChatsViewModel(@NonNull Application application) {
        super(application);

        tundaRoomDao = DatabaseRoom.getDatabase(application.getApplicationContext()).tundaRoomDao();
        chatHistoryLiveData = tundaRoomDao.getAllChatsLive(match_id);
        chatHistoryTableList = tundaRoomDao.getAllChats();

    }


    public List<ChatHistoryTable> getChatHistoryTableList() {
        return chatHistoryTableList;
    }

    public LiveData<List<ChatHistoryTable>> getChatHistoryLiveData(String match_id) {
        Log.d(TAG, "getChatHistoryLiveData: "+match_id);
        this.match_id = match_id;
        return chatHistoryLiveData;
    }

}
