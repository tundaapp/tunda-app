/*
 * Creator: Louis on 6/19/19 2:50 PM Last modified: 6/19/19 2:50 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Models.Mood;


@Entity(tableName = "MainMoodsTable",
        indices = {@Index(value = "user_id", unique = true)})

public class MainMoodsTable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "did")
    public int id;


    @ColumnInfo(name = Extras.USER_ID)
    @NonNull
    public int user_id;

    @ColumnInfo(name = Extras.FULL_NAME)
    public String full_name;

    @ColumnInfo(name = Extras.MOOD_COUNT)
    public Integer mood_count;

    @ColumnInfo(name = Extras.VIEWED_MOODS)
    public Integer viewed_moods;

    @ColumnInfo(name = Extras.USER_PHOTO)
    public String user_photo;

    @ColumnInfo(name =  Extras.DATE_CREATED)
    public String created_on;

    @TypeConverters(GithubTypeConverters.class)
    @ColumnInfo(name = Extras.USER_MOODS)
    @NonNull
    public final List<Mood> user_moods;



    public MainMoodsTable(@NonNull int user_id,
                          String full_name, Integer mood_count, Integer viewed_moods, String user_photo,
                          String created_on,@NonNull List<Mood> user_moods) {

        this.user_id = user_id;
        this.full_name = full_name;
        this.mood_count = mood_count;
        this.viewed_moods= viewed_moods;
        this.user_photo = user_photo;
        this.created_on = created_on;
        this.user_moods = user_moods;

    }


}
