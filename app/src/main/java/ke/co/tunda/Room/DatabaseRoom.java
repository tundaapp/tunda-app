/*
 * Creator: Louis on 6/19/19 4:22 PM Last modified: 6/19/19 4:22 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {MyMoodsTable.class, MainMoodsTable.class,ChatHistoryTable.class,ChatDialogsTable.class}, version = 11)
public abstract class DatabaseRoom extends RoomDatabase {

    private static DatabaseRoom INSTANCE;
    private static final String DB_NAME = "tunda.db";
    private static final String TAG = "ROOM";

    static final Migration MIGRATION = new Migration(9, 11) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.
        }
    };


    public static DatabaseRoom getDatabase(final Context context) {


        if (INSTANCE == null) {
            synchronized (DatabaseRoom.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DatabaseRoom.class, DB_NAME)
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Log.d(TAG, "onCreate: ");

                                }

                                @Override
                                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                                    super.onOpen(db);
//                                    Log.d(TAG, "onOpen: ");
                                }
                            })
                            .build();
                }

            }
        }

        return INSTANCE;

    }


    public abstract TundaRoomDao tundaRoomDao();


}
