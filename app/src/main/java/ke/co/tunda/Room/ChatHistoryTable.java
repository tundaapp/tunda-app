/*
 * Creator: Louis on 6/19/19 2:50 PM Last modified: 6/19/19 2:50 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import ke.co.tunda.Constants.Extras;


@Entity(tableName = "ChatHistoryTable")

public class ChatHistoryTable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "did")
    public int id;

    @ColumnInfo(name = Extras.CHAT_ID)
    @NonNull
    public Integer chat_id;

    @ColumnInfo(name = Extras.USER_ID)
    @NonNull
    public Integer user_id;

    @ColumnInfo(name = Extras.RECIPIENT_ID)
    @NonNull
    public Integer recipient_id;

    @ColumnInfo(name = Extras.MESSAGE)
    @NonNull
    public String message;

    @ColumnInfo(name = Extras.DATE_CREATED)
    public String date_created;

    @ColumnInfo(name = Extras.STATUS)
    public String status;

    @ColumnInfo(name = Extras.MATCH_ID)
    @NonNull
    public String match_id;






    public ChatHistoryTable(@NonNull Integer chat_id,@NonNull Integer user_id, @NonNull Integer recipient_id, @NonNull String message,
                            @NonNull String date_created, String status, @NonNull String match_id) {

        this.chat_id= chat_id;
        this.user_id = user_id;
        this.recipient_id = recipient_id;
        this.message = message;
        this.date_created = date_created;
        this.status = status;
        this.match_id = match_id;


    }


}
