/*
 * Creator: Louis on 6/19/19 4:01 PM Last modified: 6/19/19 4:01 PM Copyright: All rights reserved Ⓒ 2019 http://accuretsltns.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package ke.co.tunda.Room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface TundaRoomDao {


    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(MyMoodsTable myMoodsTable);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(MyMoodsTable myMoodsTable);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(MainMoodsTable mainMoodsTable);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insertDialog(ChatDialogsTable chatDialogsTable);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(MainMoodsTable mainMoodsTable);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertChat(ChatHistoryTable chatHistoryTable);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void UpdateChat(ChatHistoryTable chatHistoryTable);


    @Query("SELECT * FROM chatdialogstable")
    LiveData<List<ChatDialogsTable>> getAllChatDialogs();

    @Query("SELECT * FROM mymoodstable")
    LiveData<List<MyMoodsTable>> getAllMyMoods();

    @Query("SELECT * FROM mainmoodstable")
    LiveData<List<MainMoodsTable>> getAllMainMoods();

    @Query("SELECT * FROM chathistorytable WHERE match_id= :match_id")
    LiveData<List<ChatHistoryTable>> getAllChatsLive(String match_id);

    @Query("SELECT * FROM chathistorytable")
    List<ChatHistoryTable> getAllChats();

    @Query("DELETE FROM mymoodstable")
    void deleteAllMyMoods();


    @Query("DELETE FROM mainmoodstable")
    void deleteAllMainMoods();

    @Query("DELETE FROM chatdialogstable")
    void deleteAllChatDialogs();


    @Query("DELETE FROM ChatHistoryTable WHERE match_id = :match_id")
    void deleteAllChats(String match_id);

    @Query("DELETE FROM mymoodstable WHERE mood_id = :mood_id")
    void deleteMoodById(int mood_id);

    @Query("DELETE FROM mainmoodstable WHERE user_id = :user_id")
    void deleteMainMoodById(int user_id);

    @Query("DELETE FROM chatdialogstable WHERE match_id = :match_id")
    void deleteChatDialogById(int match_id);


    @Query("SELECT * FROM mainmoodstable WHERE user_id = :user_id LIMIT 1")
    MainMoodsTable findMainMoodById(int user_id);

    @Query("SELECT * FROM mymoodstable WHERE mood_id = :mood_id LIMIT 1")
    MyMoodsTable findMoodById(int mood_id);

    @Query("SELECT * FROM chatdialogstable WHERE match_id = :match_id LIMIT 1")
    ChatDialogsTable findChatDialogById(Integer match_id);

    @Query("UPDATE chatdialogstable SET last_message = :last_message WHERE match_id = :match_id")
    void updateChatDialogLastMessage(int match_id, String last_message);


}
