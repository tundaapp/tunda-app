
//
//package ke.co.tunda.fragments;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.provider.MediaStore;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.afollestad.materialdialogs.MaterialDialog;
//import com.droidninja.imageeditengine.ImageEditor;
//import com.google.android.material.floatingactionbutton.FloatingActionButton;
//
//import java.io.File;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentActivity;
//import androidx.lifecycle.Observer;
//import androidx.lifecycle.ViewModelProviders;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import de.hdodenhof.circleimageview.CircleImageView;
//import es.dmoral.toasty.Toasty;
//import ke.co.tunda.Activities.StatusStories;
//import ke.co.tunda.Adapters.StatusAdapter;
//import ke.co.tunda.ApiConnector.ApiCaller;
//import ke.co.tunda.ApiConnector.Builder;
//import ke.co.tunda.ApiConnector.Builder2;
//import ke.co.tunda.ApiConnector.Models.GetMyMoodRequest;
//import ke.co.tunda.ApiConnector.Models.GetMyMoodResponse;
//import ke.co.tunda.ApiConnector.Models.UserStatus;
//import com.bumptech.glide.Glide;
//import ke.co.tunda.Models.Mood;
//import ke.co.tunda.Models.MoodsRequest;
//import ke.co.tunda.Models.MoodsResponse;
//import ke.co.tunda.R;
//import ke.co.tunda.Room.DatabaseRoom;
//import ke.co.tunda.Room.MainMoodsTable;
//import ke.co.tunda.Room.MyMoodsTable;
//import ke.co.tunda.Room.TundaRoomDao;
//import ke.co.tunda.Room.TundaaRoomViewModel;
//import ke.co.tunda.Views.CircularStatusView;
//import ke.co.tunda.multimediapicker.GalleryPickerActivity;
//import ke.co.tunda.multimediapicker.MultimediaPicker;
//import ke.co.tunda.multimediapicker.model.Image;
//import okhttp3.MediaType;
//import okhttp3.MultipartBody;
//import okhttp3.RequestBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//import static android.app.Activity.RESULT_OK;
//
//
//public class TundaMoodFragment extends Fragment implements View.OnClickListener {
//
//
//    private RecyclerView rvStatus;
//    private StatusAdapter adapter;
//    private ArrayList<UserStatus> statusList;
//    private ArrayList<Image> images = new ArrayList<>();
//    private ArrayList<String> photoPaths = new ArrayList<>();
//    private ArrayList<Mood> moodArrayList = new ArrayList<>();
//    private List<MyMoodsTable> myMoodsTableList = new ArrayList<>();
//    private List<MainMoodsTable> mainMoodsTableList = new ArrayList<>();
//    private int REQUEST_CODE_PICKER = 2000;
//    private static String TAG = "TMF";
//    private TundaRoomDao tundaRoomDao;
//
//
//    public static final String[] profileUrls = {"https://images.pexels.com/photos/949380/pexels-photo-949380.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//            "https://images.pexels.com/photos/2661255/pexels-photo-2661255.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
//            "https://images.pexels.com/photos/2946598/pexels-photo-2946598.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
//            "https://images.pexels.com/photos/1019411/pexels-photo-1019411.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
//    };
//    @BindView(R.id.profile_image)
//    CircleImageView mProfileImage;
//    @BindView(R.id.my_status_container)
//    RelativeLayout mStatusContainer;
//    @BindView(R.id.circular_status_view)
//    CircularStatusView mStatusView;
//    @BindView(R.id.mood_time)
//    TextView mMoodTime;
//    @BindView(R.id.add_status)
//    FloatingActionButton mAddStatus;
//    @BindView(R.id.no_status)
//    RelativeLayout mNoStatus;
//
//
//    FragmentActivity mActivity;
//    SharedPreferences sp;
//    int user_id;
//    String user_photo;
//    private boolean hasMood;
//    boolean isCacheEnabled = false;
//    boolean isImmersiveEnabled = false;
//    boolean isTextEnabled = false;
//    long storyDuration = 3000L;
//    GetMyMoodResponse getMyMoodResponse;
//    ArrayList<MoodsResponse.Body> moodsBody;
//    private static String name, image;
//    TundaaRoomViewModel tundaaRoomViewModel;
//
//
//    public TundaMoodFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//
//        return inflater.inflate(R.layout.fragment_tunda_mood, container, false);
//
//
//    }
//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        ButterKnife.bind(this, view);
//        tundaRoomDao = DatabaseRoom.getDatabase(mActivity).tundaRoomDao();
//        tundaaRoomViewModel = ViewModelProviders.of(mActivity).get(TundaaRoomViewModel.class);
//
//        mStatusContainer.setOnClickListener(this);
//        mAddStatus.setOnClickListener(this);
//        initialize(view);
//        populateStatus();
//        populateMyStatus();
//
//        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        user_photo = sp.getString("image", "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1");
//        user_id = sp.getInt("user_id", 0);
//        name = sp.getString("first_name", "Me");
//        image = sp.getString("image", "");
//        new getMyMood().execute();
//        new getstatuses().execute();
//
//    }
//
//    private void populateMyStatus() {
//        tundaaRoomViewModel.getMyMoodsTableLiveData().observe(this, new Observer<List<MyMoodsTable>>() {
//            @Override
//            public void onChanged(List<MyMoodsTable> myMoodsTables) {
//                if (myMoodsTables != null && myMoodsTables.size() >= 1) {
//                    Log.d(TAG, "onChanged: " + myMoodsTables.get(0).photo_path);
//                    myMoodsTableList = myMoodsTables;
//                    int moodsize = myMoodsTables.size();
//
//
//                    mStatusView.setPortionsCount(moodsize);
//                    mStatusView.setVisibility(View.VISIBLE);
//
//
//                    if (myMoodsTables.get(0).mood_type == 1) {
//
//                        Glide.with(mActivity)
//                                .load(myMoodsTables.get(0).photo_path)
//                                .into(mProfileImage);
//                        String formatted_date = null;
//                        try {
//                            formatted_date = formatToYesterdayOrToday(myMoodsTables.get(0).time_posted);
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//                        mMoodTime.setText(formatted_date);
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putBoolean("has_mood", true);
//                        editor.apply();
//                        hasMood = true;
//
//
//                    }
//
//
//                } else {
//                    SharedPreferences.Editor editor = sp.edit();
//                    editor.putBoolean("has_mood", false);
//                    editor.apply();
//                    hasMood = false;
//                    Glide.with(mActivity)
//                            .load(user_photo)
//                            .into(mProfileImage);
//                    Log.d(TAG, "onChanged: no moods/null");
//                }
//            }
//        });
//    }
//
//    private void initialize(View view) {
//
//        statusList = new ArrayList<>();
//        rvStatus = view.findViewById(R.id.rvStatus);
//
//    }
//
//    private void populateStatus() {
//        //Population logic goes here
//
////        statusList = DummyDataGenerator.generateStatuses();
//
//        tundaaRoomViewModel.getMainMoodsTableLiveData().observe(this, new Observer<List<MainMoodsTable>>() {
//            @Override
//            public void onChanged(List<MainMoodsTable> mainMoodsTables) {
//
//                if (mainMoodsTables != null && mainMoodsTables.size() >= 1) {
//                    Log.d(TAG, "onChanged: " + mainMoodsTables.get(0).full_name);
//                    mainMoodsTableList = mainMoodsTables;
//                    int moodsize = mainMoodsTables.size();
//
//                    moodsPresent(true);
//                    setAdapter(mainMoodsTables);
//
//                } else {
//                    Log.d(TAG, "onChanged: moods null");
//                    moodsPresent(false);
//                }
//            }
//        });
//    }
//
//
////
//
//
//    private void setAdapter(List<MainMoodsTable> moodsBody) {
//
//        rvStatus.setLayoutManager(new LinearLayoutManager(getContext()));
//        adapter = new StatusAdapter(getContext(), moodsBody);
//
//        rvStatus.setAdapter(adapter);
//
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        mActivity = (FragmentActivity) context;
//
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.my_status_container:
//                if (hasMood) {
//                    openMood();
//                } else {
//                    uploadStatus();
//                }
//
//                break;
//            case R.id.add_status:
//                uploadStatus();
//                break;
//        }
//
//    }
//
//    private void openMood() {
//
//        if (moodArrayList != null) {
//            StatusStories.open(mActivity, myMoodsTableList, null, name, image, true);
//        } else {
//            uploadStatus();
//        }
//
//
//    }
//
//    private void uploadStatus() {
//        MultimediaPicker.create(mActivity)
//                .folderMode(true) // set folder mode (false by default)
//                .folderTitle("Upload Tunda Mood") // folder selection title
//                .imageTitle("Tap to select") // image selection title
//                .single() // single mode
//                .setOnlyImages(true)
//                .showCamera(true) // show camera or not (true by default)
//                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
//                .origin(images) // original selected images, used in multi mode
//                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
//
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d(TAG, "onActivityResult: " + REQUEST_CODE_PICKER);
//
//
//        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
//            Log.d(TAG, "onActivityResult: " + REQUEST_CODE_PICKER);
//            images = data.getParcelableArrayListExtra(GalleryPickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
//            StringBuilder sb = new StringBuilder();
//            photoPaths = new ArrayList<>();
//            for (int i = 0, l = images.size(); i < l; i++) {
//                photoPaths.add(images.get(i).getPath());
////                sb.append(images.get(i).getPath() + "\n");
//            }
//
//            if (photoPaths.size() > 0) {
//                if (mActivity != null) {
//                    new ImageEditor.Builder(mActivity, photoPaths.get(0))
//                            .setStickerAssets("stickers")
//                            .open();
//
//                } else if (getActivity() != null) {
//                    new ImageEditor.Builder(getActivity(), photoPaths.get(0))
//                            .setStickerAssets("stickers")
//                            .open();
//                }
//            } else {
//                Toasty.warning(mActivity, "No image selected", Toasty.LENGTH_SHORT).show();
//            }
//        } else if (requestCode == ImageEditor.RC_IMAGE_EDITOR) {
//            String imagePath = null;
//            String caption = null;
//            if (data != null) {
//                imagePath = data.getStringExtra(ImageEditor.EXTRA_EDITED_PATH);
//                caption = data.getStringExtra(ImageEditor.EXTRA_CAPTION);
//                uploadImage(data, imagePath, caption);
//
//            } else {
//                Log.d(TAG, "onActivityResult: data in null" + requestCode + resultCode);
//            }
//
//        }
//    }
//
//    private void uploadImage(Intent data, String imagePath, String caption) {
////        ImageView.setImageURI(Uri.fromFile(new File("/sdcard/cats.jpg")));
//
//
//        Uri uri = Uri.fromFile(new File(imagePath));
//        if (uri != null) {
//            String filePath = getRealPathFromURIPath(uri, mActivity);
//            ApiCaller apiCaller = Builder2.getClient().create(ApiCaller.class);
//            //Create a file object using file path
//            File file = new File(filePath);
//            Log.d(TAG, "uploadImage: " + imagePath);
//            // Create a request body with file and image media type
//            RequestBody fileReqBody = null;
//
//            if (imagePath.contains(".mp4")) {
//                fileReqBody = RequestBody.create(MediaType.parse("video/*"), file);
//
//            } else {
//                fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
//            }
//
//            RequestBody mood_type_body = RequestBody.create(MediaType.parse("text/*"), file);
//            // Create MultipartBody.Part using file request-body,file name and part name
//            RequestBody mUserId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(user_id));
//            RequestBody mMoodType = RequestBody.create(MediaType.parse("text/plain"), "1");
//            RequestBody mMoodDescription = RequestBody.create(MediaType.parse("text/plain"), "tunda mood");
//            RequestBody mMoodCaption = RequestBody.create(MediaType.parse("text/plain"), caption);
//            RequestBody mMoodText = RequestBody.create(MediaType.parse("text/plain"), "");
//
//
//            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("mood_media", file.getName(), fileReqBody);
//            Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.uploadMoodImage(fileToUpload, fileReqBody, mUserId, mMoodType, mMoodCaption, mMoodText);
//            MaterialDialog dialog;
//            MaterialDialog.Builder builder = new MaterialDialog.Builder(mActivity);
//            builder.content("Uploading ....")
//                    .widgetColor(getResources().getColor(R.color.colorAccent))
//                    .progress(true, 0);
//            dialog = builder.build();
//            dialog.setCancelable(false);
//            dialog.show();
//            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
//
//                @Override
//                public void onResponse(@NonNull Call<ke.co.tunda.ApiConnector.Models.Response> call, @NonNull Response<ke.co.tunda.ApiConnector.Models.Response> response) {
//                    if (response.isSuccessful()) {
//                        ke.co.tunda.ApiConnector.Models.Response upr = response.body();
//                        if (upr != null) {
//                            if (upr.getBody().equals("SUCCESS")) {
//                                dialog.dismiss();
//                                Toasty.success(mActivity, "success", Toasty.LENGTH_SHORT).show();
//                                SharedPreferences.Editor editor = sp.edit();
//                                editor.putBoolean("has_mood", true);
//                                hasMood = true;
//
//                                new getMyMood().execute();
//
//
//                            } else {
//                                dialog.dismiss();
//                            }
//                        }
//
//                    } else {
//                        dialog.dismiss();
//                    }
//
//                }
//
//                @Override
//                public void onFailure(@NonNull Call<ke.co.tunda.ApiConnector.Models.Response> call, @NonNull Throwable throwable) {
//                    Log.d("TESTING", "onFailure: " + throwable.getMessage());
//                    if (throwable.getMessage().contains("Failed to connect")) {
//                        Toasty.warning(mActivity, "Connection problem, check network settings", Toasty.LENGTH_SHORT).show();
//
//                    } else {
//                        Toasty.warning(mActivity, "something went wrong.its our fault..try again", Toasty.LENGTH_SHORT).show();
//
//                    }
//                    dialog.dismiss();
//
//                }
//            });
//
//
//        } else {
//            Log.d(TAG, "uploadImage:+Null ur ");
//        }
//    }
//
//    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
//        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
//        if (cursor == null) {
//            return contentURI.getPath();
//        } else {
//            cursor.moveToFirst();
//            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//            cursor.close();
//            return cursor.getString(idx);
//        }
//
//    }
//
//    private class getMyMood extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//
//
//            ApiCaller apiCallerMood = Builder.getClient().create(ApiCaller.class);
//            GetMyMoodRequest getMyMoodRequest = new GetMyMoodRequest();
//            getMyMoodRequest.setRequestType("GET_USER_MOODS");
//            getMyMoodRequest.setUserId(user_id);
//            getMyMoodRequest.setCurrentUserId(user_id);
//            Call<GetMyMoodResponse> call = apiCallerMood.getMyMood(getMyMoodRequest);
//            call.enqueue(new Callback<GetMyMoodResponse>() {
//                @Override
//                public void onResponse(Call<GetMyMoodResponse> call, Response<GetMyMoodResponse> response) {
//                    if (response.isSuccessful()) {
//                        getMyMoodResponse = response.body();
//                        if (getMyMoodResponse != null && getMyMoodResponse.getStatusCode().equals("00")) {
//
//                            ArrayList<Mood> mBody = getMyMoodResponse.getBody();
//                            tundaRoomDao.deleteAllMyMoods();
//
//                            for (int i = 0; i < mBody.size(); i++) {
//                                Mood mood = mBody.get(i);
//                                Log.d(TAG, "onResponse: try inserting" + mood.getTime_posted());
//                                MyMoodsTable myMoodsTable = new MyMoodsTable(mood.getTime_posted(), mood.getMoodId(),
//                                        mood.getMoodType(), mood.getMoodCaption(), mood.getMoodText(), mood.getMoodStatus(),
//                                        mood.getViewCount(), mood.getFontColor(), mood.getBackgroundColor(), mood.getPhotoPath(),
//                                        mood.getViewStatus(), null, null, null, null, null);
//                                tundaRoomDao.insert(myMoodsTable);
//
//                            }
//                        } else if (getMyMoodResponse != null && getMyMoodResponse.getStatusCode().equals("99")) {
//                            tundaRoomDao.deleteAllMyMoods();
//                        }
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(Call<GetMyMoodResponse> call, Throwable t) {
//
//                }
//            });
//
//
//            return null;
//        }
//    }
//
//    private class getstatuses extends AsyncTask<Void, Void, Void> {
//        @Override
//        protected Void doInBackground(Void... voids) {
//
//            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
//
//            MoodsRequest moodsRequest = new MoodsRequest();
//            moodsRequest.setRequestType("GET_MY_LIKE_MOODS");
//            moodsRequest.setUserId(user_id);
//            Call<MoodsResponse> moodsResponseCall = apiCaller.getMoods(moodsRequest);
//            moodsResponseCall.enqueue(new Callback<MoodsResponse>() {
//                @Override
//                public void onResponse(@NonNull Call<MoodsResponse> call, @NonNull Response<MoodsResponse> response) {
//                    if (response.isSuccessful()) {
//                        MoodsResponse moodsResponse = response.body();
//                        if (moodsResponse != null && moodsResponse.getStatusCode().equals("00") && moodsResponse.getBody() != null) {
//                            moodsBody = moodsResponse.getBody();
//                            tundaRoomDao.deleteAllMainMoods();
//                            for (int i = 0; i < moodsBody.size(); i++) {
//                                Log.d(TAG, "onResponse: try inserting main" + moodsBody.get(i).getFullName());
//                                MainMoodsTable mainMoodsTable = new MainMoodsTable(moodsBody.get(i).getUserId(),
//                                        moodsBody.get(i).getFullName(), moodsBody.get(i).getMoodCount(),
//                                        moodsBody.get(i).getViewedMoods(), moodsBody.get(i).getPhotoPath(),
//                                        moodsBody.get(i).getCreatedon(),
//                                        moodsBody.get(i).getUserMoods());
//                                tundaRoomDao.insert(mainMoodsTable);
//
//
//                            }
//                        } else if (moodsResponse != null && moodsResponse.getStatusCode().equals("99")) {
//                            tundaRoomDao.deleteAllMainMoods();
//                        }
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(@NonNull Call<MoodsResponse> call, @NonNull Throwable t) {
//
//
//                }
//            });
//
//            return null;
//        }
//    }
//
//    private void moodsPresent(boolean present) {
//
//        if (present) {
//            mNoStatus.setVisibility(View.GONE);
//            rvStatus.setVisibility(View.VISIBLE);
//        } else {
//            rvStatus.setVisibility(View.GONE);
//            mNoStatus.setVisibility(View.VISIBLE);
//
//        }
//
//
//    }
//
//    public static String formatToYesterdayOrToday(String date) throws ParseException {
//        Date dateTime = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss").parse(date);
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(dateTime);
//        Calendar today = Calendar.getInstance();
//        Calendar yesterday = Calendar.getInstance();
//        yesterday.add(Calendar.DATE, -1);
//        DateFormat timeFormatter = new SimpleDateFormat("hh:mm");
//
//        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
//            return "Today " + timeFormatter.format(dateTime);
//        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
//            return "Yesterday " + timeFormatter.format(dateTime);
//        } else {
//            return date;
//        }
//    }
//}
