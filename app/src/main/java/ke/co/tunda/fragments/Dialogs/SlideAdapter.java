/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/27/19 12:33 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.fragments.Dialogs;

import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ke.co.tunda.R;

public class SlideAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;


    public SlideAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_images = {
            R.drawable.ic_location,
            R.drawable.ic_boost,
            R.drawable.ic_unlock,
            R.drawable.ic_switch,
            R.drawable.ic_heart_2,
            R.drawable.ic_rewind,
            R.drawable.ic_no_ads
    };

    public String[] slide_headings = {
            "Swipe Around the World",
            "Get 1 free boost every month",
            "Choose Who Sees You",
            "Control Your Profile",
            "Unlimited Likes",
            "Unlimited Rewinds",
            "Turn Off Ads"

    };


    public String[] slide_decs = {
            "Passport to anywhere",
            "Skip the Line and get more matches",
            "Only be Shown people you have liked",
            "Limit What Others See With Tundo Plus",
            "Swipe Right as Much as You Want",
            "Go Back and Swipe Again",
            "Have Fun Swiping"
    };


    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        FloatingActionButton slideImageView = view.findViewById(R.id.slide_image);
        TextView slideHeading = view.findViewById(R.id.slide_heading);
        TextView slideDescription = view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_decs[position]);

        container.addView(view);

        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
