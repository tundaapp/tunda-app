package ke.co.tunda.fragments.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import ke.co.tunda.R;
import ke.co.tunda.listener.AuthenticationListener;
import ke.co.tunda.utils.Constants;


/**
 * Created by torzsacristian on 29/06/2017.
 */

public class AuthenticationDialog extends Dialog {

    private final AuthenticationListener listener;
    private Context context;

    private WebView web_view;
    private static String TAG = "AUTHDIAL";

    private final String url = Constants.BASE_URL
            + "/oauth/authorize?app_id="
            + Constants.CLIENT_ID
            + "&redirect_uri="
            + Constants.REDIRECT_URI
            + "&scope=user_profile,user_media"
            + "&response_type=code";

    public AuthenticationDialog(@NonNull Context context, AuthenticationListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.auth_dialog);

        initializeWebView();
    }

    private void initializeWebView() {

        Log.d(TAG, "initializeWebView: " + url);
        web_view = (WebView) findViewById(R.id.web_view);
        WebSettings settings = web_view.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        web_view.loadUrl(url);
        web_view.setWebViewClient(new WebViewClient() {
            boolean authComplete = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            String access_code;

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d(TAG, "onPageFinished: " + url);

                if (url.contains("code=") && !authComplete) {
                    String[] parts = url.split("=");
                    access_code = parts[1];
                    if (access_code.contains("#_")) {
                        access_code = access_code.replace("#_", "");
                    }
                    Log.d(TAG, "CODE : " + access_code);
                    authComplete = true;
                    listener.onCodeReceived(access_code);
                    dismiss();

                } else if (url.contains("?error")) {
                    Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }
        });
    }
}
