

package ke.co.tunda.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.Adapters.LikesAdapter;
import ke.co.tunda.Adapters.SendLikeSwipeDataInterface;
import ke.co.tunda.Adapters.TopPicksAdapter;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.Models.DefaultRequest;
import ke.co.tunda.Models.GetBoostedparams;
import ke.co.tunda.Models.HeaderItem;
import ke.co.tunda.Models.LikesBody;
import ke.co.tunda.Models.LikesResponse;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Swipe;
import ke.co.tunda.SwipeRecorder.SwipeRecordsParams;
import ke.co.tunda.SwipeRecorder.SwipeResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LikesFragment extends Fragment implements SendLikeSwipeDataInterface {
    @BindView(R.id.recycler_topPicks)
    RecyclerView mTopPicksRecycler;
    @BindView(R.id.loader)
    AVLoadingIndicatorView mLoader;
    @BindView(R.id.connection_failed_layout)
    RelativeLayout mConnectionFailedLayout;
    @BindView(R.id.error_txt)
    TextView mErrorTxt;
    @BindView(R.id.try_again_btn)
    Button mTryAgainButton;
    @BindView(R.id.swipes_finished)
    RelativeLayout mSwipesFinished;
    private View rootView;
    private LikesAdapter adapter;
    private ApiCaller apiCaller;
    private LikesBody[] mBody;
    private SharedPreferences sp;
    private int user_id;
    private String TAG = "TPF";
    private FragmentActivity mActivity;
    private GridLayoutManager mGrid;
    private List<HeaderItem> headerItems;
    private boolean isLastPage = false;
    private int mPage = 1;
    GetBoostedparams params;
    private int likes_number;

    public static int TYPE_SKIP = 1;
    public static int TYPE_LIKE = 2;
    public static int TYPE_SUPERLIKE = 3;
    private AsyncTask<Void, Void, Void> mSendTopPicks;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_likes, container, false);
        FullProfile.updateLikes(new FullProfile.UpdateLikes() {
            @Override
            public void executeSwipeAction(int type, int position) {
                if (type == TYPE_SKIP) {
                    Log.d(TAG, "executeSwipeAction: ");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            skipUser(position);

                        }
                    }, 500);


                    return;
                }

                if (type == TYPE_LIKE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            likeUser(position);

                        }
                    }, 500);

                    return;
                }

                if (type == TYPE_SUPERLIKE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            superLikeUser(position);

                        }
                    }, 500);

                }

            }


        });
        ButterKnife.bind(this, rootView);
        sp = PreferenceManager.getDefaultSharedPreferences(mActivity);
        user_id = sp.getInt("user_id", 0);


        mTryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DefaultRequest request = new DefaultRequest();
                request.setRequestType("GET_USER_LIKES");
                request.setUserId(user_id);
                Call<LikesResponse> mCall2 = apiCaller.getLikes(request);
                new getLikes(mCall2).execute();
            }
        });
        adapter = new LikesAdapter(mActivity, new ArrayList<>(), this, user_id);


//        rmViewModel = ViewModelProviders.of(mActivity).get(RmViewModel.class);

        apiCaller = Builder.getClient().create(ApiCaller.class);
        mGrid = new GridLayoutManager(mActivity, 2);
        mGrid.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                switch (adapter.getItemViewType(position)) {
                    case TopPicksAdapter.LOADING:
                        return 2;
                    case TopPicksAdapter.TOP_PICKS:
                        return 1;
                    default:
                        return 2;
                }

            }
        });


        mTopPicksRecycler.setLayoutManager(mGrid);

        HeaderItem headerItem = new HeaderItem();
        headerItem.setHeader("Your Likes");
        headerItem.setDescription(mActivity.getResources().getString(R.string.top_likes_desc));
        headerItems = new ArrayList<>();
        headerItems.add(headerItem);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mActivity, R.dimen.card_margin_right);
        mTopPicksRecycler.addItemDecoration(itemDecoration);


        DefaultRequest request = new DefaultRequest();
        request.setRequestType("GET_USER_LIKES");
        request.setUserId(user_id);
        Call<LikesResponse> mCall2 = apiCaller.getLikes(request);
        new getLikes(mCall2).execute();


        return rootView;
    }


    @Override
    public void sendLikeSwipes(int user_id, ArrayList<Swipe> mArrayList) {
        SwipeRecordsParams swipeRecordsParams = new SwipeRecordsParams();
        swipeRecordsParams.setRequestType("USER_SWIPE");
        swipeRecordsParams.setUserId(user_id);
        swipeRecordsParams.setArrayList(mArrayList);
        mSendTopPicks = new sendTopPicks(swipeRecordsParams);
        mSendTopPicks.execute();

    }


    public interface onDataLoadedListener {
        Void onDataLoaded(int position);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }


    class ItemOffsetDecoration extends RecyclerView.ItemDecoration {
        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }

    }


    private void skipUser(int position) {
        adapter.skipUser(position);
        Log.d("posin", "skipUser: " + position);


    }

    private void likeUser(int position) {
        adapter.likeUser(position);

    }

    private void superLikeUser(int position) {
        adapter.superLikeUser(position);
    }


    private class sendTopPicks extends AsyncTask<Void, Void, Void> {
        SwipeRecordsParams mSwipeRecordsParams;
        ArrayList<Swipe> mSwipe;

        sendTopPicks(SwipeRecordsParams swipeRecordsParams) {
            this.mSwipeRecordsParams = swipeRecordsParams;
            this.mSwipe = mSwipeRecordsParams != null ? mSwipeRecordsParams.getArrayList() : null;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<SwipeResponse> call = apiCaller.sendSwipeRecords(mSwipeRecordsParams);
            call.enqueue(new Callback<SwipeResponse>() {
                @Override
                public void onResponse(Call<SwipeResponse> call, Response<SwipeResponse> response) {
                    if (response.isSuccessful()) {
                        SwipeResponse swipeResponse = response.body();
                        if (swipeResponse != null && swipeResponse.getStatusCode().equals("00")) {
                            Log.d(TAG, "onResponse: 00");
                            if(isAdded()){
                                Preferences.get(mActivity.getApplicationContext()).setIsTimeToRefreshConnections(true);
                                if (mSwipe != null) {
                                    if (mSwipe.get(0).getSwipe_action().equals("LIKE") || mSwipe.get(0).getSwipe_action().equals("SUPERLIKE")) {
                                        Toasty.success(mActivity.getApplicationContext(), getString(R.string.you_got_a_match), Toasty.LENGTH_SHORT).show();
                                    }
                                }


                                if (adapter.getItemCount() <= 1) {
                                    mTopPicksRecycler.animate().alpha(0.5f);
                                    mTopPicksRecycler.setVisibility(View.GONE);
                                    mSwipesFinished.animate().alpha(1.0f);
                                    mSwipesFinished.setVisibility(View.VISIBLE);
                                }
                            }



                        } else if (swipeResponse != null && swipeResponse.getStatusCode().equals("99")) {
                            Log.d(TAG, "onResponse: 99");
                            if(isAdded()){
                                if (mSwipe != null) {
                                    if (mSwipe.get(0).getSwipe_action().equals("DISLIKE")) {
                                        Toasty.warning(mActivity.getApplicationContext(), getString(R.string.oops_you_missed), Toasty.LENGTH_SHORT).show();
                                    }
                                }


                                Log.d(TAG, "onResponse: 99");
                                if (adapter.getItemCount() <= 1) {
                                    mTopPicksRecycler.animate().alpha(0.5f);
                                    mTopPicksRecycler.setVisibility(View.GONE);
                                    mSwipesFinished.animate().alpha(1.0f);
                                    mSwipesFinished.setVisibility(View.VISIBLE);
                                }
                            }



                        }


                    } else {
                        Log.d(TAG, "onResponse: 78");

                    }
                }

                @Override
                public void onFailure(Call<SwipeResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: 001");


                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSendTopPicks != null && !mSendTopPicks.isCancelled()) {
            mSendTopPicks.cancel(false);
        }

    }

    private class getLikes extends AsyncTask<Void, Void, Void> {
        Call<LikesResponse> call;

        public getLikes(Call<LikesResponse> mCall2) {
            this.call = mCall2;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mConnectionFailedLayout.getVisibility() == View.VISIBLE) {
                        mConnectionFailedLayout.animate().alpha(0.4f);
                        mConnectionFailedLayout.setVisibility(View.GONE);
                    }

                    if (mSwipesFinished.getVisibility() == View.VISIBLE) {
                        mSwipesFinished.animate().alpha(0.5f);
                        mSwipesFinished.setVisibility(View.GONE);
                    }


                    mLoader.animate().alpha(1.0f);
                    mLoader.setVisibility(View.VISIBLE);
                }
            });


            call.enqueue(new Callback<LikesResponse>() {
                @Override
                public void onResponse(Call<LikesResponse> call, Response<LikesResponse> response) {
                    if (response.isSuccessful()) {
//                    adapter.removeLoading();
                        LikesResponse resp = response.body();
                        if (resp != null && resp.getStatusCode().equals("00") && resp.getBody() != null) {
                            mBody = resp.getBody();
                            Log.d(TAG, "onResponse: status=" + resp.getStatusCode() + "body length" + mBody.length);
                            if (mBody.length > 0) {

                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mLoader.animate().alpha(0.5f);
                                        mLoader.setVisibility(View.GONE);
                                    }
                                });


                                mBody = resp.getBody();

                                adapter.addHeader(headerItems);
                                adapter.addAll(mBody);


                                mTopPicksRecycler.setAdapter(adapter);

                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTopPicksRecycler.setVisibility(View.VISIBLE);
                                    }
                                });


//                                adapter.addLoading();


                            }

                        } else if (resp != null && resp.getStatusCode() != null && resp.getStatusCode().equals("99")) {
                            isLastPage = true;
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mLoader.animate().alpha(0.1f);
                                    mLoader.setVisibility(View.GONE);
                                    mConnectionFailedLayout.animate().alpha(1.0f);
                                    mConnectionFailedLayout.setVisibility(View.VISIBLE);
                                    mErrorTxt.setText("No likes available");

                                }
                            });


                        }
                    } else {
                        Log.d(TAG, "onResponse: ");
                    }
                }

                @Override
                public void onFailure(Call<LikesResponse> call, Throwable t) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoader.animate().alpha(0.5f);
                            mLoader.setVisibility(View.GONE);
                            mConnectionFailedLayout.animate().alpha(1.0f);
                            mConnectionFailedLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    if (t.getMessage().contains("Failed to connect")) {
                        mErrorTxt.setText("No Internet Connection");
                    } else {
                        mErrorTxt.setText("Problem Loading Data");
                    }
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }


            });

            return null;
        }
    }
}
