

package ke.co.tunda.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.ApiConnector.Models.GetUsersNearbyResponse;
import ke.co.tunda.Models.LikesResponse;
import ke.co.tunda.R;

public class MainHomeFragment extends Fragment {

    private Context mContext;
    private FragmentActivity mActivity;
    private View mView;
    private Fragment mHomeFrag = new HomeFragment();
    private Fragment mLikesPicksFrag = new LikesPicksMainFragment();

    Fragment active = mHomeFrag;
    private FragmentManager fm;
    @BindView(R.id.frame_container)
    FrameLayout mFragmentLayout;
    private OnHeadlineSelectedListener callback;
    private static final String TAG = "MNHM";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main_home, container, false);
        ButterKnife.bind(this, mView);
        fm = mActivity.getSupportFragmentManager();
        fm.beginTransaction().add(R.id.frame_container, mLikesPicksFrag, "top_picks").hide(mLikesPicksFrag).commit();
        fm.beginTransaction().add(R.id.frame_container, mHomeFrag, "home").commit();


        return mView;
    }

    public void setOnHeadlineSelectedListener(OnHeadlineSelectedListener callback) {
        this.callback = callback;
    }



    public interface OnHeadlineSelectedListener {
        void onArticleSelected(int position);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }

    public void setPosition(int position, boolean isBoostsAvailable, GetUsersNearbyResponse boostResp,
                            boolean isLikesAvailable, LikesResponse likesResp) {
        if (position == 0) {
            fm.beginTransaction().hide(active).show(mHomeFrag).commit();
            active = mHomeFrag;
        } else if(position==1) {

            fm.beginTransaction().hide(active).show(mLikesPicksFrag).commit();
            active = mLikesPicksFrag;
        }


    }





}
