

package ke.co.tunda.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import javax.security.auth.callback.Callback;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.ApiConnector.Models.GetUsersNearbyResponse;
import ke.co.tunda.Models.LikesResponse;
import ke.co.tunda.R;


public class LikesPicksMainFragment extends Fragment {

    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    private View mView;
    private FragmentActivity mActivity;
    public int likes_number;
    public boolean isBoostsAvailable_ = false;
    public boolean isLikesAvable_ = false;

    private GetUsersNearbyResponse getUsersNearbyResponse;
    private LikesResponse likesResponse;

    private final static String TAG = "LIKESPICKS";






    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView =  inflater.inflate(R.layout.fragment_likes_picks_main, container, false);
        SharedPreferences sp  = PreferenceManager.getDefaultSharedPreferences(mActivity);
        likes_number = sp.getInt("likes_number",0);
        ButterKnife.bind(this,mView);
        final TabLayout tabLayout = (TabLayout) mView.findViewById(R.id.tab_layout);
        if(likes_number>0){
            tabLayout.addTab(tabLayout.newTab().setText(mActivity.getResources().getString(R.string.number_of_likes,"")));

        }else {
            tabLayout.addTab(tabLayout.newTab().setText(mActivity.getResources().getString(R.string.number_of_likes,"")));


        }
        tabLayout.addTab(tabLayout.newTab().setText("Top Profiles"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final PagerAdapter adapter = new PageAdapter(mActivity.getSupportFragmentManager(),tabLayout.getTabCount());
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return mView;
    }


    public class PageAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PageAdapter(FragmentManager fm,int numTabs) {
            super(fm);
            this.mNumOfTabs = numTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new LikesFragment();
                case 1:
                    return new TopPicksFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity)context;
    }


}
