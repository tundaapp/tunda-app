

package ke.co.tunda.fragments;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.snackbar.Snackbar;
import com.skyfishjy.library.RippleBackground;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.RewindAnimationSetting;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.Adapters.CardStackAdapter;
import ke.co.tunda.Adapters.ItsAMatchAdapter;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.ApiConnector.Models.GetNearbyUsersParams;
import ke.co.tunda.ApiConnector.Models.GetUsersNearbyResponse;
import ke.co.tunda.Callbacks.ExitMatchView;
import ke.co.tunda.Callbacks.PaginationAdapterCallback;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.ChatKit.ChatKitModels.Message;
import ke.co.tunda.ChatKit.ChatKitModels.User;
import ke.co.tunda.ChatKit.CustomHolderMessagesActivity;

import com.bumptech.glide.Glide;
import ke.co.tunda.Helpers.MyBounceInterpolator;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.SqLite.DatabaseHandler;
import ke.co.tunda.SwipeRecorder.Matches;
import ke.co.tunda.SwipeRecorder.Swipe;
import ke.co.tunda.SwipeRecorder.SwipeRecordsParams;
import ke.co.tunda.SwipeRecorder.SwipeResponse;
import ke.co.tunda.Views.ItsAMatchView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Don on 12/3/19.
 */

public class HomeFragment extends Fragment implements CardStackListener, View.OnClickListener,
        PaginationAdapterCallback, DiscreteScrollView.ScrollStateChangeListener<ItsAMatchAdapter.ViewHolder>,
        DiscreteScrollView.OnItemChangedListener<ItsAMatchAdapter.ViewHolder>, ExitMatchView {

    private Context mContext;
    static Snackbar B;
    private FragmentActivity mActivity;
    DisplayMetrics displayMetrics;
    float widthPixel;

    private CardStackLayoutManager manager;
    private CardStackAdapter adapter;


    private ArrayList<Swipe> swipeArrayList = new ArrayList<>();
    private int recipient_id;
    private Matches[] matches;
    private ItsAMatchAdapter itsAMatchAdapter;


    private View rootView;
    private ApiCaller apiCaller;
    private String no_people_around;
    private int is_last_page;


    SharedPreferences sp;
    private int user_id;
    private String imageProfile;
    private String name, match_imageProfile;
    private static String TAG = "HF";
    Animation animFadeIn, animFadeOut;
    TextView txtFadeIn, txtFadeOut;
    Button btnFadeIn, btnFadeOut;

    @BindView(R.id.centerImage)
    CircleImageView centerImage;
    @BindView(R.id.content)
    RippleBackground content;
    @BindView(R.id.button_container)
    LinearLayout button_container;
    @BindView(R.id.finding_people)
    TextView findind_people;
    @BindView(R.id.try_search_again)
    Button try_search_again;


    @BindView(R.id.forecast_view)
    ItsAMatchView forecastView;
    @BindView(R.id.forecast_city_picker)
    DiscreteScrollView cityPicker;
    @BindView(R.id.activity_weather)
    RelativeLayout frameLayout;
    @BindView(R.id.layoutRest)
    RelativeLayout layoutRest;
    @BindView(R.id.scrollMatches)
    ScrollView mScroller;
    @BindView(R.id.card_stack_view)
    CardStackView cardStackView;
    @BindView(R.id.skip_button)
    ImageButton mSkip;
    @BindView(R.id.super_like_button)
    ImageButton mSuperLike;
    @BindView(R.id.rewind)
    ImageButton mRewind;
    @BindView(R.id.like_button)
    ImageButton mLike;


    private List<GetUsersNearbyResponse> usersNearby;
    private int currentPage = 1;
    private Body[] body;
    private boolean getNext = true;
    DatabaseHandler databaseHandler;

    public static int TYPE_SKIP = 1;
    public static int TYPE_LIKE = 2;
    public static int TYPE_SUPERLIKE = 3;
    private String swipe_action;
    private boolean searchInitiated = false;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        displayMetrics = mActivity.getResources().getDisplayMetrics();
        widthPixel = displayMetrics.widthPixels;

        try {

            FullProfile mProfile = new FullProfile();

            FullProfile.updateApi(new FullProfile.UpdateFragment() {
                @Override
                public void executeSwipeAction(int type, int position) {
                    Log.d(TAG, "executeSwipeAction: ");
                    if (type == TYPE_SKIP) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                skipUser();

                            }
                        }, 500);


                        return;
                    }

                    if (type == TYPE_LIKE) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                likeUser();

                            }
                        }, 500);

                        return;
                    }

                    if (type == TYPE_SUPERLIKE) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                superLikeUser();

                            }
                        }, 500);

                    }
                }


            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ((ViewGroup) rootView.findViewById(R.id.llRoot)).getLayoutTransition()
                        .enableTransitionType(LayoutTransition.CHANGING);
            }
            ButterKnife.bind(this, rootView);

            rootView.findViewById(R.id.rewind).setEnabled(false);


            animFadeIn = AnimationUtils.loadAnimation(mActivity,
                    R.anim.fade_in);
            animFadeOut = AnimationUtils.loadAnimation(mActivity,
                    R.anim.fade_out);

            databaseHandler = new DatabaseHandler(mActivity);
            sp = PreferenceManager.getDefaultSharedPreferences(mActivity);
            imageProfile = sp.getString("image", "");
            name = sp.getString("first_name", "");
            match_imageProfile = sp.getString("image", "");
            user_id = sp.getInt("user_id", 0);
            apiCaller = Builder.getClient().create(ApiCaller.class);
            usersNearby = new ArrayList<>();

            mLike.setOnClickListener(this);
            mSuperLike.setOnClickListener(this);
            mRewind.setOnClickListener(this);
            mSkip.setOnClickListener(this);

            Glide.with(mActivity).load(imageProfile).into(centerImage);


            itsAMatchAdapter = new ItsAMatchAdapter(mActivity);


            cityPicker.setSlideOnFling(true);
            cityPicker.setAdapter(itsAMatchAdapter);
            cityPicker.addOnItemChangedListener(this);
            cityPicker.addScrollStateChangeListener(this);
            cityPicker.setItemTransformer(new ScaleTransformer.Builder()
                    .setMinScale(0.8f)
                    .build());
            setupCardStackView();


            checkUnsendSwipes();


            try_search_again.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try_search_again.setVisibility(View.GONE);
                    checkUnsendSwipes();
                }
            });


            centerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkUnsendSwipes();
                }
            });


        } catch (Exception e) {
            Log.d(TAG, "onCreateView: " + e.getMessage());
        }


        return rootView;
    }

    private void checkUnsendSwipes() {

        updateView(true, "Finding people around you..", false);


        if (null == swipeArrayList) {
            swipeArrayList = new ArrayList<Swipe>();
        }

        List<Swipe> swipesList = databaseHandler.getAllSwipes();
        for (int i = 0; i < swipesList.size(); ++i) {
            Swipe swipe = new Swipe(swipesList.get(i).getRecipient_id(), swipesList.get(i).getSwipe_action());
            swipeArrayList.add(swipe);
        }

        if (swipeArrayList.size() > 0) {
            databaseHandler.deleteAllSwipes();
            new SendSwipeData(swipeArrayList).execute();
            delaySearch();

        } else {
            databaseHandler.deleteAllSwipes();
            startSearch();
        }

    }

    private void startSearch() {


        GetNearbyUsersParams params = new GetNearbyUsersParams();
        params.setPage(currentPage);
        params.setRequestType("GET_NEARBY_USERS");
        params.setUserId(user_id);
        Call<GetUsersNearbyResponse> call = apiCaller.getNearbyUsers(params);
        call.enqueue(new Callback<GetUsersNearbyResponse>() {
            @Override
            public void onResponse(Call<GetUsersNearbyResponse> call, Response<GetUsersNearbyResponse> response) {
                if (response.isSuccessful()) {
                    GetUsersNearbyResponse getUsersNearbyResponse = response.body();
                    if (getUsersNearbyResponse != null) {
                        if (getUsersNearbyResponse.getStatusCode().equals("00")) {
                            Preferences.get(mActivity).setDiscoveryChanged(false);

                            updateView(false, "", false);

                            body = fetchResults(response);

                            adapter.addAll(body);

                        } else if (getUsersNearbyResponse.getStatusCode().equals("99")) {
                            Preferences.get(mActivity).setDiscoveryChanged(false);
                            no_people_around = "no one new around...\nTry updating " +
                                    "discovery settings and retry";
                            updateView(false, no_people_around, true);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetUsersNearbyResponse> call, Throwable t) {

                switch (t.getMessage()) {
                    case "Failed to connect to /207.180.212.198:3333":
                        no_people_around = "No Internet Connection";
                        break;
                    default:
                        no_people_around = "Something went wrong,try again";
                        break;
                }


                updateView(false, no_people_around, true);

            }
        });

    }


    private void loadNext() {
        Glide.with(mActivity).load(imageProfile).into(centerImage);
        GetNearbyUsersParams params = new GetNearbyUsersParams();
        params.setPage(currentPage);
        params.setRequestType("GET_NEARBY_USERS");
        params.setUserId(user_id);
        Call<GetUsersNearbyResponse> call = apiCaller.getNearbyUsers(params);
        call.enqueue(new Callback<GetUsersNearbyResponse>() {
            @Override
            public void onResponse(Call<GetUsersNearbyResponse> call, Response<GetUsersNearbyResponse> response) {
                if (response.isSuccessful()) {
                    GetUsersNearbyResponse getUsersNearbyResponse = response.body();
                    if (getUsersNearbyResponse != null) {
                        if (getUsersNearbyResponse.getStatusCode().equals("00")) {
                            adapter.clear();
                            updateView(false, "", false);
                            body = fetchResults(response);
                            adapter.addAll(body);

                        } else if (getUsersNearbyResponse.getStatusCode().equals("99")) {
                            no_people_around = "There's no one new around....";
                            updateView(false, no_people_around, true);
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<GetUsersNearbyResponse> call, Throwable t) {
//                Toast.makeText(mActivity, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("TESTING", t.getMessage());
                if (t.getMessage().contains("Failed to connect to /207.180.212.198:3333")) {
                    no_people_around = "No Internet Connection";
                }  else {
                    no_people_around = "Something went wrong,try again";

                }

                adapter.showRetry(true, no_people_around);

                updateView(false, no_people_around, true);

            }
        });


    }


    private RelativeLayout getMainProfile() {
        return mActivity.findViewById(R.id.profile);
    }

    private RelativeLayout getMainHome() {
        return mActivity.findViewById(R.id.home);
    }

    private RelativeLayout getMainChats() {
        return mActivity.findViewById(R.id.chats);
    }


    private void updateView(boolean loading, String status, boolean showRetry) {
        if (loading) {
            content.setVisibility(View.VISIBLE);
            content.startRippleAnimation();
            findind_people.setText(status);
            try_search_again.setVisibility(View.GONE);
            button_container.setVisibility(View.GONE);
            cardStackView.setVisibility(View.GONE);


        } else {

            if (showRetry) {
                content.stopRippleAnimation();
                content.setVisibility(View.VISIBLE);
                try_search_again.setVisibility(View.VISIBLE);
                findind_people.setText(status);
                cardStackView.setVisibility(View.GONE);
                button_container.setVisibility(View.GONE);
            } else {
                content.stopRippleAnimation();
                content.setVisibility(View.GONE);
                try_search_again.setVisibility(View.GONE);
                cardStackView.setVisibility(View.VISIBLE);
                button_container.setVisibility(View.VISIBLE);
            }


        }
    }


    private void updateStatusView(boolean show, String status) {
        if (show) {
            findind_people.setVisibility(View.VISIBLE);
            findind_people.setText(status);
            try_search_again.setVisibility(View.VISIBLE);
        } else {
            findind_people.setVisibility(View.GONE);
            try_search_again.setVisibility(View.GONE);
        }


    }


    private void likeUser() {
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Right)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .build();
        manager.setSwipeAnimationSetting(setting);
        cardStackView.swipe();
    }

    private void superLikeUser() {
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Top)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .build();
        manager.setSwipeAnimationSetting(setting);
        cardStackView.swipe();
    }

    private void skipUser() {

        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(Direction.Left)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .build();
        manager.setSwipeAnimationSetting(setting);
        cardStackView.swipe();
    }

    private void setupCardStackView() {
        manager = new CardStackLayoutManager(mActivity, this);
        adapter = new CardStackAdapter(mActivity, this,widthPixel);
        initialize();
    }

    private void initialize() {
        manager.setStackFrom(StackFrom.None);
        manager.setVisibleCount(3);
        manager.setTranslationInterval(8.0f);
        manager.setScaleInterval(0.95f);
        manager.setSwipeThreshold(0.3f);
        manager.setMaxDegree(20.0f);
        manager.setDirections(Direction.FREEDOM);
        manager.setCanScrollHorizontal(true);
        manager.setCanScrollVertical(true);
        cardStackView.setLayoutManager(manager);
        cardStackView.setAdapter(adapter);
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setSupportsChangeAnimations(true);
        cardStackView.setItemAnimator(defaultItemAnimator);

    }

    private void setupNavigation() {
    }


    @Override
    public void onCardDragging(Direction direction, float ratio) {
        manager.setCanScrollVertical(true);
        if (direction == Direction.Bottom) {
            manager.setCanScrollVertical(false);
        } else if (direction == Direction.Top) {
            manager.setCanScrollVertical(true);
        }
    }


    private void delay() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadNext();
            }
        }, 1000);

    }

    private void delaySearch() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startSearch();
            }
        }, 1500);

    }


    public void addTask(Swipe t) {

        if (!databaseHandler.SwipeExists(String.valueOf(t.getRecipient_id()))) {
            databaseHandler.adSwipes(t);
        }
    }

    private void loadMatches(Matches match, FragmentActivity mActivity) {
        forecastView.setForecast(match, mActivity, this);

    }


    public void switchLayouts(int top) {
        if (top == 1) {
            mScroller.animate().alpha(0.1f);
            mScroller.setVisibility(View.GONE);

            frameLayout.animate().alpha(0.1f);
            frameLayout.setVisibility(View.GONE);

            layoutRest.animate().alpha(1.0f);
            layoutRest.setVisibility(View.VISIBLE);


        } else {
            mScroller.animate().alpha(1.0f);
            mScroller.setVisibility(View.VISIBLE);


            layoutRest.animate().alpha(0.1f);
            layoutRest.setVisibility(View.GONE);

            frameLayout.animate().alpha(1.0f);
            frameLayout.setVisibility(View.VISIBLE);


        }
    }


    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {

    }

    @Override
    public void onCardAppeared(View view, int position) {

        if (adapter.getItemCount() > 0) {
            recipient_id = adapter.getRecipient_id(position) > 0 ? adapter.getRecipient_id(position) : 0;

        }

    }

    @Override
    public void onCardSwiped(Direction direction) {

        if (swipeArrayList.size() > 0) {
            swipeArrayList.clear();
        }


        if (!rootView.findViewById(R.id.rewind).isEnabled()) {
//            rootView.findViewById(R.id.rewind).animate().alpha(1.0f);
            rootView.findViewById(R.id.rewind).setEnabled(true);
        }


        swipe_action = "";


        switch (direction.toString()) {
            case "Right":
                swipe_action = "LIKE";
                break;
            case "Left":
                swipe_action = "DISLIKE";
                break;
            case "Top":
                swipe_action = "SUPERLIKE";
                break;
        }

        if (recipient_id > 0) {
            swipeArrayList.add(new Swipe(recipient_id, swipe_action));
            if (swipeArrayList.size() > 0) {
                new SendSwipeData(swipeArrayList).execute();
//                sendSwipeRecords();
            }


        }


        if (adapter.getItemCount() < 5) {
            if (manager.getTopPosition() == adapter.getItemCount()) {
                updateView(true, "Finding people around you", false);
                currentPage++;
                delay();
            }
        } else if (manager.getTopPosition() == adapter.getItemCount()) {

            updateView(true, "Finding people around you", false);

            currentPage++;

            delay();
        }


    }

    @Override
    public void onCardDisappeared(View view, int position) {

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.skip_button: {
                didTapButton(rootView.findViewById(R.id.skip_button));
                skipUser();
                break;
            }
            case R.id.rewind: {
                didTapButton(rootView.findViewById(R.id.rewind));
                RewindAnimationSetting setting = new RewindAnimationSetting.Builder()
                        .setDirection(Direction.Bottom)
                        .setDuration(200)
                        .setInterpolator(new DecelerateInterpolator())
                        .build();
                manager.setRewindAnimationSetting(setting);
                cardStackView.rewind();
                break;
            }
            case R.id.like_button: {
                didTapButton(rootView.findViewById(R.id.like_button));
                likeUser();
                ;
                break;
            }
            case R.id.super_like_button: {
                didTapButton(rootView.findViewById(R.id.like_button));
                superLikeUser();
                break;
            }


        }


    }


    private Body[] fetchResults(Response<GetUsersNearbyResponse> response) {
        if (response.isSuccessful()) {
            GetUsersNearbyResponse baseResponse = response.body();
            if (baseResponse != null) {
                return baseResponse.getBody();
            }
        } else
            Toast.makeText(mActivity, "Not Fetched", Toast.LENGTH_SHORT).show();
        return null;


    }

    private Matches[] fetchSwipeResults(Response<SwipeResponse> response) {
        if (response.isSuccessful()) {
            SwipeResponse baseResponse = response.body();
            if (baseResponse != null) {
                return baseResponse.getBody();
            }
        } else
            Toast.makeText(mActivity, "Not Fetched", Toast.LENGTH_SHORT).show();
        return null;


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;

    }

    @Override
    public void retryPageLoad() {
        loadNext();
    }


    @Override
    public void onCurrentItemChanged(@Nullable ItsAMatchAdapter.ViewHolder holder, int position) {
        if (holder != null) {
            forecastView.setForecast(matches[position], mActivity, this);
            holder.showText();
        }

    }

    @Override
    public void onScrollStart(@NonNull ItsAMatchAdapter.ViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();

    }

    @Override
    public void onScrollEnd(@NonNull ItsAMatchAdapter.ViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float position,
                         int currentIndex, int newIndex,
                         @Nullable ItsAMatchAdapter.ViewHolder currentHolder,
                         @Nullable ItsAMatchAdapter.ViewHolder newHolder) {


    }


    @Override
    public void keepSwiping() {

        switchLayouts(1);

    }

    @Override
    public void sendMessage(Matches matches) {

        try {
            int match_id = matches.getMatch_id();
            String dob = matches.getDob();
            String distance = matches.getDistance();
            String bio = matches.getBio();
            String job = matches.getJob();
            String education = matches.getSchool();
            String interest = matches.getInterest();


            String avatar = matches.getPhotoPath();
            String match_name = matches.getFullName();
            int my_match_id = matches.getRecipient_id();
            ArrayList<User> users = new ArrayList<>();
            User Me = new User(
                    // dialogueObject.getString("staff_id"),
                    String.valueOf(user_id),
                    name,
                    match_imageProfile,
                    true);

            User myUser = new User(
                    //dialogueObject.getString("customer_id"),
                    String.valueOf(match_id),
                    match_name,
                    avatar,
                    true);
            int unreadMsgCount = 0;
            int photo_count = matches.getPhoto_count();

            users.add(Me);
            users.add(myUser);

            boolean has_insta_photo = matches.getHas_insta_photo()!=null&&matches.getHas_insta_photo()==1;

            String last_message = null;
            Message message = null;
            Dialog dialog = new Dialog(String.valueOf(match_id), match_name,
                    avatar, users, null,
                    unreadMsgCount, String.valueOf(my_match_id), dob, distance, bio, job, education, photo_count,
                    interest, matches.getVerification_status(), matches.getLikes_count(),has_insta_photo);
            CustomHolderMessagesActivity.open(mActivity, dialog, 1, null);

        } catch (Exception e) {
            Log.d("TEST", e.getMessage());
        }


    }


    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: ");
    }


    private class SendSwipeData extends AsyncTask<Void, Void, Void> {
        ArrayList<Swipe> mSwipeArrayList;

        public SendSwipeData(ArrayList<Swipe> swipeArrayList) {
            this.mSwipeArrayList = swipeArrayList;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            SwipeRecordsParams swipeRecordsParams = new SwipeRecordsParams();
            swipeRecordsParams.setRequestType("USER_SWIPE");
            swipeRecordsParams.setUserId(user_id);
            swipeRecordsParams.setArrayList(mSwipeArrayList);
            Call<SwipeResponse> call = apiCaller.sendSwipeRecords(swipeRecordsParams);
            call.enqueue(new Callback<SwipeResponse>() {
                @Override
                public void onResponse(Call<SwipeResponse> call, Response<SwipeResponse> response) {
                    if (response.isSuccessful()) {
                        SwipeResponse swipeResponse = response.body();
                        if (swipeResponse != null) {
                            if (swipeResponse.getStatusCode().equals("00")) {
                                matches = null;
                                swipeArrayList.clear();
                                itsAMatchAdapter.clear();
                                matches = fetchSwipeResults(response);
                                itsAMatchAdapter.addAll(matches);

                                loadMatches(matches[0], mActivity);


                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        switchLayouts(2);
                                    }
                                });

                            } else if (swipeResponse.getStatusCode().equals("99")) {
                                swipeArrayList.clear();
                                itsAMatchAdapter.clear();

                                Log.d(TAG, "onResponse: 99");

                            } else {


                                for (int i = 0; i < swipeArrayList.size(); ++i) {
                                    addTask(new Swipe(swipeArrayList.get(i).getRecipient_id(), swipeArrayList.get(i).getSwipe_action()));
                                }
                            }
                        }


                    } else {
                        for (int i = 0; i < swipeArrayList.size(); ++i) {
                            addTask(new Swipe(swipeArrayList.get(i).getRecipient_id(), swipeArrayList.get(i).getSwipe_action()));
                        }
                    }
                }

                @Override
                public void onFailure(Call<SwipeResponse> call, Throwable t) {

                    for (int i = 0; i < swipeArrayList.size(); ++i) {

                        addTask(new Swipe(swipeArrayList.get(i).getRecipient_id(), swipeArrayList.get(i).getSwipe_action()));
                    }


                }
            });

            return null;
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            Log.d(TAG, "setMenuVisibility: Home");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        if (Preferences.get(mActivity).isDicoveryChanged()) {
            Log.d(TAG, "onCreateView: Hm");
            if (adapter.getItemCount() > 0) {
                adapter.clear();
            }

            currentPage = 1;
            checkUnsendSwipes();


        }

    }

    private void didTapButton(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
//        view.startAnimation(myAnim);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        view.startAnimation(myAnim);
    }


}