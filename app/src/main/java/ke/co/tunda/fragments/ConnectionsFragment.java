/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/22/19 6:19 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.fragments;

import android.app.ActivityOptions;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stfalcon.chatkit.commons.ImageLoader;
import com.wang.avi.AVLoadingIndicatorView;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.Adapters.DialogsAdapter;
import ke.co.tunda.Adapters.MyMatchesAdapter;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.ChatInfo;
import ke.co.tunda.ApiConnector.Models.GetUserChatGroupParams;
import ke.co.tunda.ApiConnector.Models.GetUserChatGroupsResponse;
import ke.co.tunda.Callbacks.OpenMatchCallback;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.ChatKit.ChatKitModels.Message;
import ke.co.tunda.ChatKit.ChatKitModels.User;
import ke.co.tunda.ChatKit.CustomHolderMessagesActivity;
import ke.co.tunda.Models.MyMatches;
import ke.co.tunda.Models.MyMatchesParams;
import ke.co.tunda.Models.MyMatchesResponse;
import ke.co.tunda.Models.RequestViewStatus;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.Room.ChatDialogsTable;
import ke.co.tunda.Room.DatabaseRoom;
import ke.co.tunda.Room.TundaRoomDao;
import ke.co.tunda.Room.TundaaRoomViewModel;
import ke.co.tunda.SqLite.DatabaseHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ConnectionsFragment extends Fragment implements OpenMatchCallback, DiscreteScrollView.ScrollStateChangeListener<MyMatchesAdapter.ItsAMatchHolder>,
        DiscreteScrollView.OnItemChangedListener<MyMatchesAdapter.ItsAMatchHolder>, View.OnClickListener, View.OnLongClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "CN";

    //    matchViews
    @BindView(R.id.no_matches_layout)
    RelativeLayout no_matches_found_layout;
    @BindView(R.id.recyclerview_my_matches)
    RecyclerView recyclerView_my_matches;
    @BindView(R.id.matches_relative_container)
    RelativeLayout matches_relative_layout;
    @BindView(R.id.newton_cradle_loading)
    AVLoadingIndicatorView newtonCradleLoading;
//    ChatViews

    @BindView(R.id.loading_user_dialogs)
    AVLoadingIndicatorView mLoading_user_dialogs;
    @BindView(R.id.no_dialogs)
    TextView mNoDialogsTextView;
    @BindView(R.id.MyDialogsHolder)
    RelativeLayout mDialogsHolder;
    @BindView(R.id.dialogRecycler)
    RecyclerView mDialogRecycler;
    @BindView(R.id.match_count)
    TextView mMatchCount;
    @BindView(R.id.mychatsHeader)
    TextView mychatsHeader;
    @BindView(R.id.my_matches_header)
    TextView mMatchesHeader;
    @BindView(R.id.no_chats_img)
    ImageView mNoChatsImage;

    //sharedPrefs
    private int user_id;
    private String name;
    private String user_photo;
    private ApiCaller apiCaller;
    MyMatches[] myMatches;
    private MyMatchesAdapter adapter;
    DatabaseHandler databaseHandler;
    private FragmentActivity mActivity;
    private GetUserChatGroupParams getUserChatGroupParams;


    private DialogsAdapter dialogsAdapter;

    private TundaRoomDao mTundaRooomDao;
    private TundaaRoomViewModel mTundaRoomViewModel;


    private ChatInfo[] body;
    private String match_id;
    ArrayList<Dialog> chats = new ArrayList<>();
    private ImageLoader imageLoader;

    private static ConnectionsFragment mInstance;

    public static ConnectionsFragment newInstance() {
        mInstance = new ConnectionsFragment();
        return mInstance;
    }

    public static ConnectionsFragment getInstance() {
        return mInstance;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInstance = this;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connections, container, false);
        ButterKnife.bind(this, view);
//        mInstance = this;

        mTundaRoomViewModel = ViewModelProviders.of(this).get(TundaaRoomViewModel.class);
        mTundaRooomDao = DatabaseRoom.getDatabase(mActivity).tundaRoomDao();

        newtonCradleLoading.setVisibility(View.VISIBLE);
        newtonCradleLoading.show();

        apiCaller = Builder.getClient().create(ApiCaller.class);
        adapter = new MyMatchesAdapter(mActivity, this);
        databaseHandler = new DatabaseHandler(mActivity);
        dialogsAdapter = new DialogsAdapter(mActivity);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mDialogRecycler.setLayoutManager(mLayoutManager);
        mDialogRecycler.setAdapter(dialogsAdapter);
        mDialogRecycler.setHasFixedSize(true);
        mDialogRecycler.setNestedScrollingEnabled(false);


        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivity);
        user_id = sp.getInt("user_id", 0);
        name = sp.getString("first_name", "");
        user_photo = sp.getString("image", "https://i0.wp.com/www.winhelponline.com" +
                "/blog/wp-content/uploads" +
                "/2017/12/user.png?fit=256%2C256&quality=100&ssl=1");

        getUserChatGroupParams = new GetUserChatGroupParams();
        getUserChatGroupParams.setRequestType("GET_USER_CHATGROUPS");
        getUserChatGroupParams.setUserId(String.valueOf(user_id));


        LinearLayoutManager layoutManager
                = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);

        recyclerView_my_matches.setLayoutManager(layoutManager);
        recyclerView_my_matches.setHasFixedSize(true);
        recyclerView_my_matches.setAdapter(adapter);

//        loadDialogsFromDb(getDialogsFromDb());
        loadFromDb(getMatchesFromDb());

        mTundaRoomViewModel.getChatDialogsLiveData().observe(this, new Observer<List<ChatDialogsTable>>() {
            @Override
            public void onChanged(List<ChatDialogsTable> chatDialogsTables) {
                if (chatDialogsTables != null && chatDialogsTables.size() > 0) {
                    mLoading_user_dialogs.hide();
                    mLoading_user_dialogs.setVisibility(View.GONE);
                    mNoChatsImage.setVisibility(View.GONE);
                    mDialogRecycler.setVisibility(View.VISIBLE);
                    mNoDialogsTextView.setVisibility(View.GONE);
                    dialogsAdapter.clear();
                    dialogsAdapter.addAll(chatDialogsTables);
                    mychatsHeader.setVisibility(View.VISIBLE);
                    Preferences.get(mActivity).setIsTimeToRefreshCDialogss(false);
                    Preferences.get(mActivity).setIsTimeToRefreshConnections(false);

                } else {
                    mLoading_user_dialogs.hide();
                    mLoading_user_dialogs.setVisibility(View.GONE);
                    mNoChatsImage.setVisibility(View.VISIBLE);
                    mDialogRecycler.setVisibility(View.GONE);
                    mNoDialogsTextView.setVisibility(View.VISIBLE);
                    mychatsHeader.setVisibility(View.GONE);
                }
            }
        });


        new getMyMatches().execute();
        new getUserDialogs().execute();


        return view;
    }


    private ChatInfo[] fetchDialogResults(Response<GetUserChatGroupsResponse> response) {
        GetUserChatGroupsResponse response1 = response.body();
        if (response1 != null) {
            return response1.getBody();
        }
        return null;

    }


    private void loadFromDb(MyMatches[] matchesFromDb) {
        if (matchesFromDb == null) {
            Log.d("TESTING", "loadFromDb: " + 0);
            newtonCradleLoading.hide();
            newtonCradleLoading.setVisibility(View.GONE);
            recyclerView_my_matches.setVisibility(View.GONE);
            no_matches_found_layout.setVisibility(View.VISIBLE);
            mMatchesHeader.setVisibility(View.GONE);


        } else {
            recyclerView_my_matches.setVisibility(View.VISIBLE);
            Log.d("TESTING", "loadFromDb: " + 1);
            recyclerView_my_matches.setVisibility(View.VISIBLE);
            no_matches_found_layout.setVisibility(View.GONE);
            newtonCradleLoading.hide();
            newtonCradleLoading.setVisibility(View.GONE);
            mMatchCount.setVisibility(View.VISIBLE);
            mMatchCount.setText(String.valueOf(myMatches.length));
            mMatchesHeader.setVisibility(View.VISIBLE);
            adapter.clear();
            adapter.addAll(matchesFromDb);
            Preferences.get(mActivity).setIsTimeToRefreshCDialogss(false);
            Preferences.get(mActivity).setIsTimeToRefreshConnections(false);
        }


    }

    private void saveToMemory(MyMatches[] myMatches) {
        Log.d("DATABASE", "saveToMemory: void");

        for (MyMatches myMatch : myMatches) {
            Log.d("DATABASE", "Match exists= " + databaseHandler.Exists(String.valueOf(myMatch.getUser_id())));
            if (!databaseHandler.Exists(String.valueOf(myMatch.getUser_id()))) {

                Log.d("DATABASE", "saveToMemory: ");
                databaseHandler.addMatches(myMatch);
            }

        }
    }

    private MyMatches[] getMatchesFromDb() {
        List<MyMatches> myMatchesList = databaseHandler.getAllMatches();
        for (int i = 0; i < myMatchesList.size(); i++) {
            myMatches = myMatchesList.toArray(new MyMatches[0]);
        }


        return myMatches;

    }

    private MyMatches[] fetchResults(Response<MyMatchesResponse> response) {
        MyMatchesResponse base = response.body();
        if (base != null) {
            return base.getBody();
        } else {
            return null;
        }

    }

    private void deleteMatches() {
        databaseHandler.deleteFavourite(myMatches[0]);

    }


//    private void loadDialogsFromDb(ChatInfo[] dialogsFromDb) {
//        if (dialogsFromDb == null) {
//
//            mLoading_user_dialogs.hide();
//            mLoading_user_dialogs.setVisibility(View.GONE);
//            mNoChatsImage.setVisibility(View.VISIBLE);
//            mDialogRecycler.setVisibility(View.GONE);
//            mNoDialogsTextView.setVisibility(View.VISIBLE);
//            mychatsHeader.setVisibility(View.GONE);
//
//
//        } else {
//
//            mLoading_user_dialogs.hide();
//            mLoading_user_dialogs.setVisibility(View.GONE);
//            mDialogRecycler.setVisibility(View.VISIBLE);
////            mDialogList.setVisibility(View.VISIBLE);
//            mNoDialogsTextView.setVisibility(View.GONE);
////            mapItems(dialogsFromDb);
//            dialogsAdapter.clear();
//            dialogsAdapter.addAll(body);
//            mychatsHeader.setVisibility(View.VISIBLE);
//            Preferences.get(mActivity).setIsTimeToRefreshCDialogss(false);
//            Preferences.get(mActivity).setIsTimeToRefreshConnections(false);
//        }
//    }

    private ChatInfo[] getDialogsFromDb() {
        List<ChatInfo> chatInfoList = databaseHandler.getAllDialogs();
        for (int i = 0; i < chatInfoList.size(); i++) {
            body = chatInfoList.toArray(new ChatInfo[0]);
        }


        return body;
    }

    private void saveToMemoryDIalogs(ChatInfo[] body) {


        for (ChatInfo chatInfo : body) {
            if (!databaseHandler.Exists_DIALOG(String.valueOf(chatInfo.getRecipientId()))) {

                databaseHandler.addDialogs(chatInfo);
            }

        }

    }


    private Date getCalender(String lastChatDate) {
        Calendar calendar = Calendar.getInstance();

        String YEAR = lastChatDate.substring(0, 4);
        String MONTH = lastChatDate.substring(5, 7);
        String DAY = lastChatDate.substring(8, 10);
        String HOUR = lastChatDate.substring(11, 13);
        String MINUTE = lastChatDate.substring(14, 16);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));
        calendar.add(Calendar.MINUTE, Integer.valueOf(MINUTE));
        calendar.add(Calendar.HOUR, Integer.valueOf(HOUR));

        return calendar.getTime();
    }


    public void openMatch(MyMatches myMatches, View imageView) {

        if (myMatches.getViewStatus() == 0) {
            Preferences.get(mActivity).setIsTimeToRefreshConnections(true);
            new matchViewed(myMatches.getMatch_id()).execute();
        }

        int match_id = myMatches.getMatch_id();
        String dob = myMatches.getDob();
        String location = myMatches.getDistance();
        String bio = myMatches.getBio();
        String job = myMatches.getJob();
        String education = myMatches.getSchool();
        String interest = myMatches.getInterest();

        Log.d("OPEN", "openMatch: interest" + interest);

        String avatar = myMatches.getPhotoPath();
        String match_name = myMatches.getFullName();
        int my_match_id = myMatches.getUser_id();
        ArrayList<User> users = new ArrayList<>();
        User Me = new User(
                // dialogueObject.getString("staff_id"),
                String.valueOf(user_id),
                name,
                user_photo,
                true);

        User myUser = new User(
                //dialogueObject.getString("customer_id"),
                String.valueOf(myMatches.getUser_id()),
                myMatches.getFullName(),
                myMatches.getPhotoPath(),
                true);
        int unreadMsgCount = 0;
        int photo_count = myMatches.getPhoto_count();

        users.add(Me);
        users.add(myUser);
        boolean has_insta_photo = false;

        if(myMatches.getHas_insta_photo()!=null&&myMatches.getHas_insta_photo()==1){
            has_insta_photo = true;

        }else {
            has_insta_photo = false;
        }
        String last_message = null;
        Message message = null;
        Dialog dialog = new Dialog(String.valueOf(match_id), match_name,
                avatar, users, message,
                unreadMsgCount, String.valueOf(myMatches.getUser_id()), dob,
                location, bio, job, education, photo_count,
                interest, myMatches.getVerification_status(), myMatches.getLikes_count(),has_insta_photo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(mActivity, imageView, "match_photo");
//            startActivity(intent, options.toBundle());
            CustomHolderMessagesActivity.open(mActivity, dialog, 2, options);
        } else {
            CustomHolderMessagesActivity.open(mActivity, dialog, 2, null);
        }


    }


    @Override
    public void keepSwiping() {

    }


    @Override
    public void onCurrentItemChanged(@Nullable MyMatchesAdapter.ItsAMatchHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
//            viewHolder.showText();
        }

    }

    @Override
    public void onScrollStart(@NonNull MyMatchesAdapter.ItsAMatchHolder currentItemHolder, int adapterPosition) {
//        currentItemHolder.hideText();

    }

    @Override
    public void onScrollEnd(@NonNull MyMatchesAdapter.ItsAMatchHolder currentItemHolder, int adapterPosition) {

    }


    @Override
    public void onScroll(float scrollPosition,
                         int currentPosition,
                         int newPosition,
                         @Nullable MyMatchesAdapter.ItsAMatchHolder currentHolder,
                         @Nullable MyMatchesAdapter.ItsAMatchHolder newCurrent) {
        if (currentPosition >= 0 && newPosition < adapter.getItemCount()) {
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
//        if (id == R.id.dialogsList) {
////            mActivity.startActivity(new Intent(mActivity, CustomHolderMessagesActivity.class));
//
//        }

    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }


    private class matchViewed extends AsyncTask<Void, Void, Void> {
        int match_id;

        public matchViewed(int match_id) {
            this.match_id = match_id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            RequestViewStatus requestViewStatus = new RequestViewStatus();
            requestViewStatus.setMatchId(String.valueOf(match_id));
            requestViewStatus.setRequestType("UPDATE_VIEW_STATUS");
            Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.sendViewStatus(requestViewStatus);
            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                @Override
                public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                    if (response.isSuccessful()) {
                        ke.co.tunda.ApiConnector.Models.Response response1 = response.body();
                        if (response1 != null && response1.getBody() != null) {
                            if (response1.getBody().equals("SUCCESS") && response1.getStatusCode().equals("00")) {
                                ContentValues cv = new ContentValues();
                                if (user_id > 0) {

                                    try {
                                        String strSQL = "UPDATE matches SET view_status = 1 WHERE user_id = " + user_id;
                                        databaseHandler.getReadableDatabase().execSQL(strSQL);

                                    } catch (Exception e) {
                                        Log.d(TAG, "onResponse: " + e.getMessage());
                                    }


                                }


                            }
                        } else {
                            Log.d(TAG, "onResponse: ");
                        }
                    }
                }

                @Override
                public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: ");

                }
            });
            return null;
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach: HOMEFragment");
        super.onAttach(context);
        mActivity = (FragmentActivity) context;

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: resuming");

        if (Preferences.get(mActivity.getApplicationContext()).isToRefreshConnections("conn")) {

            refresh();
        }

        if (Preferences.get(mActivity.getApplicationContext()).isToRefreshDialogs("conn")) {
            refreshDialogs();
        }


    }

    private void refresh() {
        Log.d(TAG, "refreshing: ");

        new getMyMatches().execute();

    }

    private void refreshDialogs() {

        new getUserDialogs().execute();

    }

    private class getMyMatches extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            MyMatchesParams myMatchesParams = new MyMatchesParams();
            myMatchesParams.setRequestType("GET_USER_MATCHES");
            myMatchesParams.setUserId(user_id);
            Call<MyMatchesResponse> call = apiCaller.getMatches(myMatchesParams);
            call.enqueue(new Callback<MyMatchesResponse>() {
                @Override
                public void onResponse(Call<MyMatchesResponse> call, Response<MyMatchesResponse> response) {
                    if (response.isSuccessful()) {
                        MyMatchesResponse myMatchesResponse = response.body();
                        if (myMatchesResponse != null) {
                            if (myMatchesResponse.getStatusCode().equals("00")) {
                                adapter.clear();
                                myMatches = fetchResults(response);
                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        recyclerView_my_matches.setVisibility(View.VISIBLE);
                                        newtonCradleLoading.hide();
                                        newtonCradleLoading.setVisibility(View.GONE);
                                        no_matches_found_layout.setVisibility(View.GONE);
                                        mMatchCount.setVisibility(View.VISIBLE);
                                        mMatchCount.setText(String.valueOf(myMatches.length));
                                        mMatchesHeader.setVisibility(View.VISIBLE);
                                    }
                                });


                                adapter.addAll(myMatches);

                                databaseHandler.deleteAllMactches();

                                saveToMemory(myMatches);
                                if (myMatches.length < 1) {
//
                                    mActivity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            newtonCradleLoading.hide();
                                            newtonCradleLoading.setVisibility(View.GONE);
                                            recyclerView_my_matches.setVisibility(View.GONE);
                                            no_matches_found_layout.setVisibility(View.VISIBLE);
                                            mMatchesHeader.setVisibility(View.GONE);
                                        }
                                    });
//
                                }

                                Preferences.get(mActivity).setIsTimeToRefreshConnections(false);

                            } else if (myMatchesResponse.getStatusCode().equals("99")) {
                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        newtonCradleLoading.hide();
                                        newtonCradleLoading.setVisibility(View.GONE);
                                        recyclerView_my_matches.setVisibility(View.GONE);
                                        no_matches_found_layout.setVisibility(View.VISIBLE);
                                        mMatchesHeader.setVisibility(View.GONE);
                                    }
                                });

                                databaseHandler.deleteAllMactches();


                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<MyMatchesResponse> call, Throwable t) {

                    Log.d("DATABASE", "getMatchesFromDb: ");
                    loadFromDb(getMatchesFromDb());


                }
            });
            return null;
        }
    }

    private class getUserDialogs extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {


            Call<GetUserChatGroupsResponse> call = apiCaller.getChatDialogs(getUserChatGroupParams);
            call.enqueue(new Callback<GetUserChatGroupsResponse>() {
                @Override
                public void onResponse(@NonNull Call<GetUserChatGroupsResponse> call, @NonNull Response<GetUserChatGroupsResponse> response) {
                    if (response.isSuccessful()) {
                        GetUserChatGroupsResponse getUserChatGroupsResponse = response.body();
                        if (getUserChatGroupsResponse != null) {
                            if (getUserChatGroupsResponse.getStatusCode().equals("00")) {

                                mTundaRooomDao.deleteAllChatDialogs();
                                body = fetchDialogResults(response);

                                if (body != null) {
                                    for (ChatInfo chatInfo : body) {

                                        ChatDialogsTable chatDialogsTable = new ChatDialogsTable(
                                                chatInfo.getMatchId(),
                                                chatInfo.getFullName(),
                                                chatInfo.getPhotoPath(),
                                                chatInfo.getViewStatus(),
                                                chatInfo.getLastSeenDate(),
                                                chatInfo.getLastMessage(),
                                                chatInfo.getLastChatDate(),
                                                chatInfo.getUnreadMsgCount(),
                                                chatInfo.getInterest(),
                                                chatInfo.getRecipientId(),
                                                chatInfo.getDob(),
                                                chatInfo.getBio(),
                                                chatInfo.getJob(),
                                                chatInfo.getSchool(),
                                                chatInfo.getDistance(),
                                                chatInfo.getPhoto_count(),
                                                chatInfo.getVerification_status(),
                                                chatInfo.getLikes_count(),
                                                chatInfo.getHas_insta_photo());
                                        mTundaRooomDao.insertDialog(chatDialogsTable);

                                    }
                                }


//                                Preferences.get(mActivity).setIsTimeToRefreshCDialogss(false);
//                                dialogsAdapter.clear();
//
//                                mLoading_user_dialogs.setVisibility(View.GONE);
//                                mDialogRecycler.setVisibility(View.VISIBLE);
//                                mNoChatsImage.setVisibility(View.GONE);
//                                mNoDialogsTextView.setVisibility(View.GONE);
//                                mychatsHeader.setVisibility(View.VISIBLE);
//                                dialogsAdapter.addAll(body);
//                                databaseHandler.deleteAllDialogs();
//                                saveToMemoryDIalogs(body);


                            } else if (getUserChatGroupsResponse.getStatusCode().equals("99")) {
//                                databaseHandler.deleteAllDialogs();
                                mTundaRooomDao.deleteAllChatDialogs();
//                                mActivity.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        mLoading_user_dialogs.hide();
//                                        mychatsHeader.setVisibility(View.GONE);
//                                        mLoading_user_dialogs.setVisibility(View.GONE);
//                                        mDialogRecycler.setVisibility(View.GONE);
//                                        mNoChatsImage.setVisibility(View.VISIBLE);
//                                        mNoDialogsTextView.setVisibility(View.VISIBLE);
//                                    }
//                                });

                            }
                        }
                    }

                }

                @Override
                public void onFailure(@NonNull Call<GetUserChatGroupsResponse> call, @NonNull Throwable t) {
//                    loadDialogsFromDb(getDialogsFromDb());

                }
            });
            return null;
        }
    }


}



