

package ke.co.tunda.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Activities.EditProfileActivity;
import ke.co.tunda.Activities.SettingsActivity;
import ke.co.tunda.Activities.UserLikes;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.PackageIdParams;
import ke.co.tunda.ApiConnector.Models.PackageResponse;

import com.bumptech.glide.Glide;
import ke.co.tunda.Helpers.MyBounceInterpolator;
import ke.co.tunda.R;
import ke.co.tunda.fragments.Dialogs.TundaPlusBuy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by george on 10/1/18.
 */

public class MyProfileFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.profileImage)
    CircleImageView profileImage;
    @BindView(R.id.settings_button)
    TextView settings_button;
    @BindView(R.id.edit_button)
    TextView editButton;
    @BindView(R.id.my_tunda_plus)
    Button my_tunda_plus;
    @BindView(R.id.prof_details)
    TextView details;
    @BindView(R.id.linearLayout)
    LinearLayout mMain;
    @BindView(R.id.my_tunda_plus_ad_holder)
    RelativeLayout mTundaAdHolder;
    FragmentActivity mActivity;
    @BindView(R.id.my_share_tunda)
    Button mShareAppHolder;

    //    @BindView(R.id.views_holder)
//    CardView mViewHolder;
    @BindView(R.id.number_of_likes)
    TextView mNumberofLikes;

    SharedPreferences sp;
    String image;
    String first_name;
    String dob;
    String age = "";
    String year_of_birth;
    private String TAG = "PF";
    private int package_id;
    private int user_id;
    private int likes_count;
//    private String share_url;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, rootView);
        my_tunda_plus.setOnClickListener(this);


        sp = PreferenceManager.getDefaultSharedPreferences(mActivity);
        first_name = sp.getString("first_name", "");
        user_id = sp.getInt("user_id", 0);
        image = sp.getString("image", "");


        settings_button.setOnClickListener(this);
        editButton.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        mShareAppHolder.setOnClickListener(this);
        mNumberofLikes.setOnClickListener(this);

        try {

            PackageIdParams packageIdParams = new PackageIdParams();
            packageIdParams.setUserId(user_id);
            packageIdParams.setRequest_type("GET_USER_PACKAGE");
            new getPackageId(packageIdParams).execute();

            dob = sp.getString("dob", "");
            if (image != null) {
                Glide.with(mActivity).load(image)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(profileImage);
            }

            if (dob != null && !dob.isEmpty()) {
                calculateAge(dob);
            }


            if (first_name != null) {
                if (age != null && !age.isEmpty()) {
                    details.setText(first_name + ", " + age);
                } else {
                    details.setText(first_name);
                }
            }


        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }


        return rootView;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.my_tunda_plus:
                viewPackages();
                break;
            case R.id.settings_button:
                if (mActivity != null) {

                    SettingsActivity.open(mActivity);
                }

                break;
            case R.id.edit_button:
                if (mActivity != null) {
                    EditProfileActivity.open(mActivity);
                }
                break;
            case R.id.profileImage:
                EditProfileActivity.open(mActivity);
                break;
            case R.id.my_share_tunda:
                didTapButton(mShareAppHolder);
                shareApp();
                break;
            case R.id.number_of_likes:
                if (mActivity != null) {
                    Log.d(TAG, "onClick: " + user_id);

                    UserLikes.open(mActivity, user_id, String.valueOf(likes_count));

                }
                break;


        }

    }

    private void shareApp() {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_body, getResources().getString(R.string.app_name)));
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.email_client)));
    }

    private void viewPackages() {
        if (mActivity != null) {
            TundaPlusBuy.open(mActivity);
        }

    }

    private void calculateAge(String mDob) {
        Calendar calendar = Calendar.getInstance();
        String DAY;

        String YEAR = mDob.substring(0, 4);
        String MONTH = mDob.substring(5, 7);
        if (MONTH.contains("-")) {
            MONTH = MONTH.replace("-", "");
            Log.d("TESTING", "month=" + MONTH);
            DAY = mDob.substring(7, mDob.length());
        } else {
            DAY = mDob.substring(8, mDob.length());
        }


        Calendar x = new GregorianCalendar(Integer.valueOf(YEAR), Integer.valueOf(MONTH), Integer.valueOf(DAY));
        Log.d("TESTING", YEAR + "/" + MONTH + "/" + DAY);

        calendar.add(Calendar.DAY_OF_MONTH, Integer.valueOf(MONTH));
        calendar.add(Calendar.YEAR, Integer.valueOf(YEAR));
        calendar.add(Calendar.DATE, Integer.valueOf(DAY));

        Date date = x.getTime();

//        date.setYear();
//        date.setMonth();
//        date.setDate();


        Date currentDate = new Date();


        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(date));
        int d2 = Integer.parseInt(formatter.format(currentDate));
        int agex = (d2 - d1) / 10000;
        age = String.valueOf(agex);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;


    }

    private class getPackageId extends AsyncTask<Void, Void, Void> {
        PackageIdParams params;
        SharedPreferences.Editor editor;

        public getPackageId(PackageIdParams packageIdParams) {
            this.params = packageIdParams;
            this.editor = sp.edit();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
            Call<PackageResponse> call = apiCaller.getPackageId(params);
            call.enqueue(new Callback<PackageResponse>() {
                @Override
                public void onResponse(Call<PackageResponse> call, Response<PackageResponse> response) {
                    if (response.isSuccessful()) {
                        PackageResponse packageResponse = response.body();
                        if (packageResponse != null) {
                            if (packageResponse.getBody() != null) {
                                if (packageResponse.getBody().get(0).getLikes_count() != null && packageResponse.getBody().get(0).getLikes_count() > 0) {
                                    mNumberofLikes.setVisibility(View.VISIBLE);
                                    likes_count = packageResponse.getBody().get(0).getLikes_count();
                                    mNumberofLikes.setText(mActivity.getResources().getString(R.string.likes_desc,String.valueOf(packageResponse.getBody().get(0).getLikes_count())));
                                } else {
                                    mNumberofLikes.setVisibility(View.GONE);
                                }
                                if (packageResponse.getBody().get(0).getPackageId() != null) {
                                    package_id = packageResponse.getBody().get(0).getPackageId();
                                    editor.putInt("package_id", package_id);
                                    editor.commit();

                                }

                                if (package_id > 1) {
                                    mTundaAdHolder.setVisibility(View.GONE);
                                    mShareAppHolder.setVisibility(View.VISIBLE);
//                                    share_url = packageResponse.getBody().get(0).getShare_url();
//                                    editor.putString("share_url", share_url);
//                                    editor.commit();


                                } else {
                                    mShareAppHolder.setVisibility(View.GONE);
                                    mTundaAdHolder.setVisibility(View.VISIBLE);
                                }
                            } else {
                                mNumberofLikes.setVisibility(View.GONE);
                                Log.d(TAG, "onResponse: body null");
                            }


                        } else {
                            mNumberofLikes.setVisibility(View.GONE);
                            getFromSp();
                            Log.d(TAG, "onResponse: ");
                        }
                    }
                }

                @Override
                public void onFailure(Call<PackageResponse> call, Throwable t) {
                    mNumberofLikes.setVisibility(View.GONE);
                    getFromSp();
                    Log.d(TAG, "onFailure: " + t.getMessage());

                }
            });
            return null;
        }
    }

    private void getFromSp() {
        package_id = sp.getInt("package_id", 0);
//        share_url = sp.getString("share_url", "");
        if (package_id >= 1) {

            if (package_id == 3) {

                mTundaAdHolder.setVisibility(View.GONE);
                mShareAppHolder.setVisibility(View.VISIBLE);

            } else if (package_id == 2) {
                mTundaAdHolder.setVisibility(View.GONE);
                mShareAppHolder.setVisibility(View.VISIBLE);

            } else if (package_id == 1) {
                mTundaAdHolder.setVisibility(View.VISIBLE);
                mShareAppHolder.setVisibility(View.GONE);
            }

        }


    }

    private void didTapButton(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
//        view.startAnimation(myAnim);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        view.startAnimation(myAnim);
    }
}