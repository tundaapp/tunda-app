/*
 * Creator: Donbosco Muthiani on 9/24/19 3:27 PM Last modified: 9/20/19 2:42 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import butterknife.ButterKnife;
import ke.co.tunda.R;

public class ChatsStatusFragment extends Fragment {

    private Context mContext;
    private FragmentActivity mActivity;
    private View mView;
    //    private Fragment mChatsFragment = new ConnectionsFragment();
//    private Fragment mTundaMoodFragment = new TundaMoodFragment();
//
//    private Fragment active = mChatsFragment;
//    private FragmentManager fm;
//
//    @BindView(R.id.frame_container_chat)
//    FrameLayout mFragmentLayout;
    private OnChatStatusPstnChanged callback;
    private static final String TAG = "CSF";

    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

    ViewPagerAdapter adapter;

    //Fragments

//    TundaMoodFragment statusFragment;
    ConnectionsFragment chatFragment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_chat_status, container, false);
        ButterKnife.bind(this, mView);
//        fm = mActivity.getSupportFragmentManager();
//        fm.beginTransaction().add(R.id.frame_container_chat, mTundaMoodFragment, "top_picks").hide(mTundaMoodFragment).commit();
//        fm.beginTransaction().add(R.id.frame_container_chat, mChatsFragment, "chats").commit();


        viewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);
        setupViewPager(viewPager);

        //Initializing the tablayout
        tabLayout = (TabLayout) mView.findViewById(R.id.tablayout);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        return mView;
    }

    public void setOnChatStatusPstnChangedListener(OnChatStatusPstnChanged callback) {
        this.callback = callback;
    }


    public interface OnChatStatusPstnChanged {
        void onPositionChanged(int position);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }

    public void setPosition(int position) {
//        if (position == 0) {
//            Log.d("MN", "setPosition: " + position);
//            fm.beginTransaction().hide(active).show(mTundaMoodFragment).commit();
//            active = mTundaMoodFragment;
//        } else if (position == 1) {
//            Log.d("MN", "setPosition: " + position);
//
//            fm.beginTransaction().hide(active).show(mChatsFragment).commit();
//            active = mChatsFragment;
//        }


    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(mActivity.getSupportFragmentManager());
        chatFragment = new ConnectionsFragment();
//        statusFragment = new TundaMoodFragment();
        adapter.addFragment(chatFragment, "CHATS");
//        adapter.addFragment(statusFragment, "MOODS");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == ImageEditor.RC_IMAGE_EDITOR) {
//            statusFragment.onActivityResult(requestCode, resultCode, data);
//        } else {
//            statusFragment.onActivityResult(requestCode, resultCode, data);
//        }
    }
}
