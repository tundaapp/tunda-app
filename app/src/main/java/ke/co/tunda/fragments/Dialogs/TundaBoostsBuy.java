/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/19/19 6:12 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.fragments.Dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.SendPurchaseTokenParams;
import ke.co.tunda.Models.GetBoostsParams;
import ke.co.tunda.Models.GetBoostsResp;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TundaBoostsBuy extends AppCompatActivity implements View.OnClickListener,
        View.OnFocusChangeListener, PurchasesUpdatedListener {
    private ViewPager mViewPager;
    private LinearLayout mDotLayout;
    private SlideAdapterBoosts slideAdapter;
    private TextView[] mDots;
    private RelativeLayout holder;
    private ApiCaller apiCaller;
    private GetBoostsResp.Boosts[] boostsPkgs;
    SharedPreferences sp;
    int selectedMode;
    View view;
    private String mPhoneNumber = "";
    private int user_id;
    int amt;


    @BindView(R.id.package_1)
    RelativeLayout mPackage1;
    @BindView(R.id.package_2)
    RelativeLayout mPackage2;
    @BindView(R.id.package_3)
    RelativeLayout mPackage3;
    @BindView(R.id.not_buying)
    TextView not_buying;
    @BindView(R.id.most_popular_tag)
    TextView mPopular;

    @BindView(R.id.three)
    TextView mThreeValue;
    @BindView(R.id.two)
    TextView mTwoValue;
    @BindView(R.id.one)
    TextView mOneValue;
    @BindView(R.id.spot_1)
    TextView mSpotOne;
    @BindView(R.id.spot_2)
    TextView mSpotTwo;
    @BindView(R.id.spot_3)
    TextView mSPotThree;
    @BindView(R.id.buy_tunda_boosts)
    Button mBuy;


    private int oneValue, twoValue, threeValue;
    private int spotId1, spotId2, spotId3;
    private int spotOne, spotTwo, spotThree;
    private int spot_id = 2;

    private BillingClient billingClient;
    private static String TAG = "BILLING";
    private String package_time = "5";

    private String sku1,sku2,sku3;

    private SkuDetails skuDetails1;
    private SkuDetails skuDetails2;
    private SkuDetails skuDetails3;


    public static void open(Context context) {
        context.startActivity(new Intent(context, TundaBoostsBuy.class));
    }

    private int[] backgrounds = {R.drawable.bg_3

    };


    Timer timer;
    int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tundaboosts);
        ButterKnife.bind(this);

        mViewPager = findViewById(R.id.slideViewPager);
        mDotLayout = findViewById(R.id.linearLayout);
        holder = findViewById(R.id.holder);
        holder.setBackground(getResources().getDrawable(backgrounds[0]));


        slideAdapter = new SlideAdapterBoosts(this);
        mViewPager.setAdapter(slideAdapter);

        sp = PreferenceManager.getDefaultSharedPreferences(TundaBoostsBuy.this);


        apiCaller = Builder.getClient().create(ApiCaller.class);
        billingClient = BillingClient.newBuilder(this).setListener(this).enablePendingPurchases().build();


        getPackageData();


        spotOne = sp.getInt("spot_one_count", 1);
        spotTwo = sp.getInt("spot_two_count", 5);
        spotThree = sp.getInt("spot_three_count", 10);

        oneValue = sp.getInt("spot_one_value", 300);
        twoValue = sp.getInt("spot_two_value", 450);
        threeValue = sp.getInt("spot_three_value", 200);

        spotId1 = sp.getInt("spot_id_one", 1);
        spotId2 = sp.getInt("spot_id_two", 2);
        spotId3 = sp.getInt("spot_id_three", 3);

        mPhoneNumber = sp.getString("phone_number", "");
        user_id = sp.getInt("user_id", 0);


        mPackage1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        mPackage1.setOnFocusChangeListener(this);
        mPackage2.setOnFocusChangeListener(this);
        mPackage3.setOnFocusChangeListener(this);
        mPackage1.setOnClickListener(this);
        mPackage2.setOnClickListener(this);
        mPackage3.setOnClickListener(this);
        not_buying.setOnClickListener(this);
        mBuy.setOnClickListener(this);

//        changeStatus(mPackage1);
//        changeStatus(mPackage2);
//        changeStatus(mPackage3);


    }

    private void getPackageData() {

        GetBoostsParams getBoostsParams = new GetBoostsParams();
        getBoostsParams.setRequestType("GET_BOOST_SPOTS");
        Call<GetBoostsResp> call = apiCaller.getBoosts(getBoostsParams);
        call.enqueue(new Callback<GetBoostsResp>() {
            @Override
            public void onResponse(Call<GetBoostsResp> call, Response<GetBoostsResp> response) {
                if (response.isSuccessful()) {
                    GetBoostsResp resp = response.body();
                    if (resp != null && resp.getStatusCode() != null && resp.getStatusCode().equals("00")) {
                        boostsPkgs = resp.getBody();
                        mSpotOne.setText(String.valueOf(boostsPkgs[0].getSpotUnitCount()));
                        mSpotTwo.setText(String.valueOf(boostsPkgs[1].getSpotUnitCount()));
                        mSPotThree.setText(String.valueOf(boostsPkgs[2].getSpotUnitCount()));


//                        mOneValue.setText("KES " + boostsPkgs[0].getSpotUnitValue());
//                        mTwoValue.setText("KES " + boostsPkgs[1].getSpotUnitValue());
//                        mThreeValue.setText("KES " + boostsPkgs[2].getSpotUnitValue());

                        saveSpotsToSp(boostsPkgs);

                        sku1 = "6";
                        sku2 = "5";
                        sku3 = "4";


                        billingClient.startConnection(new BillingClientStateListener() {
                            @Override
                            public void onBillingSetupFinished(BillingResult billingResult) {
                                Log.d(TAG, "onBillingSetupFinished: ");
                                List<String> skuList = new ArrayList<> ();
                                skuList.add(sku1);
                                skuList.add(sku2);
                                skuList.add(sku3);
                                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                                params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
                                    billingClient.querySkuDetailsAsync(params.build(),
                                            new SkuDetailsResponseListener() {
                                                @Override
                                                public void onSkuDetailsResponse(BillingResult billingResult,
                                                                                 List<SkuDetails> skuDetailsList) {

                                                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                                                        for (SkuDetails skuDetails : skuDetailsList) {
                                                            String sku = skuDetails.getSku();
                                                            if (sku1.equals(sku)) {
                                                                skuDetails1 = skuDetails;
                                                                mOneValue.setText(getResources().getString(R.string.package_desc_boost,skuDetailsList.get(2).getPrice()));
                                                            } else if (sku2.equals(sku)) {
                                                                skuDetails2 = skuDetails;
                                                                mTwoValue.setText(getResources().getString(R.string.package_desc_boost,skuDetailsList.get(1).getPrice()));
                                                            }else if(sku3.equals(sku)){
                                                                skuDetails3 = skuDetails;
                                                                mThreeValue.setText(getResources().getString(R.string.package_desc_boost,skuDetailsList.get(0).getPrice()));

                                                            }
                                                        }
                                                        mBuy.setEnabled(true);
                                                    }else{
                                                        Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                                                        mBuy.setEnabled(false);
                                                        setFromSp();
                                                    }
                                                    // Process the result.
                                                }
                                            });



                            }

                            @Override
                            public void onBillingServiceDisconnected() {

                            }
                        });



                    } else {
                        Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                        mBuy.setEnabled(false);
                        setFromSp();
                    }
                } else {
                    Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                    mBuy.setEnabled(false);
                    setFromSp();

                }
            }

            @Override
            public void onFailure(Call<GetBoostsResp> call, Throwable t) {
                Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                mBuy.setEnabled(false);
                setFromSp();


            }
        });




    }

    private void setFromSp() {

        mSpotOne.setText(String.valueOf(spotOne));
        mSpotTwo.setText(String.valueOf(spotTwo));
        mSPotThree.setText(String.valueOf(spotThree));

        mOneValue.setText("KES " + oneValue);
        mThreeValue.setText("KES " + twoValue);
        mTwoValue.setText("KES " + threeValue);
//
//        mOneValue.setText(oneValue);
//        mTwoValue.setText(twoValue);
//        mThreeValue.setText(threeValue);
    }

    private void saveSpotsToSp(GetBoostsResp.Boosts[] boostsPkgs) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("spot_one_count", boostsPkgs[0].getSpotUnitCount());
        editor.putInt("spot_two_count", boostsPkgs[1].getSpotUnitCount());
        editor.putInt("spot_three_count", boostsPkgs[2].getSpotUnitCount());

        editor.putInt("spot_one_value", boostsPkgs[0].getSpotUnitValue());
        editor.putInt("spot_two_value", boostsPkgs[1].getSpotUnitValue());
        editor.putInt("spot_three_value", boostsPkgs[2].getSpotUnitValue());

        editor.putInt("spot_id_one", boostsPkgs[0].getSpotId());
        editor.putInt("spot_id_two", boostsPkgs[1].getSpotId());
        editor.putInt("spot_id_three", boostsPkgs[2].getSpotId());

        editor.commit();


    }


    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {


        }

        @Override
        public void onPageSelected(int i) {
//            addDotsIndicator(i);
//            setHolderColor(i);

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    private void setHolderColor(int i) {
        holder.setBackground(getResources().getDrawable(backgrounds[i]));

    }


    public void pageSwitcher(int seconds) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.package_1:
                package_time = "1";
                spot_id = spotId1;
                mPopular.setVisibility(View.GONE);
                v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                mPackage2.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                mPackage3.setBackground(getResources().getDrawable(R.drawable.package_unselected));

                break;
            case R.id.package_2:
                package_time = "5";
                spot_id = spotId2;
                mPopular.setVisibility(View.GONE);
                v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                mPackage1.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                mPackage3.setBackground(getResources().getDrawable(R.drawable.package_unselected));

                break;
            case R.id.package_3:
                package_time = "10";
                spot_id = spotId3;
                mPopular.setVisibility(View.GONE);
                v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                mPackage2.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                mPackage1.setBackground(getResources().getDrawable(R.drawable.package_unselected));

                break;
            case R.id.not_buying:
                finish();
                break;

            case R.id.buy_tunda_boosts:
                buyBoosts(spot_id, package_time);
                break;

        }

    }

    private void buyBoosts(int spot_id, String time) {
        String[] modes = {"M-PESA", "Credit Card", "payPal", "i-Lipa"};


        BillingFlowParams flowParams;
        Integer responseCode;

        switch (time){
            case "1":
                flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails1)
                        .build();
                Log.d(TAG, "buyPlus: case 12 sku = "+skuDetails1.getSku());
                responseCode = billingClient.launchBillingFlow(TundaBoostsBuy.this,flowParams).getResponseCode();

//               billingClient.launchBillingFlow(TundaPlusBuy.this, flowParams);
                break;
            case "5":
                Log.d(TAG, "buyPlus: case 12 sku = "+skuDetails2.getSku());

                flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails2)
                        .build();
                responseCode = billingClient.launchBillingFlow(TundaBoostsBuy.this,flowParams).getResponseCode();
                break;
            case "10":
                Log.d(TAG, "buyPlus: case 12 sku = "+skuDetails3.getSku());

                flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails3)
                        .build();
                responseCode = billingClient.launchBillingFlow(TundaBoostsBuy.this,flowParams).getResponseCode();
                break;
        }



//        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this);
//        builder.setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET);
//        builder.setTitle("Select Payment Mode");
//        builder.setSingleChoiceItems(modes, 1, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int index) {
//                selectedMode = index;
//            }
//        });
//        builder.addButton("NEXT", -1, -1,
//                CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        switch (selectedMode) {
//                            case 0:
//                                dialogInterface.dismiss();
//
//                                CFAlertDialog.Builder builderMpesa = new CFAlertDialog.Builder(TundaBoostsBuy.this);
//                                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                                view = inflater.inflate(R.layout.dialog_purchase_package, null);
//                                builderMpesa.setHeaderView(view);
//                                builderMpesa.setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET);
//                                RelativeLayout mLoader = view.findViewById(R.id.circular_progress);
//
//
//                                EditText mPhone = view.findViewById(R.id.phone);
//                                if (mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
//                                    mPhone.setText(mPhoneNumber);
//                                } else {
//                                    mPhone.setText("Mpesa Number");
//                                }
//
//
//                                builderMpesa.addButton("PURCHASE PACKAGE", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE,
//                                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//
//                                                MpesaPayDetails payDetails = isReady(view);
//                                                if (payDetails.isReady()) {
//
//                                                    PurchaseSpotParams params = new PurchaseSpotParams();
//                                                    params.setPaymentMode("MPESA");
//                                                    params.setRequestType("PURCHASE_SPOT");
//                                                    params.setSpotId(spot_id);
//                                                    params.setUserId(user_id);
//                                                    params.setPhone_number(payDetails.getPhone());
//
//
//                                                    MaterialDialog mDialog;
//                                                    MaterialDialog.Builder builder = new MaterialDialog.Builder(TundaBoostsBuy.this);
//                                                    builder.content("Please wait ....")
//                                                            .widgetColor(getResources().getColor(R.color.colorAccent))
//                                                            .progress(true, 0);
//                                                    mDialog = builder.build();
//                                                    mDialog.setCancelable(false);
//                                                    mDialog.show();
//
//
//                                                    ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
//                                                    Call<PurchasePackageResp> call = apiCaller.buyBoost(params);
//                                                    call.enqueue(new Callback<PurchasePackageResp>() {
//                                                        @Override
//                                                        public void onResponse(Call<PurchasePackageResp> call, Response<PurchasePackageResp> response) {
//                                                            if (response.isSuccessful()) {
//
//                                                                PurchasePackageResp resp = response.body();
//                                                                if (resp.getStatusCode().equals("00")) {
//                                                                    if (resp.getBody().getStatusCode().equals("00")) {
//                                                                        mDialog.dismiss();
//                                                                        dialog.dismiss();
//                                                                        Toasty.success(getApplicationContext(),
//                                                                                "A prompt to pay will be sent to " + payDetails.getPhone()).show();
//
//                                                                    } else {
//
//                                                                        mDialog.dismiss();
//                                                                        Toasty.error(getApplicationContext(),
//                                                                                "Something went wrong, Try Again").show();
//                                                                    }
//                                                                } else {
//                                                                    mDialog.dismiss();
//                                                                    Toasty.error(getApplicationContext(),
//                                                                            "Something went wrong, Try Again").show();
//                                                                }
//
//                                                            } else {
//                                                                mDialog.dismiss();
//                                                                Toasty.error(getApplicationContext(),
//                                                                        "Something went wrong, Try Again").show();
//                                                            }
//                                                        }
//
//                                                        @Override
//                                                        public void onFailure(Call<PurchasePackageResp> call, Throwable t) {
//                                                            mDialog.dismiss();
//                                                            Toasty.error(getApplicationContext(),
//                                                                    "Something went wrong, Try Again").show();
//
//                                                        }
//                                                    });
//
//                                                }
//
//                                            }
//
//                                            private MpesaPayDetails isReady(View view) {
//                                                ImageView mClose = view.findViewById(R.id.close);
//
//
//                                                EditText mPhone = view.findViewById(R.id.phone);
//
//
//                                                String phoneNumber = mPhone.getText().toString();
//                                                String nameregex = "^[a-zA-Z]{3,16}$";
//                                                String phonenumberregex = "^[+]?[0-9]{9,12}$";
//                                                String pattern = "^0";
//                                                String replacement = "254";
//
//
//                                                if (!TextUtils.isEmpty(phoneNumber)) {
//                                                    if (phoneNumber.matches(phonenumberregex)) {
//                                                        phoneNumber = phoneNumber.replaceFirst(pattern, replacement);
//                                                        if (phoneNumber.contains("+")) {
//                                                            phoneNumber = phoneNumber.replace("+", "");
//                                                        }
//
//                                                        return new MpesaPayDetails(phoneNumber, amt, true);
//
//
//                                                    } else {
//                                                        mPhone.setError("Please enter a valid phone");
//                                                        mPhone.requestFocus();
//                                                    }
//
//
//                                                } else {
//                                                    mPhone.setError("Enter a Phone Number");
//                                                    mPhone.requestFocus();
//                                                }
//
//
//                                                return new MpesaPayDetails(null, 0, false);
//                                            }
//                                        });
//                                builderMpesa.show();
//                                break;
//
//                        }
//                    }
//
//
//                });
//        builder.show();

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.package_1:
                if (hasFocus) {
                    v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                } else {
                    v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                }
                break;
            case R.id.package_2:
                if (hasFocus) {
                    v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                } else {
                    v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                }
                break;
            case R.id.package_3:
                if (hasFocus) {
                    v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                } else {
                    v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                }
                break;


        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);

            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            Log.d(TAG, "onPurchasesUpdated: user cancelled");
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }
    }

    private void handlePurchase(Purchase purchase) {
        String package_token = purchase.getPurchaseToken();
        String product_id = purchase.getSku();
        Log.d(TAG, "handlePurchase: token = " + purchase.getPurchaseToken());

        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.



            // Acknowledge the purchase if it hasn't already been acknowledged.
            ConsumeResponseListener listener = new ConsumeResponseListener() {
                @Override
                public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        Log.d(TAG, "onConsumeResponse: package consumed ");
                    }
                }
            };

            ConsumeParams consumeParams =
                    ConsumeParams.newBuilder()
                            .setPurchaseToken(purchase.getPurchaseToken())
                            .build();

            billingClient.consumeAsync(consumeParams, listener);

            new sendPurchaseToken(package_token,product_id).execute();

        } else if (purchase.getPurchaseState() == Purchase.PurchaseState.PENDING) {
            // Here you can confirm to the user that they've started the pending
            // purchase, and to complete it, they should follow instructions that
            // are given to them. You can also choose to remind the user in the
            // future to complete the purchase if you detect that it is still
            // pending.
        }

    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {


            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 7) {
                        page = 0;
//                        pageSwitcher(1);
                    } else {
                        mViewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }


    private void changeStatus(View v) {
        if (v.isSelected()) {
            v.setBackground(getResources().getDrawable(R.drawable.package_selected));
        } else {
            v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
        }
    }

    final class MpesaPayDetails {
        private final String phone;
        private final int amount;
        private final boolean isReady;

        public MpesaPayDetails(String phone, int amount, boolean isReady) {
            this.phone = phone;
            this.amount = amount;
            this.isReady = isReady;
        }

        public int getAmount() {
            return amount;
        }

        public String getPhone() {
            return phone;
        }

        public boolean isReady() {
            return isReady;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private class sendPurchaseToken extends AsyncTask<Void,Void,Void> {
        String purchase_token;
        String product_id;
        public sendPurchaseToken(String package_token, String product_id) {
            this.purchase_token = package_token;
            this.product_id = product_id;

        }

        @Override

        protected Void doInBackground(Void... voids) {
            SendPurchaseTokenParams sendPurchaseTokenParams = new SendPurchaseTokenParams();
            sendPurchaseTokenParams.setProductId(product_id);
            sendPurchaseTokenParams.setPurchaseType(1);
            sendPurchaseTokenParams.setToken(purchase_token);
            sendPurchaseTokenParams.setRequestType("VERIFY_GOOGLE_PURCHASE");
            sendPurchaseTokenParams.setUserId(user_id);
            Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.sendPurchaseToken(sendPurchaseTokenParams);
            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                @Override
                public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                    if(response.isSuccessful()){
                        ke.co.tunda.ApiConnector.Models.Response resp = response.body();
                        if (resp != null && resp.getStatusCode().equals("00")) {
                            Toasty.success(getApplicationContext(), "Successfully purchased").show();
                            Log.d(TAG, "onResponse: updated");
                        }

                    }
                }

                @Override
                public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onResponse: failed");

                }
            });

            return null;
        }
    }
}
