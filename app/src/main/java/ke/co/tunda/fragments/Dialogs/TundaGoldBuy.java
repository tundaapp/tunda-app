/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 8/22/19 4:36 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.fragments.Dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.crowdfire.cfalertdialog.CFAlertDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.ApiConnector.Models.SendPurchaseTokenParams;
import ke.co.tunda.Models.GetPackagesResp;
import ke.co.tunda.Models.GetpackageParams;
import ke.co.tunda.Models.PurchasePackageParams;
import ke.co.tunda.Models.PurchasePackageResp;
import ke.co.tunda.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TundaGoldBuy extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener,
        PurchasesUpdatedListener {


    private SlideAdapter2 slideAdapter;
    private ViewPager mViewPager;
    private LinearLayout mDotLayout;
    private TextView[] mDots;
    private RelativeLayout holder;

    private GetPackagesResp.Packages[] packages;
    SharedPreferences sp;
    private ApiCaller apiCaller;
    int selectedMode;
    View view;


    @BindView(R.id.package_1)
    RelativeLayout mPackage1;
    @BindView(R.id.package_2)
    RelativeLayout mPackage2;
    @BindView(R.id.package_3)
    RelativeLayout mPackage3;
    @BindView(R.id.not_buying)
    TextView not_buying;
    @BindView(R.id.most_popular_tag)
    TextView mPopular;
    @BindView(R.id.twelve_months)
    TextView mTwelveMonths;
    @BindView(R.id.six_months)
    TextView mSixMoths;
    @BindView(R.id.one_month)
    TextView mOneMonth;
    @BindView(R.id.buy_tunda_gold)
    Button mBuy;

    private int oneMonth, sixMonths, twelveMonths;
    private String mPhoneNumber = "", user_id = "";
    int amt;
    private int package_id;
    private String package_months = "6";

    private BillingClient billingClient;
    private AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener;
    private static String TAG = "BILLING";
    private String sku1,sku2,sku3;

    private SkuDetails skuDetails1;
    private SkuDetails skuDetails2;
    private SkuDetails skuDetails3;


    public static void open(Context context) {
        context.startActivity(new Intent(context, TundaGoldBuy.class));
    }

    private int[] backgrounds = {R.drawable.bg_8, R.drawable.bg_9,
            R.drawable.bg_1, R.drawable.bg_2, R.drawable.bg_3,
            R.drawable.bg_4, R.drawable.bg_5, R.drawable.bg_6,
            R.drawable.bg_7
    };

    Timer timer;
    int page = 0;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tundagold);
        ButterKnife.bind(this);
        sp = PreferenceManager.getDefaultSharedPreferences(TundaGoldBuy.this);
        oneMonth = sp.getInt("one_month_gold", 700);
        sixMonths = sp.getInt("six_months_gold", 500);
        twelveMonths = sp.getInt("twelve_months_gold", 300);
        package_id = sp.getInt("tunda_gold_id", 3);

        apiCaller = Builder.getClient().create(ApiCaller.class);
        billingClient = BillingClient.newBuilder(this).setListener(this).enablePendingPurchases().build();
        acknowledgePurchaseResponseListener = new AcknowledgePurchaseResponseListener() {
            @Override
            public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK){
                    Toasty.success(getApplicationContext(),"Purchase complete").show();
                }
            }
        };

        mPhoneNumber = sp.getString("phone_number", "");
        id = sp.getInt("user_id", 0);
        user_id = String.valueOf(id);

        getPackageData();

        mViewPager = findViewById(R.id.slideViewPager);
        mDotLayout = findViewById(R.id.linearLayout);
        holder = findViewById(R.id.holder);
        holder.setBackground(getResources().getDrawable(backgrounds[0]));


        slideAdapter = new SlideAdapter2(this);
        mViewPager.setAdapter(slideAdapter);
        pageSwitcher(2);


        addDotsIndicator(0);

        mViewPager.addOnPageChangeListener(viewListener);

        mPackage1.setOnFocusChangeListener(this);
        mPackage2.setOnFocusChangeListener(this);
        mPackage3.setOnFocusChangeListener(this);
        mPackage1.setOnClickListener(this);
        mPackage2.setOnClickListener(this);
        mPackage3.setOnClickListener(this);
        not_buying.setOnClickListener(this);
        mBuy.setOnClickListener(this);

//        changeStatus(mPackage1);
//        changeStatus(mPackage2);
//        changeStatus(mPackage3);

    }

    private void getPackageData() {
        GetpackageParams getpackageParams = new GetpackageParams();
        getpackageParams.setPackageName("Tunda Gold");
        getpackageParams.setRequestType("GET_PACKAGE");
        Call<GetPackagesResp> call = apiCaller.getPackages(getpackageParams);
        call.enqueue(new Callback<GetPackagesResp>() {
            @Override
            public void onResponse(Call<GetPackagesResp> call, Response<GetPackagesResp> response) {
                if (response.isSuccessful()) {
                    GetPackagesResp resp = response.body();
                    if (resp != null && resp.getStatusCode() != null && resp.getStatusCode().equals("00")) {
                        packages = resp.getBody();

                        saveToSp(packages[0]);

                        sku1 = packages[0].getPackageId()+"_12";
                        sku2 = packages[0].getPackageId()+"_6";
                        sku3 = packages[0].getPackageId()+"_1";

                        billingClient.startConnection(new BillingClientStateListener() {
                            @Override
                            public void onBillingSetupFinished(BillingResult billingResult) {
                                List<String> skuList = new ArrayList<> ();
                                skuList.add(sku1);
                                skuList.add(sku2);
                                skuList.add(sku3);
                                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                                params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                                if(areSubcriptionsSupported()){
                                    billingClient.querySkuDetailsAsync(params.build(),
                                            new SkuDetailsResponseListener() {
                                                @Override
                                                public void onSkuDetailsResponse(BillingResult billingResult,
                                                                                 List<SkuDetails> skuDetailsList) {

                                                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                                                        for (SkuDetails skuDetails : skuDetailsList) {
                                                            String sku = skuDetails.getSku();
                                                            if (sku1.equals(sku)) {
                                                                skuDetails1 = skuDetails;
                                                                mTwelveMonths.setText(getResources().getString(R.string.package_desc,skuDetails1.getPrice()));
                                                            } else if (sku2.equals(sku)) {
                                                                skuDetails2 = skuDetails;
                                                                mSixMoths.setText(getResources().getString(R.string.package_desc,skuDetails2.getPrice()));
                                                            }else if(sku3.equals(sku)){
                                                                skuDetails3 = skuDetails;
                                                                mOneMonth.setText(getResources().getString(R.string.package_desc,skuDetails3.getPrice()));

                                                            }
                                                        }
                                                        mBuy.setEnabled(true);
                                                    }else{
                                                        Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                                                        mBuy.setEnabled(false);
                                                        setFromSp();
                                                    }
                                                    // Process the result.
                                                }
                                            });
                                }else {
                                    Log.d(TAG, "onBillingSetupFinished: subcriptions are not supported");
                                }


                            }

                            @Override
                            public void onBillingServiceDisconnected() {

                            }
                        });



                    } else {
                        Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                        mBuy.setEnabled(false);
                        setFromSp();
                    }
                } else {
                    Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                    mBuy.setEnabled(false);
                    setFromSp();

                }
            }

            @Override
            public void onFailure(Call<GetPackagesResp> call, Throwable t) {
                Toasty.warning(getApplicationContext(),"Error connecting. Check your network settings",Toasty.LENGTH_SHORT).show();
                mBuy.setEnabled(false);
                setFromSp();
                Log.d(TAG, "onFailure: setting from sp");


            }
        });


    }

    private void setFromSp() {
        mTwelveMonths.setText(getResources().getString(R.string.package_desc, String.valueOf(twelveMonths)));
        mSixMoths.setText(getResources().getString(R.string.package_desc, String.valueOf(sixMonths)));
        mOneMonth.setText(getResources().getString(R.string.package_desc, String.valueOf(oneMonth)));
    }

    private void saveToSp(GetPackagesResp.Packages aPackage) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("twelve_months_gold", aPackage.getTwelveMonthsFee());
        editor.putInt("six_months_gold", aPackage.getSixMonthsFee());
        editor.putInt("one_month_gold", aPackage.getOneMonthFee());
        editor.putInt("tunda_gold_id", aPackage.getPackageId());
        editor.apply();
    }

    public void addDotsIndicator(int position) {

        mDots = new TextView[8];
        mDotLayout.removeAllViews();

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(45);
            mDots[i].setTextColor(getResources().getColor(R.color.white));
            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.indicator));
        }

    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
            setHolderColor(i);

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    private void setHolderColor(int i) {
        holder.setBackground(getResources().getDrawable(backgrounds[i]));

    }


    public void pageSwitcher(int seconds) {
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.package_1:
                mPopular.setVisibility(View.GONE);

                v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                mPackage2.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                mPackage3.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                package_months = "12";


                break;
            case R.id.package_2:
                mPopular.setVisibility(View.GONE);
                v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                mPackage1.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                mPackage3.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                package_months = "6";

                break;
            case R.id.package_3:
                mPopular.setVisibility(View.GONE);
                v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                mPackage2.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                mPackage1.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                package_months = "1";

                break;
            case R.id.not_buying:
                finish();
                break;
            case R.id.buy_tunda_gold:
                if (package_months != null && !package_months.isEmpty()) {
                    buyGold(package_id, package_months);
                } else {
                    Toasty.warning(getApplicationContext(), "Select a Plan").show();
                }

                break;


        }

    }

    private void buyGold(Integer packageId, String package_months) {


        BillingFlowParams flowParams;
        Integer responseCode;

        switch (package_months){
            case "12":
                flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails1)
                        .build();
                Log.d(TAG, "buyPlus: case 12 sku = "+skuDetails1.getSku());
                responseCode = billingClient.launchBillingFlow(TundaGoldBuy.this,flowParams).getResponseCode();
                Log.d(TAG, "buyGold: "+responseCode);


                break;
            case "6":
                Log.d(TAG, "buyPlus: case 12 sku = "+skuDetails2.getSku());

                flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails2)
                        .build();
                responseCode = billingClient.launchBillingFlow(TundaGoldBuy.this,flowParams).getResponseCode();
                Log.d(TAG, "buyGold: "+responseCode);
                break;
            case "1":
                Log.d(TAG, "buyPlus: case 12 sku = "+skuDetails3.getSku());

                flowParams = BillingFlowParams.newBuilder()
                        .setSkuDetails(skuDetails3)
                        .build();
                responseCode = billingClient.launchBillingFlow(TundaGoldBuy.this,flowParams).getResponseCode();
                Log.d(TAG, "buyGold: "+responseCode);
                break;
        }


    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.package_1:
                if (hasFocus) {
                    v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                } else {
                    v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                }
                break;
            case R.id.package_2:
                if (hasFocus) {
                    v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                } else {
                    v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                }
                break;
            case R.id.package_3:
                if (hasFocus) {
                    v.setBackground(getResources().getDrawable(R.drawable.package_selected));
                } else {
                    v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
                }
                break;


        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                handlePurchase(purchase);

            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            Log.d(TAG, "onPurchasesUpdated: user cancelled");
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }

    }

    private void handlePurchase(Purchase purchase) {
        String purchase_token = purchase.getPurchaseToken();
        String product_id = purchase.getSku();
        Log.d(TAG, "handlePurchase: token = " + purchase.getPurchaseToken());

        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            // Grant entitlement to the user.

            Toasty.success(getApplicationContext(),"Successfully purchased").show();

            // Acknowledge the purchase if it hasn't already been acknowledged.
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);

            }

            new sendPurchaseToken(purchase_token,product_id).execute();

        }else if (purchase.getPurchaseState() == Purchase.PurchaseState.PENDING) {
            // Here you can confirm to the user that they've started the pending
            // purchase, and to complete it, they should follow instructions that
            // are given to them. You can also choose to remind the user in the
            // future to complete the purchase if you detect that it is still
            // pending.
        }

    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {


            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 7) {
                        page = 0;
//                        pageSwitcher(1);
                    } else {
                        mViewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }


    private void changeStatus(View v) {
        if (v.isSelected()) {
            v.setBackground(getResources().getDrawable(R.drawable.package_selected));
        } else {
            v.setBackground(getResources().getDrawable(R.drawable.package_unselected));
        }
    }

    final class MpesaPayDetails {
        private final String phone;
        private final int amount;
        private final boolean isReady;

        public MpesaPayDetails(String phone, int amount, boolean isReady) {
            this.phone = phone;
            this.amount = amount;
            this.isReady = isReady;
        }

        public int getAmount() {
            return amount;
        }

        public String getPhone() {
            return phone;
        }

        public boolean isReady() {
            return isReady;
        }
    }

    private boolean isNumberValid(String phoneNumber) {
        String regex = "^[+]?[0-9]{10,12}$";
        if (phoneNumber.matches(regex)) {
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private boolean areSubcriptionsSupported() {
        BillingResult responseCode = billingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS);
        return responseCode.getResponseCode() == BillingClient.BillingResponseCode.OK;


    }

    private class sendPurchaseToken extends AsyncTask<Void,Void,Void> {
        String purchase_token;
        String product_id;
        public sendPurchaseToken(String package_token, String product_id) {
            this.purchase_token = package_token;
            this.product_id = product_id;

        }

        @Override

        protected Void doInBackground(Void... voids) {
            SendPurchaseTokenParams sendPurchaseTokenParams = new SendPurchaseTokenParams();
            sendPurchaseTokenParams.setProductId(product_id);
            sendPurchaseTokenParams.setPurchaseType(2);
            sendPurchaseTokenParams.setToken(purchase_token);
            sendPurchaseTokenParams.setRequestType("VERIFY_GOOGLE_PURCHASE");
            sendPurchaseTokenParams.setUserId(id);
            Call<ke.co.tunda.ApiConnector.Models.Response> call = apiCaller.sendPurchaseToken(sendPurchaseTokenParams);
            call.enqueue(new Callback<ke.co.tunda.ApiConnector.Models.Response>() {
                @Override
                public void onResponse(Call<ke.co.tunda.ApiConnector.Models.Response> call, Response<ke.co.tunda.ApiConnector.Models.Response> response) {
                    if(response.isSuccessful()){
                        ke.co.tunda.ApiConnector.Models.Response resp = response.body();
                        if (resp != null && resp.getStatusCode().equals("00")) {
                            Log.d(TAG, "onResponse: updated");
                        }

                    }
                }

                @Override
                public void onFailure(Call<ke.co.tunda.ApiConnector.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onResponse: failed");

                }
            });

            return null;
        }
    }
}
