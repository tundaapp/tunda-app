/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 4/15/19 12:58 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Callbacks;

import ke.co.tunda.Models.MyMatches;
import ke.co.tunda.SwipeRecorder.Matches;

public interface ExitMatchView {

    void keepSwiping();


    void sendMessage(Matches myMatches);

}
