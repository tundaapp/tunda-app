

package ke.co.tunda.Callbacks;

import android.view.View;

import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Models.MyMatches;

public interface OpenMatchCallback {

    void openMatch(MyMatches myMatches, View imageView);

    void keepSwiping();

}
