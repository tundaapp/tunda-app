/*
 * Creator: Donbosco Muthiani on 9/16/19 3:06 PM Last modified: 9/16/19 3:06 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Callbacks;

import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.Models.UserLikesArray;

public interface LoadUserLike {
    void loadUserLike(Dialog dialog,Boolean bool,Boolean its_a_match,int adapterPosition);
}
