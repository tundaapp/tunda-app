/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/2/19 12:23 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Callbacks;

import ke.co.tunda.ChatKit.ChatHistory;

public interface RetrySendingMessage {

    void retrySendMessage(ChatHistory history);
}
