

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendFcmToken {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("registration_token")
    @Expose
    private String registrationToken;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRegistrationToken() {
        return registrationToken;
    }

    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
