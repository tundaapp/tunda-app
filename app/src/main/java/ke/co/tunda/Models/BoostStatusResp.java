

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoostStatusResp {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("boost_status")
    @Expose
    private String boostStatus;
    @SerializedName("hours_left")
    @Expose
    private Integer hoursLeft;
    @SerializedName("boost_left")
    @Expose
    private Integer boostLeft;
    @SerializedName("action_state")
    @Expose
    private String actionState;
    @SerializedName("boost_type")
    @Expose
    private String boostType;
    @SerializedName("views")
    @Expose
    private Integer views;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getBoostStatus() {
        return boostStatus;
    }

    public void setBoostStatus(String boostStatus) {
        this.boostStatus = boostStatus;
    }

    public Integer getHoursLeft() {
        return hoursLeft;
    }

    public void setHoursLeft(Integer hoursLeft) {
        this.hoursLeft = hoursLeft;
    }

    public Integer getBoostLeft() {
        return boostLeft;
    }

    public void setBoostLeft(Integer boostLeft) {
        this.boostLeft = boostLeft;
    }

    public String getActionState() {
        return actionState;
    }

    public void setActionState(String actionState) {
        this.actionState = actionState;
    }

    public String getBoostType() {
        return boostType;
    }

    public void setBoostType(String boostType) {
        this.boostType = boostType;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }
}
