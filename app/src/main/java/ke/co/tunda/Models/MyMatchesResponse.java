

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyMatchesResponse {

        @SerializedName("status_code")
        @Expose
        private String statusCode;
        @SerializedName("body")
        @Expose
        private MyMatches[] body = null;

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public MyMatches[] getBody() {
            return body;
        }

        public void setBody(MyMatches[] body) {
            this.body = body;
        }
}
