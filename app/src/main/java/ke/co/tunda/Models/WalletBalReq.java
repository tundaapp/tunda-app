/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/30/19 10:09 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletBalReq {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("request_type")
    @Expose
    private String requestType;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
