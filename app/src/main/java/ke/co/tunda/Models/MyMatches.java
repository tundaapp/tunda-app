

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyMatches {

    @SerializedName("match_id")
    @Expose int match_id;

    @SerializedName("recipient_id")
    @Expose
    private int user_id;

    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("photo_path")
    @Expose
    private String photoPath;
    @SerializedName("view_status")
    @Expose
    private Integer viewStatus;
    @SerializedName("last_seen_date")
    @Expose
    private String lastSeenDate;

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("about_me")
    @Expose
    private String bio;

    @SerializedName("job_title")
    @Expose
    private String job;

    @SerializedName("school")
    @Expose
    private String school;

    @SerializedName("dist_km")
    @Expose
    private String distance;

    @SerializedName("photo_count")
    @Expose
    private int photo_count;

    @SerializedName("interest")
    @Expose
    private String interest;

    @SerializedName("verification_status")
    @Expose
    private Integer verification_status;

    @SerializedName("likes")
    @Expose
    private Integer likes_count;

    @SerializedName("has_insta_photo")
    @Expose
    private Integer has_insta_photo;



    public MyMatches(){

    }

    public MyMatches(int user_id,String fullName,String photoPath,int viewStatus,String lastSeenDate){
        this.user_id=user_id;
        this.fullName=fullName;
        this.photoPath=photoPath;
        this.viewStatus=viewStatus;
        this.lastSeenDate=lastSeenDate;

    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public int getMatch_id() {
        return match_id;
    }

    public void setMatch_id(int match_id) {
        this.match_id = match_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public Integer getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(Integer viewStatus) {
        this.viewStatus = viewStatus;
    }

    public String getLastSeenDate() {
        return lastSeenDate;
    }

    public void setLastSeenDate(String lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDistance() {
        return distance;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getBio() {
        return bio;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDob() {
        return dob;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSchool() {
        return school;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }

    public void setVerification_status(Integer verification_status) {
        this.verification_status = verification_status;
    }

    public Integer getVerification_status() {
        return verification_status;
    }

    public void setLikes_count(Integer likes_count) {
        this.likes_count = likes_count;
    }

    public Integer getLikes_count() {
        return likes_count;
    }

    public Integer getHas_insta_photo() {
        return has_insta_photo;
    }

    public void setHas_insta_photo(Integer has_insta_photo) {
        this.has_insta_photo = has_insta_photo;
    }
}
