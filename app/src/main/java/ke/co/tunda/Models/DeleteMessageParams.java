/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 6/28/19 3:55 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ke.co.tunda.ApiConnector.Models.ChatKey;

public class DeleteMessageParams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("chats")
    @Expose
    private ArrayList<ChatKey> chats;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ArrayList<ChatKey> getChats() {
        return chats;
    }

    public void setChats(ArrayList<ChatKey> chats) {
        this.chats = chats;
    }
}
