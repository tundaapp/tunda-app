/*
 * Creator: Donbosco Muthiani on 8/23/19 10:44 AM Last modified: 8/23/19 10:44 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LikesResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("body")
    @Expose
    private LikesBody[] body = null;



    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public LikesBody[] getBody() {
        return body;
    }

    public void setBody(LikesBody[] body) {
        this.body = body;
    }


}
