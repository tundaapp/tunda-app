

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletBalResp {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private List<Body> body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<Body> getBody() {
        return body;
    }

    public void setBody(List<Body> body) {
        this.body = body;
    }
    public class Body {


        @SerializedName("available_balance")
        @Expose
        private Integer availableBalance;

        public Integer getAvailableBalance() {
            return availableBalance;
        }

        public void setAvailableBalance(Integer availableBalance) {
            this.availableBalance = availableBalance;
        }
    }
}
