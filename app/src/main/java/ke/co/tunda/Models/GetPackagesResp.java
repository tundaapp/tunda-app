

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPackagesResp {

    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private Packages[] body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Packages[] getBody() {
        return body;
    }

    public void setBody(Packages[] body) {
        this.body = body;
    }

    public class Packages {

        @SerializedName("package_id")
        @Expose
        private Integer packageId;
        @SerializedName("package_name")
        @Expose
        private String packageName;
        @SerializedName("one_month_fee")
        @Expose
        private Integer oneMonthFee;
        @SerializedName("six_months_fee")
        @Expose
        private Integer sixMonthsFee;
        @SerializedName("twelve_months_fee")
        @Expose
        private Integer twelveMonthsFee;

        public Integer getPackageId() {
            return packageId;
        }

        public void setPackageId(Integer packageId) {
            this.packageId = packageId;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public Integer getOneMonthFee() {
            return oneMonthFee;
        }

        public void setOneMonthFee(Integer oneMonthFee) {
            this.oneMonthFee = oneMonthFee;
        }

        public Integer getSixMonthsFee() {
            return sixMonthsFee;
        }

        public void setSixMonthsFee(Integer sixMonthsFee) {
            this.sixMonthsFee = sixMonthsFee;
        }

        public Integer getTwelveMonthsFee() {
            return twelveMonthsFee;
        }

        public void setTwelveMonthsFee(Integer twelveMonthsFee) {
            this.twelveMonthsFee = twelveMonthsFee;
        }

    }

}
