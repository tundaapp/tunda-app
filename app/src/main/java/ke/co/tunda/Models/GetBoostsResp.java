

package ke.co.tunda.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBoostsResp {

    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    private Boosts[] body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Boosts[] getBody() {
        return body;
    }

    public void setBody(Boosts[] body) {
        this.body = body;
    }

    public class Boosts {

        @SerializedName("spot_id")
        @Expose
        private Integer spotId;
        @SerializedName("spot_name")
        @Expose
        private String spotName;
        @SerializedName("spot_unit")
        @Expose
        private String spotUnit;
        @SerializedName("spot_unit_count")
        @Expose
        private Integer spotUnitCount;
        @SerializedName("spot_unit_value")
        @Expose
        private Integer spotUnitValue;

        public Integer getSpotId() {
            return spotId;
        }

        public void setSpotId(Integer spotId) {
            this.spotId = spotId;
        }

        public String getSpotName() {
            return spotName;
        }

        public void setSpotName(String spotName) {
            this.spotName = spotName;
        }

        public String getSpotUnit() {
            return spotUnit;
        }

        public void setSpotUnit(String spotUnit) {
            this.spotUnit = spotUnit;
        }

        public Integer getSpotUnitCount() {
            return spotUnitCount;
        }

        public void setSpotUnitCount(Integer spotUnitCount) {
            this.spotUnitCount = spotUnitCount;
        }

        public Integer getSpotUnitValue() {
            return spotUnitValue;
        }

        public void setSpotUnitValue(Integer spotUnitValue) {
            this.spotUnitValue = spotUnitValue;
        }

    }

}
