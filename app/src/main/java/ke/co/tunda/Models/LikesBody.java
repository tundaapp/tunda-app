

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ke.co.tunda.Constants.Extras;
import ke.co.tunda.utils.Constants;

public class LikesBody {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("show_age")
    @Expose
    private String showAge;
    @SerializedName("show_location")
    @Expose
    private String showLocation;
    @SerializedName("my_age")
    @Expose
    private Integer myAge;
    @SerializedName("interest")
    @Expose
    private String interest;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("dist_km")
    @Expose
    private Double distKm;
    @SerializedName("photo_path")
    @Expose
    private String photoPath;
    @SerializedName("photo_count")
    @Expose
    private Integer photoCount;


    @SerializedName("verification_status")
    @Expose
    private Integer verification_status;

    @SerializedName("likes")
    @Expose
    private Integer likes_count;

    @SerializedName(Extras.HAS_INSTAGRAM)
    @Expose
    private Integer has_insta_photo;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getShowAge() {
        return showAge;
    }

    public void setShowAge(String showAge) {
        this.showAge = showAge;
    }

    public String getShowLocation() {
        return showLocation;
    }

    public void setShowLocation(String showLocation) {
        this.showLocation = showLocation;
    }

    public Integer getMyAge() {
        return myAge;
    }

    public void setMyAge(Integer myAge) {
        this.myAge = myAge;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Double getDistKm() {
        return distKm;
    }

    public void setDistKm(Double distKm) {
        this.distKm = distKm;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public Integer getPhotoCount() {
        return photoCount;
    }

    public void setPhotoCount(Integer photoCount) {
        this.photoCount = photoCount;
    }

    public Integer getVerification_status() {
        return verification_status;
    }

    public void setVerification_status(Integer verification_status) {
        this.verification_status = verification_status;
    }

    public void setLikes_count(Integer likes_count) {
        this.likes_count = likes_count;
    }

    public Integer getLikes_count() {
        return likes_count;
    }

    public void setHas_insta_photo(Integer has_insta_photo) {
        this.has_insta_photo = has_insta_photo;
    }

    public Integer getHas_insta_photo() {
        return has_insta_photo;
    }
}
