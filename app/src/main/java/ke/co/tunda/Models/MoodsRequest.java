package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoodsRequest {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
