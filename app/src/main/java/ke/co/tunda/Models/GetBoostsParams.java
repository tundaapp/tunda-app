/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/13/19 3:19 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBoostsParams {

    @SerializedName("request_type")
    @Expose
    private String requestType;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

}
