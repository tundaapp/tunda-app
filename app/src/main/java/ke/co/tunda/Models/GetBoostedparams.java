/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 7/1/19 2:08 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBoostedparams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("page")
    @Expose
    private Integer page;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
