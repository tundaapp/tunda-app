/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/15/19 4:07 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayMpesaParams {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("amount")
    @Expose
    private int amount;
    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("payment_mode")
    @Expose
    private String payment_mode;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getPayment_mode() {
        return payment_mode;
    }
}
