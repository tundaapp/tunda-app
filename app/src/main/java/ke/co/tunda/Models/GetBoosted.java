/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 6/28/19 3:55 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBoosted {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("page")
    @Expose
    private Integer page;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}
