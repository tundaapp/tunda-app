

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserLikesModel {

    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("body")
    @Expose
    private List<UserLikesArray> body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<UserLikesArray> getBody() {
        return body;
    }

    public void setBody(List<UserLikesArray> body) {
        this.body = body;
    }




}
