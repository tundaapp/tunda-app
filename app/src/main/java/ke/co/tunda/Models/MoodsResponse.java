package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MoodsResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("body")
    @Expose
    private ArrayList<Body> body = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ArrayList<Body> getBody() {
        return body;
    }

    public void setBody(ArrayList<Body> body) {
        this.body = body;
    }

    public class Body {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("full_name")
        @Expose
        private String fullName;
        @SerializedName("mood_count")
        @Expose
        private Integer moodCount;
        @SerializedName("viewed_moods")
        @Expose
        private Integer viewedMoods;
        @SerializedName("createdon")
        @Expose
        private String createdon;
        @SerializedName("photo_path")
        @Expose
        private String photoPath;
        @SerializedName("user_moods")
        @Expose
        private ArrayList<Mood> userMoods = null;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public Integer getMoodCount() {
            return moodCount;
        }

        public void setMoodCount(Integer moodCount) {
            this.moodCount = moodCount;
        }

        public Integer getViewedMoods() {
            return viewedMoods;
        }

        public void setViewedMoods(Integer viewedMoods) {
            this.viewedMoods = viewedMoods;
        }

        public String getCreatedon() {
            return createdon;
        }

        public void setCreatedon(String createdon) {
            this.createdon = createdon;
        }

        public String getPhotoPath() {
            return photoPath;
        }

        public void setPhotoPath(String photoPath) {
            this.photoPath = photoPath;
        }

        public ArrayList<Mood> getUserMoods() {
            return userMoods;
        }

        public void setUserMoods(ArrayList<Mood> userMoods) {
            this.userMoods = userMoods;
        }

//        public class UserMood {
//
//            @SerializedName("time_posted")
//            @Expose
//            private String timePosted;
//            @SerializedName("mood_id")
//            @Expose
//            private Integer moodId;
//            @SerializedName("mood_type")
//            @Expose
//            private Integer moodType;
//            @SerializedName("mood_caption")
//            @Expose
//            private String moodCaption;
//            @SerializedName("mood_text")
//            @Expose
//            private String moodText;
//            @SerializedName("mood_status")
//            @Expose
//            private Integer moodStatus;
//            @SerializedName("view_count")
//            @Expose
//            private Integer viewCount;
//            @SerializedName("font_color")
//            @Expose
//            private Object fontColor;
//            @SerializedName("background_color")
//            @Expose
//            private Object backgroundColor;
//            @SerializedName("photo_path")
//            @Expose
//            private String photoPath;
//            @SerializedName("view_status")
//            @Expose
//            private Integer viewStatus;
//
//            public String getTimePosted() {
//                return timePosted;
//            }
//
//            public void setTimePosted(String timePosted) {
//                this.timePosted = timePosted;
//            }
//
//            public Integer getMoodId() {
//                return moodId;
//            }
//
//            public void setMoodId(Integer moodId) {
//                this.moodId = moodId;
//            }
//
//            public Integer getMoodType() {
//                return moodType;
//            }
//
//            public void setMoodType(Integer moodType) {
//                this.moodType = moodType;
//            }
//
//            public String getMoodCaption() {
//                return moodCaption;
//            }
//
//            public void setMoodCaption(String moodCaption) {
//                this.moodCaption = moodCaption;
//            }
//
//            public String getMoodText() {
//                return moodText;
//            }
//
//            public void setMoodText(String moodText) {
//                this.moodText = moodText;
//            }
//
//            public Integer getMoodStatus() {
//                return moodStatus;
//            }
//
//            public void setMoodStatus(Integer moodStatus) {
//                this.moodStatus = moodStatus;
//            }
//
//            public Integer getViewCount() {
//                return viewCount;
//            }
//
//            public void setViewCount(Integer viewCount) {
//                this.viewCount = viewCount;
//            }
//
//            public Object getFontColor() {
//                return fontColor;
//            }
//
//            public void setFontColor(Object fontColor) {
//                this.fontColor = fontColor;
//            }
//
//            public Object getBackgroundColor() {
//                return backgroundColor;
//            }
//
//            public void setBackgroundColor(Object backgroundColor) {
//                this.backgroundColor = backgroundColor;
//            }
//
//            public String getPhotoPath() {
//                return photoPath;
//            }
//
//            public void setPhotoPath(String photoPath) {
//                this.photoPath = photoPath;
//            }
//
//            public Integer getViewStatus() {
//                return viewStatus;
//            }
//
//            public void setViewStatus(Integer viewStatus) {
//                this.viewStatus = viewStatus;
//            }
//
//        }

    }
}
