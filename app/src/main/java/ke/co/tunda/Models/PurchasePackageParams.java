

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchasePackageParams {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("package_id")
    @Expose
    private int packageId;
    @SerializedName("package_months")
    @Expose
    private String packageMonths;

    @SerializedName("phone_number")
    @Expose
    private String phone;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getPackageMonths() {
        return packageMonths;
    }

    public void setPackageMonths(String packageMonths) {
        this.packageMonths = packageMonths;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
