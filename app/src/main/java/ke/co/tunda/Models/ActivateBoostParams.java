/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 7/9/19 3:57 PM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivateBoostParams {

    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("hours")
    @Expose
    private Integer hours;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }
}
