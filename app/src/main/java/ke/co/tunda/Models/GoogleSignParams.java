

package ke.co.tunda.Models;

public class GoogleSignParams {
    private String token;
    private String request_type;

    public void setRegToken(String regToken) {
        this.token = regToken;
    }

    public String getRegToken() {
        return token;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }


    public String getRequest_type() {
        return request_type;
    }
}
