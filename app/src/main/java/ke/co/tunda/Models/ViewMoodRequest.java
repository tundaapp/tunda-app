package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ViewMoodRequest {
    @SerializedName("request_type")
    @Expose
    private String requestType;
    @SerializedName("mood_id")
    @Expose
    private Integer moodId;
    @SerializedName("viewer_id")
    @Expose
    private Integer viewerId;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public Integer getMoodId() {
        return moodId;
    }

    public void setMoodId(Integer moodId) {
        this.moodId = moodId;
    }

    public Integer getViewerId() {
        return viewerId;
    }

    public void setViewerId(Integer viewerId) {
        this.viewerId = viewerId;
    }
}
