/*
 * Creator: Donbosco Muthiani on 8/23/19 8:47 AM Last modified: 5/3/19 11:54 AM Copyright: All rights reserved Ⓒ 2019
 */

package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteAccountResponse {

    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("body")
    @Expose
    String body;


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
