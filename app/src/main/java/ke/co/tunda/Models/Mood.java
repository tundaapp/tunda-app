package ke.co.tunda.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mood {
    @SerializedName("mood_id")
    @Expose
    private Integer moodId;
    @SerializedName("mood_type")
    @Expose
    private Integer moodType;
    @SerializedName("mood_caption")
    @Expose
    private String moodCaption;
    @SerializedName("mood_text")
    @Expose
    private String moodText;
    @SerializedName("mood_status")
    @Expose
    private Integer moodStatus;
    @SerializedName("view_count")
    @Expose
    private Integer viewCount;
    @SerializedName("font_color")
    @Expose
    private String fontColor;
    @SerializedName("background_color")
    @Expose
    private String backgroundColor;
    @SerializedName("photo_path")
    @Expose
    private String photoPath;
    @SerializedName("view_status")
    @Expose
    private Integer viewStatus;


    @SerializedName("time_posted")
    @Expose
    private String time_posted;

    public Integer getMoodId() {
        return moodId;
    }

    public void setMoodId(Integer moodId) {
        this.moodId = moodId;
    }

    public Integer getMoodType() {
        return moodType;
    }

    public void setMoodType(Integer moodType) {
        this.moodType = moodType;
    }

    public String getMoodCaption() {
        return moodCaption;
    }

    public void setMoodCaption(String moodCaption) {
        this.moodCaption = moodCaption;
    }

    public String getMoodText() {
        return moodText;
    }

    public void setMoodText(String moodText) {
        this.moodText = moodText;
    }

    public Integer getMoodStatus() {
        return moodStatus;
    }

    public void setMoodStatus(Integer moodStatus) {
        this.moodStatus = moodStatus;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public Integer getViewStatus() {
        return viewStatus;
    }

    public void setViewStatus(Integer viewStatus) {
        this.viewStatus = viewStatus;
    }

    public void setTime_posted(String time_posted) {
        this.time_posted = time_posted;
    }

    public String getTime_posted() {
        return time_posted;
    }
}
