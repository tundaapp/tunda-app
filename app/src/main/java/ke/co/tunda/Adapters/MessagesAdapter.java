

package ke.co.tunda.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.Callbacks.RetrySendingMessage;
import ke.co.tunda.ChatKit.ChatHistory;
import ke.co.tunda.R;

public class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int INCOMING = 0;
    private int OUTGOING = 1;
    private boolean showLoader;
    private static final String CHATS = "CHATS";
    private ArrayList<ChatHistory> msgs;
    private Context mContext;
    private int mUserId;
    private int size = 0;
    private final RetrySendingMessage mCallback;


    public MessagesAdapter(Context context, int UserId, RetrySendingMessage callback) {

        this.mContext = context;
        this.mUserId = UserId;
        this.mCallback = callback;
        msgs = new ArrayList<>();


    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Log.d(CHATS, "onCreateViewHolder: ");


        Log.d(CHATS, "view>>Holder " + 0);

        View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.incoming_text_message, viewGroup, false);
        return new Incoming(v1);


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        Log.d(CHATS, "onBindViewHolder: ");

        final ChatHistory chat = msgs.get(position);
        Log.d(CHATS, "onBindViewHolder: " + chat.getUserId());
        Log.d(CHATS, "user_id_from_chat: " + chat.getUserId());
        Log.d(CHATS, "user_id: " + mUserId);


        final Incoming incomingLayout = (Incoming) viewHolder;

        if (chat.getUserId() != mUserId) {
            incomingLayout.mIncoming.setVisibility(View.VISIBLE);
            incomingLayout.mOutgoing.setVisibility(View.GONE);
            String incmessage = chat.getMessage();
            Date incdate = chat.getDateCreated();
//            String inctime = getTime(incdate);
            incomingLayout.mMessageTextIncoming.setText(incmessage);

//            incomingLayout.mMessageTimeIncoming.setText(inctime);


        } else {
            String message = null;
            incomingLayout.mIncoming.setVisibility(View.GONE);
            incomingLayout.mOutgoing.setVisibility(View.VISIBLE);
            if (chat.getMessage() != null) {
                message = chat.getMessage();
                incomingLayout.mMessageTextOutgoing.setText(message);
            }

            if (chat.getmStatus() != null) {
                incomingLayout.mMsgStatus.setVisibility(View.VISIBLE);
                incomingLayout.mMsgStatus.setText(chat.getmStatus());
            }

            Date date = chat.getDateCreated();
//            String time = getTime(date);


//            incomingLayout.mMessageTimeOutgoing.setText(time);
        }


    }


    private String getTime(Date date) {
        int hrs = date.getHours();
        int minutes = date.getMinutes();

        long time = date.getTime();

        String mMinutes = String.valueOf(minutes);
        if (mMinutes.equals("0")) {
            mMinutes = "00";
        } else if (minutes < 10) {
            mMinutes = "0" + String.valueOf(minutes);
        }
        return String.valueOf(hrs) + ":" + mMinutes;
    }


    @Override
    public int getItemCount() {
        return msgs == null ? 0 : size;
    }


    public void addMessage(ChatHistory r) {

        msgs.add(r);
        size = msgs.size();
        notifyItemInserted(size - 1);


        Log.d(CHATS, "ADD:add: message" + r.getMessage());
        Log.d(CHATS, "ADD:add: user_ID" + r.getUserId());
        Log.d(CHATS, "spots Size= " + msgs.size());
        Log.d(CHATS, "ADD:unique id= " + r.getUniqueId());
        Log.d(CHATS, "ADD:status= " + r.getmStatus());
    }

    public void updateMessage(ChatHistory r) {

        ChatHistory chatHistory = new ChatHistory();
        chatHistory.setmStatus(r.getmStatus());
        chatHistory.setDateCreated(r.getDateCreated());
        chatHistory.setMessage(r.getMessage());
        chatHistory.setUniqueId(r.getUniqueId());
        chatHistory.setRecipientId(r.getRecipientId());
        chatHistory.setUserId(r.getUserId());

        int updateIndex = findIndex(msgs, r.getUniqueId());

        if (updateIndex > -1) {
            msgs.set(updateIndex, chatHistory);
            notifyItemChanged(updateIndex);
        }

    }


    public void add(ChatHistory r) {

        msgs.add(r);
        size = msgs.size();
        notifyItemInserted(msgs.size() - 1);
        Log.d(CHATS, "add: message" + r.getMessage());
        Log.d(CHATS, "add: user_ID" + r.getUserId());
        Log.d(CHATS, "spots Size= " + msgs.size());
    }

    public void addAll(ChatHistory[] chatHistories) {
        Log.d(CHATS, "Body Size " + chatHistories.length);

        int bodyLength = chatHistories.length;

        for (ChatHistory chatHistory : chatHistories) {

            add(chatHistory);
        }


    }


    class Incoming extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.incoming)
        RelativeLayout mIncoming;
        @BindView(R.id.outgoing)
        RelativeLayout mOutgoing;
        @BindView(R.id.messageTextIn)
        TextView mMessageTextIncoming;
        @BindView(R.id.messageTimeIn)
        TextView mMessageTimeIncoming;
        @BindView(R.id.messageTextOut)
        TextView mMessageTextOutgoing;
        @BindView(R.id.messageTimeOut)
        TextView mMessageTimeOutgoing;
        @BindView(R.id.msg_status)
        TextView mMsgStatus;


        Incoming(View v1) {

            super(v1);
            ButterKnife.bind(this, v1);
            mOutgoing.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.outgoing:
                    int position = getAdapterPosition();
                    if (position < 0 || position > msgs.size()) {
                        return;
                    } else {
                        if (msgs != null && !msgs.get(position).getmStatus().isEmpty() && !msgs.get(position).getmStatus().equals("sent")) {
                            mCallback.retrySendMessage(msgs.get(getAdapterPosition()));
                        } else {
                            return;
                        }
                    }


            }

        }
    }

    class Outgoing extends RecyclerView.ViewHolder {
        @BindView(R.id.messageTextOut)
        TextView mMessageTextOutgoing;
        @BindView(R.id.messageTimeOut)
        TextView mMessageTimeOutgoing;

        Outgoing(View v2) {
            super(v2);
            ButterKnife.bind(this, v2);
        }
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        showLoader = show;
        notifyItemChanged(msgs.size() - 1);


    }

    private static int findIndex(ArrayList<ChatHistory> arr, String t) {
        Log.d("TESTING", "findIndex: " + t);

        // if array is Null
        if (arr == null) {
            return 0;
        }

        // find length of array
        int len = arr.size();
        Log.d("TESTING", "arraySize==: " + len);
//        int i = 0;

        for (int i = 0; i < len; i++) {
            Log.d("TESTING", "findIndex:index= " + i);
            if (arr.get(i).getUniqueId() != null) {
                if (arr.get(i).getUniqueId().equals(t)) {
                    Log.d("TESTING", "findIndex:index=success " + i);
                    return i;
                } else {
                    Log.d("TESTING", "findIndex:index=fail " + i);
                }
            } else {
                Log.d("TESTING", "findIndex:incrementing= uniqueID null");
            }
        }

        Log.d("TESTING", "findIndex:returning -1= not found");
        return -1;

    }
}
