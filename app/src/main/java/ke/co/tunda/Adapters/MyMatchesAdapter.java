

package ke.co.tunda.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.Callbacks.OpenMatchCallback;

import com.bumptech.glide.Glide;
import ke.co.tunda.Models.MyMatches;
import ke.co.tunda.R;

public class MyMatchesAdapter extends RecyclerView.Adapter<MyMatchesAdapter.ItsAMatchHolder> {
    private ArrayList<MyMatches> myMatches;
    private Context context;
    private OpenMatchCallback callback;
    private RecyclerView parentRecycler;

    public MyMatchesAdapter(Context mContext, OpenMatchCallback mCallback) {
        this.context = mContext;
        myMatches = new ArrayList<>();
        this.callback = mCallback;
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        parentRecycler = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ItsAMatchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ItsAMatchHolder viewHolder = null;

        View view = LayoutInflater.from(context).inflate(
                R.layout.item_my_matches, viewGroup, false);
        viewHolder = new ItsAMatchHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ItsAMatchHolder xviewHolder, int position) {

        MyMatches current = myMatches.get(position);


        if (current.getFullName() != null) {
            Glide.with(context)
                    .load(current.getPhotoPath())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(xviewHolder.imageView);

            xviewHolder.textView.setText(current.getFullName());

        }

        if (current.getViewStatus() != null && current.getViewStatus() == 0) {
            xviewHolder.mIsViewedIndicator.animate().alpha(1.0f);
            xviewHolder.mIsViewedIndicator.setVisibility(View.VISIBLE);
        } else {
            xviewHolder.mIsViewedIndicator.animate().alpha(0.1f);
            xviewHolder.mIsViewedIndicator.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return myMatches.size();
    }


    public void add(MyMatches r) {
        myMatches.add(r);
        notifyItemInserted(myMatches.size() - 1);
    }

    public void addAll(MyMatches[] matches) {
        for (MyMatches result : matches) {
            Log.d("TESTING", "addAll: Ids" + result.getUser_id());
            add(result);
        }
    }


    @Override
    public int getItemViewType(int position) {
        Log.d("HTTP", "getItemViewType: ");
        return position;

    }


    public MyMatches getItem(int position) {
        return myMatches.get(position);
    }


    public class ItsAMatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.my_match_photo)
        CircleImageView imageView;
        @BindView(R.id.my_match_name)
        TextView textView;
        @BindView(R.id.myMatchesContainer)
        RelativeLayout myMatchesContainer;
        @BindView(R.id.isViewedIndicator)
        ImageView mIsViewedIndicator;

        ItsAMatchHolder(View v3) {
            super(v3);

            ButterKnife.bind(this, v3);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            String imageUrl = sp.getString("image", "");
            myMatchesContainer.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.myMatchesContainer:
                    try {
                        parentRecycler.smoothScrollToPosition(getAdapterPosition());
                        callback.openMatch(myMatches.get(getAdapterPosition()),imageView);
                    } catch (Exception e) {
                        Log.d("TEST", e.getMessage());
                    }

                    break;


            }

        }

        public void showText() {
            int parentHeight = ((View) imageView.getParent()).getHeight();
            float scale = (parentHeight - textView.getHeight()) / (float) imageView.getHeight();
            imageView.setPivotX(imageView.getWidth() * 0.4f);
            imageView.setPivotY(0);
            imageView.animate().scaleX(scale)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            textView.setVisibility(View.VISIBLE);
//                            imageView.setColorFilter(Color.BLACK);
                        }
                    })
                    .scaleY(scale).setDuration(100)
                    .start();
        }

        public void hideText() {
            textView.setVisibility(View.INVISIBLE);
            imageView.animate().scaleX(1f).scaleY(1f)
                    .setDuration(200)
                    .start();
        }


    }

    public void clear() {

        int size = myMatches.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                myMatches.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }
//        while (myMatches.size() > 0) {
////            Log.d("TESTING", "Size= " + spots.size());
//            remove(getItem(0));
//        }
    }

    private void remove(MyMatches r) {
        int position = myMatches.indexOf(r);
        if (position > -1) {
            myMatches.remove(position);
            Log.d("CN", "remove: "+r.getFullName());

            notifyItemRemoved(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




}
