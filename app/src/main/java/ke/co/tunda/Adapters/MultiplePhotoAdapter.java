

package ke.co.tunda.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.danimahardhika.android.helpers.animation.AnimationHelper;
import com.danimahardhika.android.helpers.core.ColorHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.palette.graphics.Palette;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.PagerAdapter;
import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.Helpers.ImagePres;
import ke.co.tunda.R;

import static ke.co.tunda.Adapters.CardStackAdapter.TAG;

public class MultiplePhotoAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<String> mPhotos;
    private ArrayList<String> mPhotosToSize;
    private ArrayList<Drawable> mBackgrounds;


    public MultiplePhotoAdapter(Context context) {

        this.context = context;
        mPhotos = new ArrayList<>();
        mPhotosToSize = new ArrayList<>();
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        mBackgrounds = new ArrayList<>();
        if (mBackgrounds.size() > 0) {
            mBackgrounds.clear();
        }


    }


    @Override
    public int getCount() {
        return mPhotosToSize.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.photo_slide_layout, container, false);
        ImageView slideImageView = view.findViewById(R.id.photo_slide_image);
        CircularProgressIndicator mCircularProgress = view.findViewById(R.id.circular_progress);

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setCenterRadius(45f);
        circularProgressDrawable.setColorSchemeColors(context.getResources().getColor(R.color.colorPrimary));
        circularProgressDrawable.setStrokeWidth(context.getResources().getDimension(R.dimen.drawable_ring_size));
        circularProgressDrawable.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        circularProgressDrawable.start();


//        Glide.with(context)
//                .load(mPhotosToSize.get(position))
//                .apply(new RequestOptions().placeholder(circularProgressDrawable))
//                .into(slideImageView);

        String url = mPhotosToSize.get(position);
        url = url.replace(" ", "");


        DisplayImageOptions.Builder options = ImagePres.getRawDefaultImageOptions();

        ImageLoader.getInstance().handleSlowNetwork(true);

        ImageLoader.getInstance().displayImage(url, slideImageView, options.build(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                AnimationHelper.show(mCircularProgress).start();

            }


            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                AnimationHelper.fade(mCircularProgress).start();
                mCircularProgress.setVisibility(View.GONE);


            }


            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                AnimationHelper.fade(mCircularProgress).start();
                mCircularProgress.setVisibility(View.GONE);


                if (loadedImage != null) {
                    Palette.from(loadedImage).generate(palette -> {

                        int accent = ColorHelper.getAttributeColor(
                                context, R.attr.colorAccent);
                        int color = palette.getVibrantColor(accent);
                        if (color == accent)
                            color = palette.getMutedColor(accent);
                        GradientDrawable gd = new GradientDrawable(
                                GradientDrawable.Orientation.TOP_BOTTOM,
                                new int[]{context.getResources().getColor(R.color.transparent), color});
                        gd.setCornerRadius(8f);
                        onwallpaperLoaded(gd);


                    });
                }

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                AnimationHelper.fade(mCircularProgress).start();
                mCircularProgress.setVisibility(View.GONE);


            }
        }, (ImageLoadingProgressListener) (imageUri, viewx, current, total) -> {
            if ((total != 0) && (current != 0)) {
                mCircularProgress.setCurrentProgress(Math.round(100.0f * current / total));
                mCircularProgress.setOnProgressChangeListener(new CircularProgressIndicator.OnProgressChangeListener() {
                    @Override
                    public void onProgressChanged(double progress, double maxProgress) {
                        Log.d(TAG, String.format("Current: %1$.0f, max: %2$.0f", progress, maxProgress));
                        if (progress == maxProgress) {
                            AnimationHelper.fade(mCircularProgress).start();
                            mCircularProgress.setVisibility(View.GONE);
                        }

                    }
                });
            }

        });


//        slideImageView.setImageResource(slide_images[position]);

        container.addView(view);

        return view;
    }

    private void onwallpaperLoaded(GradientDrawable gd) {
        Log.d("TEST", "onwallpaperLoaded: ");

        if (gd != null) {
            mBackgrounds.add(gd);
        }

        if (FullProfile.isBackgroundnull() && gd != null) {
            FullProfile.setBackground(gd);
        }
        if (mBackgrounds.size() == 1) {
            FullProfile.setBackground(mBackgrounds.get(0));
        }
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }

    public void addPhotos(ArrayList<String> photo) {
        Log.d("TEST", "addPhotos:size " + photo.size());
        mPhotos.clear();
        mPhotos.addAll(photo);
        mPhotosToSize.addAll(photo);
        Log.d("TEST", "addPhotos:new size " + mPhotosToSize.size());

        notifyDataSetChanged();
    }

    public Drawable getBackground(int i) {
        Log.d("TEST", "getBackground: ");
        return mBackgrounds.size() <= 0 || mBackgrounds.size() <= i ? null : mBackgrounds.get(i);
    }
}
