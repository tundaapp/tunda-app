

package ke.co.tunda.Adapters;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.Activities.UserLikes;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.Callbacks.LoadUserLike;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;

import com.bumptech.glide.Glide;
import ke.co.tunda.Models.UserLikesArray;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Swipe;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class UserLikesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private static final String TAG = "USA";
    private List<UserLikesArray> mItems;
    final LoadUserLike mCallback;
    final SendLikeSwipeDataInterface mCallback2;

    private final static int VIEW_NORMAL = 1;
    private final static int VIEW_EMPTY = 2;
    private SharedPreferences sp;
    private int user_id;
    private int package_id;
    private boolean show_controls = false;
    private ArrayList<Swipe> mSwipeArray;


    public UserLikesAdapter(Context context, LoadUserLike callback,SendLikeSwipeDataInterface callback2) {

        mContext = context;
        mItems = new ArrayList<>();
        this.mCallback = callback;
        this.mCallback2 = callback2;
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        user_id = sp.getInt("user_id", 0);
        package_id = sp.getInt("package_id", 1);
        mSwipeArray = new ArrayList<>();

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder mV;
        switch (viewType) {
            case VIEW_EMPTY:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_placeholder, parent, false);
                mV = new ViewEmpty(v);
                break;
            case VIEW_NORMAL:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_like, parent, false);
                mV = new ViewHolderTopPicks(v);
                break;
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_like, parent, false);
                mV = new ViewHolderTopPicks(v);
                break;

        }


        return mV;


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder xviewHolder, int position) {

        if (xviewHolder.getItemViewType() == VIEW_NORMAL) {
            UserLikesArray spot = mItems.get(position);

            try {

                final ViewHolderTopPicks holder = (ViewHolderTopPicks) xviewHolder;
                String distance_to_here = "";


                if (spot.getFullName() != null) {


                    if (spot.getVerificationStatus() != null && spot.getVerificationStatus() == 1) {
                        holder.name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_verified, 0, 0, 0);

                    } else {
                        holder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    }

                    if(spot.getMatchStatus()!=null && spot.getMatchStatus()==1){
                        holder.mItsAMatch.setVisibility(View.VISIBLE);
                    }else if(spot.getMatchStatus()!=null && spot.getMatchStatus()==0) {
                        holder.mItsAMatch.setVisibility(View.GONE);
                    }


                    if (spot.getDistKm() != null) {
                        holder.distance.setVisibility(View.VISIBLE);

                        Double away = spot.getDistKm();
                        if (away < 1) {
                            distance_to_here = "Less than a km away";
                            holder.distance.setText(distance_to_here);

                        } else {

                            int dstnc = away.intValue();
                            distance_to_here = String.valueOf(dstnc) + " km away";
                            holder.distance.setText(distance_to_here);
                        }


                    } else {
                        holder.distance.setVisibility(View.GONE);
                    }

                    if (spot.getLikes_count() != null && spot.getLikes_count() > 0) {
                        holder.mNumberOfLikes.setVisibility(View.VISIBLE);
                        holder.mNumberOfLikes.setText(String.valueOf(spot.getLikes_count()));
                    } else {
                        holder.mNumberOfLikes.setVisibility(View.GONE);
                    }

                    String name = spot.getFullName();
                    if (name.contains(" ")) {
                        String[] arr = name.split(" ", 2);
                        name = arr[0];
                    }


                    if (spot.getMyAge() != null) {
                        holder.name.setText(mContext.getResources().getString(R.string.spot_name, name, String.valueOf(spot.getMyAge())));
                    } else {
                        holder.name.setText(mContext.getResources().getString(R.string.spot_name, name, ""));
                    }


                    CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
                    circularProgressDrawable.setCenterRadius(45f);
                    circularProgressDrawable.setColorSchemeColors(mContext.getResources().getColor(R.color.colorPrimary));
                    circularProgressDrawable.setStrokeWidth(mContext.getResources().getDimension(R.dimen.drawable_ring_size));
                    circularProgressDrawable.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
                    circularProgressDrawable.start();


                    if(package_id<=1){
                        Glide.with(mContext).load(spot.getPhotoPath())
                                .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                .into(holder.image);
                        holder.name.setVisibility(View.GONE);
                    }else {
                        holder.name.setVisibility(View.VISIBLE);
                        Glide.with(mContext)
                                .load(spot.getPhotoPath())
                                .apply(new RequestOptions().placeholder(circularProgressDrawable))
                                .into(holder.image);
                    }


                }


            } catch (Exception e) {
                Log.d(TAG, "onBindViewHolder: " + e.getLocalizedMessage());
            }

        }

    }

    @Override
    public int getItemCount() {


        return mItems.size();


    }


    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position).getFullName() == null) {
            return VIEW_EMPTY;
        } else return VIEW_NORMAL;
    }

    class ViewHolderTopPicks extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.item_name)
        TextView name;
        @BindView(R.id.item_image)
        ImageView image;
        @BindView(R.id.item_distance)
        TextView distance;
        @BindView(R.id.card_spot)
        CardView mCardSpot;
        @BindView(R.id.number_of_likes)
        TextView mNumberOfLikes;
        @BindView(R.id.its_a_match)
        ImageView mItsAMatch;


        public ViewHolderTopPicks(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mCardSpot.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int dstnc;
            UserLikesArray spot = mItems.get(getAdapterPosition());


            if (spot.getDistKm() != null) {

                Double away = spot.getDistKm();
                dstnc = away.intValue();
            } else {
                dstnc = -1;
            }

            boolean hasMultipleImages = false;
            if (spot.getPhotoCount() != null) {
                hasMultipleImages = spot.getPhotoCount() > 1;
            }

            if(spot.getMatchStatus()!=null && spot.getMatchStatus()==1){
                show_controls = false;
            }else if(spot.getMatchStatus()!=null && spot.getMatchStatus()==0) {
                show_controls = true;
            }

            boolean has_insta_photo;
            has_insta_photo = spot.getHas_insta_photo() != null && spot.getHas_insta_photo() == 1;


            Dialog dialog = new Dialog(String.valueOf(user_id),
                    spot.getFullName(), spot.getPhotoPath(), null, null, 0,
                    String.valueOf(spot.getUserId()), spot.getDob(),
                    String.valueOf(dstnc), spot.getAboutMe(), spot.getJobTitle(),
                    spot.getSchool(), spot.getPhotoCount(), spot.getInterest(),
                    spot.getVerificationStatus(), spot.getLikes_count(),has_insta_photo);
            mCallback.loadUserLike(dialog,hasMultipleImages,show_controls,getAdapterPosition());




        }
    }


    public void addLikes(List<UserLikesArray> mUserLikes) {

        Log.d(TAG, "setTags: size= " + mUserLikes.size());


        if (mUserLikes.size() > 0) {
            if (mItems != null) {
                clearAll();
                mItems.addAll(mUserLikes);

                notifyItemInserted(mItems.size() - 1);
            }
        }


    }

    public void clearAll() {

        int size = mItems.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                mItems.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }

    }

    private class ViewEmpty extends RecyclerView.ViewHolder {
        public ViewEmpty(View v) {
            super(v);
        }
    }

    private UserLikesArray getItem(int position) {

        return mItems.get(position);


    }

    public void skipUser(int position) {
        UserLikesArray item = getItem(position);

        if (item != null) {
            Log.d("posin", "removing: " + position);
            Log.d("posin", "removed: " + item.getFullName());
            sendSwipe("DISLIKE", item);


        }

    }

    private void sendSwipe(String action, UserLikesArray item) {
        if (mSwipeArray.size() > 0) {
            mSwipeArray.clear();
        }
        Swipe swipe = new Swipe();
        swipe.setSwipe_action(action);
        swipe.setRecipient_id(item.getUserId());
        mSwipeArray.add(swipe);
        mCallback2.sendLikeSwipes(user_id, mSwipeArray);
    }

    public void superLikeUser(int position) {
        UserLikesArray item = getItem(position);

        if (item != null) {
            sendSwipe("SUPERLIKE", item);


        }
    }

    public void likeUser(int position) {
        UserLikesArray item = getItem(position);

        if (item != null) {
            sendSwipe("LIKE", item);


        }
    }
}
