

package ke.co.tunda.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.wang.avi.AVLoadingIndicatorView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.ChatKit.CustomHolderMessagesActivity;
import ke.co.tunda.Constants.Extras;

import com.bumptech.glide.Glide;
import ke.co.tunda.Models.HeaderItem;
import ke.co.tunda.Models.LikesBody;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Swipe;
import ke.co.tunda.fragments.Dialogs.TundaPlusBuy;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

public class LikesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static List<LikesBody> mTopPickList;
    private Context mContext;
    public static final int HEADER = 0;
    public static final int TOP_PICKS = 1;
    public static final int LOADING = 2;
    private static final String TAG = "TPA";
    private List<HeaderItem> mHeaderItems;
    public boolean isLoaderVisible = false;
    public boolean isPaginationFailedShown = false;
    private String errorMsg;
    private int user_id;
    final SendLikeSwipeDataInterface mCallbackSwipe;
    private ArrayList<Swipe> mSwipeArray;
    private SharedPreferences sp;
    private int package_id;

    public LikesAdapter(Context context, List<HeaderItem> items, SendLikeSwipeDataInterface callbackSwipe, int user_id) {

        mContext = context;
        mHeaderItems = items;
        mTopPickList = new ArrayList<>();
        this.mCallbackSwipe = callbackSwipe;
        mSwipeArray = new ArrayList<>();
        this.user_id = user_id;
        sp = PreferenceManager.getDefaultSharedPreferences(mContext);
        package_id  = sp.getInt("package_id",0);

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder mV;

        switch (viewType) {
            case HEADER:
                // inflate header view
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_header, parent, false);
                mV = new ViewHolderHeader(v);
                break;
            case TOP_PICKS:
                // inflate main content view
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_likes, parent, false);
                mV = new ViewHolderTopPicks(v);
                break;

            case LOADING:
                Log.d(TAG, "onCreateViewHolder: isloading");
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_data, parent, false);
                mV = new ViewHolderLoading(v);
                break;

            default:
                // inflate main content view
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_picks, parent, false);
                mV = new ViewHolderTopPicks(v);
        }
        return mV;


//        if (viewType == TOP_PICKS) {
//            View x = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_picks, parent, false);
//            return new ViewHolderTopPicks(x);
//
//        }
//
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_header, parent, false);
//        return new ViewHolderHeader(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
//        try {
//            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams)
//                    viewHolder.itemView.getLayoutParams();
//            layoutParams.setFullSpan(getItemViewType(position) == HEADER);
//
//        } catch (Exception e) {
//            Log.d(TAG, "onBindViewHolder: ");
//        }

        if (viewHolder.getItemViewType() == HEADER) {
//            HeaderItem item = mHeaderItems.get(position);

            ((ViewHolderHeader) viewHolder).mSub.setHtml(mHeaderItems.get(0).getDescription());
            ((ViewHolderHeader) viewHolder).mHeaderTitle.setText(mHeaderItems.get(0).getHeader());


        } else if (viewHolder.getItemViewType() == TOP_PICKS) {
            Log.d(TAG, "onBindViewHolder: top picks" +
                    position
            );
            LikesBody mTopPicksClass = getItem(position - 1);
            if (mTopPicksClass != null) {
                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
                circularProgressDrawable.setCenterRadius(45f);
                circularProgressDrawable.setColorSchemeColors(mContext.getResources().getColor(R.color.colorPrimary));
                circularProgressDrawable.setStrokeWidth(mContext.getResources().getDimension(R.dimen.drawable_ring_size));
                circularProgressDrawable.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
                circularProgressDrawable.start();
                if (mTopPicksClass.getPhotoPath() != null && !mTopPicksClass.getPhotoPath().isEmpty()) {

                    if(package_id<=1){
                        Glide.with(mContext).load(mTopPicksClass.getPhotoPath())
                                .apply(bitmapTransform(new BlurTransformation(25, 3)))
                                .into(((ViewHolderTopPicks) viewHolder).mImage);
                        ((ViewHolderTopPicks) viewHolder).mName.setVisibility(View.GONE);
                    }else {
                        ((ViewHolderTopPicks) viewHolder).mName.setVisibility(View.VISIBLE);
                        Glide.with(mContext)
                                .load(mTopPicksClass.getPhotoPath())
                                .apply(new RequestOptions().placeholder(circularProgressDrawable))
                                .into(((ViewHolderTopPicks) viewHolder).mImage);
                    }

                }

                String name = mTopPicksClass.getFullName() != null ? mTopPicksClass.getFullName() : "";
                if (name != null && !name.isEmpty() && name.contains(" ")) {
                    String[] arr = name.split(" ");
                    name = arr[0];
                }
                int dstnc;

                if (mTopPicksClass.getDistKm() != null) {
                    Double away = mTopPicksClass.getDistKm();
                    dstnc = away.intValue();
                } else {
                    dstnc = 0;
                }

                if (String.valueOf(dstnc).contains(".")) {
                    Double distance = Double.parseDouble(String.valueOf(dstnc));
                    int y = (int) Math.round(distance);
                    if (y < 1) {
                        ((ViewHolderTopPicks) viewHolder).mDistance.setText(mContext.getResources().getString(R.string.less_than_km_away));
                    } else {
                        ((ViewHolderTopPicks) viewHolder).mDistance.setText(mContext.getResources().getString(R.string.km_away,String.valueOf(y)));
                    }

                } else {
                    int y = Integer.valueOf(String.valueOf(dstnc));
                    if (y < 1) {
                        ((ViewHolderTopPicks) viewHolder).mDistance.setText(mContext.getResources().getString(R.string.less_than_km_away));
                    } else {
                        ((ViewHolderTopPicks) viewHolder).mDistance.setText(mContext.getResources().getString(R.string.km_away,String.valueOf(y)));
                    }


                }

                if (name != null && !name.isEmpty() && name.contains(" ")) {
                    String[] arr = name.split(" ");
                    name = arr[0];
                }

                if (mTopPicksClass.getLikes_count() != null && mTopPicksClass.getLikes_count() > 0) {
                    ((ViewHolderTopPicks) viewHolder).mLikesHolder.setVisibility(View.VISIBLE);
                    ((ViewHolderTopPicks) viewHolder).mNumberOfLikes.setText(String.valueOf(mTopPicksClass.getLikes_count()));
                } else {
                    ((ViewHolderTopPicks) viewHolder).mLikesHolder.setVisibility(View.GONE);
                }



                if (mTopPicksClass.getMyAge() != null) {
                    ((ViewHolderTopPicks) viewHolder).mName.setText(mContext.getResources().getString(R.string.name_age, name, String.valueOf(mTopPicksClass.getMyAge())));

                } else {
                    ((ViewHolderTopPicks) viewHolder).mName.setText(mContext.getResources().getString(R.string.name_noage, name));

                }


            }


        } else if (viewHolder.getItemViewType() == LOADING) {

            if (isPaginationFailedShown) {
                ((ViewHolderLoading) viewHolder).mSpinner.setVisibility(View.GONE);
                ((ViewHolderLoading) viewHolder).mFailHolder.setVisibility(View.VISIBLE);
                ((ViewHolderLoading) viewHolder).mFailReason.setText(errorMsg != null ? errorMsg : "Erro loading Data");


            } else {
                ((ViewHolderLoading) viewHolder).mSpinner.setVisibility(View.VISIBLE);
                ((ViewHolderLoading) viewHolder).mFailHolder.setVisibility(View.GONE);

            }

        } else {
            Log.d(TAG, "onBind\"no instance of viewholder found\"ViewHolder: ");
        }


    }

    @Override
    public int getItemCount() {


        if (mHeaderItems == null) {
//            Log.d(TAG, "getItemCount: 1" + mTopPickList.size());
            return mTopPickList.size();
        } else {
//            Log.d(TAG, "getItemCount: 2" + mTopPickList.size() + 1);

            return mTopPickList.size() + 1;
        }


    }

    public void addLoading() {
        isLoaderVisible = true;
        add(new LikesBody());
    }


    public void removeLoading() {

        if (isLoaderVisible) {

            isLoaderVisible = false;
            int position = mTopPickList.size() - 1;
            LikesBody item = getItem(position);

            if (item != null) {
                mTopPickList.remove(position);
                notifyItemRemoved(position);

            }
        }


    }

    public void tryLoadingAgain(boolean show, String errorMsg) {
        Log.d(TAG, "tryLoadingAgain: ");
        isPaginationFailedShown = show;
        notifyItemChanged(mTopPickList.size());
        this.errorMsg = errorMsg;


    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private LikesBody getItem(int position) {

        return mTopPickList.get(position);


    }


    class ViewHolderTopPicks extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image)
        ImageView mImage;
        @BindView(R.id.name)
        TextView mName;
        @BindView(R.id.user_package)
        ImageView mUserPackage;
        @BindView(R.id.card_top_pick)
        CardView mCard;
        @BindView(R.id.distance)
        TextView mDistance;
        @BindView(R.id.number_of_likes)
        TextView mNumberOfLikes;
        @BindView(R.id.likes_holder)
        LinearLayout mLikesHolder;

        public ViewHolderTopPicks(View v) {
            super(v);
            ButterKnife.bind(this, v);
            mCard.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.card_top_pick:
                    if(package_id<=1){
                        TundaPlusBuy.open(mContext);
                        return;
                    }
                    int mPosition = getAdapterPosition() - 1;
                    LikesBody spot = mTopPickList.get(mPosition);
                    Log.d("posin", "onClick: " + spot.getFullName());
                    int dstnc;


                    if (spot.getDistKm() != null) {
                        Double away = spot.getDistKm();
                        dstnc = away.intValue();
                    } else {
                        dstnc = 0;
                    }

                    boolean hasMultipleImages = false;
                    if (spot.getPhotoCount() != null) {
                        hasMultipleImages = spot.getPhotoCount() > 1;
                    }

                    boolean has_insta_photo = false;
                    if(spot.getHas_insta_photo()!=null&&spot.getHas_insta_photo()==1){
                        has_insta_photo = true;
                    }else {
                        has_insta_photo = false;
                    }


                    Dialog dialog = new Dialog(String.valueOf(spot.getUserId()),
                            spot.getFullName(), spot.getPhotoPath(), null, null, 0,
                            String.valueOf(spot.getUserId()), spot.getDob(),
                            String.valueOf(dstnc), spot.getAboutMe(), spot.getJobTitle(),
                            spot.getSchool(), spot.getPhotoCount(),
                            spot.getInterest(), spot.getVerification_status(),spot.getLikes_count(),has_insta_photo);
                    try {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            ActivityOptions options = ActivityOptions
                                    .makeSceneTransitionAnimation((Activity) mContext, mImage, "match_photo");
                            FullProfile.open(mContext, dialog, hasMultipleImages, true, Extras.ORIGIN_LIKES, mPosition,options);


                        }else{
                            FullProfile.open(mContext, dialog, hasMultipleImages, true, Extras.ORIGIN_LIKES, mPosition,null);

                        }

                    } catch (Exception e) {
                        Log.d("TEST", e.getMessage());
                    }
                    break;
            }

        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        @BindView(R.id.top_pics_sub)
        HtmlTextView mSub;
        @BindView(R.id.top_picks_title)
        TextView mHeaderTitle;


        public ViewHolderHeader(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        } else {
            if (isLoaderVisible) {

                return position == mTopPickList.size() ? LOADING : TOP_PICKS;


            } else {

                return TOP_PICKS;

            }
        }


    }

    public void add(LikesBody r) {
        Log.d(TAG, "add: size = " + mTopPickList.size());


        mTopPickList.add(r);
        notifyItemInserted(mTopPickList.size() - 1);

//                publishProgress();


    }

    public void addAll(LikesBody[] Items) {


        for (LikesBody item : Items) {

            add(item);

        }
    }

    public void addHeader(List<HeaderItem> mHeader) {

        Log.d(TAG, "setTags: size= " + mHeader.size());


        if (mHeader.size() > 0) {
            if (mHeaderItems != null) {
                mHeaderItems.addAll(mHeader);
                notifyItemInserted(mHeader.size() - 1);
            }
        }


    }


    class ViewHolderLoading extends RecyclerView.ViewHolder {
        @BindView(R.id.spin_kit)
        AVLoadingIndicatorView mSpinner;
        @BindView(R.id.try_again)
        TextView mTryAgain;
        @BindView(R.id.fail_reason)
        TextView mFailReason;
        @BindView(R.id.paging_fail_holder)
        RelativeLayout mFailHolder;

        public ViewHolderLoading(View v) {

            super(v);
            ButterKnife.bind(this, v);

        }
    }


//    private class InsertAsyncTask extends AsyncTask<TopPicksClass, Void, Void> {
//        List<TopPicksClass> mArray;
//
//        InsertAsyncTask(List<TopPicksClass> mList) {
//            mArray = mList;
//
//        }
//
//        @Override
//        protected Void doInBackground(TopPicksClass... topPicksClasses) {
//
//            if (topPicksClasses[0] != null) {
//                try {
//                    if (!containsTopPick(mArray, topPicksClasses[0].user_id)) {
//                        Log.d(TAG, "doInBackground: id = " + topPicksClasses[0].user_id);
//                        mTopPickList.add(topPicksClasses[0]);
//
//                        publishProgress();
//
//
//                    } else {
//                        Log.d(TAG, "doInBackground: id null");
//                    }
//
//                } catch (Exception e) {
//                    Log.d(TAG, "doInBackground: " + e.getMessage());
//                }
//            } else {
//                Log.d(TAG, "doInBackground: its null");
//            }
//
//
//            return null;
//
//
//        }
//
//
//        @Override
//        protected void onProgressUpdate(Void... values) {
//            notifyItemInserted(mTopPickList.size() - 1);
//        }
//    }


    private static boolean containsTopPick(List<Body> mTops, int id_user) {


        for (int i = 0; i < mTops.size(); i++) {
            if (mTops.get(i).getUserId() != null) {
                if (mTops.get(i).getUserId() == id_user) {
                    return true;
                }
            }

        }
//        for (TopPicksClass ti : mTops) {
//            if (ti.user_id == id_user) {
//                return true;
//            }
//
//
//        }
        return false;
    }


    public void skipUser(int position) {
        LikesBody item = getItem(position);

        if (item != null) {
            Log.d("posin", "removing: " + position);
            Log.d("posin", "removed: " + item.getFullName());
            sendSwipe("DISLIKE", item);

            mTopPickList.remove(position);
            notifyItemRemoved(position);

            notifyItemRangeChanged(position, getItemCount() - position);

        }

    }

    private void sendSwipe(String action, LikesBody item) {
        if (mSwipeArray.size() > 0) {
            mSwipeArray.clear();
        }
        Swipe swipe = new Swipe();
        swipe.setSwipe_action(action);
        swipe.setRecipient_id(item.getUserId());
        mSwipeArray.add(swipe);
        mCallbackSwipe.sendLikeSwipes(user_id, mSwipeArray);
    }

    public void superLikeUser(int position) {
        LikesBody item = getItem(position);

        if (item != null) {
            sendSwipe("SUPERLIKE", item);
            mTopPickList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, getItemCount() - position);

        }
    }

    public void likeUser(int position) {
        LikesBody item = getItem(position);

        if (item != null) {
            sendSwipe("LIKE", item);
            mTopPickList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, getItemCount() - position);

        }
    }
}
