

package ke.co.tunda.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.danimahardhika.android.helpers.animation.AnimationHelper;
import com.danimahardhika.android.helpers.core.ColorHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;
import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.ApiConnector.Models.GetMyMoodResponse;
import ke.co.tunda.ApiConnector.Models.Progress;
import ke.co.tunda.Helpers.ImagePres;
import ke.co.tunda.Models.Mood;
import ke.co.tunda.R;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

import static ke.co.tunda.Adapters.CardStackAdapter.TAG;

public class StoriesViewPagerAdapter extends RecyclerView.Adapter {
    Context context;
    LayoutInflater layoutInflater;
    private ArrayList<Mood> mPhotosToSize;
    private ArrayList<Drawable> mBackgrounds;
    private List<Progress> mHeaderItems;


    private static final int VIEW_PROGRESS = 1;
    private static final int VIEW_MOOD = 2;


    public StoriesViewPagerAdapter(Context context) {

        this.context = context;
        mPhotosToSize = new ArrayList<>();
        mHeaderItems = new ArrayList<>();
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        mBackgrounds = new ArrayList<>();
        if (mBackgrounds.size() > 0) {
            mBackgrounds.clear();
        }


    }


    private void onwallpaperLoaded(GradientDrawable gd) {
        Log.d("TEST", "onwallpaperLoaded: ");

        if (gd != null) {
            mBackgrounds.add(gd);
        }


        if (FullProfile.isBackgroundnull() && gd != null) {
            FullProfile.setBackground(gd);
        }
        if (mBackgrounds.size() == 1) {
            FullProfile.setBackground(mBackgrounds.get(0));
        }
    }




    public Drawable getBackground(int i) {
        Log.d("TEST", "getBackground: ");
        return mBackgrounds.size() <= 0 || mBackgrounds.size() <= i ? null : mBackgrounds.get(i);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        RecyclerView.ViewHolder mV;


        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stories_slide_layout, parent, false);
        mV = new ViewHolderX(v);

        return mV;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        ViewHolderX holderX = (ViewHolderX) holder;

        String url = mPhotosToSize.get(position).getPhotoPath();
        url = url.replace(" ", "");


        DisplayImageOptions.Builder options = ImagePres.getRawDefaultImageOptions();

        ImageLoader.getInstance().handleSlowNetwork(true);

        ImageLoader.getInstance().displayImage(url, holderX.slideImageView, options.build(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                AnimationHelper.show(holderX.mCircularProgress).start();

            }


            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                AnimationHelper.fade(holderX.mCircularProgress).start();
                holderX.mCircularProgress.setVisibility(View.GONE);


            }


            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                AnimationHelper.fade(holderX.mCircularProgress).start();
                holderX.mCircularProgress.setVisibility(View.GONE);


                if (loadedImage != null) {
                    Palette.from(loadedImage).generate(palette -> {

                        int accent = ColorHelper.getAttributeColor(
                                context, R.attr.colorAccent);
                        int color = palette.getVibrantColor(accent);
                        if (color == accent)
                            color = palette.getMutedColor(accent);
                        GradientDrawable gd = new GradientDrawable(
                                GradientDrawable.Orientation.TOP_BOTTOM,
                                new int[]{context.getResources().getColor(R.color.transparent), color});
                        gd.setCornerRadius(8f);
//                        onwallpaperLoaded(gd);


                    });
                }

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                AnimationHelper.fade(holderX.mCircularProgress).start();
                holderX.mCircularProgress.setVisibility(View.GONE);


            }
        }, (ImageLoadingProgressListener) (imageUri, viewx, current, total) -> {
            if ((total != 0) && (current != 0)) {
                holderX.mCircularProgress.setCurrentProgress(Math.round(100.0f * current / total));
                holderX.mCircularProgress.setOnProgressChangeListener(new CircularProgressIndicator.OnProgressChangeListener() {
                    @Override
                    public void onProgressChanged(double progress, double maxProgress) {
                        Log.d(TAG, String.format("Current: %1$.0f, max: %2$.0f", progress, maxProgress));
                        if (progress == maxProgress) {
                            AnimationHelper.fade(holderX.mCircularProgress).start();
                            holderX.mCircularProgress.setVisibility(View.GONE);

                        }

                    }
                });
            }

        });
    }


    @Override
    public int getItemCount() {
        return mPhotosToSize.size();
    }

    public class ViewHolderX extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView slideImageView;
        private CircularProgressIndicator mCircularProgress;

        public ViewHolderX(View itemView) {
            super(itemView);
            slideImageView = itemView.findViewById(R.id.photo_slide_image);
            mCircularProgress = itemView.findViewById(R.id.circular_progress);

        }


        @Override
        public void onClick(View v) {
        }
    }

    private class ViewHolderHeaderProgress extends RecyclerView.ViewHolder {
        private MaterialProgressBar materialProgressBar;

        public ViewHolderHeaderProgress(View v) {

            super(v);
            materialProgressBar = v.findViewById(R.id.progress_story_bar);
        }
    }


    public void addHeader(List<Progress> mHeader) {


        if (mHeader.size() > 0) {
            if (mHeaderItems != null) {
                mHeaderItems.addAll(mHeader);
                notifyItemInserted(mHeader.size() - 1);
            }
        }


    }
}
