

package ke.co.tunda.Adapters;

import android.animation.Animator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.danimahardhika.android.helpers.animation.AnimationHelper;
import com.danimahardhika.android.helpers.core.ColorHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.skyfishjy.library.RippleBackground;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.ViewPager;
import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.Callbacks.PaginationAdapterCallback;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.Constants.Extras;
import com.bumptech.glide.Glide;
import ke.co.tunda.Helpers.ImagePres;
import ke.co.tunda.R;

public class CardStackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Body> spots;
    ArrayList<String> mPhotos;
    //    private ArrayList<Matches> matches;
    private Context context;
    private String errorMsg;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;
    private boolean is_a_match = false;
    private SharedPreferences sp;

    public static final int ITEM = 0;
    public static final int LOADING = 1;
    public static final int ITSAMATCH = 2;
    public static final String TAG = "CSA";
    private final PaginationAdapterCallback mCallback;
    private int user_id;
    Animator currentAnimation;
    private String url;
    private float widthPixel;
    private boolean has_insta = false;


//    private int recipient_id;

    public CardStackAdapter(Context mContext, PaginationAdapterCallback mCallback, float widthPixel) {
        this.context = mContext;
        this.mCallback = mCallback;
        this.widthPixel = widthPixel;
        spots = new ArrayList<>();
        mPhotos = new ArrayList<>();
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        user_id = sp.getInt("user_id", 0);
//        Fresco.initialize(context);
//        initImageLoader(context);

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));


//        matches = new ArrayList<>();
    }

//    private void initImageLoader(Context context) {
//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())
//                .threadPoolSize(1)
//                .memoryCache(new WeakMemoryCache())
//                .imageDownloader(new BaseImageDownloader(context.getApplicationContext(),10 * 1000, 30 * 1000))
//                .build();
//
//    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM:
                View view = LayoutInflater.from(context).inflate(
                        R.layout.item_spot, viewGroup, false);
                viewHolder = new ItemHolder(view);

//                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = LayoutInflater.from(context).inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new LoadingVH(v2);
                break;

            case ITSAMATCH:
                View v3 = LayoutInflater.from(context).inflate(R.layout.match_layout, viewGroup, false);
                viewHolder = new ItsAMatchHolder(v3);
                break;



        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder xviewHolder, int position) {

        Body spot = spots.get(position);


        switch (getItemViewType(position)) {
            case ITEM:


                try {

                    final ItemHolder holder = (ItemHolder) xviewHolder;
                    String distance_to_here = "";


                    if (spot.getFullName() != null) {


                        if (spot.getVerification_status() != null && spot.getVerification_status() == 1) {
                            holder.mUserVerified.setVisibility(View.VISIBLE);
                        } else {
                            holder.mUserVerified.setVisibility(View.GONE);
                        }


                        if (spot.getDistKm() != null) {
                            holder.distance.setVisibility(View.VISIBLE);

                            Double away = spot.getDistKm();
                            if (away < 1) {
                                distance_to_here = "Less than a km away";
                                holder.distance.setText(distance_to_here);

                            } else if (away > 100) {
                                distance_to_here = "More than 100 km away";
                                holder.distance.setText(distance_to_here);

                            } else {

                                int dstnc = away.intValue();
                                distance_to_here = String.valueOf(dstnc) + " km away";
                                holder.distance.setText(distance_to_here);
                            }


                        } else {
                            holder.distance.setVisibility(View.GONE);
                        }

                        if (spot.getLikes_count() != null && spot.getLikes_count() > 0) {
                            holder.mLikesHolder.setVisibility(View.VISIBLE);
                            holder.mNumberOfLikes.setText(String.valueOf(spot.getLikes_count()));
                        } else {
                            holder.mLikesHolder.setVisibility(View.GONE);
                        }


                        if (spot.getMyAge() != null) {
                            holder.name.setText(context.getResources().getString(R.string.spot_name, spot.getFullName(), String.valueOf(spot.getMyAge())));
                        } else {
                            holder.name.setText(context.getResources().getString(R.string.spot_name, spot.getFullName(), ""));
                        }


                        if (spot.getPhotoCount() != null && spot.getPhotoCount() > 1) {
                            holder.mRelativeLayout.setVisibility(View.VISIBLE);
                            holder.mPhotoCount.setText(String.valueOf(spot.getPhotoCount()));

                        } else {
                            holder.mRelativeLayout.setVisibility(View.GONE);
                        }


                        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
                        circularProgressDrawable.setCenterRadius(45f);
                        circularProgressDrawable.setColorSchemeColors(context.getResources().getColor(R.color.colorPrimary));
                        circularProgressDrawable.setStrokeWidth(context.getResources().getDimension(R.dimen.drawable_ring_size));
                        circularProgressDrawable.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                        circularProgressDrawable.start();

                        String url = spot.getPhotoPath();


//
//                        Glide.with(context)
//                                .load(url)
//                                .apply(new RequestOptions().placeholder(circularProgressDrawable))
//                                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                                .into(holder.mWallpaper);


                        DisplayImageOptions.Builder options = ImagePres.getRawDefaultImageOptions();

                        ImageLoader.getInstance().handleSlowNetwork(true);

                        ImageLoader.getInstance().displayImage(url, holder.image, options.build(), new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                AnimationHelper.show(holder.mCircularProgress).start();

                            }


                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                AnimationHelper.fade(holder.mCircularProgress).start();

                            }


                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                                holder.mCircularProgress.animate().alpha(0.1f);

                                AnimationHelper.hide(holder.mCircularProgress).start();


                                if (loadedImage != null) {
                                    Palette.from(loadedImage).generate(palette -> {

                                        int accent = ColorHelper.getAttributeColor(
                                                context, R.attr.colorAccent);
                                        int color = palette.getVibrantColor(accent);
                                        if (color == accent)
                                            color = palette.getMutedColor(accent);
                                        GradientDrawable gd = new GradientDrawable(
                                                GradientDrawable.Orientation.TOP_BOTTOM,
                                                new int[]{context.getResources().getColor(R.color.transparent), color});
                                        gd.setCornerRadius(8f);

                                        holder.mLinearLayout.setBackground(gd);


                                    });
                                }

                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
//                                holder.mCircularProgress.animate().alpha(0.1f);
//                                holder.mCircularProgress.setVisibility(View.GONE);
                                AnimationHelper.hide(holder.mCircularProgress).start();


                            }
                        }, (ImageLoadingProgressListener) (imageUri, view, current, total) -> {
                            if ((total != 0) && (current != 0)) {
                                holder.mCircularProgress.setCurrentProgress(Math.round(100.0f * current / total));
                                holder.mCircularProgress.setOnProgressChangeListener(new CircularProgressIndicator.OnProgressChangeListener() {
                                    @Override
                                    public void onProgressChanged(double progress, double maxProgress) {
                                        Log.d(TAG, String.format("Current: %1$.0f, max: %2$.0f", progress, maxProgress));
                                        if (progress == maxProgress) {
                                            AnimationHelper.hide(holder.mCircularProgress).start();
                                        }

                                    }
                                });
                            }

                        });


                        holder.mCardSpot.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int dstnc;


                                if (spot.getDistKm() != null) {

                                    Double away = spot.getDistKm();
                                    dstnc = away.intValue();
                                } else {
                                    dstnc = -1;
                                }

                                boolean hasMultipleImages = false;
                                if (spot.getPhotoCount() != null) {
                                    hasMultipleImages = spot.getPhotoCount() > 1;
                                }

                                if (spot.getHas_insta_photo() != null && spot.getHas_insta_photo() == 1) {
                                    has_insta = true;
                                } else {
                                    has_insta =false;
                                }


                                Dialog dialog = new Dialog(String.valueOf(user_id),
                                        spot.getFullName(), spot.getPhotoPath(), null, null, 0,
                                        String.valueOf(spot.getUserId()), spot.getDob(),
                                        String.valueOf(dstnc), spot.getAboutMe(), spot.getJob_title(),
                                        spot.getSchool(), spot.getPhotoCount(), spot.getInterests(), spot.getVerification_status(), spot.getLikes_count(),has_insta);
                                try {
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                        ActivityOptions options = ActivityOptions
                                                .makeSceneTransitionAnimation((Activity) context, holder.image, "match_photo");
                                        FullProfile.open(context, dialog, hasMultipleImages, true, Extras.ORIGIN_MAIN, 1234, options);


                                    } else {
                                        FullProfile.open(context, dialog, hasMultipleImages, true, Extras.ORIGIN_MAIN, 1234, null);

                                    }
                                } catch (Exception e) {
                                    Log.d("TEST", e.getMessage());
                                }
                            }
                        });

                    }

                } catch (Exception e) {
                    Log.d("TESTING-CATCH", "Catch error" + e.getMessage());
                }


                break;

            case LOADING: {
                final LoadingVH loadingVH = (LoadingVH) xviewHolder;
                if (retryPageLoad) {
                    loadingVH.circleImageView.setVisibility(View.VISIBLE);
                    loadingVH.rippleBackground.stopRippleAnimation();
                    loadingVH.rippleBackground.setVisibility(View.VISIBLE);
                    loadingVH.finding_status.setText(errorMsg != null ? errorMsg : context.getResources().getString(R.string.error_msg_unknown));
                    loadingVH.finding_status.setVisibility(View.VISIBLE);
                    loadingVH.search_again.setVisibility(View.VISIBLE);
                } else {
                    loadingVH.rippleBackground.setVisibility(View.VISIBLE);
                    loadingVH.rippleBackground.startRippleAnimation();
                    loadingVH.finding_status.setText("Finding People near You");
                    loadingVH.finding_status.setVisibility(View.VISIBLE);
                    loadingVH.search_again.setVisibility(View.GONE);
                }

                break;

            }

        }


    }


    @Override
    public int getItemCount() {
        return spots == null ? 0 : spots.size();
    }


    public void add(Body r) {
        spots.add(r);
        notifyItemInserted(spots.size() - 1);
    }

    public void addAll(Body[] hh) {
        int bodyLength = hh.length;

        for (int i = 0; i < bodyLength; i++) {
            add(hh[i]);
        }


    }

    public int getRecipient_id(int position) {


        return spots.get(position).getUserId() != null ? spots.get(position).getUserId() : 0;
    }


    @Override
    public int getItemViewType(int position) {


        return (position == spots.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
//        add(new Body());/
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        int position = spots.size() - 1;
        Body result = getItem(position);

        if (result != null) {
            spots.remove(position);
            notifyItemRemoved(position);
        }
    }


    private Body getItem(int position) {
        return spots.get(position);
    }


    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(spots.size() - 1);

        if (errorMsg != null)
            this.errorMsg = errorMsg;
    }


    class ItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_city)
        TextView city;
        @BindView(R.id.item_name)
        TextView name;
        @BindView(R.id.item_image)
        ImageView image;
        @BindView(R.id.item_distance)
        TextView distance;
        @BindView(R.id.more)
        ImageButton mMore;
        @BindView(R.id.photo_count)
        TextView mPhotoCount;
        @BindView(R.id.photo_count_holder)
        RelativeLayout mRelativeLayout;
        @BindView(R.id.card_spot)
        CardView mCardSpot;
        @BindView(R.id.user_verified)
        ImageView mUserVerified;
        @BindView(R.id.number_of_likes)
        TextView mNumberOfLikes;
        @BindView(R.id.likes_holder)
        LinearLayout mLikesHolder;
        @BindView(R.id.circular_progress)
        CircularProgressIndicator mCircularProgress;
        @BindView(R.id.linear_details)
        LinearLayout mLinearLayout;


        public ItemHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mCircularProgress.setMaxProgress(100);

        }
    }


    class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.finding_people1)
        TextView finding_status;
        @BindView(R.id.content1)
        RippleBackground rippleBackground;
        @BindView(R.id.centerImage1)
        CircleImageView circleImageView;
        @BindView(R.id.try_search_again1)
        Button search_again;

        LoadingVH(View v2) {

            super(v2);
            ButterKnife.bind(this, v2);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            String imageUrl = sp.getString("image", "");
            Glide.with(context).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(circleImageView);
            search_again.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.try_search_again1:
                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;

            }
        }
    }

    class ItsAMatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.user_profile_photo)
        CircleImageView user_profile_photo;
        @BindView(R.id.match_profile_photo)
        CircleImageView match_profile_photo;
        @BindView(R.id.itsAmatchText)
        TextView itsAmatchText;
        @BindView(R.id.keep_swiping)
        Button keep_swiping;
        @BindView(R.id.message_match)
        Button message_match;

        ItsAMatchHolder(View v3) {
            super(v3);

            ButterKnife.bind(this, v3);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            String imageUrl = sp.getString("image", "");
            Glide.with(context).load(imageUrl).into(user_profile_photo);

            keep_swiping.setOnClickListener(this);
            message_match.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.keep_swiping:
//                    removeMatchPopUp();
            }

        }
    }


    public void clear() {
        isLoadingAdded = false;
        while (spots.size() > 0) {
            remove(getItem(0));

        }
    }

    private void remove(Body r) {
        int position = spots.indexOf(r);
        if (position > -1) {
            spots.remove(position);

            notifyItemRemoved(position);
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
//            mButtonContainer.setBackground(multiplePhotoAdapter.getBackground(i));


        }

        @Override
        public void onPageSelected(int i) {
//            addDotsIndicator(i);
            Log.d("TEST", "onPageSelected: " + i);

//            mButtonContainer.setBackground(multiplePhotoAdapter.getBackground(i));
//            setImages(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };


}
