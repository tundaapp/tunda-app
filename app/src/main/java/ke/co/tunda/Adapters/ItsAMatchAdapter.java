

package ke.co.tunda.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import com.bumptech.glide.Glide;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Matches;


public class ItsAMatchAdapter extends RecyclerView.Adapter<ItsAMatchAdapter.ViewHolder> {

    private RecyclerView parentRecycler;
    private List<Matches> data;
    private Context context;


    public ItsAMatchAdapter(Context mContext) {
        this.context = mContext;
        data = new ArrayList<>();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentRecycler = recyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_city_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewHolder viewHolder = holder;
//        int iconTint = ContextCompat.getColor(context, R.color.grayIconTint);
        Matches forecast = data.get(position);
        Log.d("HTTP", "Data" + "photo" + forecast.getPhotoPath() + "name" + forecast.getFullName());
        Glide.with(context)
                .load(forecast.getPhotoPath())
                .into(viewHolder.imageView);
        if (!forecast.getFullName().isEmpty()) {
            viewHolder.textView.setVisibility(View.VISIBLE);
            viewHolder.textView.setText(forecast.getFullName());
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void add(Matches r) {
        data.add(r);
        notifyItemInserted(data.size() - 1);
    }

    public void addAll(Matches[] matches) {
        Log.d("HTTP", "addAll: ForestcastAdapter");
        for (Matches result : matches) {
            add(result);
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CircleImageView imageView;
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.city_image);
            textView = itemView.findViewById(R.id.city_name);

            itemView.findViewById(R.id.container).setOnClickListener(this);
        }

        public void showText() {
            int parentHeight = ((View) imageView.getParent()).getHeight();
            float scale = (parentHeight - textView.getHeight()) / (float) imageView.getHeight();
            imageView.setPivotX(imageView.getWidth() * 0.5f);
            imageView.setPivotY(0);
            imageView.animate().scaleX(scale)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            textView.setVisibility(View.VISIBLE);
                        }
                    })
                    .scaleY(scale).setDuration(200)
                    .start();
        }

        public void hideText() {
            textView.setVisibility(View.INVISIBLE);
            imageView.animate().scaleX(1f).scaleY(1f)
                    .setDuration(200)
                    .start();
        }


        @Override
        public void onClick(View v) {
            parentRecycler.smoothScrollToPosition(getAdapterPosition());
        }
    }


    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    private Matches getItem(int position) {
        return data.get(position);
    }

    private void remove(Matches r) {
        int position = data.indexOf(r);
        if (position > -1) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }


}
