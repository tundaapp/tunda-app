
//
//package ke.co.tunda.Adapters;
//
//import android.content.Context;
//import android.graphics.drawable.Drawable;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.engine.DiskCacheStrategy;
//import com.droidninja.imageeditengine.views.PhotoEditorView;
//import com.droidninja.imageeditengine.views.imagezoom.ImageViewTouch;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
//
//import java.util.ArrayList;
//
//import androidx.annotation.NonNull;
//import androidx.viewpager.widget.PagerAdapter;
//import com.bumptech.glide.Glide;
//import ke.co.tunda.R;
//
//public class EditPhotoAdapter extends PagerAdapter {
//    Context context;
//    LayoutInflater layoutInflater;
//    ArrayList<String> mPhotos;
//    private ArrayList<String> mPhotosToSize;
//    private ArrayList<Drawable> mBackgrounds;
//
//
//    public EditPhotoAdapter(Context context) {
//
//        this.context = context;
//        mPhotos = new ArrayList<>();
//        mPhotosToSize = new ArrayList<>();
//        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
//        mBackgrounds = new ArrayList<>();
//        if (mBackgrounds.size() > 0) {
//            mBackgrounds.clear();
//        }
//
//
//    }
//
//
//    @Override
//    public int getCount() {
//        return mPhotosToSize.size();
//    }
//
//    @Override
//    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
//        return view == (LinearLayout) o;
//    }
//
//
//    @NonNull
//    @Override
//    public Object instantiateItem(@NonNull ViewGroup container, int position) {
//        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = layoutInflater.inflate(R.layout.edit_photo_slide, container, false);
//        ImageViewTouch slideImageView = view.findViewById(R.id.image_iv);
//        PhotoEditorView photoEditorView = view.findViewById(R.id.photo_editor_view);
//
//
//        String url = mPhotosToSize.get(position);
//
//        Glide.with(context)
//                .load(url)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(slideImageView);
//
//
//        container.addView(view);
//
//        return view;
//    }
//
//
//    @Override
//    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        container.removeView((RelativeLayout) object);
//    }
//
//    public void addPhotos(ArrayList<String> photo) {
//        Log.d("TEST", "addPhotos:size " + photo.size());
//        mPhotos.clear();
//        mPhotos.addAll(photo);
//        mPhotosToSize.addAll(photo);
//        Log.d("TEST", "addPhotos:new size " + mPhotosToSize.size());
//
//        notifyDataSetChanged();
//    }
//
//    public Drawable getBackground(int i) {
//        Log.d("TEST", "getBackground: ");
//        return mBackgrounds.size() <= 0 || mBackgrounds.size() <= i ? null : mBackgrounds.get(i);
//    }
//}
