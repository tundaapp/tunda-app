

package ke.co.tunda.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.danimahardhika.android.helpers.animation.AnimationHelper;
import com.danimahardhika.android.helpers.core.ColorHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.palette.graphics.Palette;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;
import androidx.viewpager.widget.PagerAdapter;
import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;
import ke.co.tunda.Helpers.ImagePres;
import ke.co.tunda.Models.Mood;
import ke.co.tunda.R;
import ke.co.tunda.Room.MyMoodsTable;

import static ke.co.tunda.Adapters.CardStackAdapter.TAG;

public class StoriesPhotoAdapter extends PagerAdapter implements DateFormatter.Formatter {
    Context context;
    LayoutInflater layoutInflater;

    private List<MyMoodsTable> mPhotosToSize;
    private List<Mood> mMainPhotoSize;
    private boolean is_self;
    private ArrayList<Drawable> mBackgrounds;
    private StoriesInterface mCallback;
    private SharedPreferences sp;
    private String name, image;
    private String url;
    private String caption;
    private String date;
    private ArrayList<Integer> allLoaded;


    public StoriesPhotoAdapter(List<MyMoodsTable> mMoods, List<Mood> mainMoods, boolean is_self, Context context, StoriesInterface storiesInterface) {

        this.context = context;
        mCallback = storiesInterface;
        mPhotosToSize = new ArrayList<>();
        mPhotosToSize = mMoods;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        mBackgrounds = new ArrayList<>();
        if (mBackgrounds.size() > 0) {
            mBackgrounds.clear();
        }
        allLoaded = new ArrayList<>();
        mMainPhotoSize = mainMoods;
        this.is_self = is_self;
        sp = sp = PreferenceManager.getDefaultSharedPreferences(context);
        name = sp.getString("first_name", "Me");
        image = sp.getString("image", "");


    }


    @Override
    public int getCount() {
        if (is_self) {
            return mPhotosToSize.size();
        } else {
            return mMainPhotoSize.size();
        }

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem: " + position);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.stories_slide_layout, container, false);
        ImageView slideImageView = view.findViewById(R.id.photo_slide_image);
        View reverse = view.findViewById(R.id.reverse);
        View center = view.findViewById(R.id.center);
        View skip = view.findViewById(R.id.skip);

        if (is_self) {
            url = mPhotosToSize.get(position).photo_path;
            url = url.replace(" ", "");
            caption = mPhotosToSize.get(position).mood_caption;
            date = mPhotosToSize.get(position).time_posted;
        } else {
            url = mMainPhotoSize.get(position).getPhotoPath();
            url = url.replace(" ", "");
            caption = mMainPhotoSize.get(position).getMoodCaption();
            date = mMainPhotoSize.get(position).getTime_posted();
        }


        mCallback.setTime(position);


        if (caption != null) {
            mCallback.setCaption();
        } else {
            mCallback.setCaption();
        }

        CircularProgressIndicator mCircularProgress = view.findViewById(R.id.circular_progress);

        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setCenterRadius(45f);
        circularProgressDrawable.setColorSchemeColors(context.getResources().getColor(R.color.colorPrimary));
        circularProgressDrawable.setStrokeWidth(context.getResources().getDimension(R.dimen.drawable_ring_size));
        circularProgressDrawable.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        circularProgressDrawable.start();


        DisplayImageOptions.Builder options = ImagePres.getRawDefaultImageOptions();

        ImageLoader.getInstance().handleSlowNetwork(true);

        ImageLoader.getInstance().displayImage(url, slideImageView, options.build(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                AnimationHelper.show(mCircularProgress).start();
                mCallback.pause();

            }


            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                AnimationHelper.fade(mCircularProgress).start();
                mCircularProgress.setVisibility(View.GONE);
                mCallback.pause();


            }


            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                AnimationHelper.fade(mCircularProgress).start();
                mCircularProgress.setVisibility(View.GONE);


                if (loadedImage != null) {
                    Palette.from(loadedImage).generate(palette -> {

                        int accent = ColorHelper.getAttributeColor(
                                context, R.attr.colorAccent);
                        int color = palette.getVibrantColor(accent);
                        if (color == accent)
                            color = palette.getMutedColor(accent);
                        GradientDrawable gd = new GradientDrawable(
                                GradientDrawable.Orientation.TOP_BOTTOM,
                                new int[]{context.getResources().getColor(R.color.transparent), color});
                        gd.setCornerRadius(8f);
                        onwallpaperLoaded(gd,position);
                        allLoaded.add(position);


                    });
                }

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                AnimationHelper.fade(mCircularProgress).start();
                mCircularProgress.setVisibility(View.GONE);
                mCallback.pause();


            }
        }, (ImageLoadingProgressListener) (imageUri, viewx, current, total) -> {
            if ((total != 0) && (current != 0)) {
                mCircularProgress.setCurrentProgress(Math.round(100.0f * current / total));
                mCircularProgress.setOnProgressChangeListener(new CircularProgressIndicator.OnProgressChangeListener() {
                    @Override
                    public void onProgressChanged(double progress, double maxProgress) {
                        Log.d(TAG, String.format("Current: %1$.0f, max: %2$.0f", progress, maxProgress));
                        if (progress == maxProgress) {
                            AnimationHelper.fade(mCircularProgress).start();
                            mCircularProgress.setVisibility(View.GONE);
                            mCallback.resume();

                        }

                    }
                });
            }

        });

        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.reverse();
            }
        });


        center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.pause();
            }
        });


        center.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    mCallback.pause();
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    mCallback.resume();
                } else if (event.getAction() == android.view.MotionEvent.ACTION_CANCEL) {
                    mCallback.resume();
                }
                return true;
            }
        });


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.skip();
            }
        });


        container.addView(view);

        return view;
    }

    private void onwallpaperLoaded(GradientDrawable gd,int position) {

        if (gd != null) {
            mBackgrounds.add(gd);
        }
        mCallback.resume();

        if(position==0){
            mCallback.play();
        }
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }


    public Drawable getBackground(int i) {
        Log.d("TEST", "getBackground: ");
        return mBackgrounds.size() <= 0 || mBackgrounds.size() <= i ? null : mBackgrounds.get(i);
    }

    public void setPage(int i) {

    }

    @Override
    public String format(Date date) {
        return null;
    }

    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss").parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday " + timeFormatter.format(dateTime);
        } else {
            return date;
        }
    }

    public boolean loadedAt(int position) {
        Log.d(TAG, "loadedAt: "+position+"status"+allLoaded.contains(position));
        if (allLoaded != null) {
            return allLoaded.contains(position);
        } else {
            return false;
        }


    }
}
