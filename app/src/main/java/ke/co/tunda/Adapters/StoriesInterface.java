

package ke.co.tunda.Adapters;

import java.util.ArrayList;

import ke.co.tunda.SwipeRecorder.Swipe;

public interface StoriesInterface {
    void resume();

    void reverse();

    void skip();

    void pause();

    void back();

    void setTime(int position);

    void setCaption();

    void play();
}
