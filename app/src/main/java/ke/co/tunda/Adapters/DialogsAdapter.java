

package ke.co.tunda.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crowdfire.cfalertdialog.CFAlertDialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import ke.co.tunda.ApiConnector.ApiCaller;
import ke.co.tunda.ApiConnector.Builder;
import ke.co.tunda.Callbacks.OpenMatchCallback;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.ChatKit.ChatKitModels.Message;
import ke.co.tunda.ChatKit.ChatKitModels.User;
import ke.co.tunda.ChatKit.CustomHolderMessagesActivity;
import com.bumptech.glide.Glide;
import ke.co.tunda.Models.DefaultResponse;
import ke.co.tunda.Models.DeleteBulkParams;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;
import ke.co.tunda.Room.ChatDialogsTable;
import ke.co.tunda.Room.DatabaseRoom;
import ke.co.tunda.Room.TundaRoomDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class DialogsAdapter extends RecyclerView.Adapter<DialogsAdapter.ItsAMatchHolder> {
    private List<ChatDialogsTable> myMatches;
    private Context context;
    private OpenMatchCallback callback;
    private RecyclerView parentRecycler;
    private Date date;
    private static String TAG = "DAPTER";
    SharedPreferences sp;
    int user_id;
    private TundaRoomDao tundaRoomDao;


    public DialogsAdapter(Context mContext) {
        this.context = mContext;
        myMatches = new ArrayList<>();
        sp = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        user_id = sp.getInt("user_id", 0);
        tundaRoomDao = DatabaseRoom.getDatabase(context).tundaRoomDao();
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        parentRecycler = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ItsAMatchHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        ItsAMatchHolder viewHolder = null;

        View view = LayoutInflater.from(context).inflate(
                R.layout.custom_dialog_item, viewGroup, false);
        viewHolder = new ItsAMatchHolder(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ItsAMatchHolder xviewHolder, int position) {

        ChatDialogsTable current = myMatches.get(position);

        if (current.unread_messages > 0) {
            xviewHolder.applyUnreadStyle();
        } else {
            xviewHolder.applyDefaultStyle();
        }


        if (current.full_name != null) {
            xviewHolder.tvName.setText(current.full_name);
        }

        if (current.last_message != null) {
            xviewHolder.tvLastMessage.setText(current.last_message);
        } else {
            xviewHolder.tvLastMessage.setText(null);
        }

        if (current.photo_path != null) {
            Glide.with(context)
                    .load(current.photo_path)
                    .into(xviewHolder.ivAvatar);
        }


        xviewHolder.tvBubble.setText(String.valueOf(current.unread_messages));
        xviewHolder.tvBubble.setVisibility(current.unread_messages > 0 ? VISIBLE : GONE);


    }

    @Override
    public int getItemCount() {
        return myMatches.size();
    }


    public void add(ChatDialogsTable r) {

        myMatches.add(r);
        notifyItemInserted(myMatches.size() - 1);


    }


    public void addAll(List<ChatDialogsTable> matches) {
        if (myMatches != null) {
            for (int i = 0; i < matches.size(); i++) {
                add(matches.get(i));

            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        return 0;

    }


    public ChatDialogsTable getItem(int position) {
        return myMatches.get(position);
    }


    public class ItsAMatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private DialogsListStyle dialogStyle;
        @BindView(R.id.rootdialogContainer)
        RelativeLayout container;
        //        @BindView(R.id.rootDialogRootLayout)
//        FrameLayout root;
        @BindView(R.id.rootdialogName)
        TextView tvName;

        //        @BindView(R.id.rootdialogLastMessageUserAvatar)
//        ImageView ivLastMessageUser;
        @BindView(R.id.rootdialogLastMessage)
        TextView tvLastMessage;
        @BindView(R.id.rootdialogUnreadBubble)
        TextView tvBubble;
        @BindView(R.id.rootdialogDividerContainer)
        FrameLayout dividerContainer;
        @BindView(R.id.rootdialogDivider)
        View divider;
        @BindView(R.id.rootdialogAvatar)
        CircleImageView ivAvatar;


        ItsAMatchHolder(View v3) {
            super(v3);


            ButterKnife.bind(this, v3);
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
            String imageUrl = sp.getString("image", "");
            container.setOnClickListener(this);
            container.setOnLongClickListener(this);


        }



        private void applyDefaultStyle() {
            if (dialogStyle != null) {
                if (container != null) {
                    container.setBackgroundColor(dialogStyle.getDialogItemBackground());
                }

                if (tvName != null) {
                    tvName.setTextColor(dialogStyle.getDialogTitleTextColor());
                    tvName.setTypeface(Typeface.DEFAULT, dialogStyle.getDialogTitleTextStyle());
                }

                if (tvLastMessage != null) {
                    tvLastMessage.setTextColor(dialogStyle.getDialogMessageTextColor());
                    tvLastMessage.setTypeface(Typeface.DEFAULT, dialogStyle.getDialogMessageTextStyle());
                }
            }
        }

        private void applyUnreadStyle() {
            if (dialogStyle != null) {
                if (container != null) {
                    container.setBackgroundColor(dialogStyle.getDialogUnreadItemBackground());
                }

                if (tvName != null) {
                    tvName.setTextColor(dialogStyle.getDialogUnreadTitleTextColor());
                    tvName.setTypeface(Typeface.DEFAULT, dialogStyle.getDialogUnreadTitleTextStyle());
                }


                if (tvLastMessage != null) {
                    tvLastMessage.setTextColor(dialogStyle.getDialogUnreadMessageTextColor());
                    tvLastMessage.setTypeface(Typeface.DEFAULT, dialogStyle.getDialogUnreadMessageTextStyle());
                }
            }
        }


        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.rootdialogContainer:

                    if (getAdapterPosition() >= 0) {
                        String name = sp.getString("first_name", "");
                        String user_photo = sp.getString("image", "https://i0.wp.com/www.winhelponline.com" +
                                "/blog/wp-content/uploads" +
                                "/2017/12/user.png?fit=256%2C256&quality=100&ssl=1");

                        User Me = new User(
                                // dialogueObject.getString("staff_id"),
                                String.valueOf(user_id),
                                name,
                                user_photo,
                                true);

                        User myUser = new User(
                                //dialogueObject.getString("customer_id"),
                                String.valueOf(myMatches.get(getAdapterPosition()).recipient_id),
                                myMatches.get(getAdapterPosition()).full_name,
                                myMatches.get(getAdapterPosition()).photo_path,
                                true);
                        ArrayList<User> users = new ArrayList<>();
                        users.add(Me);
                        users.add(myUser);
//                        String lastChatTime = getCalender(myMatches.get(getAdapterPosition()).last_chat_date);


                        Message mymessage = null;

                        boolean has_insta_photo = false;
                        if(myMatches.get(getAdapterPosition()).has_insta_photo!=null&&myMatches.get(getAdapterPosition()).has_insta_photo==1){
                            has_insta_photo = true;
                        }else {
                            has_insta_photo = false;
                        }

                        Dialog dialog = new Dialog(String.valueOf(myMatches.get(getAdapterPosition()).match_id),
                                myMatches.get(getAdapterPosition()).full_name,
                                myMatches.get(getAdapterPosition()).photo_path,
                                users,
                                mymessage,
                                myMatches.get(getAdapterPosition()).unread_messages,
                                String.valueOf(myMatches.get(getAdapterPosition()).recipient_id),
                                myMatches.get(getAdapterPosition()).dob,
                                myMatches.get(getAdapterPosition()).getDistance,
                                myMatches.get(getAdapterPosition()).about_me,
                                myMatches.get(getAdapterPosition()).job_title,
                                myMatches.get(getAdapterPosition()).getSchool,
                                myMatches.get(getAdapterPosition()).getPhoto_Count, myMatches.get(getAdapterPosition()).interests,
                                myMatches.get(getAdapterPosition()).getVerificatonStatus, myMatches.get(getAdapterPosition()).getLikesCount,has_insta_photo);
                        if (myMatches.get(getAdapterPosition()).unread_messages != null && myMatches.get(getAdapterPosition()).unread_messages > 0) {
                            Preferences.get(context).setIsTimeToRefreshCDialogss(true);

                        }


                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            ActivityOptions options = ActivityOptions
                                    .makeSceneTransitionAnimation((Activity) context, ivAvatar, "match_photo");
                            CustomHolderMessagesActivity.open(context, dialog, 2, options);
                        } else {
                            CustomHolderMessagesActivity.open(context, dialog, 2, null);
                        }
                    }


            }


        }


        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()) {
                case R.id.rootdialogContainer:
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context);
                    builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                    builder.setTitle("Delete this chat?");
                    builder.setTextGravity(Gravity.CENTER);
                    builder.setMessage("Warning! This action cannot be reversed");
                    int position = getAdapterPosition();

                    if (myMatches.get(position).match_id != 0) {
                        builder.addButton("Delete", -1, context.getResources().getColor(R.color.colorAccent),
                                CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Toasty.normal(context.getApplicationContext(), "deleting...", Toasty.LENGTH_SHORT).show();
                                        ApiCaller apiCaller = Builder.getClient().create(ApiCaller.class);
                                        DeleteBulkParams params = new DeleteBulkParams();
                                        params.setRequestType("BULK_DELETE_CHAT");
                                        params.setMatchId(myMatches.get(position).match_id);
                                        params.setUserId(user_id
                                        );
                                        Call<DefaultResponse> call = apiCaller.deleteBulk(params);
                                        call.enqueue(new Callback<DefaultResponse>() {
                                            @Override
                                            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                                                if (response.isSuccessful()) {
                                                    DefaultResponse defaultResponse = response.body();

                                                    if (defaultResponse != null && defaultResponse.getStatusMessage().equals("SUCCESS")) {
                                                        Toasty.success(context, "done", Toasty.LENGTH_SHORT).show();

                                                        tundaRoomDao.deleteChatDialogById(myMatches.get(position).match_id);
                                                        remove(myMatches.get(getAdapterPosition()));

                                                        dialogInterface.dismiss();
                                                    } else {
                                                        Toasty.warning(context, "Error occurred, not deleted", Toasty.LENGTH_SHORT).show();
                                                        dialogInterface.dismiss();
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                                                Toasty.error(context, "Error occurred, not connected", Toasty.LENGTH_SHORT).show();
                                                dialogInterface.dismiss();


                                            }
                                        });


                                    }
                                });

                        builder.addButton("Cancel", -1, context.getResources().getColor(R.color.colorAccent),
                                CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.JUSTIFIED,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();


                                    }
                                });


                        builder.show();
                    } else {
                        Toasty.warning(context.getApplicationContext(), "Item cannot be deleted now", Toasty.LENGTH_SHORT).show();
                    }


            }
            return false;
        }
    }

    private String getCalender(String lastChatDate) {

        String time = lastChatDate.split("T")[1];

        String hour = time.split(":")[0];
        String minute = time.split(":")[1];


        return hour + ":" + minute;
    }

    public void clear() {

        int size = myMatches.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                myMatches.remove(0);
            }

            notifyItemRangeRemoved(0, size);
        }

    }

    private void remove(ChatDialogsTable r) {
        int position = myMatches.indexOf(r);
        if (position > -1) {
            myMatches.remove(position);

            notifyItemRemoved(position);
        }
    }
}
