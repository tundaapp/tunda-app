

package ke.co.tunda.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.ApiConnector.Models.Progress;
import ke.co.tunda.R;

public class StoriesProgressAdapter extends RecyclerView.Adapter {
    Context context;
    LayoutInflater layoutInflater;
    private ArrayList<Progress> mPhotosToSize;
    private ArrayList<Drawable> mBackgrounds;
    private int progress;


    public StoriesProgressAdapter(Context context) {

        this.context = context;
        mPhotosToSize = new ArrayList<>();
        progress = 0;

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        mBackgrounds = new ArrayList<>();
        if (mBackgrounds.size() > 0) {
            mBackgrounds.clear();
        }


    }


    private void onwallpaperLoaded(GradientDrawable gd) {
        Log.d("TEST", "onwallpaperLoaded: ");

        if (gd != null) {
            mBackgrounds.add(gd);
        }


        if (FullProfile.isBackgroundnull() && gd != null) {
            FullProfile.setBackground(gd);
        }
        if (mBackgrounds.size() == 1) {
            FullProfile.setBackground(mBackgrounds.get(0));
        }
    }


    public void addData(ArrayList<Progress> mProgressitems) {
        mPhotosToSize.addAll(mProgressitems);
        Log.d("TEST", "addPhotos:new size " + mPhotosToSize.size());

        notifyDataSetChanged();
    }

    public Drawable getBackground(int i) {
        Log.d("TEST", "getBackground: ");
        return mBackgrounds.size() <= 0 || mBackgrounds.size() <= i ? null : mBackgrounds.get(i);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.stories_progress_layout, parent, false);
        return new ViewHolderX(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolderX holderX = (ViewHolderX) holder;
        Integer progress_number = mPhotosToSize.get(position).getProgress_size();
//        if (holderX.materialProgressBar.isIndeterminate()) {
//            holderX.materialProgressBar.setIndeterminate(false);
//        }
        holderX.materialProgressBar.setMax(100);

        final Thread pBarThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (progress <= holderX.materialProgressBar.getMax()) {
                        holderX.materialProgressBar.setProgress(progress);
                        sleep(1000);
                        ++progress;
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        pBarThread.start();


    }


    @Override
    public int getItemCount() {
        return mPhotosToSize.size();
    }

    public class ViewHolderX extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ProgressBar materialProgressBar;

        public ViewHolderX(View v) {

            super(v);
            materialProgressBar = v.findViewById(R.id.progress_story_bar);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
