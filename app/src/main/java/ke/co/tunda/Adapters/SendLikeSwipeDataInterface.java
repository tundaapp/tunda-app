

package ke.co.tunda.Adapters;

import java.util.ArrayList;

import ke.co.tunda.SwipeRecorder.Swipe;

public interface SendLikeSwipeDataInterface {
    void sendLikeSwipes(int user_id, ArrayList<Swipe> mArrayList);
}
