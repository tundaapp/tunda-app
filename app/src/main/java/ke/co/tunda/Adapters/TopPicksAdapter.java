

package ke.co.tunda.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wang.avi.AVLoadingIndicatorView;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.tunda.Activities.FullProfile;
import ke.co.tunda.ApiConnector.Models.Body;
import ke.co.tunda.ChatKit.ChatKitModels.Dialog;
import ke.co.tunda.Constants.Extras;
import ke.co.tunda.Models.HeaderItem;
import ke.co.tunda.R;
import ke.co.tunda.SwipeRecorder.Swipe;

public class TopPicksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static List<Body> mTopPickList;
    private Context mContext;
    public static final int HEADER = 0;
    public static final int TOP_PICKS = 1;
    public static final int LOADING = 2;
    private static final String TAG = "TPA";
    private List<HeaderItem> mHeaderItems;
    public boolean isLoaderVisible = false;
    public boolean isPaginationFailedShown = false;
    private String errorMsg;
    private int user_id;
    final TryAgainInterface mCallback;
    final SendSwipeDataInterface mCallbackSwipe;
    private ArrayList<Swipe> mSwipeArray;

    public TopPicksAdapter(Context context, List<HeaderItem> items, TryAgainInterface callback, SendSwipeDataInterface callbackSwipe, int user_id) {

        mContext = context;
        mHeaderItems = items;
        mTopPickList = new ArrayList<>();
        this.mCallback = callback;
        this.mCallbackSwipe = callbackSwipe;
        mSwipeArray = new ArrayList<>();
        this.user_id = user_id;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        RecyclerView.ViewHolder mV;

        switch (viewType) {
            case HEADER:
                // inflate header view
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_header, parent, false);
                mV = new ViewHolderHeader(v);
                break;
            case TOP_PICKS:
                // inflate main content view
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_picks, parent, false);
                mV = new ViewHolderTopPicks(v);
                break;

            case LOADING:
                Log.d(TAG, "onCreateViewHolder: isloading");
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_data, parent, false);
                mV = new ViewHolderLoading(v);
                break;

            default:
                // inflate main content view
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_picks, parent, false);
                mV = new ViewHolderTopPicks(v);
        }
        return mV;


//        if (viewType == TOP_PICKS) {
//            View x = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_picks, parent, false);
//            return new ViewHolderTopPicks(x);
//
//        }
//
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_header, parent, false);
//        return new ViewHolderHeader(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
//        try {
//            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams)
//                    viewHolder.itemView.getLayoutParams();
//            layoutParams.setFullSpan(getItemViewType(position) == HEADER);
//
//        } catch (Exception e) {
//            Log.d(TAG, "onBindViewHolder: ");
//        }

        if (viewHolder.getItemViewType() == HEADER) {
//            HeaderItem item = mHeaderItems.get(position);

            ((ViewHolderHeader) viewHolder).mSub.setHtml(mHeaderItems.get(0).getDescription());
            ((ViewHolderHeader) viewHolder).mHeaderTitle.setText(mHeaderItems.get(0).getHeader());

        } else if (viewHolder.getItemViewType() == TOP_PICKS) {
            Log.d(TAG, "onBindViewHolder: top picks" +
                    position
            );
            Body mTopPicksClass = getItem(position - 1);
            if (mTopPicksClass != null) {
                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(mContext);
                circularProgressDrawable.setCenterRadius(45f);
                circularProgressDrawable.setColorSchemeColors(mContext.getResources().getColor(R.color.colorPrimary));
                circularProgressDrawable.setStrokeWidth(mContext.getResources().getDimension(R.dimen.drawable_ring_size));
                circularProgressDrawable.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
                circularProgressDrawable.start();
                if (mTopPicksClass.getPhotoPath() != null && !mTopPicksClass.getPhotoPath().isEmpty()) {
                    Glide.with(mContext)
                            .load(mTopPicksClass.getPhotoPath())
                            .apply(new RequestOptions().placeholder(circularProgressDrawable))
                            .into(((ViewHolderTopPicks) viewHolder).mImage);
                }

                String name = mTopPicksClass.getFullName() != null ? mTopPicksClass.getFullName() : "";
                if (name != null && !name.isEmpty() && name.contains(" ")) {
                    String[] arr = name.split(" ");
                    name = arr[0];
                }

                if (mTopPicksClass.getMyAge() != null) {
                    ((ViewHolderTopPicks) viewHolder).mName.setText(mContext.getResources().getString(R.string.name_age, name, String.valueOf(mTopPicksClass.getMyAge())));

                } else {
                    ((ViewHolderTopPicks) viewHolder).mName.setText(mContext.getResources().getString(R.string.name_noage, name));

                }

                if (mTopPicksClass.getHrs_left() != null) {
                    if (mTopPicksClass.getHrs_left() < 1) {
                        ((ViewHolderTopPicks) viewHolder).mHrsLeft.setText(mContext.getResources().getString(R.string.hours_left_one, ""));

                    } else {
                        ((ViewHolderTopPicks) viewHolder).mHrsLeft.setText(mContext.getResources().getString(R.string.hours_left, String.valueOf(mTopPicksClass.getHrs_left())));

                    }

                }

                if (mTopPicksClass.getViews_count() != null && mTopPicksClass.getViews_count() > 0) {
                    Log.d(TAG, "onBindViewHolder:views " + mTopPicksClass.getViews_count());
                    ((ViewHolderTopPicks) viewHolder).mViewsHolder.setVisibility(View.VISIBLE);
                    ((ViewHolderTopPicks) viewHolder).mNumberOfViews.setText(String.valueOf(mTopPicksClass.getViews_count()));
                } else {
                    ((ViewHolderTopPicks) viewHolder).mViewsHolder.setVisibility(View.GONE);
                }


            }


        } else if (viewHolder.getItemViewType() == LOADING) {

            if (isPaginationFailedShown) {
                ((ViewHolderLoading) viewHolder).mSpinner.setVisibility(View.GONE);
                ((ViewHolderLoading) viewHolder).mFailHolder.setVisibility(View.VISIBLE);
                ((ViewHolderLoading) viewHolder).mFailReason.setText(errorMsg != null ? errorMsg : "Erro loading Data");


            } else {
                ((ViewHolderLoading) viewHolder).mSpinner.setVisibility(View.VISIBLE);
                ((ViewHolderLoading) viewHolder).mFailHolder.setVisibility(View.GONE);

            }

        } else {
            Log.d(TAG, "onBind\"no instance of viewholder found\"ViewHolder: ");
        }


    }

    @Override
    public int getItemCount() {


        if (mHeaderItems == null) {
//            Log.d(TAG, "getItemCount: 1" + mTopPickList.size());
            return mTopPickList.size();
        } else {
//            Log.d(TAG, "getItemCount: 2" + mTopPickList.size() + 1);

            return mTopPickList.size() + 1;
        }


    }

    public void addLoading() {
        isLoaderVisible = true;
        add(new Body());
    }


    public void removeLoading() {

        if (isLoaderVisible) {

            isLoaderVisible = false;
            int position = mTopPickList.size() - 1;
            Body item = getItem(position);

            if (item != null) {
                mTopPickList.remove(position);
                notifyItemRemoved(position);

            }
        }


    }

    public void tryLoadingAgain(boolean show, String errorMsg) {
        Log.d(TAG, "tryLoadingAgain: ");
        isPaginationFailedShown = show;
        notifyItemChanged(mTopPickList.size());
        this.errorMsg = errorMsg;


    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private Body getItem(int position) {

        return mTopPickList.get(position);


    }


    class ViewHolderTopPicks extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image)
        ImageView mImage;
        @BindView(R.id.name)
        TextView mName;
        @BindView(R.id.hrs_left)
        TextView mHrsLeft;
        @BindView(R.id.user_package)
        ImageView mUserPackage;
        @BindView(R.id.card_top_pick)
        CardView mCard;
        @BindView(R.id.number_of_views)
        TextView mNumberOfViews;
        @BindView(R.id.views_holder)
        LinearLayout mViewsHolder;


        public ViewHolderTopPicks(View v) {
            super(v);
            ButterKnife.bind(this, v);
            mCard.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.card_top_pick:
                    int mPosition = getAdapterPosition() - 1;
                    Body spot = mTopPickList.get(mPosition);
                    Log.d("posin", "onClick: " + spot.getFullName());
                    int dstnc;


                    if (spot.getDistKm() != null) {
                        Double away = spot.getDistKm();
                        dstnc = away.intValue();
                    } else {
                        dstnc = 0;
                    }

                    boolean hasMultipleImages = false;
                    if (spot.getPhotoCount() != null) {
                        hasMultipleImages = spot.getPhotoCount() > 1;
                    }

                    boolean has_insta_photo = false;
                    if(spot.getHas_insta_photo()!=null&&spot.getHas_insta_photo()==1){
                        has_insta_photo = true;
                    }else {
                        has_insta_photo = false;
                    }


                    Dialog dialog = new Dialog(String.valueOf(spot.getUserId()),
                            spot.getFullName(), spot.getPhotoPath(), null, null, 0,
                            String.valueOf(spot.getUserId()), spot.getDob(),
                            String.valueOf(dstnc), spot.getAboutMe(), spot.getJob_title(),
                            spot.getSchool(), spot.getPhotoCount(), spot.getInterests(),
                            spot.getVerification_status(), spot.getLikes_count(),has_insta_photo);
                    try {


                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            ActivityOptions options = ActivityOptions
                                    .makeSceneTransitionAnimation((Activity) mContext, mImage, "match_photo");
                            FullProfile.open(mContext, dialog, hasMultipleImages, true, Extras.ORIGIN_TOP_PICKS, mPosition, options);


                        } else {
                            FullProfile.open(mContext, dialog, hasMultipleImages, true, Extras.ORIGIN_TOP_PICKS, mPosition, null);

                        }
                    } catch (Exception e) {
                        Log.d("TEST", e.getMessage());
                    }
                    break;
            }

        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        @BindView(R.id.top_pics_sub)
        HtmlTextView mSub;
        @BindView(R.id.top_picks_title)
        TextView mHeaderTitle;

        public ViewHolderHeader(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADER;
        } else {
            if (isLoaderVisible) {

                return position == mTopPickList.size() ? LOADING : TOP_PICKS;


            } else {

                return TOP_PICKS;

            }
        }


    }

    public void add(Body r) {
        Log.d(TAG, "add: size = " + mTopPickList.size());


        mTopPickList.add(r);
        notifyItemInserted(mTopPickList.size() - 1);

//                publishProgress();


    }

    public void addAll(Body[] Items) {


        for (Body item : Items) {

            add(item);

        }
    }

    public void addHeader(List<HeaderItem> mHeader) {

        Log.d(TAG, "setTags: size= " + mHeader.size());


        if (mHeader.size() > 0) {
            if (mHeaderItems != null) {
                mHeaderItems.addAll(mHeader);
                notifyItemInserted(mHeader.size() - 1);
            }
        }


    }


    class ViewHolderLoading extends RecyclerView.ViewHolder {
        @BindView(R.id.spin_kit)
        AVLoadingIndicatorView mSpinner;
        @BindView(R.id.try_again)
        TextView mTryAgain;
        @BindView(R.id.fail_reason)
        TextView mFailReason;
        @BindView(R.id.paging_fail_holder)
        RelativeLayout mFailHolder;

        public ViewHolderLoading(View v) {

            super(v);
            ButterKnife.bind(this, v);
            mFailHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.TryAgain();
                }
            });
        }
    }


//    private class InsertAsyncTask extends AsyncTask<TopPicksClass, Void, Void> {
//        List<TopPicksClass> mArray;
//
//        InsertAsyncTask(List<TopPicksClass> mList) {
//            mArray = mList;
//
//        }
//
//        @Override
//        protected Void doInBackground(TopPicksClass... topPicksClasses) {
//
//            if (topPicksClasses[0] != null) {
//                try {
//                    if (!containsTopPick(mArray, topPicksClasses[0].user_id)) {
//                        Log.d(TAG, "doInBackground: id = " + topPicksClasses[0].user_id);
//                        mTopPickList.add(topPicksClasses[0]);
//
//                        publishProgress();
//
//
//                    } else {
//                        Log.d(TAG, "doInBackground: id null");
//                    }
//
//                } catch (Exception e) {
//                    Log.d(TAG, "doInBackground: " + e.getMessage());
//                }
//            } else {
//                Log.d(TAG, "doInBackground: its null");
//            }
//
//
//            return null;
//
//
//        }
//
//
//        @Override
//        protected void onProgressUpdate(Void... values) {
//            notifyItemInserted(mTopPickList.size() - 1);
//        }
//    }


    private static boolean containsTopPick(List<Body> mTops, int id_user) {


        for (int i = 0; i < mTops.size(); i++) {
            if (mTops.get(i).getUserId() != null) {
                if (mTops.get(i).getUserId() == id_user) {
                    return true;
                }
            }

        }
//        for (TopPicksClass ti : mTops) {
//            if (ti.user_id == id_user) {
//                return true;
//            }
//
//
//        }
        return false;
    }


    public void skipUser(int position) {
        Body item = getItem(position);

        if (item != null) {
            Log.d("posin", "removing: " + position);
            Log.d("posin", "removed: " + item.getFullName());
            sendSwipe("DISLIKE", item);

            mTopPickList.remove(position);
            notifyItemRemoved(position);

            notifyItemRangeChanged(position, getItemCount() - position);

        }

    }

    private void sendSwipe(String action, Body item) {
        if (mSwipeArray.size() > 0) {
            mSwipeArray.clear();
        }
        Swipe swipe = new Swipe();
        swipe.setSwipe_action(action);
        swipe.setRecipient_id(item.getUserId());
        mSwipeArray.add(swipe);
        mCallbackSwipe.sendTopPicksSwipe(user_id, mSwipeArray);
    }

    public void superLikeUser(int position) {
        Body item = getItem(position);

        if (item != null) {
            sendSwipe("SUPERLIKE", item);
            mTopPickList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, getItemCount() - position);

        }
    }

    public void likeUser(int position) {
        Body item = getItem(position);

        if (item != null) {
            sendSwipe("LIKE", item);
            mTopPickList.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, getItemCount() - position);

        }
    }
}
