

package ke.co.tunda.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import ke.co.tunda.Activities.IgFull;
import ke.co.tunda.POJO.SectionsList;
import ke.co.tunda.R;
import ke.co.tunda.utils.Constants;


public class MultiplePhotoAdapterIg extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<String> mPhotos;
    private ArrayList<SectionsList> mPhotosToSize;
    private ArrayList<SectionsList> mPhotosWorker;
    private ArrayList<String> mPhotosWork;
    private ArrayList<Drawable> mBackgrounds;
    private MiniAdapter miniAdapter;
    private static String TAG = "MMADAP";
    private int limit = 3;
    private String name_user,photo_path;


    public MultiplePhotoAdapterIg(Context context) {

        this.context = context;
        mPhotos = new ArrayList<>();
        mPhotosToSize = new ArrayList<>();
        mPhotosWork = new ArrayList<>();
        mPhotosWorker = new ArrayList<>();
        mPhotos = new ArrayList<>();
        mBackgrounds = new ArrayList<>();
        if (mBackgrounds.size() > 0) {
            mBackgrounds.clear();
        }


    }


    @Override
    public int getCount() {
        Log.d(TAG, "getCount: size" + mPhotosToSize.size());

        return mPhotosToSize.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem: position = " + position);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.photo_slide_layout_ig, container, false);
        RecyclerView slideRec = view.findViewById(R.id.photo_slide_image);
        GridLayoutManager layoutManager =
                new GridLayoutManager(context, 3);
        slideRec.setLayoutManager(layoutManager);
        MiniAdapter miniAdapter = new MiniAdapter(context);
        slideRec.setAdapter(miniAdapter);


        miniAdapter.addPhoto(mPhotosToSize.get(position).getuRls());


        container.addView(view);

        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }


//    public void addPhoto(ArrayList<SectionsList> photo) {
//        mPhotosWorker.clear();
//        mPhotosWorker.addAll(photo);
//        mPhotosToSize.add(mPhotosWorker);
//        Log.d("TEST", "addPhotos:new size " + mPhotosToSize.size());
//
//        notifyDataSetChanged();
//    }

    public void add(SectionsList r) {

        mPhotosToSize.add(r);
        notifyDataSetChanged();


    }


    public void addPhoto(List<SectionsList> matches,String name,String photo) {
        Log.d(TAG, "addPhoto: " + matches.size());
        if (mPhotosToSize != null) {
            for (int i = 0; i < matches.size(); i++) {
                add(matches.get(i));

            }
            name_user = name;
            photo_path = photo;
        }
    }

    public Drawable getBackground(int i) {
        Log.d("TEST", "getBackground: ");
        return mBackgrounds.size() <= 0 || mBackgrounds.size() <= i ? null : mBackgrounds.get(i);
    }

    private class MiniAdapter extends RecyclerView.Adapter<MiniAdapter.MiniHolder> {
        Context context;
        ArrayList<String> mX;
        ArrayList<String> wR;

        public MiniAdapter(Context context) {
            this.context = context;
            mX = new ArrayList<>();
            wR = new ArrayList<>();

        }

        @NonNull
        @Override
        public MiniHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            MiniHolder viewHolder = null;

            View view = LayoutInflater.from(context).inflate(
                    R.layout.custom_mini, viewGroup, false);
            viewHolder = new MiniHolder(view);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull MiniHolder miniHolder, int i) {
//            Log.d(TAG, "onBindViewHolder: adding image" + mX.get(i));
            try {
                String urlstamp = mX.get(i);
                String [] parts = urlstamp.split("~~");

                Glide.with(context).load(parts[0]).into(miniHolder.mImage);

            } catch (Exception e) {
                Log.d(TAG, "onBindViewHolder: " + e.getMessage());
            }
        }


        @Override
        public int getItemCount() {
            Log.d(TAG, "getItemCount: size of mini = " + mX
                    .size());
            return mX.size();
        }

        public void addPhoto(List<String> photo) {
            Log.d(TAG, "addPhoto miniadapter: " + photo.size());
            wR.clear();
            wR.addAll(photo);
            mX.addAll(wR);
            notifyDataSetChanged();
        }

        public class MiniHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            ImageView mImage;

            public MiniHolder(@NonNull View itemView) {
                super(itemView);

                mImage = itemView.findViewById(R.id.item_image);
                mImage.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.item_image:


                        ActivityOptions options = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            options = ActivityOptions.makeSceneTransitionAnimation((Activity) context, mImage, context.getResources().getString(R.string.ig_image));
                            IgFull.open(context,mX.get(getAdapterPosition()),options,name_user,photo_path);
                        }else {
                            IgFull.open(context,mX.get(getAdapterPosition()),null,name_user,photo_path);
                        }
                        // start the new activity

                }

            }
        }
    }
}
