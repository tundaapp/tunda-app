

package ke.co.tunda.Adapters;

import java.util.ArrayList;

import ke.co.tunda.SwipeRecorder.Swipe;

public interface SendSwipeDataInterface {
    void sendTopPicksSwipe(int user_id, ArrayList<Swipe> mArrayList);
}
