

package ke.co.tunda.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import ke.co.tunda.Activities.StatusStories;
import com.bumptech.glide.Glide;
import ke.co.tunda.R;
import ke.co.tunda.Room.MainMoodsTable;
import ke.co.tunda.Views.CircularStatusView;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.StatusViewHolder> {
    private Context context;
    private List<MainMoodsTable> statusArrayList;
    private static final String TAG = "STATUS_ADAPTER";

    public StatusAdapter(Context context, List<MainMoodsTable> status) {
        this.context = context;
        this.statusArrayList = status;
    }

    @NonNull
    @Override
    public StatusViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new StatusViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.status_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StatusViewHolder statusViewHolder, int i) {

        MainMoodsTable userStatus = statusArrayList.get(i);
//        List<Status> statusList = userStatus.getStatusList();


        Glide.with(context)
                .load(userStatus.user_moods.get(0).getPhotoPath())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .apply(new RequestOptions().placeholder(R.drawable.profile))
                .into(statusViewHolder.image);

        statusViewHolder.tvName.setText(userStatus.full_name);
        try {
            String formatted_date = formatToYesterdayOrToday(userStatus.created_on);
            statusViewHolder.tvTime.setText(formatted_date);
        } catch (ParseException e) {
            Log.d(TAG, "onBindViewHolder: ");

        }


        statusViewHolder.circularStatusView.setPortionsCount(userStatus.mood_count);
        int notSeenColor = context.getResources().getColor(R.color.colorPrimary);
        int seenColor = Color.GRAY;

        if (userStatus.viewed_moods == userStatus.mood_count) {
            //set all portions color
            statusViewHolder.circularStatusView.setPortionsColor(seenColor);
        } else {

            for (int x = 0; x < userStatus.mood_count; x++) {
                int view_status = userStatus.user_moods.get(x).getViewStatus();
                int color = view_status == 1 ? seenColor : notSeenColor;
                //set specific color for every portion
                statusViewHolder.circularStatusView.setPortionColorForIndex(x, color);
            }

        }


    }

    @Override
    public int getItemCount() {
        if (statusArrayList != null) {
            return statusArrayList.size();
        }
        return 0;
    }

    class StatusViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView image;
        TextView tvName;
        TextView tvTime;
        CircularStatusView circularStatusView;
        RelativeLayout chat_row_container;

        StatusViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.profile_image);
            tvName = itemView.findViewById(R.id.tvName);
            tvTime = itemView.findViewById(R.id.tvTime);
            circularStatusView = itemView.findViewById(R.id.circular_status_view);
            chat_row_container = itemView.findViewById(R.id.chat_row_container);
            chat_row_container.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.chat_row_container:
                    int position = getAdapterPosition();
//                    ArrayList<Mood> moodArrayList = statusArrayList.get(position).getUserMoods();
                    StatusStories.open(context, null, statusArrayList.get(position).user_moods, statusArrayList.get(position).full_name, statusArrayList.get(position).user_photo, false);
            }
        }
    }

    public static String formatToYesterdayOrToday(String date) throws ParseException {
        Date dateTime = new SimpleDateFormat("yyyy-MM-dd-hh.mm.ss").parse(date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm");

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today " + timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday " + timeFormatter.format(dateTime);
        } else {
            return date;
        }
    }
}
