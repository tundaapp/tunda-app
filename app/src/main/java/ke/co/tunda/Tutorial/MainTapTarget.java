

package ke.co.tunda.Tutorial;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.danimahardhika.android.helpers.core.UnitHelper;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.yuyakaido.android.cardstackview.CardStackView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import ke.co.tunda.Preferences.Preferences;
import ke.co.tunda.R;

public class MainTapTarget {
    public static void showMainIntro(@NonNull Context context, boolean peopleAround, View view, RelativeLayout profile, RelativeLayout home, RelativeLayout chats) {
        if (Preferences.get(context).isTimeForMainIntro()) {
            AppCompatActivity activity = (AppCompatActivity) context;
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    TapTargetSequence tapTargetSequence = new TapTargetSequence(activity);
                    tapTargetSequence.continueOnCancel(true);
                    if (profile != null) {
                        TapTarget tapTargetProfile = TapTarget.forView(profile, "Your Profile",
                                "Click here to edit your personal details and change your settings")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequence.target(tapTargetProfile);

                    }
                    if (home != null) {
                        TapTarget tapTargetHome = TapTarget.forView(home, "Home",
                                "You can click here to see people around you")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequence.target(tapTargetHome);
                    }

                    if (chats != null) {
                        TapTarget tapTargetChats = TapTarget.forView(chats, "Chats",
                                "After getting matches, you can click here to see them and also find your conversations")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequence.target(tapTargetChats);
                    }
                    tapTargetSequence.listener(new TapTargetSequence.Listener() {
                        @Override
                        public void onSequenceFinish() {
                            if (peopleAround) {
                                showSwipeTutorial(context, view);
                                Preferences.get(context).setKeyTimeForMainIntro(false);
                            } else {
                                Preferences.get(context).setKeyTimeForMainIntro(false);
                            }
                        }

                        @Override
                        public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                        }

                        @Override
                        public void onSequenceCanceled(TapTarget lastTarget) {

                        }
                    });
                    tapTargetSequence.start();


                }
            }, 100);
        }

        if(Preferences.get(context).isTimeForSwipeIntro()){
            showSwipeTutorial(context, view);
        }

    }

    private static void showSwipeTutorial(@NonNull Context context, View view) {
        if (Preferences.get(context).isTimeForSwipeIntro()) {
            AppCompatActivity activity = (AppCompatActivity) context;
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    TapTargetSequence tapTargetSequenceSwipe = new TapTargetSequence(activity);
                    tapTargetSequenceSwipe.continueOnCancel(true);
                    if (view != null) {
                        ImageView rewind = view.findViewById(R.id.rewind);
                        TapTarget tapTargetRewind = TapTarget.forView(rewind, "Rewind",
                                "You can click here to revisit a person you have just swiped")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequenceSwipe.target(tapTargetRewind);

                        ImageView skip_button = view.findViewById(R.id.skip_button);
                        TapTarget tapTargetSkip = TapTarget.forView(skip_button, "Pass",
                                "You can click here if not interested in a person")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequenceSwipe.target(tapTargetSkip);

                        ImageView like_button = view.findViewById(R.id.like_button);
                        TapTarget tapTargetLike = TapTarget.forView(like_button, "Pass",
                                "You can click here if interested in a person")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequenceSwipe.target(tapTargetLike);

                        ImageView superLike = view.findViewById(R.id.super_like_button);
                        TapTarget tapTargetSuperLike = TapTarget.forView(superLike, "SuperLike",
                                "You can click here if super interested in a person")
                                .drawShadow(true)
                                .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                .textColorInt(activity.getResources().getColor(R.color.white));
                        tapTargetSequenceSwipe.target(tapTargetSuperLike);

                        CardStackView cardStackView = view.findViewById(R.id.card_stack_view);
                        if(cardStackView.getAdapter()!=null){
                            int position = 0;
                            if (position < cardStackView.getAdapter().getItemCount()) {
                                RecyclerView.ViewHolder holder = cardStackView.findViewHolderForAdapterPosition(position);
                                if (holder == null) return;

                                View view = holder.itemView.findViewById(R.id.card_spot);
                                if (view != null) {
                                    float targetRadius = UnitHelper.toDp(context, (float) view.getMeasuredWidth()) - 60f;

                                    TapTarget tapTargetTop = TapTarget.forView(view,
                                            "Swipe Area",
                                            "You can swipe left to dislike and swipe right to like someone here ")
                                            .drawShadow(true)
                                            .targetRadius((int)targetRadius)
                                            .tintTarget(false)
                                            .targetCircleColorInt(activity.getResources().getColor(R.color.colorAccent))
                                            .outerCircleColorInt(activity.getResources().getColor(R.color.colorPrimary))
                                            .textColorInt(activity.getResources().getColor(R.color.white));



                                    tapTargetSequenceSwipe.target(tapTargetTop);
                                }

                            }
                        }

                        tapTargetSequenceSwipe.listener(new TapTargetSequence.Listener() {
                            @Override
                            public void onSequenceFinish() {
                                Preferences.get(context).setKeyTimeForSwipeIntro(false);
                            }

                            @Override
                            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                            }

                            @Override
                            public void onSequenceCanceled(TapTarget lastTarget) {

                            }
                        });

                        tapTargetSequenceSwipe.start();


                    }
                }
            }, 100);


        }
    }
}
