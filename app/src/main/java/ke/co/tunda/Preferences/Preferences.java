

package ke.co.tunda.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;



public class Preferences {

    private static final String PREFERENCES_NAME = "tunda-app-preferences";

    private static final String KEY_LICENSED = "licensed";
    private static final String KEY_FIRST_RUN = "first_run";

    private static final String KEY_IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_IS_FIRST_LOGIN = "isFirstLogin";
    private static final String KEY_IS_IMAGE_REM = "isImageRemaining";
    private static final String KEY_IS_ENTERING_NO = "isEnteringNo";
    private static final String KEY_IS_VERIFYING_NO_NO = "is_verifying_no";
    private static final String KEY_NUMBER = "phone_number";
    private static final String KEY_VERIFICATION_ID = "number";
    private static final String KEY_CODE="code";
    private static final String KEY_LOCATION_SET="location_set";
    private static final String KEY_TIME_FOR_MAIN_INTRO="time_for_main_intro";
    private static final String KEY_TIME_FOR_SWIPE_INTRO="time_for_swipe_intro";
    private static final String KEY_REFRESH_CONNECTION="refresh_connections";
    private static final String KEY_REFRESH_DIALOGS="refresh_dialogs";
    private static final String KEY_DISCOVERY_SETTINGS_CHANGED="discovery_changed";
    private static final String KEY_IS_IMAGE_REM_PES = "is_image_rem_pes";


    private static WeakReference<Preferences> mPreferences;
    private final Context mContext;

    private Preferences(@NonNull Context context) {
        mContext = context;
    }

    @NonNull
    public static Preferences get(@NonNull Context context) {
        if (mPreferences == null || mPreferences.get() == null) {
            mPreferences = new WeakReference<>(new Preferences(context));
        }
        return mPreferences.get();
    }

    private SharedPreferences getSharedPreferences() {
        return mPreferences.get().mContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public void clearPreferences() {
        boolean isLicensed = isLicensed();
        getSharedPreferences().edit().clear().apply();

        if (isLicensed) {
            setFirstRun(false);
            setLicensed(true);
        }
    }

    public boolean isLicensed() {
        return getSharedPreferences().getBoolean(KEY_LICENSED, false);
    }

    public void setLicensed(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_LICENSED, bool).apply();
    }

    public boolean isFirstRun() {

        return getSharedPreferences().getBoolean(KEY_FIRST_RUN, true);
    }

    public void setFirstRun(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_FIRST_RUN, bool).apply();
    }

    public boolean isImageRemaining() {
        return getSharedPreferences().getBoolean(KEY_IS_IMAGE_REM, false);

    }

    public void setIsImageRemaining(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_IMAGE_REM, bool).apply();

    }

    public boolean isImageRemainingPes() {
        return getSharedPreferences().getBoolean(KEY_IS_IMAGE_REM_PES, false);

    }

    public void setIsImageRemainingPes(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_IMAGE_REM_PES, bool).apply();

    }

    public boolean isFirstLaunch() {
        return getSharedPreferences().getBoolean(KEY_IS_FIRST_TIME_LAUNCH, true);
    }

    public void setKeyIsFirstTimeLaunch(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_FIRST_TIME_LAUNCH, bool).apply();
    }

    public boolean isLoggedIn() {
        return getSharedPreferences().getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void setKeyIsLoggedIn(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_LOGGED_IN, bool).apply();
    }


    public boolean isFirstLogin() {
        return getSharedPreferences().getBoolean(KEY_IS_FIRST_LOGIN, true);
    }

    public void setKeyIsFirstLogin(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_FIRST_LOGIN, bool).apply();
    }

    public boolean isEnteringNo() {
        return getSharedPreferences().getBoolean(KEY_IS_ENTERING_NO, false);
    }

    public void setKeyIsEnteringNo(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_ENTERING_NO, bool).apply();
    }

    public boolean isVerifyingNo() {
        return getSharedPreferences().getBoolean(KEY_IS_VERIFYING_NO_NO, false);
    }

    public void setKeyIsVerifyingNo(boolean bool) {
        getSharedPreferences().edit().putBoolean(KEY_IS_VERIFYING_NO_NO, bool).apply();
    }


    public String getNumber() {
        return getSharedPreferences().getString(KEY_NUMBER, "");
    }

    public void setNumber(String phone) {
        getSharedPreferences().edit().putString(KEY_NUMBER, phone).apply();
    }

    public void setVerificatioId(String verificatioId) {
        getSharedPreferences().edit().putString(KEY_VERIFICATION_ID, verificatioId).apply();
    }

    public String getKeyVerificationId() {
        return getSharedPreferences().getString(KEY_VERIFICATION_ID, "");
    }




    public boolean isLocationSet(){
        return getSharedPreferences().getBoolean(KEY_LOCATION_SET,false);
    }

    public void setLocationAsSet(boolean bool){
        getSharedPreferences().edit().putBoolean(KEY_LOCATION_SET,bool).apply();
    }

    public boolean isTimeForMainIntro(){
        return getSharedPreferences().getBoolean(KEY_TIME_FOR_MAIN_INTRO,true);
    }

    public void setKeyTimeForMainIntro(boolean bool){
        getSharedPreferences().edit().putBoolean(KEY_TIME_FOR_MAIN_INTRO,bool).apply();
    }

    public boolean isTimeForSwipeIntro(){
        return getSharedPreferences().getBoolean(KEY_TIME_FOR_SWIPE_INTRO,true);
    }

    public void setKeyTimeForSwipeIntro(boolean bool){
        getSharedPreferences().edit().putBoolean(KEY_TIME_FOR_SWIPE_INTRO,bool).apply();
    }

    public boolean isToRefreshConnections(String from){
        Log.d("PREFS", "isToRefreshConnections: "+from+getSharedPreferences().getBoolean(KEY_REFRESH_CONNECTION,false));
        return getSharedPreferences().getBoolean(KEY_REFRESH_CONNECTION,false);
    }

    public void setIsTimeToRefreshConnections(boolean bool){
        Log.d("PREFS", "setIsTimeToRefreshConnections: "+bool);
        getSharedPreferences().edit().putBoolean(KEY_REFRESH_CONNECTION,bool).commit();
    }

    public boolean isToRefreshDialogs(String from){
        Log.d("PREFS", "isToRefreshdialogs: "+from+getSharedPreferences().getBoolean(KEY_REFRESH_DIALOGS,false));

        return getSharedPreferences().getBoolean(KEY_REFRESH_DIALOGS,false);
    }

    public void setIsTimeToRefreshCDialogss(boolean bool){
        Log.d("PREFS", "setistimerefdialogs: "+bool);

        getSharedPreferences().edit().putBoolean(KEY_REFRESH_DIALOGS,bool).commit();
    }

    public int getCode() {
        return getSharedPreferences().getInt(KEY_CODE, 0);
    }

    public void setCode(int code) {
        getSharedPreferences().edit().putInt(KEY_CODE, code).apply();
    }

    public void setDiscoveryChanged(boolean bool){
        getSharedPreferences().edit().putBoolean(KEY_DISCOVERY_SETTINGS_CHANGED,bool).apply();
    }

    public boolean isDicoveryChanged(){
        return getSharedPreferences().getBoolean(KEY_DISCOVERY_SETTINGS_CHANGED,false);
    }




}
